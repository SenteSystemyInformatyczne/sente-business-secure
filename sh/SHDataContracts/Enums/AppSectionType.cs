﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Enums
{
  /// <summary>
  /// Typy sekcji APPINI
  /// </summary>
  public enum AppSectionType
  {
    Action,
    Grid,
    Menu,
    Controls,
    Navigator,
    NavigatorTab,
    NavigatorGroup,
    Unknown
  }
}
