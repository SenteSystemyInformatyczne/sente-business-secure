﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Enums
{
  /// <summary>
  /// Typy elementów bazodanowych
  /// </summary>
  public enum DbTypes
  {
    None = 0,
    Domain,
    Sequence,
    Exception,
    Table,
    Field,
    Procedure,
    View,
    Trigger,
    TransferProcedure,
    PrimaryKey,
    UniqueKey,
    ForeignKey,
    Index,
    AppSection,
    XkProcedure,
    ReplicationRule,
    Check,
    Role
  }
}
