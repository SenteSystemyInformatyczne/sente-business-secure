﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Enums
{
  /// <summary>
  /// Typy połączeń Signature Helpera
  /// </summary>
  public enum ConnectionType
  {
    File,
    Local,
    Source,
    Remote
  }
}
