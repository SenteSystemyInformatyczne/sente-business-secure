﻿using SHDataContracts.Model.AppSection.Subtypes;
using SHDataContracts.Model.AppSection.Subtypes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection
{
  public static class AppSectionExtensions
  {
    /// <summary>
    /// Metoda parsująca obiekt sekcji appini i tworząca szczegółowy obiekt akcji appini
    /// </summary>
    /// <param name="appObject"></param>
    /// <returns></returns>
    public static ActionBase ParseAction(this AppSectionRoot appObject)
    {
      if (appObject.Type == Enums.AppSectionType.Action)
      {
        var type = appObject.GetActionSubType();
        return BuildActionForType(appObject, type);
      }
      else
      {
        return null;
      }
    }

    /// <summary>
    /// Metoda tworząca obiekt akcji appini danego typu
    /// </summary>
    /// <param name="appObject"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private static ActionBase BuildActionForType(AppSectionRoot appObject, AppSectionSubtype type)
    {
      switch (type)
      {
        case AppSectionSubtype.Unknown:
          return new ActionUnknown(type, appObject);
        case AppSectionSubtype.BrowseQuery:
          return new ActionBrowseQuery(type, appObject);
        case AppSectionSubtype.BrowseTable:
          return new ActionBrowseTable(type, appObject);
        case AppSectionSubtype.DefineMediums:
          return new ActionDefineMediums(type, appObject);
        case AppSectionSubtype.DeleteRecord:
          return new ActionDeleteRecord(type, appObject);
        case AppSectionSubtype.DesignReport:
          return new ActionDesignReport(type, appObject);
        case AppSectionSubtype.DisplayRecord:
          return new ActionDisplayRecord(type, appObject);
        case AppSectionSubtype.DisplayWindow:
          return new ActionDisplayWindow(type, appObject);
        case AppSectionSubtype.EditCopy:
          return new ActionEditCopy(type, appObject);
        case AppSectionSubtype.EditDefines:
          return new ActionEditDefines(type, appObject);
        case AppSectionSubtype.EditRecord:
          return new ActionEditRecord(type, appObject);
        case AppSectionSubtype.MultiAction:
          return new ActionMultiAction(type, appObject);
        case AppSectionSubtype.NewRecord:
          return new ActionNewRecord(type, appObject);
        case AppSectionSubtype.Parser:
          return new ActionParser(type, appObject);
        case AppSectionSubtype.PopupSQLu:
          return new ActionPopupSQLu(type, appObject);
        case AppSectionSubtype.PrintReport:
          return new ActionPrintReport(type, appObject);
        case AppSectionSubtype.Refresh:
          return new ActionRefresh(type, appObject);
        case AppSectionSubtype.Return:
          return new ActionReturn(type, appObject);
        case AppSectionSubtype.RunNotification:
          return new ActionRunNotification(type, appObject);
        case AppSectionSubtype.RunSQL:
          return new ActionRunSQL(type, appObject);
        case AppSectionSubtype.ShowAction:
          return new ActionShowAction(type, appObject);
        case AppSectionSubtype.ShowAppForm:
          return new ActionShowAppForm(type, appObject);
        case AppSectionSubtype.ShowDefines:
          return new ActionShowDefines(type, appObject);
        case AppSectionSubtype.ShowImportDefinesForm:
          return new ActionShowImportDefinesForm(type, appObject);
        case AppSectionSubtype.ShowMediumsForm:
          return new ActionShowMediumsForm(type, appObject);
        case AppSectionSubtype.ShowNavigatorAction:
          return new ActionShowNavigatorAction(type, appObject);
        case AppSectionSubtype.ShowNotificationForm:
          return new ActionShowNotificationForm(type, appObject);
        case AppSectionSubtype.ShowReplicatDefinition:
          return new ActionShowReplicatDefinition(type, appObject);
        case AppSectionSubtype.ShowRights:
          return new ActionShowRights(type, appObject);
        case AppSectionSubtype.SimpleBlank:
          return new ActionSimpleBlank(type, appObject);
        case AppSectionSubtype.Query:
          return new ActionQuery(type, appObject);
        case AppSectionSubtype.SYSMESSAGE_CSHOW:
          return new ActionSysMessageCShow(type, appObject);
        case AppSectionSubtype.SYSMESSAGE_CSHOWRECORD:
          return new ActionSysMessageCShowRecord(type, appObject);
        case AppSectionSubtype.SYSMESSAGE_CSHOWNEWRECORD:
          return new ActionSysMessageCShowNewRecord(type, appObject);
        case AppSectionSubtype.SYSMESSAGE_CMETHOD:
          return new ActionSysMessageCMethod(type, appObject);
        default:
          return null;
      }
    }

    /// <summary>
    /// Metoda pobierająca typ akcji appini
    /// </summary>
    /// <param name="appObject"></param>
    /// <returns></returns>
    public static AppSectionSubtype GetActionSubType(this AppSectionRoot appObject)
    {
      return GetSubtype(appObject.Elements);
    }

    /// <summary>
    /// Metoda pobierająca typ akcji appini
    /// </summary>
    /// <param name="elements"></param>
    /// <returns></returns>
    private static AppSectionSubtype GetSubtype(IList<AppElement> elements)
    {
      var functionElement = elements.SingleOrDefault(e => e.Identifier == "Function");
      if (functionElement != null)
      {
        return AppSectionUtils.GetActionSubtype(functionElement.Value);
      }
      else
      {
        return AppSectionSubtype.Unknown;
      }
    }
  }
}
