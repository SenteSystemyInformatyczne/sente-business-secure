﻿using SHDataContracts.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection
{
  public class AppElementEqualityComparer : IEqualityComparer<AppElement>
  {
    public bool Equals(AppElement x, AppElement y)
    {
      bool result = true;
      if (x == null && y == null)
      {
        result = true;
      }
      else if (x == null | y == null)
      {
        result = false;
      }
      else
      {
        result &= x.Identifier == y.Identifier;
        result &= x.NS == y.NS;
        result &= x.Prefix == y.Prefix;
      }
      return result;
    }

    public int GetHashCode(AppElement obj)
    {
      string combine = $"{obj.Identifier}~{obj.NS}~{obj.Prefix}";
      return combine.GetHashCode();
    }
  }

  public class AppElement : IComparable<AppElement>
  {
    /// <summary>
    /// Identyfikator parametru
    /// </summary>
    public string Identifier
    {
      get;
      set;
    }

    /// <summary>
    /// Wartość parametru
    /// </summary>
    public string Value
    {
      get;
      set;
    }

    /// <summary>
    /// Przestrzeń nazw parametru
    /// </summary>
    public string NS
    {
      get;
      set;
    }

    /// <summary>
    /// Prefix parametru
    /// </summary>
    public string Prefix
    {
      get;
      set;
    }

    /// <summary>
    /// Elementy AppSection są równe, gdy wszystkie ich własności są sobie równe.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      var element = obj as AppElement;
      if (element != null)
      {
        var result = Identifier == element.Identifier;
        result &= Value == element.Value;
        result &= NS == element.NS;
        result &= Prefix == element.Prefix;
        return result;
      }
      else
      {
        return false;
      }
    }

    /// <summary>
    /// Porównywanie elementów odbywa się przez porównanie ich identyfikatora namespace'u i prefixu.
    /// </summary>
    /// <param name="y"></param>
    /// <returns></returns>
    public int CompareTo(AppElement y)
    {
      var strX = $"{Identifier}~{NS}~{Prefix}";
      var strY = $"{y.Identifier}~{y.NS}~{y.Prefix}";
      return strX.CompareTo(strY);
    }
  }
}
