﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionDisplayWindow : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "ModalWin", new string[] { "yes", "no", "app", "free" } },
          { "FormName", new string[0] },
          { "FormIdent", new string[0] },
          { "Editable", new string[] { "yes", "no" } },
          { "SetControlName", new string[0] },
          { "FormUserName", new string[0] },
          { "GetGlobal", new string[] { "yes", "no" } },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] },
          { "If", new string[0] }
        };
    public ActionDisplayWindow(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
