﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionQuery : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Nazwa", new string[0] },
          { "SQL", new string[0] },
          { "SecurityLog", new string[] { "1", "0" } },
          { "Ref", new string[0] },
          { "QueryBase", new string[0] },
          { "NativeTable", new string[0] },
          { "NativeBase", new string[0] },
          { "WinName", new string[0] },
          { "GridName", new string[0] },
          { "QueryName", new string[0] },
          { "SearchField", new string[] { "1", "0" } },
          { "Buffered", new string[] { "1", "0" } },
          { "IsRefreshable", new string[] { "1", "0" } },
          { "Columns", new string[0] },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionQuery(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
