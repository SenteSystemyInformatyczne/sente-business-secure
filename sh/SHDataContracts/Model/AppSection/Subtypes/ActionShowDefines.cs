﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  class ActionShowDefines : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "WinName", new string[0] },
          { "Caption", new string[0] },
          { "Editable", new string[] { "yes", "no" } },
          { "HasCancelButton", new string[] { "yes", "no" } },
          { "Kind", new string[0] },
          { "SavedParamsName", new string[0] },
          { "Define#:Label", new string[0] },
          { "Define#:Acronym", new string[0] },
          { "Define#:Value", new string[0] },
          { "Define#:Type", new string[] { "Number", "Lookup", "String", "Date", "Flags", "Bool", "Memo", "File", "Separator", "Combo", "SSCombo", "Script" } },
          { "Define#:Width", new string[0] },
          { "Define#:Height", new string[0] },
          { "Define#:Editable", new string[] { "1", "0" } },
          { "Define#:DictBase", new string[0] },
          { "Define#:Dict", new string[0] },
          { "Define#:DictTable", new string[0] },
          { "Define#:DictField", new string[0] },
          { "Define#:DictFilter", new string[0] },
          { "Define#:DictKeyFields", new string[0] },
          { "Define#:EditMask", new string[0] },
          { "Define#:AllowedChars", new string[0] },
          { "Define#:Storable", new string[0] },
          { "Define#:PasswordChar", new string[0] },
          { "Define#:MultiSelect", new string[0] },
          { "Define#:SQL", new string[0] },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionShowDefines(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
