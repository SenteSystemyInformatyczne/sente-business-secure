﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionUnknown : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>();
    public ActionUnknown(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
