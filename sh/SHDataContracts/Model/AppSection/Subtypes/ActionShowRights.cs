﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionShowRights : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Table", new string[0] },
          { "Field", new string[0] },
          { "FieldGr", new string[0] },
          { "FieldDef", new string[0] },
          { "OperFilter", new string[0] },
          { "GroupFilter", new string[0] },
          { "EditRights", new string[] { "yes", "no" } },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionShowRights(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
