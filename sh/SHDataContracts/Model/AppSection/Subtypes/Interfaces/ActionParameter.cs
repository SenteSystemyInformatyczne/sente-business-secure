﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionParameter
  {
    public ActionParameter(string name, string @namespace, string prefix, string[] allowedValues, string value = null)
    {
      Name = name;
      Namespace = @namespace;
      Prefix = prefix;
      AllowedValues = allowedValues;
      Value = value;
    }

    /// <summary>
    /// Tablica dopuszczalnych wartości parametru
    /// </summary>

    public string[] AllowedValues
    {
      get;
      private set;
    }

    /// <summary>
    /// Czy wartość parametru jest zgodna z tablicą dopuszczalnych wartości
    /// </summary>
    public bool IsValid
    {
      get;
      set;
    }

    /// <summary>
    /// Nazwa parametru
    /// </summary>
    public string Name
    {
      get;
      set;
    }

    /// <summary>
    /// Przestrzeń nazw parametru
    /// </summary>
    public string Namespace
    {
      get;
      set;
    }

    /// <summary>
    /// Prefix parametru
    /// </summary>
    public string Prefix
    {
      get;
      set;
    }

    private string _value;
    /// <summary>
    /// Wartość parametru
    /// </summary>
    public string Value
    {
      get
      {
        return _value;
      }
      set
      {
        if (AllowedValues.Length > 0 && !AllowedValues.Contains(value))
        {
          _value = value;
          IsValid = false;
        }
        else
        {
          _value = value;
          IsValid = true;
        }
      }
    }
  }
}
