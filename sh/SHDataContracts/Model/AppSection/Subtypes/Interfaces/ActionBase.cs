﻿using SHDataContracts.Model.AppSection.Subtypes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public abstract class ActionBase
  {
    /// <summary>
    /// Obiekt sekcji appini, dla którego wygenerowano obiekt akcji
    /// </summary>
    private AppSectionRoot _rawJsonObj
    {
      get;
      set;
    }

    /// <summary>
    /// Mapa (nazwa parametru => tablica dozwolonych wartości) oczekiwanych w modelu danego typu parametrów appini.
    /// </summary>
    protected abstract Dictionary<string, string[]> ExpectedFields
    {
      get;
    }

    /// <summary>
    /// Parametry akcji appini
    /// </summary>
    public List<ActionParameter> Parameters
    {
      get;
      private set;
    }

    /// <summary>
    /// Typ akcji appini
    /// </summary>
    public AppSectionSubtype Type
    {
      get;
      private set;
    }

    /// <summary>
    /// Czy istnieją parametry nie wchodzące w skłąd standardowego modelu dla tej akcji appini
    /// </summary>
    public bool HasNonModelFields
    {
      get
      {
        return NonModelFields.Count > 0;
      }
    }

    /// <summary>
    /// Parametry spoza modelu akcji appini
    /// </summary>
    public List<AppElement> NonModelFields
    {
      get
      {
        return _rawJsonObj.Elements.Where(e =>
        {
          var exact = ExpectedFields.Keys.Contains(e.Identifier);
          var param = ExpectedFields.Keys.Where(k => k.Contains('#')).Any(k =>
          {
            var split = k.Split('#');
            return e.Identifier.StartsWith(split[0]) && e.Identifier.EndsWith(split[1]);
          });

          return !(exact || param);
        }).ToList();
      }
    }

    public ActionBase(AppSectionSubtype type, AppSectionRoot jsonObject)
    {
      _rawJsonObj = jsonObject;
      Type = type;
      Parameters = BuildParametersList();
    }

    /// <summary>
    /// Metoda tworząca listę parametrów akcji appini na podstawie obiektu sekcji appini
    /// </summary>
    /// <returns></returns>
    private List<ActionParameter> BuildParametersList()
    {
      List<ActionParameter> result = new List<ActionParameter>();

      foreach (var expectedField in ExpectedFields)
      {
        Func<AppElement, bool> query = e => e.Identifier == expectedField.Key;

        if (expectedField.Key.Contains("#"))
        {
          var splitKey = expectedField.Key.Split('#');
          if (splitKey.Length == 2)
            query = e => e.Identifier.StartsWith(splitKey[0]) && e.Identifier.EndsWith(splitKey[1]);
        }

        foreach (var field in _rawJsonObj.Elements.Where(query))
        {
          result.Add(new ActionParameter(field.Identifier, field.NS, field.Prefix, expectedField.Value, field.Value));
        }
      }

      return result;
    }
  }
}
