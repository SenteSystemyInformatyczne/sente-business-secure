﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionBrowseTable : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Table", new string[0] },
          { "Partial", new string[] { "0", "1" } },
          { "IsDictionary", new string[] { "yes","no" } },
          { "GlobalDict",new string[] { "yes", "no" } },
          { "Kind", new string[0] },
          { "Database", new string[0] },
          { "Value", new string[0] },
          { "ResultField", new string[0] },
          { "Filter", new string[0] },
          { "WinName", new string[0] },
          { "GridName", new string[0] },
          { "Caption", new string[0]},
          { "FormIdent", new string[0] },
          { "DisplayFields", new string[0] },
          { "Buffered", new string[] { "yes", "no"} },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionBrowseTable(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
      
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
