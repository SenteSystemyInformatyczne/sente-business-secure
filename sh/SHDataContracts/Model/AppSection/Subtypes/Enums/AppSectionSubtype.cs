﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection.Subtypes.Enums
{
  /// <summary>
  /// Typy akcji appini
  /// </summary>
  public enum AppSectionSubtype
  {
    Unknown = 0,
    BrowseQuery,
    BrowseTable,
    DefineMediums,
    DeleteRecord,
    DesignReport,
    DisplayRecord,
    DisplayWindow,
    EditCopy,
    EditDefines,
    EditRecord,
    MultiAction,
    NewRecord,
    Parser,
    PopupSQLu,
    PrintReport,
    Refresh,
    Return,
    RunNotification,
    RunSQL,
    ShowAction,
    ShowAppForm,
    ShowDefines,
    ShowImportDefinesForm,
    ShowMediumsForm,
    ShowNavigatorAction,
    ShowNotificationForm,
    ShowReplicatDefinition,
    ShowRights,
    SimpleBlank,
    Query,
    SYSMESSAGE_CSHOW,
    SYSMESSAGE_CSHOWRECORD,
    SYSMESSAGE_CSHOWNEWRECORD,
    SYSMESSAGE_CMETHOD
  }
}
