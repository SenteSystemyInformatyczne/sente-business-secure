﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionReturn : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Code", new string[] { "OK", "CANCEL", "NONE", "Ok", "Cancel", "None" } },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionReturn(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
