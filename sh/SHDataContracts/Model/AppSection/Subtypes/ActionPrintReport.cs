﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionPrintReport : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Report", new string[0] },
          { "GetParams", new string[] { "yes", "no" } },
          { "FromPage", new string[0] },
          { "ToPage", new string[0] },
          { "Copies", new string[0] },
          { "PrinterName", new string[0] },
          { "Preview", new string[] { "yes", "no" } },
          { "ShowPanel", new string[] { "yes", "no" } },
          { "Table", new string[0] },
          { "Field", new string[0] },
          { "Rights", new string[0] },
          { "Version", new string[0] },
          { "PDFPath", new string[0] },
          { "PDFFile", new string[0] },
          { "PDFType", new string[0] },
          { "PDFSaveConfigFile", new string[0] },
          { "PDFVersion", new string[0] },
          { "SecurityLog", new string[] { "0", "1" } },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionPrintReport(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
