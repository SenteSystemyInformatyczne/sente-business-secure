﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionMultiAction : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "Action", new string[0] },
          { "Dataset", new string[0] },
          { "LogForm", new string[] { "yes", "no" } },
          { "LogFormTitle", new string[0] },
          { "ShowRange", new string[] { "yes", "no" } },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionMultiAction(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
