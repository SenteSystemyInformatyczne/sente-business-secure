﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionBrowseQuery : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "SQL", new string[0] },
          { "Query", new string[0] },
          { "Partial", new string[] { "0", "1" } },
          { "Kind", new string[0] },
          { "Database", new string[0] },
          { "Value", new string[0] },
          { "ResultField", new string[0] },
          { "NativeTable", new string[0] },
          { "NativeDatabase", new string[0] },
          { "Filter", new string[0] },
          { "Signature", new string[0] },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] }
        };

    public ActionBrowseQuery(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
