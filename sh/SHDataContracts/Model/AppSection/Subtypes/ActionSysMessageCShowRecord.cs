﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection.Subtypes.Enums;

namespace SHDataContracts.Model.AppSection.Subtypes
{
  public class ActionSysMessageCShowRecord : ActionBase
  {
    private static Dictionary<string, string[]> _expectedFields = new Dictionary<string, string[]>
        {
          { "Function", new string[0] },
          { "ObjectName", new string[0] },
          { "FormName", new string[0] },
          { "Mode", new string[] { "B", "E", "D" } },
          { "FormStyle", new string[] { "M", "D", "F" } },
          { "Param#:Name", new string[0] },
          { "Param#:Value", new string[0] },
          { "Signature", new string[0] }
        };

    public ActionSysMessageCShowRecord(AppSectionSubtype type, AppSectionRoot jsonObject) : base(type, jsonObject)
    {
    }

    protected override Dictionary<string, string[]> ExpectedFields
    {
      get
      {
        return _expectedFields;
      }
    }
  }
}
