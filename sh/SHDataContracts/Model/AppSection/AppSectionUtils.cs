﻿using Newtonsoft.Json;
using SHDataContracts.Enums;
using SHDataContracts.Model.AppSection.Subtypes.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection
{
  public static class AppSectionUtils
  {
    /// <summary>
    /// Metoda pobierająca typ sekcji appini z nazwy
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static AppSectionType GetTypeFromName(string name)
    {
      var split = name.Split(':', '_');
      switch (split[0])
      {
        case "Action":
          return AppSectionType.Action;
        case "Controls":
          return AppSectionType.Controls;
        case "Menu":
          return AppSectionType.Menu;
        case "Grid":
          return AppSectionType.Grid;
        case "Navigator":
          return AppSectionType.Navigator;
        case "NavigatorGroup":
          return AppSectionType.NavigatorGroup;
        case "NavigatorTab":
          return AppSectionType.NavigatorTab;
        default:
          return AppSectionType.Unknown;
      }
    }

    /// <summary>
    /// Metoda pobierająca typ akcji appini na podstawie nazwy funkcji
    /// </summary>
    /// <param name="functionName"></param>
    /// <returns></returns>
    public static AppSectionSubtype GetActionSubtype(string functionName)
    {
      switch (functionName)
      {
        case "BrowseTable":
          return AppSectionSubtype.BrowseTable;
        case "SimpleBlank":
          return AppSectionSubtype.SimpleBlank;
        case "DisplayWindow":
          return AppSectionSubtype.DisplayWindow;
        case "EditRecord":
          return AppSectionSubtype.EditRecord;
        case "NewRecord":
          return AppSectionSubtype.NewRecord;
        case "PrintReport":
          return AppSectionSubtype.PrintReport;
        case "DisplayRecord":
          return AppSectionSubtype.DisplayRecord;
        case "RunSQL":
          return AppSectionSubtype.RunSQL;
        case "BrowseQuery":
          return AppSectionSubtype.BrowseQuery;
        case "Parser":
          return AppSectionSubtype.Parser;
        case "DeleteRecord":
          return AppSectionSubtype.DeleteRecord;
        case "Refresh":
          return AppSectionSubtype.Refresh;
        case "Zapytanie":
          return AppSectionSubtype.Query;
        case "ShowRights":
          return AppSectionSubtype.ShowRights;
        case "ShowDefines":
          return AppSectionSubtype.ShowDefines;
        case "Return":
          return AppSectionSubtype.Return;
        case "MultiAction":
          return AppSectionSubtype.MultiAction;
        case "ShowNotificationForm":
          return AppSectionSubtype.ShowNotificationForm;
        case "RunNotification":
          return AppSectionSubtype.RunNotification;
        case "DefineMediums":
          return AppSectionSubtype.DefineMediums;
        case "ShowMediumsForm":
          return AppSectionSubtype.ShowMediumsForm;
        case "ShowImportDefinesForm":
          return AppSectionSubtype.ShowImportDefinesForm;
        case "ShowAction":
          return AppSectionSubtype.ShowAction;
        case "ShowReplicatDefinition":
          return AppSectionSubtype.ShowReplicatDefinition;
        case "ShowNavigatorAction":
          return AppSectionSubtype.ShowNavigatorAction;
        case "ShowAppForm":
          return AppSectionSubtype.ShowAppForm;
        case "EditCopy":
          return AppSectionSubtype.EditCopy;
        case "DesignReport":
          return AppSectionSubtype.DesignReport;
        case "EditDefines":
          return AppSectionSubtype.EditDefines;
        case "PopupSQLu":
          return AppSectionSubtype.PopupSQLu;
        case "SYSMESSAGE_CMETHOD":
          return AppSectionSubtype.SYSMESSAGE_CMETHOD;
        case "SYSMESSAGE_CSHOWNEWRECORD":
          return AppSectionSubtype.SYSMESSAGE_CSHOWNEWRECORD;
        case "SYSMESSAGE_CSHOWRECORD":
          return AppSectionSubtype.SYSMESSAGE_CSHOWRECORD;
        case "SYSMESSAGE_CSHOW":
          return AppSectionSubtype.SYSMESSAGE_CSHOW;
        default:
          return AppSectionSubtype.Unknown;
      }
    }
  }
}
