﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHDataContracts.Model.AppSection.Subtypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Model.AppSection
{
  public class AppSectionRoot
  {
    public AppSectionRoot(string name)
    {
      Name = name;
      Elements = new List<AppElement>();
    }

    private ActionBase _actionDetails;

    /// <summary>
    /// Szczegóły akcji appini
    /// </summary>
    [JsonIgnore]
    public ActionBase ActionDetails
    {
      get
      {
        if (_actionDetails == null)
          _actionDetails = this.ParseAction();
        return _actionDetails;
      }
    }

    /// <summary>
    /// Nazwa sekcji appini
    /// </summary>
    public string Name
    {
      get;
      set;
    }

    /// <summary>
    /// Elementy sekcji appini
    /// </summary>
    public List<AppElement> Elements
    {
      get;
      set;
    }

    /// <summary>
    /// Typ sekcji appini
    /// </summary>
    [JsonIgnore]
    public AppSectionType Type
    {
      get
      {
        return AppSectionUtils.GetTypeFromName(Name);
      }
    }

    /// <summary>
    /// Podfolder appini
    /// </summary>
    [JsonIgnore]
    public string Subfolder
    {
      get
      {
        switch (Type)
        {
          case AppSectionType.Action:
            return "action";
          case AppSectionType.Controls:
            return "controls";
          case AppSectionType.Grid:
            return "grids";
          case AppSectionType.Menu:
            return "menus";
          case AppSectionType.Navigator:
            return "navigator";
          case AppSectionType.NavigatorGroup:
            return "navigator";
          case AppSectionType.NavigatorTab:
            return "navigator";
          default:
            return "unknown";
        }
      }
    }

    /// <summary>
    /// AppSection są równe gdy ich nazwy oraz wszystkie elmenty kolekcji Elements są sobie równe.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      if (obj is AppSectionRoot)
      {
        var root = obj as AppSectionRoot;
        var result = Name == root.Name;
        if (result && Elements.Count == root.Elements.Count)
        {
          for (int i = 0; i < Elements.Count; i++)
          {
            result &= Elements[i].Equals(root.Elements[i]);
            if (!result)
            {
              break;
            }
          }
        }
        return result;
      }
      else
      {
        return false;
      }
    }
  }
}
