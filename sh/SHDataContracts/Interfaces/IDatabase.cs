﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHDataContracts.Interfaces
{
  public interface IDatabase
  {
    IDbSettings DbSettings
    {
      get;
    }
    /// <summary>
    /// Metoda pozwala wykonać procedurę składowaną i pobrać jej wyniki
    /// </summary>
    /// <param name="procedureName">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista spodziewanych parametrów wyjściowych</param>
    /// <returns>Wynik wykonania procedury (może być wiele krotek)</returns>
    DataTable ExecuteProcedure(string procedureName, Dictionary<string, object> inputParameters, List<string> outputParameters);
    /// <summary>
    /// Metoda pozwala wykonać procedurę składowaną i pobrać jej wyniki
    /// </summary>
    /// <param name="procedureName">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista spodziewanych parametrów wyjściowych</param>
    /// <param name="command">Obiekt który umie wykonać polecenie sql, musi być już zainicjowany</param>
    /// <returns>Wynik wykonania procedury (może być wiele krotek)</returns>
    DataTable ExecuteProcedure(string procedureName, Dictionary<string, object> inputParameters, List<string> outputParameters, FbCommand command);
    /// <summary>
    /// Do wgrywania skryptów. Uwaga, ma problem z @ w komentarzach.
    /// </summary>
    /// <param name="x"></param>
    void ExecuteScript(string x);
    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    void ExecuteStatement(string statement, Dictionary<string, object> inputParameters);
    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql i pobrać jego wyniki
    /// od Select różni się tym, że może być ono parametryczne
    /// np SELECT * FROM TABELA WHERE POLE=@WARTOSC
    /// <seealso cref="Select(string)"/>
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista oczekiwanych parametrów wyjściowych</param>
    DataTable ExecuteStatement(string statement, Dictionary<string, object> inputParameters, List<string> outputParameters);
    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="command">Obiekt który umie wykonać polecenie sql, musi być już zainicjowany</param>
    void ExecuteStatement(string statement, Dictionary<string, object> inputParameters, FbCommand command);
    /// <summary>
    /// Metoda zwracjąca obiekt połączenia do DB jesli istnieje lub tworzaca nowy.
    /// </summary>
    /// <param name="new"></param>
    /// <returns></returns>
    FbConnection GetConnection(bool @new = false);
    string GetName();
    /// <summary>
    /// Metoda pozwala zadać zapytanie i pobrać wyniki w formie tabeli
    /// </summary>
    /// <param name="sql">zapytanie sql</param>
    /// <returns>wynik</returns>
    DataTable Select(string sql);
    /// <summary>
    /// Metoda sprwadzająca czy połączenie do DB jest OK.
    /// </summary>
    /// <param name="errorMsg"></param>
    /// <returns></returns>
    bool TestConnection(out string errorMsg);
  }
}
