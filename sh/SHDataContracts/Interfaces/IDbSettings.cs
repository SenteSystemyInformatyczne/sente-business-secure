﻿using SHDataContracts.Enums;

namespace SHDataContracts.Interfaces
{
  public interface IDbSettings
  {
    string ConnectionString
    {
      get;
    }
    string DatabaseFilePath
    {
      get;
      set;
    }
    string Encoding
    {
      get;
      set;
    }
    string DeltaFilePath
    {
      get;
      set;
    }
    string Name
    {
      get;
      set;
    }
    string Password
    {
      get;
      set;
    }
    string Port
    {
      get;
      set;
    }
    string Role
    {
      get;
      set;
    }
    string ServerAddress
    {
      get;
      set;
    }
    ConnectionType Type
    {
      get;
      set;
    }
    string User
    {
      get;
      set;
    }
    string ConnectionPoolSize
    {
      get;
      set;
    }

    object Clone();
    void CreateDatabase(int pageSize);
    bool Equals(object obj);
    bool IsValid();
    string ToString();
  }
}