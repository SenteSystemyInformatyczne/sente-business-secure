﻿using NLog;
using SHDataContracts.Interfaces;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using static SHlib.DataExtraction.MetadataExtractor;

namespace SHlib.DataExtraction
{
  /// <summary>
  /// Fabryka tworząca obiekty bazodanowe
  /// </summary>
  public abstract class MetadataFactory
  {
    public const int MODULO = 10;

    public Func<bool, IDbSettings> GetConnection;


    protected abstract string RETRIEVESQL
    {
      get;
    }
    public abstract string DirName
    {
      get;
    }

    public static string CreateFilter(string signature, string field)
    {
      //Poprawka żeby w sygnaturze można było używać znaku '-' i '_'
      signature = signature.Replace("-", "\\-").Replace("_", "\\_");
      return " iif(position('\n', trim(" + field + "))=0, upper(trim(" + field + ")||';'), upper(substring(trim(" + field + ") from 1 for position('\n', trim(" + field + ")))||';')) similar to upper('%(TR[AB]\\_)?(BS|PR|ZG|X{0,3}SE\\-)?" + signature + "(\\_[0-9]+)?;%') escape '\\'";
    }
    protected abstract string AddFilter(string signature);
    public abstract List<Metadata> CreateCollection(string signature);
    public abstract List<Metadata> CreateCollection(ProgressInfo pi = null, bool allApp = false);
    public abstract int GetCollectionCount();
    public abstract Task<List<Metadata>> CreateCollectionFromFiles(string signature, List<string> files, ProgressInfo pi = null);
    public abstract Metadata AddMetadataInfo(string name, string signature, string DDL);
  }

  /// <summary>
  /// Implementacja fabryki tworzącej obiekty BD
  /// </summary>
  class CustomMetadataFactory : MetadataFactory
  {
    protected override string RETRIEVESQL
    {
      get
      {
        return "";
      }
    }
    public override string DirName
    {
      get
      {
        return "metadata";
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "RDB$DESCRIPTION");
    }

    /// <summary>
    /// Utwórz obiekty z DB z daną sygnaturą
    /// </summary>
    /// <param name="signature"></param>
    /// <returns></returns>
    public override List<Metadata> CreateCollection(string signature)
    {
      List<Metadata> md = new List<Metadata>();
      using (Database db = new Database(GetConnection(true)))
      {
        string sql = AddFilter(signature);
        if (!String.IsNullOrEmpty(sql))
        {
          System.Data.DataTable t = null;
          try
          {
            t = db.Select(sql);
          }
          catch (Exception e)
          {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Error, "Error", e);
          }
          if (t != null)
          {
            for (int i = 0; i < t.Rows.Count; i++)
            {
              string description = t.Rows[i]["DESCRIPTION"].ToString().ToUpper();
              string name = t.Rows[i]["OBJECTNAME"].ToString().Trim();
              if (description.Contains(signature))
                md.Add(AddMetadataInfo(name, signature));
            }
          }
        }
      }
      return md;
    }

    /// <summary>
    /// Utwórz obiekty dla wszystkich obiektów w DB
    /// </summary>
    /// <param name="pi"></param>
    /// <returns></returns>
    public override List<Metadata> CreateCollection(ProgressInfo pi = null, bool allApp = false)
    {
      List<Metadata> md = new List<Metadata>();
      using (Database db = new Database(GetConnection(true)))
      {

        string sql = RETRIEVESQL;
        if (!String.IsNullOrEmpty(sql))
        {
          try
          {

            System.Data.DataTable t = db.Select(sql);
            if (t != null)
            {
              var lastReport = DateTime.MaxValue;
              if (pi != null)
              {
                lastReport = DateTime.MinValue;
              }

              for (int i = 0; i < t.Rows.Count; i++)
              {
                string description = t.Rows[i]["DESCRIPTION"].ToString().ToUpper();
                string name = t.Rows[i]["OBJECTNAME"].ToString().Trim();
                md.Add(AddMetadataInfo(name, description));

                if (pi != null)
                {
                  pi.ReportProgress();
                }
              }
            }
          }
          catch (Exception e)
          {
            e.Data["message"] = "Błąd poczas pobierania struktur " + this.DirName + ". Będą pominięte przy eksporcie.";
            if (pi != null)
              pi.ReportError(e);
          }
        }
      }
      return md;
    }

    /// <summary>
    /// Pobierz ile elementów jest łącznie w bazie
    /// </summary>
    /// <returns></returns>
    public override int GetCollectionCount()
    {
      using (Database db = new Database(GetConnection(true)))
      {
        string sql = RETRIEVESQL;
        if (!String.IsNullOrEmpty(sql))
        {
          try
          {
            System.Data.DataTable t = db.Select(sql);
            if (t != null)
            {
              return t.Rows.Count;
            }
          }
          catch
          {
            //obsługa jest robiona później
          }
        }
      }
      return 0;
    }

    /// <summary>
    /// Wczytaj kolekcje z bazy danych
    /// </summary>
    /// <param name="signature"></param>
    /// <param name="files"></param>
    /// <param name="pi"></param>
    /// <returns></returns>
    public override async Task<List<Metadata>> CreateCollectionFromFiles(string signature, List<string> files, ProgressInfo pi = null)
    {
      List<Metadata> mlist = new List<Metadata>();

      foreach (var file in files)
      {
        string name = Path.GetFileNameWithoutExtension(file);
        string DDL = "";

        using (StreamReader sr = new StreamReader(file))
        {
          DDL = await sr.ReadToEndAsync();
        }

        mlist.Add(AddMetadataInfo(name, "", DDL));

        if (pi != null)
        {
          pi.ReportProgress();
        }
      }

      return mlist;
    }

    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new CustomMetadata(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class DomainFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające domeny z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.DomainFactory_Select, @"select RDB$FIELD_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$FIELDS 
                                                    where RDB$FIELD_NAME not starting with 'RDB$'
                                                          and RDB$FIELD_NAME not containing 'SYS_TRHIST'
                                                          and RDB$FIELD_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "domains";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL)
    {
      return new Domain(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class SequenceFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające generatory z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ExceptionFactory_Select, @"select RDB$GENERATOR_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$GENERATORS 
                                                    where RDB$SYSTEM_FLAG = 0
                                                      and RDB$GENERATOR_NAME not starting with 'IBE$'
                                                      and RDB$GENERATOR_NAME not starting with 'RDB$'
                                                      and RDB$GENERATOR_NAME not containing 'SYS_TRHIST'
                                                      and RDB$GENERATOR_NAME not containing 'SYS_HASHCODES'
                                                      and RDB$GENERATOR_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "sequences";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Sequence(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class SExceptionFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające wyjątki z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ExceptionFactory_Select, @"select RDB$EXCEPTION_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$EXCEPTIONS 
                                                    where RDB$EXCEPTION_NAME not containing 'SYS_TRHIST'
                                                      and RDB$EXCEPTION_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "exceptions";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new SException(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class FieldFactory : CustomMetadataFactory
  {
    public static readonly string STATEMENT = SQLRepository.Instance.GetSql(SQL.FieldFactory_Select,
      @"select trim(rf.rdb$relation_name)||'.'||trim(rf.rdb$field_name) as OBJECTNAME,
                  rf.rdb$description as DESCRIPTION
                  from rdb$relation_fields rf
                  join rdb$relations r
                  on r.rdb$relation_name=rf.rdb$relation_name
                  left join rdb$types t
                  on r.rdb$relation_type = t.rdb$type
                  where rf.rdb$relation_name not containing 'RDB$' and
                        rf.rdb$relation_name not containing 'MON$' and
                        rf.rdb$relation_name not containing 'IBE$' and
                        rf.rdb$relation_name not containing 'SYS_TRHIST' and
                        rf.rdb$relation_name not containing 'SYS_TR_TEMP' and
                        rf.rdb$relation_name not containing 'SYS_HASHCODES' and
                        rf.rdb$relation_name not containing 'SYS_TR_TEMP' and
                        (t.rdb$type_name = 'PERSISTENT' or r.rdb$relation_type is null) and
                        r.rdb$view_blr is null and
                        not exists (
                          select NULL
                          from rdb$relation_constraints rc
                          join rdb$index_segments s
                          on rc.rdb$constraint_name = s.rdb$index_name
                          where rc.rdb$constraint_type = 'PRIMARY KEY' and
                          rc.rdb$relation_name = rf.rdb$relation_name and
                          s.rdb$field_name = rf.rdb$field_name
                        ) and
                        not exists (
                          select NULL
                          from rdb$relation_constraints rc
                          join rdb$index_segments s
                          on rc.rdb$index_name = s.rdb$index_name
                          where rc.rdb$constraint_type = 'PRIMARY KEY' and
                          rc.rdb$relation_name = rf.rdb$relation_name and
                          s.rdb$field_name = rf.rdb$field_name
                        ) and
                        not exists (
                          select NULL
                          from rdb$relation_fields rff
                          where rff.rdb$relation_name = rf.rdb$relation_name and
                            rff.rdb$field_name = rf.rdb$field_name and
                            rff.rdb$field_position = 0 and
                            not exists (
                              select rc.rdb$relation_name
                              from rdb$relation_constraints rc
                              where rc.rdb$constraint_type = 'PRIMARY KEY' and
                              rc.rdb$relation_name = rff.rdb$relation_name
                            )
                        )");

    /// <summary>
    /// Zapytanie pobierające pola z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return STATEMENT;
      }
    }
    public override string DirName
    {
      get
      {
        return "fields";
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "rf.RDB$DESCRIPTION");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Field(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class TableFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające tabele z bazy danych
    /// </summary>
    private string _RETRIEVESQL = SQLRepository.Instance.GetSql(SQL.TableFactory_Select,
      @"select RDB$RELATION_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$RELATIONS
                                                    where RDB$VIEW_BLR is NULL
                                                          and RDB$RELATION_NAME not starting with 'RDB$'
                                                          and RDB$RELATION_NAME not starting with 'IBE$'
                                                          and RDB$RELATION_NAME not starting with 'MON$'
                                                          and RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                          and RDB$RELATION_NAME not containing 'SYS_HASHCODES'
                                                          and RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");
    protected override string RETRIEVESQL
    {
      get
      {
        return _RETRIEVESQL;
      }
    }
    public override string DirName
    {
      get
      {
        return "tables";
      }
    }
    /// <summary>
    /// Metoda rozszerzająca zapytanie pobierające o tabelę SYS_TRHIST
    /// </summary>
    public void IncludeTrHist()
    {
      _RETRIEVESQL = RETRIEVESQL.Replace("and RDB$RELATION_NAME not containing 'SYS_TRHIST'", "");
    }
    /// <summary>
    /// Metoda rozszerzająca zapytanie pobierające o tabelę SYS_HASHCODES
    /// </summary>
    public void IncludeHashCodes()
    {
      _RETRIEVESQL = RETRIEVESQL.Replace("and RDB$RELATION_NAME not containing 'SYS_HASHCODES'", "");
    }
    /// <summary>
    /// Metoda rozszerzająca zapytanie pobierające o tabelę SYS_TR_TEMP
    /// </summary>
    public void IncludeTrTemp()
    {
      _RETRIEVESQL = RETRIEVESQL.Replace("and RDB$RELATION_NAME not containing 'SYS_TR_TEMP'", "");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Table(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class ProcedureFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające procedury z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ProcedureFactory_Select, @"select RDB$PROCEDURE_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$PROCEDURES
                                                    where (RDB$PROCEDURE_NAME not starting 'TR_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRD_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRI_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRL_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRF_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRA_'
                                                          and RDB$PROCEDURE_NAME not starting 'TRB_'
                                                          and RDB$PROCEDURE_NAME not starting 'XK_'
                                                          and RDB$PROCEDURE_NAME not starting 'XX_EF_'
                                                          and RDB$PROCEDURE_NAME <> 'EALGORITHM_RUN'
                                                          and RDB$PROCEDURE_NAME not starting 'XX_FR_'
                                                          and RDB$PROCEDURE_NAME not containing 'SYS_TRHIST'
                                                          and RDB$PROCEDURE_NAME not containing 'SYS_TR_TEMP')");
      }
    }
    public override string DirName
    {
      get
      {
        return "procedures";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Procedure(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class XKProcedureFactory : CustomMetadataFactory
  {

    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.XkProcedureFactory_Select, @"select RDB$PROCEDURE_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$PROCEDURES
                                                    where RDB$PROCEDURE_NAME starting 'XK_'");
      }
    }
    public override string DirName
    {
      get
      {
        return "xk_procedures";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new XkProcedure(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class ViewFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające widoki z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ViewFactory_Select, @"select RDB$RELATION_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$RELATIONS
                                                    where RDB$VIEW_BLR IS NOT NULL
                                                      and RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                      and RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "views";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL)
    {
      return new View(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class TriggerFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające triggery z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.TriggerFactory_Select, @"select RDB$TRIGGER_NAME as OBJECTNAME, RDB$DESCRIPTION as description
                                                    from RDB$TRIGGERS
                                                    where RDB$TRIGGER_NAME not starting with 'RDB$'
                                                          and RDB$TRIGGER_NAME not starting with 'IBE$'
                                                          and RDB$TRIGGER_NAME not starting with 'CHECK'
                                                          and RDB$TRIGGER_NAME not containing 'SYS_TRHIST'
                                                          and RDB$TRIGGER_NAME not containing 'SYS_HASHCODES'
                                                          and RDB$TRIGGER_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "triggers";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL)
    {
      return new Trigger(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class TransferProcedureFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające procedury transferowe z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.TransferProcedureFactory_Select, @"select trim(RDB$PROCEDURE_NAME) as OBJECTNAME,
                            iif(position('_' in substring(RDB$PROCEDURE_NAME from position('_' in RDB$PROCEDURE_NAME)+1)) > 0,
                                trim(substring(RDB$PROCEDURE_NAME from position('_' in RDB$PROCEDURE_NAME)+1
                                    for position('_' in substring(RDB$PROCEDURE_NAME from position('_' in RDB$PROCEDURE_NAME)+1))-1)),
                                trim(substring(RDB$PROCEDURE_NAME from position('_' in RDB$PROCEDURE_NAME)+1))) as description
                            from RDB$PROCEDURES
                            where (
                                RDB$PROCEDURE_NAME starting 'TRB_'
                                or RDB$PROCEDURE_NAME starting 'TRA_')
                                and RDB$PROCEDURE_NAME not containing 'SYS_TRHIST'
                                and RDB$PROCEDURE_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "RDB$PROCEDURE_NAME");
    }
    public override string DirName
    {
      get
      {
        return "tr";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new TransferProcedure(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class PrimaryKeyFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające klucze główne z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.PrimaryKeyFactory_Select,
          @"select trim(i.RDB$RELATION_NAME)||'.'||trim(iif(c.rdb$constraint_name starting with 'INTEG_', 'PK_' || c.rdb$relation_name, c.rdb$constraint_name)) as OBJECTNAME, i.RDB$DESCRIPTION as description
                                                      from RDB$INDICES i
                                                        left join RDB$RELATION_CONSTRAINTS c on c.rdb$index_name = i.RDB$INDEX_NAME
                                                        where c.RDB$CONSTRAINT_TYPE = 'PRIMARY KEY'
                                                          and i.rdb$relation_name not starting with 'RDB$'
                                                          and i.rdb$relation_name not starting with 'IBE$'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_HASHCODES'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "primarykeys";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new PrimaryKey(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class ForeignKeyFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające klucze obce z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ForeignKeyFactory_Select, @"select trim(i.RDB$RELATION_NAME)||'.'||
trim(
iif(c.rdb$constraint_name starting with 'INTEG_',
    iif(char_length('FK_' || trim(c.rdb$relation_name) || '_' || trim(i.rdb$foreign_key))>31,
        substring(('FK_' || trim(c.rdb$relation_name) || '_' || trim(i.rdb$foreign_key)) from 1 for 31),
        'FK_' || trim(c.rdb$relation_name) || '_' || trim(i.rdb$foreign_key)
        )
    , c.rdb$constraint_name)
) as OBJECTNAME, i.RDB$DESCRIPTION as description
                                                      from RDB$INDICES i
                                                        left join RDB$RELATION_CONSTRAINTS c on c.RDB$INDEX_NAME = i.RDB$INDEX_NAME
                                                        where
                                                          c.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "foreignkeys";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new ForeignKey(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class UniqueKeyFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające klucze unikatowe z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.UniqueKeyFactory_Select, @"select trim(i.RDB$RELATION_NAME)||'.'||trim(i.RDB$INDEX_NAME) as OBJECTNAME, i.RDB$DESCRIPTION as description
                                                      from RDB$INDICES i
                                                        left join RDB$RELATION_CONSTRAINTS c on c.RDB$CONSTRAINT_NAME = i.RDB$INDEX_NAME
                                                        where i.RDB$SYSTEM_FLAG = 0
                                                          and c.RDB$CONSTRAINT_TYPE = 'UNIQUE'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                          and i.RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");
      }
    }
    public override string DirName
    {
      get
      {
        return "uniquekeys";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new UniqueKey(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class IndexFactory : CustomMetadataFactory
  {
    /// <summary>
    /// Zapytanie pobierające indexy z bazy danych
    /// </summary>
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.IndexFactory_Select, @"select trim(i.RDB$RELATION_NAME)||'.'||trim(i.RDB$INDEX_NAME) as OBJECTNAME, i.RDB$DESCRIPTION as description
                                                    from RDB$INDICES i
                                                    left join rdb$relation_constraints rc on rc.rdb$index_name = i.rdb$index_name
                                                    where
                                                      i.RDB$SYSTEM_FLAG = 0
                                                      and rc.rdb$index_name is null
                                                      and i.RDB$INDEX_NAME not starting with 'IBE$'
                                                      and i.RDB$RELATION_NAME not containing 'SYS_TRHIST'
                                                      and i.RDB$RELATION_NAME not containing 'SYS_HASHCODES'
                                                      and i.RDB$RELATION_NAME not containing 'SYS_TR_TEMP'");//rdb$ muszą być
      }
    }
    public override string DirName
    {
      get
      {
        return "indices";
      }
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Index(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

  }

  class AppSectionFactory : CustomMetadataFactory
  {
    private string _RETRIEVESQL = SQLRepository.Instance.GetSql(SQL.AppSectionFactory_Select,
          @"select section as objectname, val as description 
              from s_appini where ident = 'Signature' and (prefix is null or upper(prefix) not starting with 'U')");

    protected override string RETRIEVESQL
    {
      get
      {
        return _RETRIEVESQL;
      }
    }
    public override string DirName
    {
      get
      {
        return "appini";
      }
    }

    public string[] Subfolders
    {
      get;
    } = { "action", "controls", "grids", "menus", "navigator", "unknown" };

    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "val");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new AppSection(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }

    /// <summary>
    /// Funkcja ustawia zapytanie które zwraca wszystkie nazwy sekcji 
    /// niezależnie od tego czy sekcja posiada sygnature
    /// </summary>
    public void GetAll()
    {
      _RETRIEVESQL = SQLRepository.Instance.GetSql(SQL.AppSectionFactory_Select,
          @"select distinct s.section as objectname, (select first 1 val from s_appini ss where s.section = ss.section and ss.ident = 'Signature' order by val desc nulls last) as description
            from s_appini s where (s.prefix is null or upper(s.prefix) not starting with 'U')");
    }

    /// <summary>
    /// FUnkcja w zależności od parametru zwraca wszystkie wpisy appini albo tylko z sygnatura
    /// </summary>
    /// <param name="pi"></param>
    /// <param name="allApp">Flaga która informuje nas czy mają zostać zwrócone wszyskie wpisy appini</param>
    /// <returns></returns>
    public override List<Metadata> CreateCollection(ProgressInfo pi = null, bool allApp = false)
    {
      if (allApp)
        GetAll();

      return base.CreateCollection(pi, allApp);
    }
  }

  class ReplicationRuleFactory : CustomMetadataFactory
  {
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.ReplicationRuleFactory_Select, "select  source_dbase || '~' || dest_dbase || '~' || name as objectname, signature as description from RP_REPLICAT where 1=1");
      }
    }
    public override string DirName
    {
      get
      {
        return "replication";
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "signature");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new ReplicationRule(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class CheckConstraintFactory : CustomMetadataFactory
  {
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.CheckConstraintFactory_Select,
@"select t.objectname, d.description as description from
  (select distinct trim(rc.rdb$relation_name) as objectname
  from rdb$relation_constraints rc
       join rdb$check_constraints cc on rc.rdb$constraint_name = cc.rdb$constraint_name
       join rdb$triggers t on cc.rdb$trigger_name = t.rdb$trigger_name
  where rc.rdb$constraint_type = 'CHECK'
    and t.rdb$trigger_type = 1
    and rc.rdb$constraint_name not starting with 'ibe$') t
left join (select relname, list(description,';') as description from (
        select r.rdb$relation_name as relname, r.rdb$description as description
          from rdb$relations r
        union all
        select rf.rdb$relation_name as relname, rf.rdb$description as description
          from rdb$relation_fields rf
     ) group by relname) d on d.relname = t.objectname
where 1=1
  and t.objectname NOT STARTING WITH 'RDB$'
  and t.objectname NOT STARTING WITH 'IBE$'");
      }
    }
    public override string DirName
    {
      get
      {
        return "checks";
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "description");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new CheckConstraint(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }

  class RolesFactory : CustomMetadataFactory
  {
    protected override string RETRIEVESQL
    {
      get
      {
        return SQLRepository.Instance.GetSql(SQL.Roles_Select, @"select rdb$role_name as objectname, rdb$description as description 
                                                                 from RDB$ROLES
                                                                 where rdb$role_name NOT STARTING WITH 'RDB$'");
      }
    }

    public override string DirName
    {
      get
      {
        return "roles";
      }
    }
    protected override string AddFilter(string signature)
    {
      return RETRIEVESQL + " and" + CreateFilter(signature, "description");
    }
    public override Metadata AddMetadataInfo(string name, string signature, string DDL = "")
    {
      return new Role(name, signature, DDL)
      {
        GetConnection = GetConnection
      };
    }
  }
}
