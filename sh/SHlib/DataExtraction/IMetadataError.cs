﻿using SHlib.DataExtraction.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DataExtraction
{
  public interface IMetadataError
  {
    Metadata Source
    {
      get;
    }

    MetadataErrorType Type
    {
      get;
    }

    string Message
    {
      get;
      set;
    }
  }

  public enum MetadataErrorType
  {
    SUSPEND_IN_TR,
    DROP_IN_TR
  }
}
