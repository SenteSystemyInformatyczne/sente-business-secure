﻿using SHDataContracts.Enums;
using SHlib.DatabaseUpdate;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using SHlib.Utilities.FormsWrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SHDataContracts.Model.AppSection;

namespace SHlib.DataExtraction
{
  public class DatabaseExtractor : MetadataExtractor
  {
    static Crc32 crc = new Crc32();

    public DatabaseExtractor(ConnectionType connection, Action<string> writeMessage, Action<Exception> writeErrorMessage, bool grantFilter)
      : base(writeMessage, writeErrorMessage, grantFilter)
    {
      _connectionType = connection;
    }

    /// <summary>
    /// Weksportuj wszystko
    /// </summary>
    /// <param name="srcpath"></param>
    /// <param name="pi"></param>
    public void ExportAll(string srcpath, ProgressInfo pi = null, bool allwayspath = false, bool removeFileBeforeUpdate = false, bool removeLeftovers = false)
    {
      DatabasePatch.CheckSHStructuresIntegrity(pi, AppendMessage, updateHashes: false);
      DatabasePatch.CheckDatabaseId(srcpath, Connection.Instance.GetConnection(ConnectionType.Source), pi);

      CheckAppSectionEntries(pi);

      List<Metadata> filesList = new List<Metadata>();
      if (removeLeftovers)
      {
        AppendMessage("Ładowanie listy plików z repozytorium");
        FilesystemExtractor fe = new FilesystemExtractor(AppendMessage, ErrorMessage);
        filesList = new List<Metadata>(fe.ParseFiles("", srcpath, pi, TrMode.NoTr, true));
      }

      List<Metadata> list = ExtractAllObjects(pi, allwayspath);

      CheckSizeOfTransferProcedures(list);

      if (removeLeftovers)
      {
        List<Metadata> leftovers = filesList.Except(list, new FileComparer()).ToList();
        if (leftovers.Count > 0)
        {
          AppendMessage("Usuwanie nadmiarowych plików z repozytorium");
          foreach (var lf in leftovers.Where(lo => lo.DbType != DbTypes.TransferProcedure).Where(lo => !(lo.DbType == DbTypes.Procedure && lo.Name == "EALGORITHM_RUN")))
          {
            AppendMessage(string.Format("Usuwanie [{0}] {1}", lf.DbType, lf.Name));
            string JoinedPath = Path.Combine(srcpath, lf.FilePath);
            File.Delete(JoinedPath);
          }
        }
      }

      srcpath = srcpath + Path.DirectorySeparatorChar;

      if (list.Count > 0)
      {
        SetSmallLettersEscape(list);

        AppendMessage("Kontrola katalogów");

        //raz sprawdź, czy wszystkie katalogi które są potrzebne istnieją
        var dirs = mfList
          .Select(l =>
          {
            List<string> paths = new List<string>();
            paths.Add(Path.Combine(srcpath, l.Value.DirName));
            if (l.Value is AppSectionFactory)
            {
              paths.AddRange((l.Value as AppSectionFactory).Subfolders.Select(sf => Path.Combine(paths.First(), sf)));
            }
            foreach (var p in paths)
            {
              try
              {
                if (!Directory.Exists(p))
                  Directory.CreateDirectory(p);
              }
              catch (Exception e)
              {
                AppendMessage(e.Message);
              }
            }

            return 1;
          }).ToList();

        List<Task> saveTasks = new List<Task>();
        var groups = Math.Max(int.Parse(Connection.Instance.GetConnection(ConnectionType.Source).ConnectionPoolSize) - 1, 1);
        var listOfLists = list.AsParallel().Select((l, i) => new
        {
          index = (int) l.DbType % groups,
          val = l
        }).GroupBy(g => g.index).ToList();

        if (pi != null)
          pi.ReportCount(list.Count, "Eksport i zapis obiektów do plików");

        foreach (var sublist in listOfLists)
        {
          saveTasks.Add(Task.Run(async () =>
          {
            List<Task> writeTasks = new List<Task>();
            foreach (var sc in sublist)
            {
              var val = sc.val.SaveObject(GrantFilter).Replace("\r\n", "\n").Replace("\n", "\r\n");
              string JoinedPath = Path.Combine(srcpath, sc.val.FilePath);
              if (removeFileBeforeUpdate && File.Exists(JoinedPath))
                File.Delete(JoinedPath);
              writeTasks.Add(WriteFileAsync(JoinedPath, val, pi));
            }
            await Task.WhenAll(writeTasks);
          }));
        }

        Task.WaitAll(saveTasks.ToArray());
      }
      else
      {
        AppendMessage("Zmiany bazodanowe - brak sygnatur\n\n");
      }

      if (pi != null)
      {
        pi.ReportEnd();
      }
    }

    private async Task WriteFileAsync(string path, string val, ProgressInfo pi = null)
    {
      try
      {
        using (StreamWriter sw = new StreamWriter(path))
        {
          await sw.WriteAsync(val);
        }
      }
      catch (Exception ex)
      {
        ErrorMessage(new Exception("Błąd zapisu pliku " + path, ex));
      }

      if (pi != null)
      {
        pi.ReportProgress();
      }
    }


    #region EXTRACT

    public List<Metadata> ExtractAllObjects(ProgressInfo pi, bool allwayspath)
    {
      List<Metadata> list;
      list = SearchAll(pi);
      return list;
    }

    /// <summary>
    /// Tworzy i zwraca pola, indeksy, generatory z tabel z podanej listy
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    public List<Tuple<Metadata, Metadata>> CreateTableObjects(IEnumerable<Metadata> list)
    {
      List<Tuple<Metadata, Metadata>> toReturn = new List<Tuple<Metadata, Metadata>>();
      foreach (Table obj in list.Where(o => o.DbType == DbTypes.Table))
      {
        //Tworzenie pol tabeli i dodanie do listy
        var tableFields = obj.GetFields();
        foreach (var field in tableFields)
        {
          toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Field(obj.Name + "." + field.Trim(), "", "")
          {
            GetConnection = GetConnection
          }));
        }
        //Tworzenie indeksow tabeli i dodanie do listy
        var tableIndexes = obj.GetIndexes();
        foreach (var index in tableIndexes)
        {
          if (index.Trim().StartsWith("PK_"))
          {
            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new PrimaryKey(obj.Name + '.' + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
          else if (index.Trim().StartsWith("FK_"))
          {
            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new ForeignKey(obj.Name + '.' + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
          else
          {
            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Index(obj.Name + '.' + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
        }
        //Tworzenie generatorow tabeli i dodanie do listy
        var tableSequences = obj.GetSequences();
        foreach (var sequence in tableSequences)
        {
          toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Sequence(sequence.Trim(), "", "")
          {
            GetConnection = GetConnection
          }));
        }
      }
      return toReturn;
    }
    /// <summary>
    /// Tworzy i zwraca indeksy pól z listy.
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    public List<Tuple<Metadata, Metadata>> CreateFieldObjects(IEnumerable<Metadata> list)
    {
      List<Tuple<Metadata, Metadata>> toReturn = new List<Tuple<Metadata, Metadata>>();
      foreach (Field obj in list.Where(o => o.DbType == DbTypes.Field))
      {
        //Tworzenie indeksów z danego pola i dodanie do listy
        var fieldIndexes = obj.GetIndexes();
        foreach (var index in fieldIndexes)
        {
          if (index.Trim().StartsWith("FK_"))
          {

            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new ForeignKey(obj.Name.Substring(0, obj.Name.IndexOf('.') + 1) + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
          else if (index.Trim().StartsWith("UNQ"))
          {

            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new UniqueKey(obj.Name.Substring(0, obj.Name.IndexOf('.') + 1) + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
          else
          {
            toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Index(obj.Name.Substring(0, obj.Name.IndexOf('.') + 1) + index.Trim(), "", "")
            {
              GetConnection = GetConnection
            }));
          }
        }
      }
      return toReturn;
    }
    /// <summary>
    /// Tworzy i zwraca pola, których dotyczą znajdujące się w liście triggery
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    public List<Tuple<Metadata, Metadata>> CreateTriggerObjects(IEnumerable<Metadata> list)
    {
      List<Tuple<Metadata, Metadata>> toReturn = new List<Tuple<Metadata, Metadata>>();
      foreach (Trigger obj in list.Where(o => o.DbType == DbTypes.Trigger))
      {
        //Utworzenie obiektów pól, których dotyczy trigger. Dodawane są do listy.
        var triggerFields = obj.GetFields();
        foreach (var field in triggerFields)
        {
          toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Field(field.Item1.Trim() + '.' + field.Item2.Trim(), "", "")
          {
            GetConnection = GetConnection
          }));
        }
      }
      return toReturn;
    }
    /// <summary>
    /// Tworzy i zwraca indeksy i generator dla kluczy głównych z listy 
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    public List<Tuple<Metadata, Metadata>> CreatePrimaryKeyObjects(IEnumerable<Metadata> list)
    {
      List<Tuple<Metadata, Metadata>> toReturn = new List<Tuple<Metadata, Metadata>>();
      foreach (PrimaryKey obj in list.Where(o => o.DbType == DbTypes.PrimaryKey))
      {
        var primaryKeyGenerator = obj.GetGenerator();
        foreach (var generator in primaryKeyGenerator)
        {
          toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Sequence(primaryKeyGenerator.First().Trim(), "", "")
          {
            GetConnection = GetConnection
          }));
        }

        var primaryKeyIndexes = obj.GetIndexes();
        foreach (var index in primaryKeyIndexes)
        {
          toReturn.Add(new Tuple<Metadata, Metadata>(obj, new Index(obj.Name.Substring(0, obj.Name.IndexOf('.') + 1) + index, "", "")
          {
            GetConnection = GetConnection
          }));
        }
      }
      return toReturn;
    }

    /// <summary>
    /// Eksportuj sekcje
    /// </summary>
    /// <param name="srcpath">Ścieżka gdzie generować skrypty</param>
    /// <param name="signature"></param>
    public string ExportSignature(string srcpath, string signature, ProgressInfo pi, bool allwayspath = true)
    {
      DatabasePatch.CheckSHStructuresIntegrity(pi, AppendMessage, allwayspath, false);
      DatabasePatch.CheckDatabaseId(srcpath, Connection.Instance.GetConnection(ConnectionType.Source), pi);

      //CheckAppSectionEntries(pi);

      signature = signature.ToUpper().Trim();
      AppendMessage("Wyniki dla tematu " + signature);
      List<Metadata> list;

      list = SearchForSignatures(signature);

      //usuwa niechciane powtórki.
      list = list.GroupBy(o => o.Name + "_" + o.DbType).Select(g => g.First()).ToList();

      CheckSizeOfTransferProcedures(list);

      var objectsWithSignatureCount = list.Count;

      Dictionary<string, List<Tuple<Metadata, Metadata>>> dependencies = new Dictionary<string, List<Tuple<Metadata, Metadata>>>();
      dependencies["field"] = CreateFieldObjects(list);
      dependencies["primaryKey"] = CreatePrimaryKeyObjects(list);
      dependencies["table"] = CreateTableObjects(list);
      dependencies["trigger"] = CreateTriggerObjects(list);

      list.AddRange(dependencies["field"].Select(i => i.Item2));
      list.AddRange(dependencies["primaryKey"].Select(i => i.Item2));
      list.AddRange(dependencies["table"].Select(i => i.Item2));
      list.AddRange(dependencies["trigger"].Select(i => i.Item2));

      //Usuwanie duplikatów z listy.
      List<Metadata> distinctList = list.GroupBy(o => o.Name + "_" + o.DbType).Select(g => g.First()).ToList();
      srcpath = srcpath + Path.DirectorySeparatorChar;

      StringBuilder script = new StringBuilder();

      if (distinctList.Count > 0)
      {
        SetSmallLettersEscape(distinctList);

        AppendMessage("Zmiany bazodanowe - liczba sygnatur: " + objectsWithSignatureCount + "\n");
        AppendMessage("Zmiany bazodanowe zmiany bez sygnatur: " + (distinctList.Count - objectsWithSignatureCount) + "\n");
        distinctList = MetadataChecksum.CalculateHashes(pi, distinctList).ToList();
        MetadataChecksum.UpdateHashes(ConnectionType.Source, distinctList, pi);

        if (Directory.Exists(Path.Combine(srcpath, "fields")))
        {
          List<string> leftovers = new List<string>();
          foreach (Table obj in distinctList.Where(o => o.DbType == DbTypes.Table))
          {
            var tableFiles = Directory.GetFiles(srcpath + "fields", obj.Name + ".*").ToList();
            var scripts = obj.GetFields().Select(f => srcpath + "fields\\" + obj.Name.Trim() + "." + f.Trim() + ".sql").ToList();
            leftovers.AddRange(tableFiles.Except(scripts));
          }
          if (leftovers.Count > 0)
            AppendMessage("Usunięte pola: " + leftovers.Count + "\n");
          foreach (var lo in leftovers)
          {
            AppendMessage(Regex.Matches(lo, @".*\\([\w\d\.]+).sql")[0].Groups[1].Value + "\n");
            File.Delete(lo);
          }
        }

        AppendMessage("Wyemitowane zmiany:");
        foreach (var sc in distinctList)
        {
          var deps = GetDependencies(sc, dependencies);
          string msg = "[" + sc.DbType + "] " + sc.Name + (deps.Count > 0 ? " (Na podstawie zależności dla: " + string.Join(", ", deps.Select(d => "<" + d.Item1.DbType + ">" + d.Item1.Name).ToArray()) + ")" : "");
          AppendMessage(msg);
          string JoinedPath = Path.Combine(srcpath, sc.FilePath);
          try
          {
            if (!Directory.Exists(Path.GetDirectoryName(JoinedPath)))
              Directory.CreateDirectory(Path.GetDirectoryName(JoinedPath));
            using (StreamWriter sw = new StreamWriter(JoinedPath))
            {
              //Ale to bszydkie!!!
              string content = sc.SaveObject(GrantFilter).Replace("\r\n", "\n").Replace("\n", "\r\n");
              sw.Write(content);
              script.Append(content);
              sw.Close();
            }
          }
          catch (Exception ex)
          {
            ErrorMessage(ex);
          }

          if (objectsWithSignatureCount < distinctList.Count && distinctList[objectsWithSignatureCount - 1] == sc)
            AppendMessage("Wyeksportowane obiekty związane ze zmianami:");
        }
      }
      else
      {
        AppendMessage("Zmiany bazodanowe - brak sygnatur");
      }
      return script.ToString();
    }

    private List<Tuple<Metadata, Metadata>> GetDependencies(Metadata metadata, Dictionary<string, List<Tuple<Metadata, Metadata>>> dependencies)
    {
      return dependencies.SelectMany(d => d.Value).Where(t => t.Item2 == metadata).ToList();
    }
    #endregion

    /// <summary>
    /// Klasa do obsługi nieco innej releacji równoważności na typie DbType
    /// Traktuje obiekty o typach DbTypesEqualityGroup jakby były tego samego typu przy porównaniu
    /// </summary>
    class MetaTuple
    {
      public DbTypes DbType
      {
        get;
        set;
      }

      public string Name
      {
        get;
        set;
      }

      public string OriginalName
      {
        get;
        set;
      }

      public MetaTuple(string name, DbTypes dbtype, string originalName = "")
      {
        DbType = dbtype;
        Name = name;
        OriginalName = originalName;
      }

      private static readonly List<List<DbTypes>> DbTypesEqualityGroup = new List<List<DbTypes>>()
      {
        new List<DbTypes> { DbTypes.ForeignKey, DbTypes.PrimaryKey },
        new List<DbTypes> { DbTypes.Procedure, DbTypes.TransferProcedure, DbTypes.XkProcedure }
      };

      private static readonly List<DbTypes> PlainEqualityGroups = DbTypesEqualityGroup.SelectMany(g => g).ToList();

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(obj, this))
          return true;
        if (obj is MetaTuple)
        {
          var met = (MetaTuple) obj;
          if (Name != met.Name)
            return false;

          if (!PlainEqualityGroups.Contains(DbType) && !PlainEqualityGroups.Contains(met.DbType))
            return DbType == met.DbType;

          var group = DbTypesEqualityGroup.First(g => g.Contains(DbType));
          var metgroup = DbTypesEqualityGroup.First(g => g.Contains(met.DbType));

          return ReferenceEquals(group, metgroup);
        }

        return false;
      }

      public override int GetHashCode()
      {
        if (!PlainEqualityGroups.Contains(DbType))
          return DbType.GetHashCode() * 31 + Name.GetHashCode();

        var group = DbTypesEqualityGroup.First(g => g.Contains(DbType));
        return group.First().GetHashCode() * 31 + Name.GetHashCode();
      }
    }

    /// <summary>
    /// Metoda generująca skrypt aktualizacyjny z wykrytych różnic miedzy bazą danych a plikami skryptów.
    /// </summary>
    /// <param name="srcpath">Ścieżka do dbscripts</param>
    /// <param name="pi">Obiekt raportowania postępu</param>
    /// <param name="grantFilter">Czy filtrować granty</param>
    /// <returns></returns>
    public string FastUpdate(string srcpath, ProgressInfo pi, bool grantFilter)
    {
      bool exportInfo = false;

      AppendMessage("Sprawdzanie spójności bazy");
      CheckTransferProcIntegrity(ConnectionType.Remote, pi);
      DatabasePatch.CheckDatabaseId(srcpath, Connection.Instance.GetConnection(ConnectionType.Remote), pi);

      srcpath = srcpath + Path.DirectorySeparatorChar;
      FilesystemExtractor fe = new FilesystemExtractor(WriteMessage, WriteErrorMessage);
      StringBuilder scriptContent = new StringBuilder();

      //Generowanie skryptu dla procedur transferowych
      AppendMessage("Generowanie skryptów dla procedur TRB");
      var scriptProceduresTRB = GenerateTRScript(srcpath, pi, fe, TrMode.TrBefore);
      AppendMessage("Generowanie skryptów dla procedur TRA");
      var scriptProceduresTRA = GenerateTRScript(srcpath, pi, fe, TrMode.TrAfter);

      Dictionary<MetaTuple, uint> dbhashes = GetMetadataCRCFromDB(pi);

      AppendMessage("Wczytywanie plików");
      List<Metadata> list = fe.ParseFiles("", srcpath, pi, TrMode.Default);
      Dictionary<MetaTuple, Tuple<uint, Metadata>> scripthashes = GetMetadataCRCFromFiles(pi, list);

      AppendMessage("Walidacja listy obiektów z plików");
      Dictionary<MetaTuple, Tuple<bool, Metadata>> filteredScripts = GetChangedMetadataList(pi, ref exportInfo, dbhashes, scripthashes, grantFilter);

      AppendMessage("Generowanie skryptu");
      _mList = list.Where(meta => filteredScripts.Keys.Contains(new MetaTuple(meta.Name, meta.DbType)))
              .Select(s =>
              {
                var changes = filteredScripts[new MetaTuple(s.Name, s.DbType)];
                s.Changed = changes.Item1;
                s.CurrentDbMetadata = changes.Item2;
                return s;
              }).ToList();

      scriptContent.AppendLine(scriptProceduresTRB);
      scriptContent.AppendLine(ScriptCreate(TrMode.NoTr));
      scriptContent.AppendLine(scriptProceduresTRA);
      scriptContent.AppendLine(AppiniSQLCreate(true, AppendMessage));

      if (exportInfo)
      {
        AppendMessage("#warn#UWAGA! Niektóre zmiany mogą wynikać ze zmiany formatu plików skryptów, jeśli informacje będą się powtarzać po wykonaniu procesu pełnej aktualizacji to należy ponownie wykonać pełny eksport metadanych.");
      }

      return scriptContent.ToString();
    }

    /// <summary>
    /// Metoda tworząca listę zmodyfikowanych skryptów do aktualizacji
    /// </summary>
    /// <param name="pi"></param>
    /// <param name="exportInfo"></param>
    /// <param name="dbhashes"></param>
    /// <param name="scripthashes"></param>
    /// <param name="grantFilter"></param>
    /// <returns></returns>
    private Dictionary<MetaTuple, Tuple<bool, Metadata>> GetChangedMetadataList(ProgressInfo pi, ref bool exportInfo, Dictionary<MetaTuple, uint> dbhashes, Dictionary<MetaTuple, Tuple<uint, Metadata>> scripthashes, bool grantFilter)
    {
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Remote, WriteMessage, WriteErrorMessage, grantFilter);
      var filteredScripts = new Dictionary<MetaTuple, Tuple<bool, Metadata>>();

      List<Metadata> dbObjects = null;

      pi?.ReportCount(scripthashes.Count, "Sprawdzanie różnic");
      foreach (var sh in scripthashes)
      {
        if (dbhashes.Keys.Contains(sh.Key))
        {
          if (dbhashes[sh.Key] != sh.Value.Item1)
          {
            dbObjects = dbObjects ?? de.ExtractAllObjects(null, false);
            var dbMetaKey = dbhashes.Keys.Single(m => m.Name == sh.Key.Name && m.DbType == sh.Key.DbType);
            var dbObj = dbObjects.SingleOrDefault(m => m.Name == dbMetaKey.OriginalName && m.DbType == dbMetaKey.DbType);

            if (dbObj != null)
            {
              var objDbType = sh.Key.DbType;
              switch (objDbType)
              {
                case DbTypes.AppSection:
                  filteredScripts.Add(new MetaTuple(sh.Key.Name, objDbType), new Tuple<bool, Metadata>(true, dbObj));
                  break;
                case DbTypes.Check:
                  GenerateAdditiveChecks(filteredScripts, sh, dbObj);
                  break;
                case DbTypes.Table:
                  if (CheckObjectForIgnoredPattern(sh.Value.Item2?.DDL, dbObj?.DDL, objDbType))
                  {
                    exportInfo = ProcessItem(exportInfo, filteredScripts, sh, dbObj);
                    AppendMessage("#warn#Zmieniono tabelę " + sh.Key.Name + "! Należy przeprowadzić pełny proces aktualizacji.");
                  }
                  break;
                case DbTypes.Procedure:
                  if (sh.Key.Name != "EALGORITHM_RUN")
                  {
                    filteredScripts.Add(new MetaTuple(sh.Key.Name, objDbType), new Tuple<bool, Metadata>(true, dbObj));
                  }
                  break;
                default:
                  if (CheckObjectForIgnoredPattern(sh.Value.Item2?.DDL, dbObj?.DDL, objDbType))
                  {
                    filteredScripts.Add(new MetaTuple(sh.Key.Name, objDbType), new Tuple<bool, Metadata>(true, dbObj));
                  }
                  break;
              }
            }
          }
        }
        else
        {
          if (!(sh.Key.DbType == DbTypes.Procedure && sh.Key.Name == "EALGORITHM_RUN"))
          {
            filteredScripts.Add(new MetaTuple(sh.Key.Name, sh.Key.DbType), new Tuple<bool, Metadata>(false, null));
          }
        }
        pi?.ReportProgress();
      }
      pi?.ReportEnd();
      return filteredScripts;
    }

    /// <summary>
    /// Metoda sprawdza czy różnice w DDL są istotne i należy obiekt aktualizować.
    /// </summary>
    /// <param name="fileDDL"></param>
    /// <param name="dbDDL"></param>
    /// <param name="type"></param>
    /// <returns>Czy należy zaktualizować obiekt w DB</returns>
    private bool CheckObjectForIgnoredPattern(string fileDDL, string dbDDL, DbTypes type)
    {
      fileDDL = MetadataChecksum.PrepareDDL(fileDDL);
      dbDDL = MetadataChecksum.PrepareDDL(dbDDL);
      //usuwamy z pliku skryptu zawartość zwróconą w DDL z bazy dla obiektu
      var diff = Tools.RemoveIntersectionFromHashDDL(fileDDL, dbDDL);
      //collate nie może być modyfikowany na istniejącym obiekcie więc można go zignorować jeśli to jedyna zmiana
      return !Constants.Constants.IgnoredPatternsInDiffs.Keys.Contains(type) || !Constants.Constants.IgnoredPatternsInDiffs[type].Any(d => d.Match(diff).Success);
    }

    /// <summary>
    /// Metoda generuje listę hashy skryptów wczytanych z plików.
    /// </summary>
    /// <param name="pi"></param>
    /// <param name="list">Lista wczytanych metadanych z plików</param>
    /// <returns></returns>
    private Dictionary<MetaTuple, Tuple<uint, Metadata>> GetMetadataCRCFromFiles(ProgressInfo pi, List<Metadata> list)
    {
      Dictionary<MetaTuple, Tuple<uint, Metadata>> scripthashes = new Dictionary<MetaTuple, Tuple<uint, Metadata>>();
      List<Tuple<Metadata, Metadata>> duplicateslist = new List<Tuple<Metadata, Metadata>>();
      pi?.ReportCount(list.Count, "Generowanie hashy z plików");
      foreach (var m in list.Where(h => !ProcedurePatch.PatchedInputDatabaseObjects.Any(p => p.Name.ToLower() == h.Name.ToLower())))
      {
        var ddl = MetadataChecksum.PrepareDDL(m.DDL);
        var hash = crc.ComputeChecksum(Encoding.UTF8.GetBytes(ddl));

        var sh = new MetaTuple(m.Name, m.DbType);
        if (!scripthashes.ContainsKey(sh))
        {
          scripthashes.Add(sh, new Tuple<uint, Metadata>(hash, m));
        }
        else
        {
          Tuple<uint, Metadata> prev = scripthashes[sh];

          duplicateslist.Add(new Tuple<Metadata, Metadata>(prev.Item2, m));
        }

        pi?.ReportProgress();
      }
      pi?.ReportEnd();

      if (duplicateslist.Count > 0)
      {
        HandleDuplicates(duplicateslist);
      }

      return scripthashes;
    }

    /// <summary>
    /// Metoda pobiera hashe obiektów z tabeli SYS_HASHCODES
    /// </summary>
    /// <returns></returns>
    private Dictionary<MetaTuple, uint> GetMetadataCRCFromDB(ProgressInfo pi)
    {
      AppendMessage("Wczytywanie hashy z bazy docelowej");
      Dictionary<MetaTuple, uint> dbhashes = new Dictionary<MetaTuple, uint>();
      using (var db = new Database(Connection.Instance.GetConnection(ConnectionType.Remote)))
      {
        var data = db.Select(SQLRepository.Instance.GetSql(SQL.DatabaseExtractor_SelectFromSysHashcodes, "SELECT * FROM SYS_HASHCODES"));
        pi?.ReportCount(data.Rows.Count, "Wczytywanie hashy");
        foreach (System.Data.DataRow row in data.Rows)
        {
          var originalName = row["NAME"].ToString();
          var name = originalName.Replace(':', '_').Replace('/', '^');
          var type = (DbTypes) int.Parse(row["TYPE"].ToString());
          var hash = (uint) int.Parse(row["HASHCODE"].ToString());
          var t = new MetaTuple(name, type, originalName);
          dbhashes.Add(t, hash);
          pi?.ReportProgress();
        }
        pi?.ReportEnd();
      }
      return dbhashes;
    }

    /// <summary>
    /// Metoda generuje warunki check, ale tylko te, których nie ma już w bazie danych.
    /// </summary>
    /// <param name="filteredScripts"></param>
    /// <param name="sh"></param>
    /// <param name="dbObj"></param>
    private static void GenerateAdditiveChecks(Dictionary<MetaTuple, Tuple<bool, Metadata>> filteredScripts, KeyValuePair<MetaTuple, Tuple<uint, Metadata>> sh, Metadata dbObj)
    {
      var fileDLLEntries = sh.Value.Item2.DDL
                        .Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(s => s.Trim());

      var dbDDLEntries = dbObj.DDL
        .Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)
        .Select(s => s.Trim());

      var onlyInFileDDL = fileDLLEntries.Except(dbDDLEntries);
      var diffDDL = onlyInFileDDL.Aggregate((acc, e) => acc + e + "\n");
      sh.Value.Item2.DDL = diffDDL + "\n";

      filteredScripts.Add(new MetaTuple(sh.Key.Name, sh.Key.DbType), new Tuple<bool, Metadata>(true, dbObj));
    }

    /// <summary>
    /// Metoda obsługuje poszczególne pozycje skryptu i rozpoznaje czy do poprawnej aplikacji danej zmiany
    /// nie trzeba użyć pełnego procesu
    /// </summary>
    /// <param name="exportInfo"></param>
    /// <param name="filteredScripts"></param>
    /// <param name="sh"></param>
    /// <param name="dbObj"></param>
    /// <returns></returns>
    private bool ProcessItem(bool exportInfo, Dictionary<MetaTuple, Tuple<bool, Metadata>> filteredScripts, KeyValuePair<MetaTuple, Tuple<uint, Metadata>> sh, Metadata dbObj)
    {
      var fileDLL = MetadataChecksum.PrepareDDL(sh.Value.Item2.DDL);
      var dbDDL = MetadataChecksum.PrepareDDL(dbObj.DDL);

      //usuwamy z pliku skryptu zawartość zwróconą w DDL z bazy dla obiektu
      var diff = Tools.RemoveIntersectionFromHashDDL(fileDLL, dbDDL);

      //collate nie może być modyfikowany na istniejącym obiekcie więc można go zignorować jeśli to jedyna zmiana
      if (!Constants.Constants.IgnoredPatternsInDiffs.Keys.Contains(sh.Key.DbType) || !Constants.Constants.IgnoredPatternsInDiffs[sh.Key.DbType].Any(d => d.Match(diff).Success))
      {
        if (sh.Key.DbType == DbTypes.Table)
        {
          exportInfo = true;
          AppendMessage("#warn#Zmieniono tabelę " + sh.Key.Name + "! Należy przeprowadzić pełny proces aktualizacji.");
        }
        else
        {
          filteredScripts.Add(new MetaTuple(sh.Key.Name, sh.Key.DbType), new Tuple<bool, Metadata>(true, dbObj));
        }
      }

      return exportInfo;
    }

    private void HandleDuplicates(List<Tuple<Metadata, Metadata>> duplicateslist)
    {
      var logger = NLog.LogManager.GetLogger("MainLogger");

      var duplicates = duplicateslist.GroupBy(d => new Tuple<string, DbTypes>(d.Item1.Name, d.Item1.DbType));
      bool abort = false;
      int i = 0;
      foreach (var kvp in duplicates)
      {
        logger.Log(NLog.LogLevel.Warn, "Wiele obiektów bazodanowych ma tą samą nazwę i typ: " + kvp.Key.Item1 + " " + kvp.Key.Item2 + ": ");

        var all = kvp.SelectMany(k => new List<Metadata> { k.Item1, k.Item2 }).Distinct();

        foreach (var met in all)
        {
          logger.Log(NLog.LogLevel.Warn, " * " + met.FilePath);
        }

        var firstDDL = all.Select(a => a.DDL).First();

        if (all.Any(a => a.DDL != firstDDL))
        {
          AppendMessage("#warn#Wiele obiektów bazodanowych ma tą samą nazwę i typ: " + kvp.Key.Item1 + " " + kvp.Key.Item2);
          AppendMessage("ale RÓŻNIĄ SIĘ ZAWARTOŚCIĄ: ");
          foreach (var met in all)
          {
            AppendMessage(" * " + met.FilePath + Environment.NewLine + met.DDL);
          }
          i++;
          abort = true;
        }

        if (i > 10)
          break;
      }

      if (i > 10)
      {
        AppendMessage(" >>> problem występuje jeszcze w " + (duplicates.Count() - i) + " innych obiektach");
      }

      if (abort)
      {
        throw new Exception("Jest to poważny problem bo jedna z wersji nadgra drugą na bazie danych. Rozstrzygnij która jest poprawna, drugą usuń, zacommituj i zaktualizuj bazę szybkim procesem.");
      }
    }

    private string GenerateTRScript(string srcpath, ProgressInfo pi, FilesystemExtractor fe, TrMode type)
    {
      var TRtype = "";
      if (type == TrMode.TrBefore)
        TRtype = "TRB";
      else if (type == TrMode.TrAfter)
        TRtype = "TRA";
      else
        return null;

      //Pobranie transferówek z plików
      var mList = fe.ParseFiles("", srcpath, pi, type);
      CheckSizeOfTransferProcedures(mList);
      _mList = mList.Where(x => x.DbType == DbTypes.TransferProcedure && x.Name.StartsWith(TRtype)).ToList();
      //Wykluczenie transferówek które już są wgrane poprawnie
      var missingTR = GenerateTransferProceduresList(type);
      if (missingTR.ToList().Count > 0)
        _mList = (List<Metadata>) missingTR.ToList();
      return ScriptCreate(type);
    }
  }
}
