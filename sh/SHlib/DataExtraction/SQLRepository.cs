﻿using SHlib.DatabaseUpdate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DataExtraction
{
  /// <summary>
  /// Enum grupujący wszystkie zapytania (nie procedury) które podlegają mechanizmowi przeciążania
  /// </summary>
  public enum SQL
  {
    DatabaseExtractor_SelectFromSysHashcodes,
    Table_GetFields,
    Table_GetIndexes,
    Table_GetSequences,
    Field_GetIndexes,
    Trigger_GetFields,
    PrimaryKey_GetGenerator,
    PrimaryKey_GetIndexes,
    Metadata_GetTransferProceduresList,
    DomainFactory_Select,
    ExceptionFactory_Select,
    TableFactory_Select,
    ProcedureFactory_Select,
    XkProcedureFactory_Select,
    ViewFactory_Select,
    TriggerFactory_Select,
    TransferProcedureFactory_Select,
    PrimaryKeyFactory_Select,
    ForeignKeyFactory_Select,
    UniqueKeyFactory_Select,
    IndexFactory_Select,
    AppSectionFactory_Select,
    ReplicationRuleFactory_Select,
    FieldFactory_Select,
    CheckConstraintFactory_Select,
    Roles_Select
  }

  /// <summary>
  /// Repozytorium z kodem SQL podlegającym przeciążeniu
  /// </summary>
  public class SQLRepository
  {
    /// <summary>
    /// Podkatalog ze skryptami
    /// </summary>
    internal const string OVERRIDEPATCHINFODIRECTORY = "SQL";
    /// <summary>
    /// Rozszerzenie plików ze skryptami
    /// </summary>
    internal const string OVERRIDEFILEEXTENSION = "sql";

    internal static string OverridePath;
    private static bool? overridePatchInfoDirectoryExists = null;

    /// <summary>
    /// Czy istnieje katalog z przeciążeniem sql
    /// </summary>
    internal static bool OverridePatchInfoDirectoryExist
    {
      get
      {
        if (overridePatchInfoDirectoryExists == null)
        {
          OverridePath = Path.Combine(Directory.GetCurrentDirectory(), OVERRIDEPATCHINFODIRECTORY);
          overridePatchInfoDirectoryExists = Directory.Exists(OverridePath);
        }
        return overridePatchInfoDirectoryExists ?? false;
      }
    }

    private static SQLRepository sqlrepository = null;
    /// <summary>
    /// Pseudo-singleton z repozytorium dostępny globalnie
    /// </summary>
    public static SQLRepository Instance
    {
      get
      {
        if (sqlrepository == null)
          sqlrepository = new SQLRepository();
        return sqlrepository;
      }
    }

    /// <summary>
    /// Metoda do wypisywania komunikatów o przeciążaniu
    /// </summary>
    public Action<string> AppendMessage
    {
      get;
      set;
    }

    /// <summary>
    /// Metoda do pobrania treści przeciążonego zapytania sql
    /// </summary>
    /// <param name="type">Jakie zapytanie ma zostać pobrane</param>
    /// <param name="default">Domyślne zapytanie w przypadku braku przeciążenia</param>
    /// <param name="args">Dodatkowe argumenty, jeżeli zapytanie jest parametryczne</param>
    /// <returns>SQL</returns>
    public string GetSql(SQL type, string @default, params object[] args)
    {
      return GetSql(type.ToString(), @default, args);
    }

    /// <summary>
    /// Metoda do pobrania treści przeciążonego skryptu sql
    /// </summary>
    /// <param name="name">Nazwa procedury</param>
    /// <param name="default">Domyślny skrypt w przypadku braku przeciążenia</param>
    /// <returns>SQL</returns>
    public string GetSqlForPatch(string name, string @default)
    {
      return GetSql(name, @default);
    }

    /// <summary>
    /// Wewnętrzna metoda obsługująca przeciążanie
    /// </summary>
    /// <param name="type">Nazwa obiektu którego sql chcemy pobrać</param>
    /// <param name="default">Domyślna treść sql</param>
    /// <param name="args">Dodatkowe parametry jeżeli treść jest parametryczna</param>
    /// <returns>SQL</returns>
    protected string GetSql(string type, string @default, params object[] args)
    {
      var sql = @default;
      var key = type;

      if (OverridePatchInfoDirectoryExist)
      {
        var fileName = Path.Combine(OVERRIDEPATCHINFODIRECTORY,
          Path.ChangeExtension(key, OVERRIDEFILEEXTENSION));

        if (File.Exists(fileName))
        {
          if (AppendMessage != null)
            AppendMessage("Znaleziono plik " + fileName + ".");
          try
          {
            sql = File.ReadAllText(fileName);
            AppendMessage("Wczytano przeciążenie " + key + " z pliku.");
          }
          catch (Exception e)
          {
            if (AppendMessage != null)
              AppendMessage(e.Message);
          }
        }
        else
          sql = @default;
      }

      var result = String.Format(sql, args);
      return result;
    }
  }
}
