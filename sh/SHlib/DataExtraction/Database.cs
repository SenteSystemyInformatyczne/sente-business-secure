﻿using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using FirebirdSql.Data.Isql;
using SHlib.Utilities;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;

namespace SHlib.DataExtraction
{
  /// <summary>
  /// Klasa odpowiedzialna za komunikację z BD
  /// </summary>
  public class Database : IDatabase, IDisposable
  {
    public int MaxConnectionPoolSize
    {
      get
      {
        return int.Parse(DbSettings.ConnectionPoolSize);
      }
    }

    private string _name;
    private FbConnection _conn;
    public IDbSettings DbSettings
    {
      get;
      private set;
    }

    public string GetName()
    {
      return _name;
    }

    public FbConnection GetConnection(bool @new = false)
    {
      if (@new)
        return new FbConnection(DbSettings.ConnectionString);
      if (_conn == null || _conn.State == ConnectionState.Closed || _conn.State == ConnectionState.Broken)
        _conn = new FbConnection(DbSettings.ConnectionString);
      else
        try
        {
          //sprawdzamy czy połączenie jest ok
          _conn.Open();
          _conn.Close();
        }
        catch
        {
          _conn = new FbConnection(DbSettings.ConnectionString);
        }
      return _conn;
    }

    public bool TestConnection(out string errorMsg)
    {
      errorMsg = "";
      try
      {
        using (FbConnection fbc = GetConnection(true))
        {
          fbc.Open();
          fbc.Close();
        }
      }
      catch (Exception e)
      {
        errorMsg = e.Message;
        return false;
      }
      return true;
    }

    public Database(IDbSettings database)
    {
      _name = database.Name;
      DbSettings = database;
    }

    /// <summary>
    /// Metoda pozwala zadać zapytanie i pobrać wyniki w formie tabeli
    /// </summary>
    /// <param name="sql">zapytanie sql</param>
    /// <returns>wynik</returns>
    public DataTable Select(string sql)
    {
      FbDataAdapter da = null;
      FbTransaction tr = null;
      DataTable dataTable = null;

      using (FbConnection fbc = GetConnection(true))
      {
        fbc.Open();
        using (tr = fbc.BeginTransaction())
        {
          FbCommand cmd = new FbCommand(sql, fbc, tr);
          cmd.CommandType = CommandType.Text;
          da = new FbDataAdapter(cmd);
          dataTable = new DataTable();
          da.Fill(dataTable);
          tr.Commit();
          fbc.Close();
          return dataTable;
        }
      }
    }

    /// <summary>
    /// Metoda pozwala wykonać procedurę składowaną i pobrać jej wyniki
    /// </summary>
    /// <param name="procedureName">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista spodziewanych parametrów wyjściowych</param>
    /// <param name="command">Obiekt który umie wykonać polecenie sql, musi być już zainicjowany</param>
    /// <returns>Wynik wykonania procedury (może być wiele krotek)</returns>
    public DataTable ExecuteProcedure(string procedureName, Dictionary<string, object> inputParameters, List<string> outputParameters, FbCommand command)
    {
      FbDataReader reader = null;
      DataTable dataTable = null;

      if (command == null)
      {
        throw new ArgumentNullException("command must be not null");
      }

      if (inputParameters != null)
      {
        command.Parameters.Clear();
        foreach (string param in inputParameters.Keys)
          command.Parameters.Add(new FbParameter(param, inputParameters[param]));
      }

      if (outputParameters != null)
      {
        dataTable = new DataTable();
        foreach (string output in outputParameters)
          dataTable.Columns.Add(output);

        using (reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            object[] values = new object[outputParameters.Count];
            int i = 0;
            foreach (string output in outputParameters)
            {
              values[i] = reader[output];
              i++;
            }
            dataTable.Rows.Add(values);
          }
        }
      }
      else
      {
        command.ExecuteNonQuery();
      }
      return dataTable;
    }

    /// <summary>
    /// Metoda pozwala wykonać procedurę składowaną i pobrać jej wyniki
    /// </summary>
    /// <param name="procedureName">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista spodziewanych parametrów wyjściowych</param>
    /// <returns>Wynik wykonania procedury (może być wiele krotek)</returns>
    public DataTable ExecuteProcedure(string procedureName, Dictionary<string, object> inputParameters, List<string> outputParameters)
    {
      using (FbConnection fbc = GetConnection(true))
      {
        fbc.Open();
        using (var command = new FbCommand(procedureName, fbc))
        {
          command.CommandType = CommandType.StoredProcedure;
          return ExecuteProcedure(procedureName, inputParameters, outputParameters, command);
        }
      }
    }

    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="command">Obiekt który umie wykonać polecenie sql, musi być już zainicjowany</param>
    public void ExecuteStatement(string statement, Dictionary<string, object> inputParameters, FbCommand command)
    {
      command.CommandText = statement;
      command.CommandType = CommandType.Text;
      ExecuteProcedure(statement, inputParameters, null, command);
    }


    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    public void ExecuteStatement(string statement, Dictionary<string, object> inputParameters)
    {
      ExecuteStatement(statement, inputParameters, outputParameters: null);
    }

    /// <summary>
    /// Metoda pozwala wykonać dowolne zapytanie sql i pobrać jego wyniki
    /// od Select różni się tym, że może być ono parametryczne
    /// np SELECT * FROM TABELA WHERE POLE=@WARTOSC
    /// <seealso cref="Select(string)"/>
    /// </summary>
    /// <param name="statement">Nazwa procedury</param>
    /// <param name="inputParameters">Lista parametrów wejściowych z wartościami</param>
    /// <param name="outputParameters">Lista oczekiwanych parametrów wyjściowych</param>
    public DataTable ExecuteStatement(string statement, Dictionary<string, object> inputParameters, List<string> outputParameters)
    {
      using (FbConnection fbc = GetConnection(true))
      {
        fbc.Open();
        using (var command = new FbCommand(statement, fbc))
        {
          command.CommandType = CommandType.Text;
          return ExecuteProcedure(statement, inputParameters, outputParameters, command);
        }
      }
    }

    /// <summary>
    /// Do wgrywania skryptów. Uwaga, ma problem z @ w komentarzach.
    /// </summary>
    /// <param name="x"></param>
    public void ExecuteScript(string x)
    {
      //konfiguracja transakcji, aby oczekiwała na możliwość zapisu zmian
      //standardowy ReadCommited z wait zamiast nowait
      FbTransactionOptions opts = new FbTransactionOptions()
      {
        TransactionBehavior = FbTransactionBehavior.ReadCommitted | FbTransactionBehavior.RecVersion | FbTransactionBehavior.Wait
      };
      using (var connection = GetConnection(true))
      {
        connection.Open();

        FbScript fbs = new FbScript(x);
        fbs.Parse();

        using (var transaction = connection.BeginTransaction(opts))
        {
          //batchowe wykonanie skryptu nie pozwala na pełną kontrolę transakcji więc musimy rozbić FbScript na kolejne zapytania i wykonywać je kolejno
          foreach (var s in fbs.Results.Select(s => s.Text.TrimStart('\r', '\n')))
          {
            using (var cmd = new FbCommand(s, connection, transaction))
            {
              cmd.Connection = connection;
              cmd.Transaction = transaction;

              //specjalna obsługa commit work w skrypcie
              if (!s.ToLower().Contains("commit work"))
              {
                cmd.CommandText = s;
                cmd.ExecuteNonQuery();
              }
              else
              {
                transaction.CommitRetaining();
              }
            }
          }
          transaction.Commit();
        }
        connection.Close();
      }
    }

    internal const string DATABASE_UUID_NAME = "DATABASE.UUID";
    internal const string SELECT_DATABASE_UUID = "select first 1 WARTOSC from CONSTS where ID=@ID";
    internal static readonly string SELECT_DATABASE_UUID_MANUAL = SELECT_DATABASE_UUID.Replace("@ID", "'" + DATABASE_UUID_NAME + "'");

    internal string ReadUUID()
    {
      var res = ExecuteStatement(SELECT_DATABASE_UUID,
              new Dictionary<string, object> { { "ID", DATABASE_UUID_NAME } },
              new List<string>() { "WARTOSC" });

      if (res.Rows.Count > 0)
      {
        object val = res.Rows[0]["WARTOSC"];
        if (val != null && val != DBNull.Value)
          return (string) val;
      }

      return null;
    }

    internal void SaveUUID(string uuid)
    {
      ExecuteStatement($"update or insert into CONSTS(ID,WARTOSC) VALUES(@ID,@VAL) MATCHING (ID)",
              new Dictionary<string, object> { { "ID", DATABASE_UUID_NAME },
                { "VAL", uuid } });
    }

    public void Dispose()
    {
      _conn?.Dispose();
    }
  }

  /// <summary>
  /// Klasa gromadząca dane potrzebne do połączenia z bazą
  /// </summary>
  public class DbSettings : ICloneable, IDbSettings
  {
    public DbSettings(ConnectionType Type)
    {
      this.Type = Type;
    }
    public DbSettings()
    {
    }
    public DbSettings(string Name)
    {
      this.Name = Name;
    }
    public ConnectionType Type
    {
      get;
      set;
    }
    public string Name
    {
      get;
      set;
    }

    public string Encoding
    {
      get;
      set;
    }

    public string ServerAddress
    {
      get;
      set;
    }
    public string Port
    {
      get;
      set;
    }
    public string User
    {
      get;
      set;
    }
    public string Password
    {
      get;
      set;
    }
    public string Role
    {
      get;
      set;
    }
    public string DatabaseFilePath
    {
      get;
      set;
    }

    public string DeltaFilePath
    {
      get;
      set;
    }

    private FbConnectionStringBuilder ConnectionBuilder
    {
      get
      {
        var csb = new FbConnectionStringBuilder();

        csb.DataSource = ServerAddress;

        if (!String.IsNullOrEmpty(Port))
          csb.Port = int.Parse(Port);

        csb.Database = DatabaseFilePath;
        csb.UserID = User;
        csb.Password = Password;
        csb.Charset = Encoding;
        csb.Role = Role;
        if(!string.IsNullOrEmpty(ConnectionPoolSize))
          csb.MaxPoolSize = int.Parse(ConnectionPoolSize);
        //csb.Pooling = false;
        return csb;
      }
    }

    public string ConnectionString
    {
      get
      {
        return ConnectionBuilder.ConnectionString;
      }
    }

    public string ConnectionPoolSize
    {
      get;
      set;
    } = "20";

    public override string ToString()
    {
      return Name + " " + ServerAddress + (string.IsNullOrWhiteSpace(Port) ? ":" + Port : "") + DatabaseFilePath;
    }

    #region ICloneable Members

    public object Clone()
    {
      var instance = new DbSettings(this.Type);
      instance.Name = this.Name;
      instance.Password = this.Password;
      instance.Port = this.Port;
      instance.ServerAddress = this.ServerAddress;
      instance.User = this.User;
      instance.Encoding = this.Encoding;
      instance.ConnectionPoolSize = this.ConnectionPoolSize;
      return instance;
    }

    #endregion

    public void CreateDatabase(int pageSize)
    {
      FbConnection.CreateDatabase(ConnectionString, pageSize);
    }

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;

      DbSettings dbs = obj as DbSettings;

      return (Name == dbs.Name) &&
        (ServerAddress == dbs.ServerAddress) &&
        (Port == dbs.Port) &&
        (User == dbs.User) &&
        (Password == dbs.Password) &&
        (Role == dbs.Role) &&
        (Encoding == dbs.Encoding) &&
        (DatabaseFilePath == dbs.DatabaseFilePath) &&
        (DeltaFilePath == dbs.DeltaFilePath) &&
        (ConnectionPoolSize == dbs.ConnectionPoolSize);
    }

    public bool IsValid()
    {
      return (!string.IsNullOrWhiteSpace(DatabaseFilePath)) &&
        (!string.IsNullOrWhiteSpace(ServerAddress)) &&
        (!string.IsNullOrWhiteSpace(User)) &&
        (!string.IsNullOrWhiteSpace(Encoding)) &&
        (!string.IsNullOrWhiteSpace(Password));
    }
  }
}
