﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHlib.Utilities;
using System.IO;
using System.Text.RegularExpressions;
using SHDataContracts.Enums;
using SHlib.DataExtraction.Model;

namespace SHlib.DataExtraction
{
  public class FilesystemExtractor : MetadataExtractor
  {
    public FilesystemExtractor(Action<string> writeMessage, Action<Exception> writeErrorMessage) : base(writeMessage, writeErrorMessage)
    {
      _connectionType = ConnectionType.File;
    }

    /// <summary>
    /// Generowanie skryptu sql
    /// </summary>
    /// <param name="srcpath">gdzie generować</param>
    /// <param name="appinsert">appini jako inserty</param>
    /// <param name="pi">obiekt informujący o postępie</param>
    /// <param name="breakOnError">czy przerywać gdy błąd logiczny walidacji plików</param>
    /// <param name="trproc"></param>
    /// <param name="destpath"></param>
    /// <returns>Skrypt</returns>
    public ScriptsResult GenerateScript(string srcpath, ProgressInfo pi, ConnectionType connection, bool appinsert = true, bool breakOnError = false, TrMode trproc = TrMode.Default, string destpath = null, bool alwaysGenerateHashes = true, bool checkIntegrity = true)
    {
      if (string.IsNullOrWhiteSpace(destpath))
        destpath = srcpath;
      ScriptsResult result = new ScriptsResult();

      AppendMessage("Generowanie skryptu aktualizującego");

      if (checkIntegrity)
      {
        AppendMessage("Sprawdzanie spójności bazy");
        CheckTransferProcIntegrity(ConnectionType.Remote, pi, alwaysGenerateHashes);
      }

      srcpath = srcpath + Path.DirectorySeparatorChar;
      AppendMessage("Wczytywanie plików");
      List<Metadata> list = this.ParseFiles("", srcpath, pi, trproc, true);

      CheckSizeOfTransferProcedures(list);

      if (eList.Count > 0)
      {
        var msg = "Poniższe pliki zawierają błędy. WYGENEROWANE SKRYPTY MOGĄ NIE WGRYWAC SIĘ PRAWIDŁOWO!!:\n";
        foreach (string s in eList)
          msg += s + "\n";

        if (!breakOnError)
        {
          AppendMessage(msg);
        }
        else
        {
          throw new Exception(msg);
        }
      }

      string content = string.Empty;

      Encoding encoding = Constants.Constants.Encodings[Connection.Instance.GetConnection(ConnectionType.Remote).Encoding ?? "WIN1250"];

      if (trproc == TrMode.Default || trproc == TrMode.NoTr)
      {
        content = ScriptCreate(TrMode.NoTr, pi, true);
        result.Metadata = content;
        SaveOutputFile(destpath, "esystem.sql", content, encoding: encoding);
      }

      if (trproc == TrMode.Default || trproc == TrMode.TrBefore)
      {
        content = ScriptCreate(TrMode.TrBefore, pi);
        result.TrBefore = content;
        SaveOutputFile(destpath, "before.esystem.sql", content, encoding: encoding);
      }

      if (trproc == TrMode.Default || trproc == TrMode.TrAfter)
      {
        content = ScriptCreate(TrMode.TrAfter, pi);
        result.TrAfter = content;
        SaveOutputFile(destpath, "after.esystem.sql", content, encoding: encoding);
      }

      if (trproc == TrMode.Default || trproc == TrMode.NoTr)
      {
        if (appinsert)
        {
          content = AppiniSQLCreate();
          result.AppIni = content;
          SaveOutputFile(srcpath, "app.sql", content, encoding: encoding);
        }
        else
        {
          content = AppiniCreate();
          SaveOutputFile(srcpath, "app.app", content, encoding: encoding);
        }
      }

      return result;
    }

    /// <summary>
    /// Metoda generująca skrypt aktualizacyjny z plików, które mają daną sygnaturę.
    /// </summary>
    /// <param name="srcpath">Ścieżka do dbscripts</param>
    /// <param name="pi">Obiekt raportowania postępu</param>
    /// <param name="signature">Sygnatura</param>
    /// <returns></returns>
    public string FastUpdateFromSignature(string srcpath, ProgressInfo pi, string signature)
    {
      //AppendMessage("Sprawdzanie spójności bazy");
      //CheckTransferProcIntegrity(ConnectionType.Remote, pi);
      //DatabasePatch.CheckDatabaseId(srcpath, Connection.Instance.GetConnection(ConnectionType.Remote), pi);

      AppendMessage($"Generowanie skryptu dla tematu {signature}");
      AppendMessage("#warn#Uwaga! Zależności w obrębie tej funkcjonalności nie są obecnie obsługiwane - obiekty, które nie są oznaczone bezpośrednio sygnaturą, ale zostały wyeksportowane na podstawie zależności nie wchodzą w skład generowanego skryptu.");

      srcpath = srcpath + Path.DirectorySeparatorChar;
      StringBuilder scriptContent = new StringBuilder();

      AppendMessage("Wczytywanie plików");
      List<Metadata> list = ParseFiles("", srcpath, pi, TrMode.Default);

      //Filtrowanie tylko tych obiektów bazodanowych które mają daną sygnaturę
      _mList = new List<Metadata>();
      var signaturesFromComment = new Regex("(?<=IS ').*(?=')");
      var signatureFromTRName = new Regex("(PR|BS|ZG)[0-9]+");
      foreach (Metadata metadata in list)
      {
        string allSignatures = "";
        switch (metadata.DbType)
        {
          case DbTypes.AppSection:
            AppSection md = metadata as AppSection;
            allSignatures = md.AppSectionObject.Elements.Where(m => m.Identifier == "Signature").Select(m => m.Value).FirstOrDefault() ?? "";
            break;
          case DbTypes.TransferProcedure:
            var signatureMatch = signatureFromTRName.Match(metadata.Name);
            if (signatureMatch.Success) allSignatures = signatureMatch.Value;
            break;
          default:
            var signaturesMatch = signaturesFromComment.Match(metadata.Comment.ToUpper());
            if (signaturesMatch.Success) allSignatures = signaturesMatch.Value;
            break;
        }
        if (allSignatures.Split(';').Any(s => s.Trim() == signature))
        {
          _mList.Add(metadata);
        }
      }

      CheckSizeOfTransferProcedures(_mList);

      AppendMessage($"Znaleziono {_mList.Count} obiektów z sygnaturą {signature}");
      foreach (Metadata metadata in _mList)
      {
        AppendMessage($"[{metadata.DbType}]{metadata.Name}");
      }

      scriptContent.AppendLine(ScriptCreate(TrMode.TrBefore));
      scriptContent.AppendLine(ScriptCreate(TrMode.NoTr));
      scriptContent.AppendLine(ScriptCreate(TrMode.TrAfter));
      scriptContent.AppendLine(AppiniSQLCreate(true, AppendMessage));

      return scriptContent.ToString();
    }


    public List<Metadata> ParseFiles(string signature, string path, out List<string> errors, ProgressInfo pi = null, TrMode trproc = TrMode.Default, bool flushCollection = false)
    {
      errors = _eList;
      return ParseFiles(signature, path, pi, trproc, flushCollection);
    }

    /// <summary>
    /// Wczytuje obiekty z plików .sql i .app
    /// </summary>
    /// <param name="signature">Sygnatura (może być pusta)</param>
    /// <param name="path">Ścieżka gdzie są pliki</param>
    /// <param name="pi">Obiekt informacji o postępie</param>
    /// <returns>Lista obiektów danych</returns>
    public List<Metadata> ParseFiles(string signature, string path, ProgressInfo pi = null, TrMode trproc = TrMode.Default, bool flushCollection = false)
    {
      Dictionary<MetadataFactory, List<string>> allfiles = new Dictionary<MetadataFactory, List<string>>();
      List<Metadata> mlist = new List<Metadata>();
      var currentList = mfList.AsEnumerable();
      if (trproc == TrMode.TrAfter || trproc == TrMode.TrBefore)
        currentList = mfList.Where(mf => mf.Key == DbTypes.TransferProcedure);

      foreach (var mf in currentList)
      {
        if (Directory.Exists(Path.Combine(path, mf.Value.DirName)))
        {
          var suffix = mf.Key == DbTypes.AppSection ? "json" : "sql";
          var prefix = string.Empty;
          if (mf.Key == DbTypes.TransferProcedure)
          {
            if (trproc == TrMode.TrAfter)
            {
              prefix = "tra";
            }
            else if (trproc == TrMode.TrBefore)
            {
              prefix = "trb";
            }
          }

          //http://stackoverflow.com/q/5262025/431878
          var files = Directory.EnumerateFiles(Path.Combine(path, mf.Value.DirName), "*", SearchOption.AllDirectories)
                           .Where(file =>
                           {
                             var lfile = Path.GetFileName(file).ToLower();
                             return lfile.EndsWith(suffix) &&
                              lfile.StartsWith(prefix);
                           }).ToList();

          allfiles.Add(mf.Value, files);
        }
      }
      var cnt = allfiles.Aggregate(0, (a, v) => a + v.Value.Count);
      if (pi != null)
        pi.ReportCount(cnt, "Wczytywanie obiektów bazy z plików");


      pi.Counter = 0;
      var tasks = new List<Task<List<Metadata>>>();
      foreach (var a in allfiles)
        tasks.Add(a.Key.CreateCollectionFromFiles(signature, a.Value, pi));

      Task.WhenAll(tasks);

      _mList = tasks.SelectMany(t => t.Result).ToList();

      pi.ReportEnd();
      return mList;
    }

    /// <summary>
    /// Sprawdzenie czy plik jest mniej więcej poprawnie wyeksportowany
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    private bool CheckFile(string file)
    {
      int status = 0;
      string line;
      StreamReader sr = new StreamReader(file);
      while ((line = sr.ReadLine()) != null)
      {
        switch (status)
        {
          case 2:
            sr.Close();
            return true;
          case 1:
            if ((line.Contains(CustomMetadata.SCOMMENT)) || (file.Contains("\\tr\\")) || (file.Contains("\\appini\\")))
              status++;
            break;
          case 0:
            if (line.Contains(CustomMetadata.SKEY))
              status++;
            break;
        }
      }
      sr.Close();
      return false;
    }
  }
}
