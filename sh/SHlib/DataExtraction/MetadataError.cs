﻿using SHlib.DataExtraction.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DataExtraction
{
  public class TransferProcedureError : IMetadataError
  {
    public TransferProcedureError(Metadata src, MetadataErrorType errType, string msg = null)
    {
      Message = msg;
      Type = errType;
      Source = src;
    }

    public string Message
    {
      get;
      set;
    }

    public Metadata Source
    {
      get;
      private set;
    }

    public MetadataErrorType Type
    {
      get;
      private set;
    }
  }
}
