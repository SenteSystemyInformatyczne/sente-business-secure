﻿using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Services;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHDataContracts.Model.AppSection;
using SHlib.DatabaseUpdate;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SHlib.DataExtraction
{
  /// <summary>
  /// Klasa obiektu potrafiącego wyeksportować wszystkie interesujące nas metadane z bazy
  /// </summary>
  public class MetadataExtractor
  {
    /// <summary>
    /// Delegat do przekazywania komunikatów w trakcie działania
    /// </summary>
    internal Action<string> WriteMessage
    {
      get;
      set;
    }

    /// <summary>
    /// Delegat do przekazywania komunikatów o błędzie
    /// </summary>
    internal Action<Exception> WriteErrorMessage
    {
      get;
      set;
    }

    /// <summary>
    /// Jeśli flaga włączona, to będą pobierane granty tylko dla standardowych użytkowników
    /// </summary>
    internal bool GrantFilter
    {
      get;
      set;
    }

    internal Dictionary<DbTypes, MetadataFactory> _mflist = new Dictionary<DbTypes, MetadataFactory>();
    internal List<Metadata> _mList = new List<Metadata>();
    internal List<String> _eList = new List<String>();
    /// <summary>
    /// Lista błędów
    /// </summary>
    public List<String> eList
    {
      get
      {
        return _eList;
      }
    }
    /// <summary>
    /// Lista ekstraktorów konkretnych typów
    /// </summary>
    public List<Metadata> mList
    {
      get
      {
        return _mList;
      }
    }
    /// <summary>
    /// Ta sama lista tylko po kluczu
    /// </summary>
    public Dictionary<DbTypes, MetadataFactory> mfList
    {
      get
      {
        return _mflist;
      }
    }

    /// <summary>
    /// Konstruktor klasy
    /// </summary>
    /// <param name="writeMessage">Delegat do przekazywania komunikatów</param>
    /// <param name="writeErrorMessage">Delegat do przekazywania błedów</param>
    /// <param name="grantFilter">jeśli flaga włączona, to będą pobierane granty tylko dla standardowych użytkowników</param>
    public MetadataExtractor(
      Action<string> writeMessage,
      Action<Exception> writeErrorMessage,
      bool grantFilter = false)
    {
      WriteMessage = writeMessage;
      WriteErrorMessage = writeErrorMessage;
      GrantFilter = grantFilter;

      mfList.Add(DbTypes.Domain, new DomainFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Sequence, new SequenceFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Exception, new SExceptionFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Table, new TableFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Field, new FieldFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Procedure, new ProcedureFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.XkProcedure, new XKProcedureFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.View, new ViewFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Trigger, new TriggerFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.TransferProcedure, new TransferProcedureFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.PrimaryKey, new PrimaryKeyFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.ForeignKey, new ForeignKeyFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.UniqueKey, new UniqueKeyFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Index, new IndexFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.AppSection, new AppSectionFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.ReplicationRule, new ReplicationRuleFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Check, new CheckConstraintFactory()
      {
        GetConnection = GetConnection
      });
      mfList.Add(DbTypes.Role, new RolesFactory()
      {
        GetConnection = GetConnection
      });
    }

    internal ConnectionType _connectionType;

    public ConnectionType GetConnectionType()
    {
      return _connectionType;
    }
    internal IDbSettings GetConnection(bool throwExceptions = true)
    {
      IDbSettings dbSettings = Connection.Instance.GetConnection(_connectionType);
      if (throwExceptions && string.IsNullOrWhiteSpace(dbSettings.ConnectionString))
        throw new NullReferenceException();
      return dbSettings;
    }

    /// <summary>
    /// Generowanie skryptu
    /// </summary>
    /// <param name="pi">Obiekt informacji o postępie</param>
    /// <returns>skrypt</returns>
    public string ScriptCreate(TrMode trproc = TrMode.Default, ProgressInfo pi = null, bool combineTableFields = false)
    {
      StringBuilder sb = new StringBuilder();
      IList<IMetadataError> errors = new List<IMetadataError>();

      IEnumerable<Metadata> trBefore = Enumerable.Empty<Metadata>();
      if (trproc == TrMode.Default || trproc == TrMode.TrBefore)
        trBefore = GenerateTransferProceduresList(trproc);

      IEnumerable<Metadata> domains = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> sequence = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> exceptions = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> tables = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> fields = Enumerable.Empty<Metadata>();

      if (trproc.NoTr())
      {
        domains = mList.Where(item => item.DbType == DbTypes.Domain);
        sequence = mList.Where(item => item.DbType == DbTypes.Sequence);
        exceptions = mList.Where(item => item.DbType == DbTypes.Exception);
        tables = mList.Where(item => item.DbType == DbTypes.Table);
        fields = mList.Where(item => item.DbType == DbTypes.Field);
      }

      IEnumerable<Metadata> empty_proc = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> views = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> triggers = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> procedures = Enumerable.Empty<Metadata>();

      if (trproc.NoTr())
      {
        empty_proc = mList.Where(item => item.DbType == DbTypes.Procedure || item.DbType == DbTypes.XkProcedure);
        views = mList.Where(item => item.DbType == DbTypes.View);
        triggers = mList.Where(item => item.DbType == DbTypes.Trigger);
        procedures = mList.Where(item => item.DbType == DbTypes.Procedure || item.DbType == DbTypes.XkProcedure);
      }

      IEnumerable<Metadata> pk = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> fk = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> unq = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> indices = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> replicationRules = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> checks = Enumerable.Empty<Metadata>();
      IEnumerable<Metadata> roles = Enumerable.Empty<Metadata>();

      if (trproc.NoTr())
      {
        pk = mList.Where(item => item.DbType == DbTypes.PrimaryKey);
        fk = mList.Where(item => item.DbType == DbTypes.ForeignKey);
        unq = mList.Where(item => item.DbType == DbTypes.UniqueKey);
        indices = mList.Where(item => item.DbType == DbTypes.Index);
        replicationRules = mList.Where(item => item.DbType == DbTypes.ReplicationRule);
        checks = mList.Where(item => item.DbType == DbTypes.Check);
        roles = mList.Where(item => item.DbType == DbTypes.Role);
      }

      IEnumerable<Metadata> trAfter = Enumerable.Empty<Metadata>();
      if (trproc == TrMode.Default || trproc == TrMode.TrAfter)
        trAfter = GenerateTransferProceduresList(trproc);

      var count = trBefore.Count() +
                  domains.Count() +
                  sequence.Count() +
                  exceptions.Count() +
                  tables.Count() +
                  fields.Count() +
                  empty_proc.Count() +
                  views.Count() +
                  pk.Count() +
                  triggers.Count() +
                  procedures.Count() +
                  fk.Count() +
                  unq.Count() +
                  indices.Count() +
                  replicationRules.Count() +
                  checks.Count() +
                  roles.Count() +
                  trAfter.Count();

      if (pi != null)
        pi.ReportCount(count, "Generowanie skryptu");

      GenerateItems(pi, sb, trBefore, GrantFilter, errors);
      GenerateItems(pi, sb, roles, GrantFilter, errors);
      GenerateItems(pi, sb, domains, GrantFilter, errors);
      GenerateItems(pi, sb, sequence, GrantFilter, errors);
      GenerateItems(pi, sb, exceptions, GrantFilter, errors);
      if (combineTableFields)
      {
        foreach (var t in tables)
        {
          foreach (var f in fields.Where(i => i.Name.StartsWith(t.Name + ".")))
          {
            var fieldSql = f.DDL.Remove(0, f.DDL.IndexOf(" ADD") + 5).Replace(";", "");
            if (!t.DDL.Contains("," + fieldSql.Trim()))
            {
              t.DDL = t.DDL.Replace(");", "," + fieldSql.Trim() + "\n);");
              t.Comment = f.Comment + t.Comment;
            }
          }
        }
        GenerateItems(pi, sb, tables, GrantFilter, errors);
      }
      else
      {
        GenerateItems(pi, sb, tables, GrantFilter, errors);
        GenerateItems(pi, sb, fields, GrantFilter, errors);
      }

      foreach (var item in empty_proc)
      {
        var v = item.SaveObject(GrantFilter);
        var matches = Regex.Matches(item.SaveObject(GrantFilter), @"\Wbegin\W", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        var headerMatcher = new Regex("type of column ([\\w\\.]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        if (matches.Count > 0)
        {
          var idx = matches[0].Index;
          var procedureHeader = v.Substring(0, idx + 1);
          var headerMatch = headerMatcher.Matches(procedureHeader);
          if (headerMatch.Count > 0 && tables.Count() > 0)
          {
            var primitiveTypesMap = headerMatch.Cast<Match>().Select(m => GetPrimitiveTypeDeclaration(m, tables, views)).Distinct().ToDictionary(k => k.Key, val => val.Value);
            foreach (var m in headerMatch.Cast<Match>().OrderByDescending(x => x.Index))
            {
              procedureHeader = procedureHeader.Remove(m.Index, m.Length);
              procedureHeader = procedureHeader.Insert(m.Index, primitiveTypesMap[m.Groups[1].Value]);
            }
          }
          sb.Append(procedureHeader + "begin\r\n  suspend;\r\nend^\r\nSET TERM ; ^ \r\n\r\n");
        }

        if (pi != null)
        {
          pi.ReportProgress();
        }
      }

      GenerateItems(pi, sb, pk, GrantFilter, errors);
      GenerateItems(pi, sb, unq, GrantFilter, errors);
      GenerateItems(pi, sb, fk, GrantFilter, errors);
      GenerateItems(pi, sb, indices, GrantFilter, errors);
      GenerateItems(pi, sb, views, GrantFilter, errors);
      GenerateItems(pi, sb, triggers, GrantFilter, errors);
      GenerateItems(pi, sb, procedures, GrantFilter, errors);
      GenerateItems(pi, sb, replicationRules, GrantFilter, errors);
      GenerateItems(pi, sb, checks, GrantFilter, errors);
      GenerateItems(pi, sb, trAfter, GrantFilter, errors);

      if (errors.Count > 0)
      {
        if (HandleErrors(errors, AppendMessage))
          throw new Exception("#warn#Zatrzymano z błędem.");
      }

      if (pi != null)
      {
        pi.ReportEnd();
      }

      return sb.ToString();
    }

    private KeyValuePair<string, string> GetPrimitiveTypeDeclaration(Match arg, IEnumerable<Metadata> tables, IEnumerable<Metadata> views)
    {
      var tableName = arg.Groups[1].Value.Split('.')[0];
      var fieldName = arg.Groups[1].Value.Split('.')[1];

      var table = tables.SingleOrDefault(t => t.Name.ToUpper() == tableName.ToUpper());
      if (table != null)
      {
        var typeMatcher = new Regex($"\"?{fieldName}\"? (.+)", RegexOptions.IgnoreCase);
        var match = typeMatcher.Match(table.DDL);
        if (match.Success)
        {
          return new KeyValuePair<string, string>(arg.Groups[1].Value, match.Groups[1].Value.Trim().Trim(','));
        }
      }

      var view = views.SingleOrDefault(v => v.Name == tableName.ToUpper());
      if (view != null)
      {
        var typeMatcher = new Regex($"{fieldName} .+-- (.+)", RegexOptions.IgnoreCase);
        var match = typeMatcher.Match(view.DDL);
        if (match.Success)
        {
          return new KeyValuePair<string, string>(arg.Groups[1].Value, match.Groups[1].Value.Trim().Trim(','));
        }
      }

      throw new KeyNotFoundException("Nie znaleziono definicji pola " + arg.Groups[1].Value);
    }

    /// <summary>
    /// Metoda obsługująca błędy wykryte podczas generowania skryptu
    /// </summary>
    /// <param name="errors"></param>
    /// <param name="writeMessage"></param>
    private bool HandleErrors(IList<IMetadataError> errors, Action<string> writeMessage = null)
    {
      bool stop = false;
      var groups = errors.GroupBy(e => e.Type);
      StringBuilder sb;
      foreach (var g in groups)
      {
        switch (g.Key)
        {
          case MetadataErrorType.SUSPEND_IN_TR:
            sb = LogError("#warn#Wykryto użycie niedozwolonej operacji 'suspend' w procedurach transferowych:", g);
            writeMessage?.Invoke(sb.ToString());
            stop = true;
            break;
          case MetadataErrorType.DROP_IN_TR:
            sb = LogError("#warn#Wykryto użycie niedozwolonej operacji 'drop' w procedurach transferowych:", g);
            writeMessage?.Invoke(sb.ToString());
            stop = true;
            break;
        }
      }
      return stop;
    }

    private static StringBuilder LogError(string msg, IGrouping<MetadataErrorType, IMetadataError> g)
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine(msg);
      foreach (var e in g)
      {
        sb.AppendLine($"#warn#{e.Source.Name}");
      }
      sb.AppendLine($"#warn#Popraw procedury i uruchom proces ponownie.");
      return sb;
    }

    /// <summary>
    /// Tworzenie listy procedur transferowych, które nie zostały jeszcze wgrane na bazę docelową.
    /// </summary>
    /// <param name="trMode">Typ p.transferowej (before/after)</param>
    /// <returns></returns>
    internal IEnumerable<Metadata> GenerateTransferProceduresList(TrMode trMode)
    {
      string trFileStartsWith;
      switch (trMode)
      {
        case TrMode.TrAfter:
          trFileStartsWith = "TRA_";
          break;
        case TrMode.TrBefore:
          trFileStartsWith = "TRB_";
          break;
        default:
          throw new Exception("Błąd w pobieraniu listy procedur transferowych.");
      }

      using (var db = new Database(Connection.Instance.GetConnection(ConnectionType.Remote)))
      {

        var t = db.Select(SQLRepository.Instance.GetSql(
          SQL.Metadata_GetTransferProceduresList, "select nazwa from sys_trhist where stan={0}", "1"));

        IList<string> completedTrs = new List<string>();
        for (int i = 0; i < t.Rows.Count; i++)
        {
          string name = t.Rows[i]["NAZWA"].ToString();
          completedTrs.Add(name);
        }

        return SortTransferProcedures(mList.Where(item => item.DbType == DbTypes.TransferProcedure && item.Name.StartsWith(trFileStartsWith) && !completedTrs.Contains(item.Name)).Select(item => (TransferProcedure) item));
      }
    }

    /// <summary>
    /// Metoda generująca treść metadanych do skryptu
    /// </summary>
    /// <param name="pi"></param>
    /// <param name="sb"></param>
    /// <param name="pk"></param>
    /// <param name="grantFilter"></param>
    /// <param name="errors"></param>
    internal static void GenerateItems(ProgressInfo pi, StringBuilder sb, IEnumerable<Metadata> pk, bool grantFilter, IList<IMetadataError> errors)
    {
      foreach (var item in pk)
      {
        if (ValidateMetadata(item, errors))
        {
          var content = item.SaveObject(grantFilter, true);
          sb.Append(content);
        }

        if (pi != null)
        {
          pi.ReportProgress();
        }
      }
    }

    /// <summary>
    /// Metoda walidująca treść DDL obiektu metadanych
    /// </summary>
    /// <param name="item">Obiekt do walidacji</param>
    /// <param name="errors">Lista błędów</param>
    /// <returns>Czy obiekt jest poprawny</returns>
    public static bool ValidateMetadata(Metadata item, IList<IMetadataError> errors)
    {
      bool result = true;
      if (item is TransferProcedure)
      {
        Dictionary<MetadataErrorType, Regex> patterns = new Dictionary<MetadataErrorType, Regex>();
        patterns.Add(MetadataErrorType.SUSPEND_IN_TR, new Regex("^[^-]*suspend;", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline));
        patterns.Add(MetadataErrorType.DROP_IN_TR, new Regex(@"^\s*(?!--)\s*DROP (INDEX|TABLE|SHADOW|VIEW|TRIGGER|PROCEDURE|EXTERNAL FUNCTION|FILTER|SEQUENCE|GENERATOR|EXCEPTION|COLLATION).*$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline));

        foreach (var p in patterns)
        {
          if (p.Value.Match(item.DDL).Success)
          {
            errors.Add(new TransferProcedureError(item, p.Key));
            result = false;
          }
        }
      }
      return result;
    }

    /// <summary>
    /// Generowanie appini do pliku
    /// </summary>
    /// <returns></returns>
    public string AppiniCreate()
    {
      StringBuilder sb = new StringBuilder();
      var appini = mList.Where(item => item.DbType == DbTypes.AppSection);
      foreach (var item in appini)
      {
        sb.Append(item.SaveObject(GrantFilter));
      }
      return sb.ToString();
    }

    /// <summary>
    /// Generowanie appini do insertów sql
    /// </summary>
    /// <returns></returns>
    public string AppiniSQLCreate(bool appendXChanges = false, Action<string> writeMessage = null)
    {
      StringBuilder sb = new StringBuilder();
      var appini = mList.Where(item => item.DbType == DbTypes.AppSection).Cast<AppSection>();
      foreach (var item in appini)
      {
        sb.AppendLine(AppIniSqlCreateSection(item, appendXChanges, writeMessage));
      }
      return sb.ToString();
    }

    /// <summary>
    /// Generowanie pojedynczej sekcji appini w formie insertów SQL
    /// </summary>
    /// <param name="appSection"></param>
    /// <returns></returns>
    private static string AppIniSqlCreateSection(AppSection appSection, bool appendXChanges, Action<string> writeMessage)
    {
      StringBuilder result = new StringBuilder();
      var name = $"'{appSection.AppSectionObject.Name}'";
      result.AppendLine($"DELETE FROM S_APPINI WHERE SECTION = {name} AND ( PREFIX is null or upper(prefix) not starting with 'U');");
      foreach (var e in appSection.AppSectionObject.Elements)
      {
        BuildAppSectionInsert(result, name, e);
      }
      //Zachowanie zmian "X'owych"
      AppSection dbAppSection = (appSection.CurrentDbMetadata as AppSection);
      if (dbAppSection != null && appendXChanges)
      {
        var comparer = new AppElementEqualityComparer();
        var intersect = appSection.AppSectionObject.Elements.Intersect(dbAppSection.AppSectionObject.Elements, comparer);
        var xchanges = dbAppSection.AppSectionObject.Elements.Except(intersect, comparer);
        if (xchanges.Count() > 0)
        {
          writeMessage?.Invoke("#warn#Wykryto zmiany X'owe w sekcji APPINI: " + name + "\nZmiany nie zostały uwzględnione w skrypcie do aktualizacji! Należy wykonać pełny eksport skryptów.");
          foreach (var x in xchanges)
          {
            writeMessage?.Invoke($"(ns:{x.NS} p:{x.Prefix}) {x.Identifier} : {x.Value}");
            //BuildAppSectionInsert(result, name, x); //jeśli kiedyś przyjdą z pochodniami, to możemy to odkomentować :P
          }
        }
      }
      return result.ToString();
    }

    /// <summary>
    /// Metoda tworzy SQL INSERT dla pojedynczego elementu AppSection
    /// </summary>
    /// <param name="result"></param>
    /// <param name="name"></param>
    /// <param name="e"></param>
    private static void BuildAppSectionInsert(StringBuilder result, string name, AppElement e)
    {
      var id = $"'{e.Identifier}'";
      var val = $"'{e.Value.Replace("'", "''")}'";
      var prefix = string.IsNullOrEmpty(e.Prefix) ? "NULL" : $"'{e.Prefix}'";
      var ns = string.IsNullOrEmpty(e.NS) ? "'SENTE'" : $"'{e.NS}'";
      result.AppendLine($"INSERT INTO S_APPINI (SECTION, IDENT, VAL, NAMESPACE, PREFIX) VALUES ({name}, {id}, {val}, {ns}, {prefix});");
    }

    /// <summary>
    /// Obsługa przekazywania komunikatów
    /// </summary>
    /// <param name="msg"></param>
    internal void AppendMessage(string msg)
    {
      WriteMessage?.Invoke(msg);
    }

    /// <summary>
    /// Obsługa przekazywania błędów
    /// </summary>
    /// <param name="e"></param>
    internal void ErrorMessage(Exception e)
    {
      if (WriteErrorMessage != null)
      {
        WriteErrorMessage(e);
      }
    }

    public enum TrMode
    {
      Default,
      NoTr,
      TrBefore,
      TrAfter
    };


    internal void CheckTransferProcIntegrity(ConnectionType connection, ProgressInfo pi, bool alwaysGenerateHashes = true)
    {
      DatabasePatch.CheckSHStructuresIntegrity(pi, AppendMessage, updateHashes: alwaysGenerateHashes);

      using (var db = new Database(Connection.Instance.GetConnection(connection)))
      {
        var t = db.Select(SQLRepository.Instance.GetSql(
          SQL.Metadata_GetTransferProceduresList, "select nazwa from sys_trhist where stan={0}", "0"));
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < t.Rows.Count; i++)
        {
          string name = t.Rows[i]["NAZWA"].ToString();

          sb.AppendLine(name);
        }

        if (t.Rows.Count > 0)
        {
          throw new Exception("Brak spójności docelowej bazy danych. Poniższe procedury transferowe nie zostały poprawnie wgrane:\n" + sb.ToString());
        }
      }
    }

    internal void SaveOutputFile(string srcpath, string fname, string content, bool removeFileBeforeUpdate = false, Encoding encoding = null)
    {
      encoding = encoding ?? Encoding.GetEncoding(1250);
      string fpath;
      try
      {
        fpath = Path.Combine(srcpath, fname);

        if (removeFileBeforeUpdate && File.Exists(fpath))
          File.Delete(fpath);

        using (StreamWriter file = new StreamWriter(fpath, false, encoding))
        {
          file.Write(content.Replace("\r\n", "\n").Replace("\n", "\r\n"));
        }
        AppendMessage("Wygenerowano plik " + fpath);
      }
      catch (Exception ex)
      {
        ErrorMessage(ex);
      }
    }

    internal static void SetSmallLettersEscape(List<Metadata> list)
    {
      var escapeNeeded = list
      .Where(q => q.DbType == DbTypes.AppSection)
      .GroupBy(q => q.Name.ToLower())
      .Where(q => q.Count() > 1);
      foreach (var group in escapeNeeded)
      {
        foreach (var elem in group)
        {
          elem.NeedEscaping = true;
        }
      }
    }

    #region SEARCH
    /// <summary>
    /// Szukaj wszystkie obiekty
    /// </summary>
    /// <param name="progressInfo"></param>
    /// <returns></returns>
    internal List<Metadata> SearchAll(ProgressInfo progressInfo = null)
    {
      var processorCount = Environment.ProcessorCount;
      int maxDegreeOfParallelism;
      if (!int.TryParse(GetConnection(true).ConnectionPoolSize, out maxDegreeOfParallelism))
        maxDegreeOfParallelism = processorCount;

      var count = mfList.AsParallel().WithDegreeOfParallelism(maxDegreeOfParallelism).Sum(mf => mf.Value.GetCollectionCount());

      if (progressInfo != null)
        progressInfo.ReportCount(count, "Tworzenie listy obiektów z bazy danych");

      try
      {
        _mList = mfList.AsParallel().WithDegreeOfParallelism(maxDegreeOfParallelism).SelectMany(mf => mf.Value.CreateCollection(progressInfo, true)).ToList();
      }
      catch (AggregateException ae)
      {
        foreach (var ex in ae.InnerExceptions)
        {
          progressInfo.ReportError(ex);
        }
      }
      catch (Exception e)
      {
        progressInfo.ReportError(e);
      }

      if (progressInfo != null)
        progressInfo.ReportEnd();
      return mList;
    }
    /// <summary>
    /// Metoda zwraca listę obiektów przekazanego typu
    /// </summary>
    /// <param name="type">typ obiektu</param>
    /// <param name="progressInfo"></param>
    /// <returns></returns>
    internal List<Metadata> SearchAllForTypeObject(DbTypes type, ProgressInfo progressInfo = null)
    {
      var count = 0;
      List<Metadata> result = new List<Metadata>();

      var mf = mfList.First(m => m.Key == type);
      count = mf.Value.GetCollectionCount();

      if (progressInfo != null)
        progressInfo.ReportCount(count, $"Tworzenie listy obiektów z bazy danych typu: {type.ToString()}");

      try
      {
        result = mf.Value.CreateCollection();
      }
      catch (Exception e)
      {
        progressInfo.ReportError(e);
      }

      if (progressInfo != null)
        progressInfo.ReportEnd();
      return result;
    }

    /// <summary>
    /// Eksportuje metadane dla podanej sygnatury
    /// </summary>
    /// <param name="signature">sygnatura</param>
    /// <returns></returns>
    public List<Metadata> SearchForSignatures(string signature)
    {
      foreach (var mf in mfList)
      {
        _mList.AddRange(mf.Value.CreateCollection(signature));
      }
      return mList;
    }
    #endregion

    /// <summary>
    /// Sprawdza czy na liście nie ma za dużych procedur transferowych. Jeśli są, to rzuca wyjątek.
    /// </summary>
    internal static void CheckSizeOfTransferProcedures(List<Metadata> list)
    {
      var badTrs = list.Where(m => m.DbType == DbTypes.TransferProcedure && ((TransferProcedure) m).DDLWithLogging.Length > 65535).ToList();
      if (badTrs.Count > 0)
      {
        StringBuilder sb = new StringBuilder("Następujące procedury transferowe przekraczają maksymalny rozmiar:\n");
        foreach (Metadata tr in badTrs)
        {
          sb.AppendLine(tr.Name);
        }
        throw new Exception(sb.ToString());
      }
    }

    /// <summary>
    /// Sprawdza czy wpisy z S_APPINI w bazie źródłowej są powielone i czy wartości powielonych wpisów się różnią. Jeśli wartości powielonych wpisów się różnią, to rzuca wyjątek.
    /// </summary>
    internal static void CheckAppSectionEntries(ProgressInfo pi)
    {
      using (var db = new Database(Connection.Instance.GetConnection(ConnectionType.Source)))
      {


        var t = db.Select(@"select distinct SECTION, count(distinct VAL) as VALCOUNT from S_APPINI group by SECTION, IDENT, NAMESPACE, PREFIX having count(REF)>1");

        var duplicates = new List<string>();
        var invalid = new List<string>();

        if (t != null)
        {
          for (int i = 0; i < t.Rows.Count; i++)
          {
            string section = t.Rows[i]["SECTION"].ToString(); // nazwa sekcji
            string valcount = t.Rows[i]["VALCOUNT"].ToString(); // liczba różnych wartości
            if (valcount == "1")
            {
              duplicates.Add(section);
            }
            else
            {
              invalid.Add(section);
            }
          }
        }

        var sb = new StringBuilder();
        if (invalid.Count > 0)
        {
          sb.AppendLine("#warn#Uwaga! W tabeli S_APPINI znajdują się nieprawidłowe wpisy. Błąd dotyczy następujących sekcji:");
          foreach (var section in invalid)
          {
            sb.AppendLine($"#warn#{section}");
          }
          sb.AppendLine(" ");
        }
        pi?.Write(sb.ToString());

        sb.Clear();
        if (duplicates.Count > 0)
        {
          sb.AppendLine("#warn#W tabeli S_APPINI znajdują się powielone wpisy. Błąd dotyczy następujących sekcji:");
          foreach (var section in duplicates)
          {
            sb.AppendLine($"#warn#{section}");
          }
        }
        pi?.Write(sb.ToString());
        pi?.Write("#warn#Należy poprawić te wpisy w bazie danych.");

        if (invalid.Count > 0)
        {
          throw new Exception("W tabeli S_APPINI znajdują się nieprawidłowe wpisy. Należy poprawić te wpisy przed ich eksportem.");
        }
      }
    }

    /// <summary>
    /// Sortuje procedury transferowe według numerów w nazwie.
    /// </summary>
    internal static IEnumerable<Metadata> SortTransferProcedures(IEnumerable<TransferProcedure> list)
    {
      var result = list.ToList();
      result.Sort();
      return result;
    }
  }

  static class TrModeExtensions
  {
    public static bool NoTr(this MetadataExtractor.TrMode e)
    {
      return e != MetadataExtractor.TrMode.TrAfter && e != MetadataExtractor.TrMode.TrBefore;
    }
  }
}
