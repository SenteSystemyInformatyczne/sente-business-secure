﻿using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DataExtraction
{
  public class ScriptsResult
  {
    public string TrBefore
    {
      get;
      set;
    }
    public string Metadata
    {
      get;
      set;
    }
    public string TrAfter
    {
      get;
      set;
    }
    public string AppIni
    {
      get;
      set;
    }
  }

  public static class Scripts
  {
    /// <summary>
    /// Wykonaj skrypt
    /// </summary>
    /// <param name="srcpath"></param>
    /// <param name="appinsert"></param>
    /// <param name="pi"></param>
    /// <param name="breakOnError"></param>
    public static void ExecuteScript(string srcpath, bool appinsert, FilesystemExtractor me, ProgressInfo pi, bool breakOnError = false)
    {
      srcpath = srcpath + Path.DirectorySeparatorChar;
      StringBuilder script = new StringBuilder();
      List<Metadata> list;
      List<string> eList;
      list = me.ParseFiles("", srcpath, out eList, pi);

      if (eList.Count > 0)
      {
        var msg = "Poniższe pliki zawierają błędy. WYGENEROWANE SKRYPTY MOGĄ NIE WGRYWAC SIĘ PRAWIDŁOWO!!:\n";
        foreach (string s in eList)
          msg += s + "\n";

        if (!breakOnError)
        {
          me.AppendMessage(msg);
        }
        else
        {
          throw new Exception("Poniższe pliki zawierają błędy. WYGENEROWANE SKRYPTY MOGĄ NIE WGRYWAC SIĘ PRAWIDŁOWO!!:\n");
        }
      }
    }

    public const string GET_TABLE_DEPENDENCIES = @"
select d.rdb$dependent_name as ""NAME"",
case(d.rdb$dependent_type)
when 0 then 'TABLE'
when 1 then 'VIEW'
when 2 then 'TRIGGER'
when 3 then 'COMPUTED COLUMN'
when 4 then 'CHECK'
when 5 then 'PROCEDURE'
when 6 then 'INDEX EXPRESSION'
when 7 then 'EXCEPTION'
when 8 then 'USER'
when 9 then 'COLUMN'
when 10 then 'INDEX'
else 'UNKNOWN'
end as ""TYPE""
from rdb$dependencies d
where d.rdb$depended_on_name = '{0}'
union
select rc.rdb$constraint_name, rc.rdb$constraint_type
from rdb$relation_constraints rc
join rdb$index_segments i
on i.rdb$index_name = rc.rdb$index_name
where rc.rdb$relation_name = '{0]'
";

    /// <summary>
    /// Działa ale aktualnie driver się sypie
    /// </summary>
    /// <param name="x"></param>
    //private static void ExecuteScript(string x)
    //{
    //  Database db = new Database(Connection.GetConnection(SourceMode.Remote));
    //  db.ExecuteScript(x);
    //}
  }
}
