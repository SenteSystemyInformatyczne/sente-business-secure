﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class ForeignKey : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.ForeignKey;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_CONSTRAINT_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "foreignkeys";
      }
    }

    #endregion
    #region Constructors
    public ForeignKey(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    public ForeignKey()
    {
    }
    public ForeignKey(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public ForeignKey(string path)
    {
      ParseDDL(path);
    }
    #endregion
  }
}
