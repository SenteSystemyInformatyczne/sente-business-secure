﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHlib.DataExtraction.Model
{
  /// <summary>
  /// Ogólna implementacja obiektu bazodanowego
  /// </summary>
  public class CustomMetadata : Metadata
  {
    /// <summary>
    /// Taki tam nasz wartownik na początek
    /// </summary>
    public const string SKEY = "--@@@***SKEY***@@@--";
    /// <summary>
    /// Drugi wartownik, po nim komentarz
    /// </summary>
    public const string SCOMMENT = "--@@@***SCOMMENT***@@@--";

    #region Properties and Fields

    /// <summary>
    /// Typ obiektu
    /// </summary>
    public override DbTypes DbType
    {
      get
      {
        return SHDataContracts.Enums.DbTypes.None;
      }
    }

    /// <summary>
    /// Flaga określająca czy obiekt różni się od już istniejącego w bazie
    /// </summary>
    public override bool Changed
    {
      get
      {
        return _changed;
      }

      set
      {
        _changed = value;
      }
    }

    /// <summary>
    /// Procedura która musi być na bazie aby wyłuskać dane o obiekcie
    /// </summary>
    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "";
      }
    }

    /// <summary>
    /// Nazwa obiektu
    /// </summary>
    public override string Name
    {
      get
      {
        return _name;
      }
      set
      {
        this._name = value;
      }
    }

    /// <summary>
    /// Opis obiektu
    /// </summary>
    public override string Description
    {
      get
      {
        return _description;
      }
      set
      {
        this._description = value;
      }
    }

    /// <summary>
    /// DDL obiektu, leniwie inicjalizowany
    /// </summary>
    public override string DDL
    {
      get
      {
        if (GetConnection != null && GetConnection(false).Type != ConnectionType.File && String.IsNullOrEmpty(_DDL))
          _DDL = GetDDL();
        return _DDL;
      }
      set
      {
        this._DDL = value;
      }
    }

    /// <summary>
    /// Komentarz obiektu, leniwie inicjalizowany
    /// </summary>
    public override string Comment
    {
      get
      {
        if (GetConnection(false).Type != ConnectionType.File && String.IsNullOrEmpty(_comment))
          _comment = GetComment();
        return _comment;
      }
      set
      {
        this._comment = value;
      }

    }
    /// <summary>
    /// Czy filtrować granty dla standardowych użytkowników
    /// </summary>
    public override bool GrantFilter
    {
      get
      {
        return _grantfilter;
      }
      set
      {
        _grantfilter = value;
      }
    }

    /// <summary>
    /// Granty obiektu, leniwie inicjalizowany
    /// </summary>
    public override string Grants
    {
      get
      {
        if (GetConnection(false).Type != ConnectionType.File && String.IsNullOrEmpty(_grants))
          _grants = GetGrants(GrantFilter);
        return _grants;
      }
    }

    /// <summary>
    /// Do jakiego podkatalogu eksportujemy dane
    /// </summary>
    public override string DirName
    {
      get
      {
        return "metadata";
      }
    }

    /// <summary>
    /// Pełna ścieżka pliku do pliku z treścią obiektu
    /// </summary>
    public override string FilePath
    {
      get
      {
        return DirName + Path.DirectorySeparatorChar + Name + ".sql";
      }
    }

    /// <summary>
    /// Taki tam klucz obiektu po naszemu
    /// </summary>
    public override string SKey
    {
      get
      {
        return SKEY + "\r\n";
      }
    }

    /// <summary>
    /// Komentarz obiektu po naszemu
    /// </summary>
    public override string Scomment
    {
      get
      {
        return CustomMetadata.SCOMMENT + "\r\n" + Comment + "\r\n";
      }
    }
    #endregion

    public CustomMetadata()
    {
    }

    /// <summary>
    /// Konsturktor, używać gdy pobieramy obiekt z bazy
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Description"></param>
    public CustomMetadata(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }

    /// <summary>
    /// Konstruktor, używać gdy pobieramy z pliku
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Description"></param>
    /// <param name="DDL"></param>
    public CustomMetadata(string Name, string Description, string DDL)
      : this(Name, Description)
    {
      if (!String.IsNullOrEmpty(DDL))
        ParseDDL(DDL);
    }

    /// <summary>
    /// Escapuje [tekst] typowy statement ddl np COMMENT ON asdadada '[tekst]'
    /// </summary>
    /// <returns></returns>
    public static string EscapeDDLStatementString(string ddl)
    {
      int firstapostrophe = ddl.IndexOf("'");
      int lastapostrophe = ddl.LastIndexOf("';");
      if (firstapostrophe > -1 && lastapostrophe > -1 && firstapostrophe < lastapostrophe)
      {
        var prefix = ddl.Substring(0, firstapostrophe + 1);
        var body = ddl.Substring(firstapostrophe + 1, lastapostrophe - firstapostrophe - 1);
        var suffix = ddl.Substring(lastapostrophe);

        ddl = prefix + body.Replace("'", "''") + suffix;
      }
      return ddl;
    }

    /// <summary>
    /// Procedura umożliwia nadpisanie standardowych parametrów wykonania funkcji ekstaktujących
    /// </summary>
    /// <param name="inputParams"></param>
    protected virtual void SetDDLProcInputParams(Dictionary<string, object> inputParams)
    {
      inputParams.Add("NAZWA", Name);
      inputParams.Add("CREATE_OR_ALTER", "0");
    }

    /// <summary>
    /// Procedura wyciąga DDL z bazy danych
    /// </summary>
    /// <returns>DDL</returns>
    protected override string GetDDL()
    {
      string ddl = "";
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        Dictionary<string, object> inputParams = new Dictionary<string, object>();
        SetDDLProcInputParams(inputParams);

        List<string> outputParams = new List<string>();
        outputParams.Add("DDL");
        try
        {
          DataTable t = db.ExecuteProcedure(EXTRACTPROCEDURE, inputParams, outputParams);
          if (t != null)
            for (int i = 0; i < t.Rows.Count; i++)
            {
              ddl = t.Rows[i]["DDL"].ToString() + "\r\n";
            }
          return ddl;
        }
        catch (Exception ex)
        {
          string sql = EXTRACTPROCEDURE + "(";
          bool first = true;
          foreach (var i in inputParams)
          {
            sql += (first ? "" : ",") + i.Value;
            first = false;
          }

          throw new Exception("Błąd wykonania procedury " + sql + (ex.Message != null ? "\r\n" + ex.Message : ""), ex);
        }
      }
    }

    /// <summary>
    /// Procedura wyciąga komentarze z DB
    /// </summary>
    /// <returns>Komentarz</returns>
    protected override string GetComment()
    {
      string comment = "";
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        Dictionary<string, object> inputParams = new Dictionary<string, object>();
        inputParams = new Dictionary<string, object>();
        inputParams.Add("OBJECTNAME", Name);
        inputParams.Add("OBJECTTYPE", DbType.ToString("D"));
        List<string> outputParams = new List<string>();
        outputParams = new List<string>();
        outputParams.Add("RESULT");
        try
        {
          DataTable t = db.ExecuteProcedure("SYS_GETCOMMENT", inputParams, outputParams);
          if (t != null)
            for (int i = 0; i < t.Rows.Count; i++)
            {
              comment = t.Rows[i]["RESULT"].ToString() + "\r\n";
            }
          comment = EscapeComment(comment.Trim());
          return comment;
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    /// <summary>
    /// Procedura wyciąga granty z DB
    /// </summary>
    /// <returns>Komentarz</returns>
    protected override string GetGrants(bool grantFilter)
    {
      string grants = "";
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        Dictionary<string, object> inputParams = new Dictionary<string, object>();
        inputParams = new Dictionary<string, object>();
        inputParams.Add("NAME", Name);
        inputParams.Add("TYP", DbType.ToString("D"));
        inputParams.Add("GRANDSFILTER", grantFilter ? "1" : "0");
        List<string> outputParams = new List<string>();
        outputParams = new List<string>();
        outputParams.Add("DDL");
        try
        {
          DataTable t = db.ExecuteProcedure("SYS_GRANT_DDL", inputParams, outputParams);
          if (t != null)
            for (int i = 0; i < t.Rows.Count; i++)
            {
              grants = t.Rows[i]["DDL"].ToString() + "\r\n";
            }
          grants = EscapeComment(grants);
          return grants;
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    private string EscapeComment(string comment)
    {
      return EscapeDDLStatementString(comment);
    }

    /// <summary>
    /// Zwraca pełen tekst DDL, tak jak jest eksportowany do pliku i skryptu
    /// </summary>
    /// <returns></returns>
    public override string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false)
    {
      GrantFilter = grantFilter;
      var content = SKey + DDL + Scomment + Grants;
      if (generateScript)
        content = FixAtSymbol(content, removeSymbols);
      return content;
    }

    public string FixAtSymbol(string content, bool removeSymbols)
    {
      return content.Replace(SKEY, removeSymbols ? "" : "--").Replace(SCOMMENT, removeSymbols ? "" : "--");
    }

    /// <summary>
    /// Wczytuje obiekt z ciagu tekstowego DDLki
    /// </summary>
    /// <param name="DDL">DDL</param>
    protected override void ParseDDL(string DDL)
    {
      bool commentline = false;
      var split = Regex.Split(DDL, "(-{2}@{3}[*]{3}S[^*]*[*]{3}@{3}-{2}.*\\s?)", RegexOptions.Compiled);
      foreach (string s in split.Where(tt => tt.Length > 0))
      {
        if (s.Contains(SKEY))
        {
          this.Description = s.Replace(SKEY, "").Trim();
        }
        else if (s.Contains(SCOMMENT))
        {
          commentline = true;
        }
        else
        {
          if (commentline == true)
          {
            this.Comment = s;
          }
          else
          {
            this.DDL = s;
          }
        }
      }
    }

    /// <summary>
    /// Metoda validuje skrypt DDL obiektu bazodanowego i zwraca czy jest poprawny.
    /// Wykorzystywane głównie dla walidacji widoków w BD przed podnoszczeniem klienta.
    /// </summary>
    /// <returns>true - jest OK. false - validacja nie przeszła.</returns>
    public override bool ValidateDDL()
    {
      return true;
    }

  }
}
