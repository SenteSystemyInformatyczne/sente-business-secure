﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHlib.DataExtraction.Model
{
  class Field : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return SHDataContracts.Enums.DbTypes.Field;
      }
    }
    /// <summary>
    /// Zwraca w liscie nazwy indeksow dla danego pola
    /// </summary>
    /// <returns></returns>
    public IList<string> GetIndexes()
    {
      List<string> indexNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        var separatorPosition = Name.Trim().IndexOf('.');
        var nameLength = Name.Trim().Length - 1;
        var tableName = Name.Trim().Substring(0, separatorPosition);
        var fieldName = Name.Trim().Substring(separatorPosition + 1, nameLength - separatorPosition);
        string sql = SQLRepository.Instance.GetSql(SQL.Field_GetIndexes,
  @"select seg.RDB$INDEX_NAME as NAME 
  from rdb$index_segments seg 
  left join rdb$indices i on seg.rdb$index_name = i.rdb$index_name 
  where trim(i.rdb$relation_name) = '{0}' 
    and trim(seg.rdb$field_name) = '{1}'", tableName, fieldName);
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          indexNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return indexNames;
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_FIELD_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "fields";
      }
    }

    public override string FilePath
    {
      get
      {
        return DirName + Path.DirectorySeparatorChar + Name + ".sql";
      }
    }

    #endregion
    #region Constructors
    public Field(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion

    /// <summary>
    /// Zwraca pełen tekst DDL, tak jak jest eksportowany do pliku i skryptu.
    /// Gdy wykryto modyfikację w szybkim procesie zwróci modyfikujący DDL zamiast adytywnego
    /// </summary>
    /// <returns></returns>
    public override string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false)
    {
      GrantFilter = grantFilter;
      var content = SKey + (Changed ? GenerateModificationScript() : DDL) + Scomment + Grants;
      if (generateScript)
        content = FixAtSymbol(content, removeSymbols);
      return content;
    }

    /// <summary>
    /// Generator zapytania zmieniającego treść ze skłądni adytywnej na modyfikującą
    /// </summary>
    /// <returns></returns>
    private string GenerateModificationScript()
    {
      string[] paramNames = { "ALTER TABLE", "ADD", "CHARACTER SET", "COLLATE", "DEFAULT" };
      Dictionary<string, string> paramDict = paramNames.ToDictionary(k => k, v => ExtractParameter(v));

      string newDDL = "";
      foreach (var k in paramDict.Keys)
      {
        if (!string.IsNullOrWhiteSpace(paramDict[k]))
          if (k != "ADD")
            newDDL += k + " " + paramDict[k] + " ";
          else
          {
            string[] tmp = paramDict[k].Split(' ');
            newDDL += "ALTER COLUMN " + tmp[0] + " TYPE ";
            foreach (var s in tmp.Skip(1))
              newDDL += s + " ";
          }
      }
      return newDDL + ";\n";
    }

    /// <summary>
    /// Metoda wyszukująca wartości parametrów z DDL
    /// </summary>
    /// <param name="paramName"></param>
    /// <returns></returns>
    private string ExtractParameter(string paramName)
    {
      string value = "";
      string pattern = ".*" + paramName + @" ([""\w\d_]+)" + (paramName == "ADD" ? @"[ ]{1,}(BLOB [\w\d]+ [\d]|[\w\d]+)" : "") + ".*";
      var match = Regex.Match(DDL, pattern);
      if (match.Success)
      {
        value = match.Groups[1].Value + (paramName == "ADD" ? " " + match.Groups[2].Value : "");
      }
      return value;
    }
  }
}
