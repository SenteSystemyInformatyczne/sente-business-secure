﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class UniqueKey : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.UniqueKey;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_CONSTRAINT_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "uniquekeys";
      }
    }

    #endregion
    #region Constructors
    public UniqueKey(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    public UniqueKey()
    {
    }
    public UniqueKey(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public UniqueKey(string path)
    {
      ParseDDL(path);
    }
    #endregion
  }
}
