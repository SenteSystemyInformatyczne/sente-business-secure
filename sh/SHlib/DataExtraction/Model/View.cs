﻿using SHDataContracts.Enums;
using SHlib.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SHlib.DataExtraction.Model
{
  class View : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.View;
      }
    }

    /// <summary>
    /// Przeciążenie obchodzi problem, że są wygenerowane już widoki z CREATE VIEW i aktualizacja się sypie
    /// </summary>
    public override string DDL
    {
      get
      {
        var ddl = base.DDL;
        if (ddl.Contains("CREATE VIEW"))
          ddl = ddl.Replace("CREATE VIEW", "CREATE OR ALTER VIEW");
        return ddl;
      }

      set
      {
        base.DDL = value;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_TABLE_OR_VIEW_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "views";
      }
    }

    /// <summary>
    /// Dla widoków emituj CREATE OR ALTER
    /// </summary>
    /// <param name="inputParams"></param>
    protected override void SetDDLProcInputParams(Dictionary<string, object> inputParams)
    {
      base.SetDDLProcInputParams(inputParams);
      inputParams["CREATE_OR_ALTER"] = "1";
    }
    #endregion
    #region Constructors
    public View(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion

    #region Validate script DDL
    public override bool ValidateDDL()
    {
      var script = DDL.ToLower();

      // Wycinam wszystkie białe znaki
      script = new string(script.Where(c => !char.IsWhiteSpace(c)).ToArray());

      // Wycinam wszystkie teksty
      script = Tools.RemoveTextBetween(script, "'", "'");

      // Jeśli jest * w widoku to jest błąd
      return !script.Contains(".*") && !script.Contains("select*from");
    }
    #endregion
  }
}
