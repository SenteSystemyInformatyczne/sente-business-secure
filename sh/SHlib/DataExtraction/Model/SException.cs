﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class SException : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Exception;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_EXCEPTION_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "exceptions";
      }
    }

    #endregion
    #region Constructors
    public SException(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }

    /// <summary>
    /// dług technologiczny obsługa escapowania na poziomie kodu a nie procedury która źle robi
    /// </summary>
    /// <returns></returns>
    protected override string GetDDL()
    {
      return EscapeDDLStatementString(base.GetDDL());
    }
    #endregion
  }
}
