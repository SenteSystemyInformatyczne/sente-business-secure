﻿using SHDataContracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DataExtraction.Model
{
  class Role : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Role;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_ROLE_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "roles";
      }
    }

    #endregion

    #region Constructors
    public Role(string Name, string Description, string DDL) : base(Name, Description, DDL)
    {
    }

    public Role() {

    }

    public Role(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public Role(string path)
    {
      ParseDDL(path);
    }
    #endregion

    protected override string GetComment()
    {
      //role nie mają komentarzy, są wybierane na podstawie ...
      return "";
    }

  }
}

