﻿using SHDataContracts.Enums;
using System.IO;

namespace SHlib.DataExtraction.Model
{
  class ReplicationRule : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.ReplicationRule;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_REPLICAT_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "replication";
      }
    }

    public override string FilePath
    {
      get
      {
        return DirName + Path.DirectorySeparatorChar + Name + ".sql";
      }
    }

    #endregion
    #region Constructors
    public ReplicationRule(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    public ReplicationRule()
    {
    }
    public ReplicationRule(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public ReplicationRule(string path)
    {
      ParseDDL(path);
    }
    #endregion

    public override string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false)
    {
      return SKey + DDL;
    }
  }

}
