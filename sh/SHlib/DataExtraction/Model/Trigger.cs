﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;

namespace SHlib.DataExtraction.Model
{
  class Trigger : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return SHDataContracts.Enums.DbTypes.Trigger;
      }
    }

    /// <summary>
    /// Zwraca liste tupli z nazwa tabeli, nazwa pola, ktorych dotyczy trigger
    /// </summary>
    /// <returns></returns>
    public IList<Tuple<string, string>> GetFields()
    {
      List<Tuple<string, string>> fieldNames = new List<Tuple<string, string>>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        string sql = SQLRepository.Instance.GetSql(SQL.Trigger_GetFields,
  @"select distinct dep.rdb$depended_on_name as TABNAME, dep.rdb$field_name as NAME
  from rdb$dependencies dep
  where trim(dep.rdb$dependent_name) = '" + Name + @"' and
    (dep.rdb$depended_on_type = 0 or dep.rdb$depended_on_type = 1) and
    dep.rdb$field_name is distinct from null and
    dep.rdb$field_name not starting with 'RDB$' and
    dep.rdb$field_name not starting with 'IBE$' and
    dep.rdb$field_name not containing 'SYS_TRHIST' and
    dep.rdb$field_name not containing 'SYS_HASHCODES' and
    dep.rdb$field_name not containing 'SYS_TR_TEMP' and
    not exists (
      select FIRST 1 seg.*
      from rdb$index_segments seg
      where seg.rdb$index_name = 'PK_'||dep.rdb$depended_on_name and
        dep.rdb$field_name = seg.rdb$field_name)");
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          var tableName = t.Rows[i]["TABNAME"].ToString();
          var fieldName = t.Rows[i]["NAME"].ToString();
          fieldNames.Add(new Tuple<string, string>(tableName, fieldName));
        }
        return fieldNames;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_TRIGGER_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "triggers";
      }
    }

    #endregion
    #region Constructors
    public Trigger(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {

    }
    #endregion
  }
}
