﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace SHlib.DataExtraction.Model
{
  class PrimaryKey : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return SHDataContracts.Enums.DbTypes.PrimaryKey;
      }
    }
    /// <summary>
    /// Pobiera nazwę generatora związanego z polem klucza głównego
    /// </summary>
    /// <returns></returns>
    public IList<string> GetGenerator()
    {
      List<string> generatorNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        var separatorPosition = Name.Trim().IndexOf('.');
        var tableName = Name.Trim().Substring(0, separatorPosition);
        string sql = SQLRepository.Instance.GetSql(SQL.PrimaryKey_GetGenerator,
          @"select g.rdb$generator_name as NAME 
            from rdb$generators g 
            where g.rdb$generator_name = 'GEN_{0}'
              and RDB$SYSTEM_FLAG = 0
              and RDB$GENERATOR_NAME not starting with 'IBE$'
              and RDB$GENERATOR_NAME not starting with 'RDB$'
              and RDB$GENERATOR_NAME not containing 'SYS_TRHIST'
              and RDB$GENERATOR_NAME not containing 'SYS_HASHCODES'
              and RDB$GENERATOR_NAME not containing 'SYS_TR_TEMP'", tableName);
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          generatorNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return generatorNames;
    }
    /// <summary>
    /// Zwraca w liście nazwy indeksów z pól klucza głównego, poza indeksem klucza głównego ("PK_*")
    /// </summary>
    /// <returns></returns>
    public IList<string> GetIndexes()
    {
      List<string> indexesNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        var separatorPosition = Name.Trim().IndexOf('.');
        var tableName = Name.Trim().Substring(0, separatorPosition);
        string sql = SQLRepository.Instance.GetSql(SQL.PrimaryKey_GetIndexes,
  @"select seg.rdb$index_name as NAME from rdb$index_segments seg
  left join rdb$indices i on seg.rdb$index_name = i.rdb$index_name
  where i.rdb$relation_name = '" + tableName + @"' and
    i.rdb$foreign_key is null and
    seg.rdb$index_name not containing 'RDB$' and
    seg.rdb$index_name not containing 'MON$' and
    seg.rdb$index_name not containing 'IBE$' and
    i.rdb$relation_name not containing 'SYS_TRHIST' and
    i.rdb$relation_name not containing 'SYS_HASHCODES' and
    i.rdb$relation_name not containing 'SYS_TR_TEMP' and
    seg.rdb$index_name is distinct from 'PK_{0}' and
    seg.rdb$field_name in (
        select rdb$field_name from rdb$index_segments
        where rdb$index_name = 'PK_{0}')", tableName);
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          indexesNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return indexesNames;
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_CONSTRAINT_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "primarykeys";
      }
    }

    /// <summary>
    /// Pełna ścieżka pliku do pliku z treścią obiektu
    /// </summary>
    public override string FilePath
    {
      get
      {
        return DirName + Path.DirectorySeparatorChar + (Name.Contains("RDB$") ? FixFileName(DDL) : Name) + ".sql";
      }
    }

    #endregion
    #region Constructors
    public PrimaryKey(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }

    public PrimaryKey(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }

    public PrimaryKey(string path)
    {
      ParseDDL(path);
    }
    #endregion
    #region Methods
    private string FixFileName(string DDL)
    {
      var match = new Regex(@"TABLE(.+)ADD CONSTRAINT(.+)PRIMARY").Match(DDL);
      var tableName = match.Groups[1].Value.Trim();
      var pkName = match.Groups[2].Value.Trim();
      return tableName + "." + pkName;
    }
    #endregion
  }
}
