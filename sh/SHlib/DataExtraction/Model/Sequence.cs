﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class Sequence : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Sequence;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_SEQUENCE_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "sequences";
      }
    }

    #endregion
    #region Constructors
    public Sequence(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
      this.Name = Name;
      this.Description = Description;
    }
    #endregion
  }
}
