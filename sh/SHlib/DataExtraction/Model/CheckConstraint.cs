﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class CheckConstraint : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Check;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_CHECK_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "checks";
      }
    }

    #endregion
    #region Constructors
    public CheckConstraint(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    public CheckConstraint()
    {
    }
    public CheckConstraint(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public CheckConstraint(string path)
    {
      ParseDDL(path);
    }
    #endregion

    protected override string GetComment()
    {
      //checki nie mają komentarzy, są wybierane na podstawie descriptionów tabeli/pola
      return "";
    }
  }
}
