﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class Domain : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Domain;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_DOMAIN_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "domains";
      }
    }

    #endregion
    #region Constructors
    public Domain(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion
  }
}
