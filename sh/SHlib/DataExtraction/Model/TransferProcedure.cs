﻿using SHDataContracts.Enums;
using System;
using System.Text.RegularExpressions;

namespace SHlib.DataExtraction.Model
{
  class TransferProcedure : CustomMetadata, IComparable<TransferProcedure>
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.TransferProcedure;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_PROCEDURE_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "tr";
      }
    }

    #endregion
    #region Constructors
    public TransferProcedure(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion

    public string CommentedDDL
    {
      get
      {
        return GenCommentedDDL(DDL);
      }
    }

    /// <summary>
    /// Metoda zwracjąca zakomentowaną treść procedury
    /// </summary>
    /// <param name="DDL"></param>
    /// <returns></returns>
    private string GenCommentedDDL(string DDL)
    {
      const string BEGIN = "BEGIN";
      const string END = "END";

      var upperDDL = DDL.ToUpper();
      var firstBegin = upperDDL.IndexOf(BEGIN);
      var lastEnd = upperDDL.LastIndexOf(END);
      var newDDL = string.Empty;
      if (firstBegin > -1 && lastEnd > -1 && firstBegin < lastEnd)
      {

        var prefix = DDL.Substring(0, firstBegin + BEGIN.Length);
        var body = DDL.Substring(firstBegin + BEGIN.Length, lastEnd - firstBegin - BEGIN.Length);
        var suffix = DDL.Substring(lastEnd);

        newDDL = prefix + "/*" + body.Replace("*/", "*\\/") + "*/" + suffix;
      }
      return newDDL;
    }

    public string DDLWithLogging
    {
      get
      {
        return GenDDLWithLogging(DDL);
      }
    }

    /// <summary>
    /// Metoda wstrzykująca SQL aktualizacji historii wykonania procedur do treści procedur transferowych.
    /// </summary>
    /// <param name="DDL"></param>
    /// <returns></returns>
    private string GenDDLWithLogging(string DDL)
    {
        var upperDDL = DDL.ToUpper();
        var firstBegin = Regex.Match(upperDDL, @"(?<=\s)BEGIN(?=\s)"); //Matchuje pierwsze wystąpienie BEGIN upewniając się, że przed i po są białe znaki
        var lastEnd = Regex.Match(upperDDL, @"(?<=\s)END(?=[\s^;])", RegexOptions.RightToLeft); //Matchuje ostatnie wystąpienie END. Przed END musi wystąpić biały znak, a po ^ lub ; lub biały znak
        var firstWhen = Regex.Match(upperDDL, @"([ \t]*)WHEN(.*?)DO(.*)"); //Matchuje pierwsze wystąpienie WHEN ... DO.
        var newDDL = string.Empty;
        
        var prefix = DDL.Substring(0, firstBegin.Index + firstBegin.Length);
        var check = "\nIF ((SELECT STAN FROM SYS_TRHIST WHERE NAZWA = '" + this.Name + "') = 1) THEN EXIT;";
        var insertLog = "\nUPDATE OR INSERT INTO SYS_TRHIST (TEMAT, NAZWA, STAN, TIMEST) VALUES ('" + new Regex(@"(PR|BS|ZG)[0-9]+").Match(this.Name).Value + "','" + this.Name + "',0,CURRENT_TIMESTAMP) MATCHING (NAZWA);";
        var body = string.Empty;
        var updateLog = "UPDATE SYS_TRHIST SET STAN = 1 WHERE NAZWA = '" + this.Name + "';\n";
        var suffix = string.Empty;
        
        if (firstBegin.Success && firstWhen.Success)
        {
            body = DDL.Substring(firstBegin.Index + firstBegin.Length, firstWhen.Index - firstBegin.Index - firstBegin.Length);
            suffix = DDL.Substring(firstWhen.Index);
            newDDL = prefix + check + insertLog + body + updateLog + suffix;
        }
        else if (firstBegin.Success && lastEnd.Success && firstBegin.Index < lastEnd.Index)
        {
            body = DDL.Substring(firstBegin.Index + firstBegin.Length, lastEnd.Index - firstBegin.Index - firstBegin.Length);
            suffix = DDL.Substring(lastEnd.Index);
            newDDL = prefix + check + insertLog + body + updateLog + suffix;
        }
        
        return newDDL;
    }

    public override string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false)
    {
      string content = string.Empty;
      GrantFilter = grantFilter;
      if (generateScript)
        content = SKey + DDLWithLogging + SCOMMENT +
  @"
execute procedure " + Name + @";
commit work;
drop procedure " + Name + @";";
      else
        content = SKey + DDL + SCOMMENT;

      if (generateScript)
        content = FixAtSymbol(content, removeSymbols);
      return content;
    }

    private static readonly Regex nameParser = new Regex(@"(PR|BS|ZG)(?<number>\d+)(_(?<subnumber>\d+)|_(?<postfix>\w+))?");

    public int CompareTo(TransferProcedure that)
    {
      var thisMatch = nameParser.Match(this.Name);
      var thatMatch = nameParser.Match(that.Name);
      if (thisMatch.Success && thatMatch.Success)
      {
        int thisNumber = int.Parse(thisMatch.Groups["number"].Value);
        int thatNumber = int.Parse(thatMatch.Groups["number"].Value);
        if (thisNumber > thatNumber)
        {
          return 1;
        }
        else if (thisNumber < thatNumber)
        {
          return -1;
        }
        else if (thisNumber == thatNumber && thisMatch.Groups["subnumber"].Success && thatMatch.Groups["subnumber"].Success)
        {
          int thisSubnumber = int.Parse(thisMatch.Groups["subnumber"].Value);
          int thatSubnumber = int.Parse(thatMatch.Groups["subnumber"].Value);
          return thisSubnumber.CompareTo(thatSubnumber);
        }
      }

      return this.Name.CompareTo(that.Name);
    }
  }
}
