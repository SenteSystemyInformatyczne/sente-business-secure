﻿namespace SHlib.DataExtraction.Model
{
  class XkProcedure : Procedure
  {
    public XkProcedure(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }

    public override string DirName
    {
      get
      {
        return "xk_procedures";
      }
    }
  }
}
