﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class Index : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Index;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_INDEX_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "indices";
      }
    }

    #endregion
    #region Constructors
    public Index(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    public Index()
    {
    }
    public Index(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public Index(string path)
    {
      ParseDDL(path);
    }
    #endregion
  }



}
