﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;

namespace SHlib.DataExtraction.Model
{
  class Table : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return SHDataContracts.Enums.DbTypes.Table;
      }
    }
    /// <summary>
    /// Zwraca w liście, nazwy pól należące do tabeli
    /// </summary>
    /// <returns></returns>
    public IList<string> GetFields()
    {
      List<string> fieldNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        string sql = SQLRepository.Instance.GetSql(SQL.Table_GetFields,
  @"select skip (iif(exists(select first 1 1
                        from rdb$relation_constraints rc
                        join rdb$index_segments seg on (seg.rdb$index_name = rc.rdb$index_name)
                      where rc.rdb$relation_name = '{0}'
                              and rc.rdb$constraint_type = 'PRIMARY KEY'),0,1))
        rf.rdb$field_name AS NAME 
      from rdb$relation_fields rf
    where rf.rdb$relation_name = '{0}' and
      not exists (select first 1 1
                    from rdb$relation_constraints rc
                    join rdb$index_segments seg on (seg.rdb$index_name = rc.rdb$index_name)
                  where rc.rdb$relation_name = rf.rdb$relation_name
                          and rc.rdb$constraint_type = 'PRIMARY KEY'
                          and seg.rdb$field_name = rf.rdb$field_name)", Name.Trim());
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          fieldNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return fieldNames;
    }
    /// <summary>
    /// Zwraca w liście, nazwy indeksów należących do tabeli
    /// </summary>
    /// <returns></returns>
    public IList<string> GetIndexes()
    {
      List<string> indexNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        string sql = SQLRepository.Instance.GetSql(SQL.Table_GetIndexes,
          "select i.rdb$index_name AS NAME from rdb$indices i where i.rdb$relation_name = '{0}'", Name);
        ;
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          indexNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return indexNames;
    }
    /// <summary>
    /// Zwraca w liście nazwę generatora tabeli
    /// </summary>
    /// <returns></returns>
    public IList<string> GetSequences()
    {
      List<string> sequenceNames = new List<string>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        string sql = SQLRepository.Instance.GetSql(SQL.Table_GetSequences,
  @"select g.rdb$generator_name AS NAME 
    from rdb$generators g 
    where trim(g.rdb$generator_name) similar to upper('GEN_{0}')
      and RDB$SYSTEM_FLAG = 0
      and RDB$GENERATOR_NAME not starting with 'IBE$'
      and RDB$GENERATOR_NAME not starting with 'RDB$'
      and RDB$GENERATOR_NAME not containing 'SYS_TRHIST'
      and RDB$GENERATOR_NAME not containing 'SYS_HASHCODES'
      and RDB$GENERATOR_NAME not containing 'SYS_TR_TEMP'", Name.Trim());
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          sequenceNames.Add(t.Rows[i]["NAME"].ToString());
        }
      }
      return sequenceNames;
    }

    public IList<Tuple<string, SHDataContracts.Enums.DbTypes>> GetDependencies()
    {
      List<Tuple<string, SHDataContracts.Enums.DbTypes>> dependencies = new List<Tuple<string, SHDataContracts.Enums.DbTypes>>();
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {

        string sql = string.Format(Scripts.GET_TABLE_DEPENDENCIES, Name);
        DataTable t = db.Select(sql);
        for (int i = 0; i < t.Rows.Count; i++)
        {
          string dname = t.Rows[i]["NAME"].ToString();
          SHDataContracts.Enums.DbTypes type;
          switch (dname)
          {
            case "TABLE":
              type = SHDataContracts.Enums.DbTypes.Table;
              break;
            case "VIEW":
              type = SHDataContracts.Enums.DbTypes.View;
              break;
            case "TRIGGER":
              type = SHDataContracts.Enums.DbTypes.Trigger;
              break;
            case "PROCEDURE":
              type = SHDataContracts.Enums.DbTypes.Procedure;
              break;
            case "EXCEPTION":
              type = SHDataContracts.Enums.DbTypes.Exception;
              break;
            case "COLUMN":
              type = SHDataContracts.Enums.DbTypes.Field;
              break;
            case "INDEX":
              type = SHDataContracts.Enums.DbTypes.Index;
              break;
            case "PRIMARY KEY":
              type = SHDataContracts.Enums.DbTypes.PrimaryKey;
              break;
            case "FOREIGN KEY":
              type = SHDataContracts.Enums.DbTypes.ForeignKey;
              break;
            case "UNIQUE":
              type = SHDataContracts.Enums.DbTypes.UniqueKey;
              break;
            default:
              type = SHDataContracts.Enums.DbTypes.None;
              break;
          }
          dependencies.Add(new Tuple<string, SHDataContracts.Enums.DbTypes>(dname, type));
        }
      }
      return dependencies;
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_TABLE_OR_VIEW_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "tables";
      }
    }

    #endregion
    #region Constructors
    public Table(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion
  }
}
