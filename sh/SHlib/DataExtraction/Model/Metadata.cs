﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using System;

namespace SHlib.DataExtraction.Model
{
  /// <summary>
  /// Abstrakcyjna klasa reprezentująca dane o obiektach bazodanowych
  /// </summary>
  public abstract class Metadata
  {
    public Func<bool, IDbSettings> GetConnection;

    protected string _name = "";
    protected string _ddlname = "";
    protected string _description = "";
    protected string _DDL = "";
    protected string _comment = "";
    protected string _grants = "";
    protected string _scomment = "";
    protected bool _changed = false;
    protected bool _grantfilter = false;

    public abstract bool GrantFilter
    {
      get;
      set;
    }
    public abstract bool Changed
    {
      get;
      set;
    }
    public abstract DbTypes DbType
    {
      get;
    }
    public abstract string EXTRACTPROCEDURE
    {
      get;
    }
    public abstract string Name
    {
      get;
      set;
    }
    public abstract string Description
    {
      get;
      set;
    }

    public abstract string DDL
    {
      get;
      set;
    }

    /// <summary>
    /// Obiekt aktualnego stanu metadanych w bazi danych.
    /// Wykorzystywany tylko przy aktualizacji.
    /// </summary>
    public Metadata CurrentDbMetadata
    {
      get;
      set;
    }

    public uint? CRC
    {
      get;
      set;
    }

    public int? ID
    {
      get;
      set;
    }

    public abstract string Comment
    {
      get;
      set;
    }
    public abstract string Grants
    {
      get;
    }
    public abstract string DirName
    {
      get;
    }

    public bool NeedEscaping
    {
      get;
      set;
    }

    public abstract string FilePath
    {
      get;
    }

    public abstract string SKey
    {
      get;
    }

    public abstract string Scomment
    {
      get;
    }
    // abstract public List<string> SearchForSignatures(string signature);
    protected abstract string GetDDL();
    protected abstract string GetComment();
    protected abstract string GetGrants(bool grantFilter);
    public abstract string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false);
    //public abstract void SaveToFile(string path); 
    protected abstract void ParseDDL(string DDL);
    public abstract bool ValidateDDL();
    public override int GetHashCode()
    {
      return DDL.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      if (obj is Metadata)
      {
        Metadata me = (Metadata) obj;
        return me.DDL == this.DDL;
      }
      return false;
    }

    public override string ToString()
    {
      return base.ToString();
    }
  }
}
