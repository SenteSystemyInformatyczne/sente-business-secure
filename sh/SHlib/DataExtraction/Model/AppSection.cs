﻿using Newtonsoft.Json;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHDataContracts.Model.AppSection;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SHlib.DataExtraction.Model
{
  public class AppSection : CustomMetadata
  {
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.AppSection;
      }
    }

    /// <summary>
    /// Zapytanie select o dane dla sekcji APPINI
    /// </summary>
    public override string EXTRACTPROCEDURE
    {
      get
      {
        return $"select trim(ident), trim(val), trim(prefix), trim(namespace) from s_appini where section = '{orginalName}' and (prefix is null or upper(prefix) not starting with 'U');";
      }
    }

    private string orginalName;
    public override string Name
    {
      get
      {
        return base.Name;
      }
      set
      {
        orginalName = value;
        if (value.StartsWith("Action_")
            || value.StartsWith("Grid_")
            || value.StartsWith("Menu_")
            || value.StartsWith("Controls_")
            || value.StartsWith("Navigator_"))
        {
          Regex r = new Regex("_");
          base.Name = r.Replace(value, ":", 1);
        }
        base.Name = value.Replace("#", "");
      }
    }

    public override string DDL
    {
      get
      {
        if (DDLNeedsRefresh)
        {
          DDL = JsonConvert.SerializeObject(AppSectionObject, Formatting.Indented);
          DDLNeedsRefresh = false;
        }
        return base.DDL;
      }

      set
      {
        base.DDL = value;
      }
    }

    private string DDLName
    {
      get
      {
        if (string.IsNullOrWhiteSpace(_ddlname))
        {
          if (AppSectionObject == null)
          {
            DDL = GetDDL();
          }
          _ddlname = AppSectionObject.Name;
        }
        return _ddlname;
      }
    }

    private string EscapeName(string name)
    {
      bool upperCase = true;
      string[] omit = { "Navigator:", "NavigatorTab:", "NavigatorGroup:", "Menu:", "Grid:", "Controls:", "Action:" };
      StringBuilder resultName = new StringBuilder();
      foreach (string startStr in omit)
      {
        if (name.StartsWith(startStr))
        {
          resultName.Append(startStr);
          int startPos = name.IndexOf(startStr);
          name = name.Remove(startPos, startStr.Length);
          break;
        }
      }
      foreach (char c in name)
      {
        if (char.IsLower(c) && upperCase)
        {
          resultName.Append("#");
          upperCase = false;
        }
        else if (char.IsUpper(c) && !upperCase)
        {
          resultName.Append("#");
          upperCase = true;
        }
        resultName.Append(c);
      }
      return resultName.ToString();
    }

    public override string DirName
    {
      get
      {
        return "appini";
      }
    }

    public override string FilePath
    {
      get
      {
        string name = Name;
        try
        {
          name = DDLName;
        }
        catch (Exception e)
        {

        }
        if (NeedEscaping)
          name = EscapeName(DDLName);
        return DirName + Path.DirectorySeparatorChar + AppSectionObject.Subfolder + Path.DirectorySeparatorChar + name.Replace(':', '_').Replace('/', '^') + ".json";
      }
    }

    public bool DDLNeedsRefresh
    {
      get;
      set;
    } = true;

    private AppSectionRoot _appSectionObject;
    public AppSectionRoot AppSectionObject
    {
      get
      {
        if (_appSectionObject == null)
        {
          GetDDL();
        }
        return _appSectionObject;
      }
      private set
      {
        _appSectionObject = value;
        DDL = JsonConvert.SerializeObject(_appSectionObject, Formatting.Indented);
      }
    }

    /// <summary>
    /// Procedura wyciąga DDL z bazy danych
    /// </summary>
    /// <returns>DDL</returns>
    protected override string GetDDL()
    {
      IDbSettings dbSettings = GetConnection(true);
      using (Database db = new Database(dbSettings))
      {
        try
        {
          DataTable t = db.Select(EXTRACTPROCEDURE);
          if (t != null)
          {
            AppSectionObject = new AppSectionRoot(orginalName);
            for (int i = 0; i < t.Rows.Count; i++)
            {
              var element = new AppElement();

              var ident = t.Rows[i][0].ToString();
              var val = t.Rows[i][1].ToString();
              var prefix = t.Rows[i][2].ToString();
              var ns = t.Rows[i][3].ToString();

              element.Identifier = ident;
              element.Value = val;
              element.Prefix = string.IsNullOrWhiteSpace(prefix) || prefix.ToUpper() == "NULL" ? null : prefix;
              element.NS = string.IsNullOrWhiteSpace(ns) ? "SENTE" : ns;

              if (!AppSectionObject.Elements.Contains(element))
              {
                AppSectionObject.Elements.Add(element);
              }
            }
            AppSectionObject.Elements.Sort();
          }
          return JsonConvert.SerializeObject(AppSectionObject, Formatting.Indented);
        }
        catch (Exception ex)
        {
          throw new Exception("Błąd wykonania zapytania: " + EXTRACTPROCEDURE, ex);
        }
      }
    }

    #region Constructors
    public AppSection(string Name, string Description, string DDL)
      : base(Name, Description)
    {
      if (!string.IsNullOrWhiteSpace(DDL))
        ParseDDL(DDL);
    }
    public AppSection()
    {
    }
    public AppSection(string Name, string Description)
    {
      this.Name = Name;
      this.Description = Description;
    }
    public AppSection(string path)
    {
      ParseDDL(path);
    }
    public AppSection(AppSectionRoot sectionObject, string description) : base(sectionObject.Name, description)
    {
      AppSectionObject = sectionObject;
    }

    /// <summary>
    /// Wczytuje obiekt z ciagu tekstowego DDLki
    /// </summary>
    /// <param name="DDL">DDL</param>
    protected override void ParseDDL(string DDL)
    {
      var split = Regex.Split(DDL, "(^-{2}@{3}[*]{3}S[^*]*[*]{3}@{3}-{2}.*\\s?)", RegexOptions.Compiled);
      foreach (string s in split.Where(tt => tt.Length > 0))
      {
        if (s.StartsWith(SKEY))
        {
          this.Description = s.Replace(SKEY, "").Trim();
        }
        else
        {
          AppSectionObject = JsonConvert.DeserializeObject<AppSectionRoot>(s);
          this.DDL = s;
        }
      }
    }
    #endregion

    public override string SaveObject(bool grantFilter = false, bool generateScript = false, bool removeSymbols = false, bool generateChange = false)
    {
      return SKey + DDL;
    }
  }
}
