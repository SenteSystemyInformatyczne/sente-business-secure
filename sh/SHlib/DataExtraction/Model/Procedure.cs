﻿using SHDataContracts.Enums;

namespace SHlib.DataExtraction.Model
{
  class Procedure : CustomMetadata
  {
    #region Properties and Fields
    public override DbTypes DbType
    {
      get
      {
        return DbTypes.Procedure;
      }
    }

    public override string EXTRACTPROCEDURE
    {
      get
      {
        return "SYS_PROCEDURE_DDL";
      }
    }

    public override string DirName
    {
      get
      {
        return "procedures";
      }
    }

    #endregion
    #region Constructors
    public Procedure(string Name, string Description, string DDL)
      : base(Name, Description, DDL)
    {
    }
    #endregion
  }
}
