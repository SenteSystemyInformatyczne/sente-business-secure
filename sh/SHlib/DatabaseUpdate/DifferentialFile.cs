﻿using FirebirdSql.Data.Isql;
using SHDataContracts.Enums;
using SHlib.DataExtraction;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SHlib.DatabaseUpdate
{
  public class DifferentialFile
  {
    private const string SET_TERM = "SET TERM";
    private const string SET_TERM_DASH_COLON = SET_TERM + " ^ ;";
    private const string SET_TERM_COLON_DASH = SET_TERM + " ; ^";

    /// <summary>
    /// Metoda konfiugurująca skrypt różnicowy do działania z tranferówkami after
    /// </summary>
    /// <param name="diffpath"></param>
    /// <returns></returns>
    public static string Postprocess(string diffpath, ProgressInfo pi = null, string scriptPath = null, FilesystemExtractor fe = null)
    {
      if (fe == null)
      {
        fe = new FilesystemExtractor(null, null);
        fe.ParseFiles("", scriptPath, pi);
      }

      var filename = Path.GetFileName(diffpath);
      var respath = diffpath.Replace(filename, "postdiffscript.sql");
      var afterfile = diffpath.Replace(filename, "after.esystem.sql");

      using (var outfile = new StreamWriter(respath, false, Encoding.Default))
      {
        string script = RemoveAllComments(File.ReadAllText(diffpath, Encoding.Default));
        FbScript fbs = new FbScript(script);
        fbs.UnknownStatement += Fbs_UnknownStatement;
        fbs.Parse();

        ReorderScript(fbs);

        pi?.ReportCount(fbs.Results.Count, "Parsowanie skryptu różnicowego");
        foreach (var statement in fbs.Results.Distinct(new FbStatementComparer()))
        {
          var finalStatement = HandleStatement(statement, fe);
          if (statement.StatementType == SqlStatementType.AlterTable && Regex.IsMatch(finalStatement.Trim().ToLower(), @"^alter\stable\s.*\sadd\sconstraint"))
            File.AppendAllText(afterfile, "\n" + finalStatement, Encoding.Default);
          else
          {
            outfile.WriteLine(finalStatement);
          }
          pi?.ReportProgress();
        }

        pi?.ReportEnd();
      }
      return respath;
    }

    private static void ReorderScript(FbScript fbs)
    {
      //drop fields before domains
      var dropStatements = fbs.Results.Where(r => r.StatementType == SqlStatementType.AlterTable && r.Text.Contains("DROP")).ToList();
      if (dropStatements.Count > 0)
      {
        var dropIdx = fbs.Results.FindIndex(r => r.StatementType == SqlStatementType.DropDomain);
        if (dropIdx > -1)
        {
          fbs.Results.RemoveAll(r => dropStatements.Contains(r));
          fbs.Results.InsertRange(dropIdx, dropStatements);
        }
      }
    }

    /// <summary>
    /// Usuwa wszystkie wygenerowane komentarze przez IBblock
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    private static string RemoveAllComments(string v)
    {
      Regex regex = new Regex(@"\/\*\*.*\*\*\/");

      while (regex.IsMatch(v))
      {
        string s = regex.Match(v).Groups[0].Value;
        v = v.Replace(s, "");
      }

      return v;
    }

    private static string HandleStatement(FbStatement statement, FilesystemExtractor fe)
    {
      string statementText = statement.Text.Trim();
      switch (statement.StatementType)
      {
        case SqlStatementType.AlterDomain:
          var name = Regex.Match(statementText, @"ALTER DOMAIN (\w+)").Groups[1].Value;
          var dbCharset = Regex.Match(statementText, @"TYPE ([\w\(\),]+)").Groups[1].Value;
          var domain = fe.mList.FirstOrDefault(m => m.DbType == DbTypes.Domain && m.Name == name);
          var scriptCharset = Regex.Match(domain?.DDL ?? "", @"AS ([\w\(\),]+)").Groups[1].Value;
          if (scriptCharset.ToLower() == dbCharset.ToLower())
          {
            statementText = "/* Jeżeli poniższe polecenie generuje błąd to znaczy, że:\n"
                           +"   1) nastąpiła próba zmiany COLLATION za pomocą ALTER, co jest nieobsługiwane przez firebirda (CORE-1202)\n"
                           +"   2) nastąpiła próba wydłużenia domeny, która jest używana w kluczu obcym, co również nie jest obsługiwane przez firebirda (CORE-1427)\n"
                           +"   3) nastąpiła próba wgrania domen z bazy na której mają inne wielkości na odrębną bazę oraz punkty 1) lub (2)\n"
                           +"   4) nastąpiła próba obejścia problemu przez ręczną edycję pliku z domeną, albo bezpośredni update rdb$relation oraz punkty 1) lub 2)\n"
                           +"   Co robić? Jak żyć? Najlepiej jest ręcznie stworzyć nową domenę i przepiąć odwołania ze starej na nową.*/\n"
                           +statementText;
          }
          break;
        case SqlStatementType.Grant:
          if (statementText.EndsWith(" TO"))
            statementText = "";
          break;
        case SqlStatementType.Revoke:
          statementText = "";
          break;
        case SqlStatementType.Connect:
          statementText = "";
          break;
        default:
          break;
      }

      string resultStatement = string.Empty;
      if (statement.StatementType == SqlStatementType.AlterTrigger || statement.StatementType == SqlStatementType.AlterProcedure)
        resultStatement = SET_TERM_DASH_COLON + "\n" + statementText + "\n^\n" + SET_TERM_COLON_DASH;
      else
        resultStatement = string.IsNullOrEmpty(statementText.Trim()) ? statementText : statementText + "\n;";
      return resultStatement + "\n";
    }

    private static void Fbs_UnknownStatement(object sender, UnknownStatementEventArgs e)
    {
      if (e.Statement.Text.Contains("RECONNECT"))
      {
        e.NewStatementType = SqlStatementType.Close;
        e.Handled = true;
      }
      else
      {
        e.Ignore = true;
      }
    }
  }

  public class FbStatementComparer : IEqualityComparer<FbStatement>
  {
    public bool Equals(FbStatement x, FbStatement y)
    {
      return x.Text.Trim() == y.Text.Trim();
    }

    public int GetHashCode(FbStatement obj)
    {
      return obj.Text.Trim().GetHashCode();
    }
  }
}
