﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHlib.DataExtraction;
using SHlib.Utilities;
using SHlib.Utilities.FormsWrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.DatabaseUpdate
{
  public class DatabasePatch
  {
    public static string GetPatchForInputDatabase()
    {
      StringBuilder sb = new StringBuilder();
      foreach (var patch in ProcedurePatch.PatchedInputDatabaseObjects)
        sb.AppendLine(patch.PatchDDL);
      return sb.ToString();
    }

    public static string GetPatchForOutputDatabase()
    {
      StringBuilder sb = new StringBuilder();
      foreach (var patch in ProcedurePatch.PatchedOutputDatabaseObjects)
        sb.AppendLine(patch.PatchDDL);
      return sb.ToString();
    }

    public static void CheckDatabaseId(string srcpath, IDbSettings dbsettings, ProgressInfo pi)
    {
      pi.Write("Sprawdzanie zgodności uuid bazy danych i repozytorium.");

      using (var db = new Database(dbsettings))
      {

        //sprawdź czy baza danych ma już nadane ID
        string databaseuuid = db.ReadUUID();

        //sprawdź czy repozytorium ma już nadane uuid
        string repodatabaseuuid = ReadUUIDFromRepository(srcpath);

        //no i teraz klasyczne przypadki
        var dbhasuuid = !string.IsNullOrWhiteSpace(databaseuuid);
        var repohasuuid = !string.IsNullOrWhiteSpace(repodatabaseuuid);

        //ani baza ani repo nie ma uuid

        if (!dbhasuuid && !repohasuuid)
        {
          var uuid = Guid.NewGuid().ToString();
          pi.Write("#warn#Signature helper nadał twojej bazie uuid " + uuid);
          pi.Write("#warn#Zostało ono zapisane do bazy danych oraz do pliku " + Database.DATABASE_UUID_NAME);
          pi.Write("#warn#Zacommituj ten plik razem z resztą zmian");
          db.SaveUUID(uuid);
          SaveUUIDToRepository(srcpath, uuid);
          return;
        }

        if (dbhasuuid && !repohasuuid)
        {
          pi.Write("#warn#Twoja baza " + db.DbSettings.ToString() + " ma uuid " + databaseuuid);
          pi.Write("#warn#Zostało ono zapisane do do pliku " + Database.DATABASE_UUID_NAME);
          pi.Write("#warn#Zacommituj ten plik razem z resztą zmian");
          SaveUUIDToRepository(srcpath, databaseuuid);
          return;
        }

        if (!dbhasuuid && repohasuuid)
        {
          var msg = "SH wykrył, że repozytorium " + srcpath + " ma przypisany identyfikator bazy danych\n" + repodatabaseuuid + "\n" +
            "podczas gdy nie ma go w bazie " + db.DbSettings.ToString() + ".\n\nZastanów się czy repozytorium jest reprezentacją tej bazy.\n\nNa przykład" +
            " jeżeli wgrywasz zmiany z bazy workowej klienta X na bazę testową X to jest ok, ale jeżeli próbujesz eksportować z bazy STANDARDOWEJ" +
            " na repozytorium X to bardzo źle i nie wiesz jak działa proces.\n\nCzy zapisać UUID do bazy danych?";

          var result = pi.MessageBoxForUUIDMismatch("Uwaga", msg,
            (int) MessageBoxButtons.YesNoCancel,
            (int) MessageBoxIcon.Question,
            (int) MessageBoxDefaultButton.Button1);

          if (result == (int) DialogResult.Yes)
            db.SaveUUID(repodatabaseuuid);
          else if (result != (int) DialogResult.No)
            throw new InvalidOperationException("Przerwano na życzenie użytkownika");

          return;
        }

        if (dbhasuuid && repohasuuid && repodatabaseuuid != databaseuuid && !IgnoreUUIDError)
        {
          pi.Write("#warn#SH wykrył, że uuid bazy danych i repozytorium reprezentującego bazę różnią się");
          pi.Write("#warn#repozytorium " + srcpath + ":");
          pi.Write("#warn#" + repodatabaseuuid);
          pi.Write("#warn#baza danych " + db.DbSettings.ToString() + ":");
          pi.Write("#warn#" + databaseuuid);
          pi.Write("#warn#");
          pi.Write("#warn#Tak się zdarza kiedy próbujesz eksportować z bazy lub importować do niej");
          pi.Write("#warn#dane z repozytorium przeznaczonego dla INNEJ bazy - na przykład eksportujesz");
          pi.Write("#warn#obiekt z bazy ARGO wprost na repozytorium STANDARDOWE. ");
          pi.Write("#warn#Taka operacja może świadczyć o POWAŻNYM BŁĘDZIE.");
          pi.Write("#warn#");
          pi.Write("#warn#Jeżeli mimo wszystko chcesz to zrobić, doprowadź do sytuacji gdy w pliku");
          pi.Write("#warn#" + Database.DATABASE_UUID_NAME + " będzie to samo uuid, jakie zwróci na bazie zapytanie ");
          pi.Write("#warn#" + Database.SELECT_DATABASE_UUID_MANUAL);
        }
      }
    }

    public static bool IgnoreUUIDError { get; set; } = false;

    private static void SaveUUIDToRepository(string srcpath, string uuid)
    {
      string path = Path.Combine(srcpath, Database.DATABASE_UUID_NAME);
      File.WriteAllText(path, uuid);
    }

    private static string ReadUUIDFromRepository(string srcpath)
    {
      string path = Path.Combine(srcpath, Database.DATABASE_UUID_NAME);
      if (File.Exists(path))
        return File.ReadAllText(path);
      return null;
    }

    /// <summary>
    /// Procedura sprawdzająca ustawienia skonfigurowanych baz danych i uruchamiająca proces weryfikacji struktur SH znajdujących się w tych baza celem ich aktualizacji.
    /// </summary>
    /// <param name="pi">Obiekt postępu</param>
    /// <param name="AppendMessage">Metoda do komunikacji z wyjściem użytkownika</param>
    /// <param name="allwaysupdate">>Czy zawsze aktualizować</param>
    /// <param name="updateHashes">Czy przeliczyć hasze</param>
    public static void CheckSHStructuresIntegrity(ProgressInfo pi, Action<string> AppendMessage, bool allwaysupdate = true, bool updateHashes = true)
    {
      IDbSettings sourceDb = Connection.Instance.GetConnection(ConnectionType.Source);
      IDbSettings remoteDb = Connection.Instance.GetConnection(ConnectionType.Remote);
      if (sourceDb.Equals(remoteDb))
      {
        PatchDatabase(sourceDb, pi, AppendMessage, allwaysupdate, updateHashes);
      }
      else
      {
        PatchDatabase(sourceDb, remoteDb, pi, AppendMessage, allwaysupdate, updateHashes);
      }
    }

    /// <summary>
    /// Procedura patchuje obie bazy w obu kierunkach
    /// </summary>
    /// <param name="indbsettings">Dane dostępowe do bazy źródłowej</param>
    /// <param name="outdbsettings">Dane dostępowe do bazy docelowej</param>
    /// <param name="pi">Obiekt postępu</param>
    /// <param name="AppendMessage">Metoda do komunikacji z wyjściem użytkownika</param>
    /// <param name="allwaysupdate">Czy zawsze aktualizować</param>
    /// <param name="updateHashes">Czy przeliczyć hasze</param>
    public static void PatchDatabase(IDbSettings indbsettings, IDbSettings outdbsettings, ProgressInfo pi, Action<string> AppendMessage, bool allwaysupdate = true, bool updateHashes = true)
    {
      if (indbsettings != null && indbsettings.IsValid())
        PatchDatabase(indbsettings, pi, AppendMessage, allwaysupdate, updateHashes);
      if (outdbsettings != null && outdbsettings.IsValid())
        PatchDatabase(outdbsettings, pi, AppendMessage, allwaysupdate, updateHashes);
    }

    /// <summary>
    /// Procedura patchuje bazę w obu kierunkach
    /// </summary>
    /// <param name="indbsettings">Dane dostępowe do bazy źródłowej</param>
    /// <param name="pi">Obiekt postępu</param>
    /// <param name="AppendMessage">Metoda do komunikacji z wyjściem użytkownika</param>
    /// <param name="allwaysupdate">Czy zawsze aktualizować</param>
    /// <param name="updateHashes">Czy przeliczyć hasze</param>
    public static void PatchDatabase(IDbSettings dbsettings, ProgressInfo pi, Action<string> AppendMessage, bool allwaysupdate = true, bool updateHashes = true)
    {
      if (AppendMessage != null)
        AppendMessage("Aktualizuję bazę danych " + dbsettings.Name);
      PatchInputDatabase(dbsettings, pi, AppendMessage, allwaysupdate, updateHashes);
      PatchOutputDatabase(dbsettings, pi, AppendMessage, allwaysupdate, updateHashes);
    }

    public static void PatchInputDatabase(IDbSettings dbsettings, ProgressInfo pi, Action<string> AppendMessage, bool allwaysupdate = false, bool updateHashes = true)
    {
      foreach (var patch in ProcedurePatch.PatchedInputDatabaseObjects)
        PatchDBObjectGeneral(patch, dbsettings, AppendMessage, allwaysupdate, pi);
    }

    public static void PatchOutputDatabase(IDbSettings dbsettings, ProgressInfo pi, Action<string> AppendMessage, bool allwaysupdate = false, bool updateHashes = true)
    {
      foreach (var patch in ProcedurePatch.PatchedOutputDatabaseObjects)
      {
        if (patch is HashCodeTablePatchInfo)
          (patch as HashCodeTablePatchInfo).updateHashesAfterPatch = updateHashes;
        PatchDBObjectGeneral(patch, dbsettings, AppendMessage, allwaysupdate, pi);
      }
    }

    private static void PatchDBObjectGeneral(PatchInfo patch, IDbSettings dbsettings, Action<string> AppendMessage, bool allwaysupdate = false, ProgressInfo pi = null)
    {
      using (var db = new Database(dbsettings))
      {

        bool update = allwaysupdate;
        if (patch is FieldPatchInfo || patch is TablePatchInfo)
          update = patch.NeedPatch(db);
        else
          update |= patch.NeedPatch(db);
        if (update)
        {
          AppendMessage("Wgrywam nową wersję " + patch.Name);
        }

        if (update)
        {
          patch.Patch(db, pi);
        }
      }
    }
  }

  /// <summary>
  /// Abstrakcyjna klasa do przechowywania informacji które obiekty bazy danych należy spatchować przed uruchomieniem procesów
  /// </summary>
  public abstract class PatchInfo
  {
    public string Name;
    public string Signature;
    public string PatchDDL;

    public abstract bool NeedPatch(Database db);
    public virtual void Patch(Database db, ProgressInfo pi = null)
    {
      db.ExecuteScript(PatchDDL);
    }
  }

  /// <summary>
  /// Abstrakcyjna klasa, która potrafi standardową treść patcha przysłonić sqlem z plików
  /// </summary>
  public abstract class OverloadedPatchInfo : PatchInfo
  {
    public override void Patch(Database db, ProgressInfo pi = null)
    {
      OverridePatch(pi);

      base.Patch(db, pi);
    }

    private void OverridePatch(ProgressInfo pi)
    {
      this.PatchDDL = SQLRepository.Instance.GetSqlForPatch(this.Name, this.PatchDDL);
    }
  }

  public class ProcedurePatchInfo : OverloadedPatchInfo
  {
    public override bool NeedPatch(Database db)
    {
      ProcedureFactory pf = new ProcedureFactory() { GetConnection = delegate { return db.DbSettings; } };
      bool update = false;
      if (!string.IsNullOrEmpty(Signature))
      {
        var res = pf.CreateCollection(Signature);
        update |= res.Count == 0 || res.All(r => r.Name.ToLower() != Name);
      }
      else
        update = true;

      return update;
    }
  }

  public class FieldPatchInfo : OverloadedPatchInfo
  {
    public const string RP_REPLICAT_SIGNATURE = @"
ALTER TABLE RP_REPLICAT ADD SIGNATURE VARCHAR(255) CHARACTER SET WIN1250 COLLATE PXW_PLK;
COMMENT ON COLUMN RP_REPLICAT.SIGNATURE IS 'PR96748';
";

    public override bool NeedPatch(Database db)
    {
      var sql = FieldFactory.STATEMENT + " and trim(rf.rdb$relation_name) = 'RP_REPLICAT' and trim(rf.rdb$field_name)='SIGNATURE'";
      var ret = DatabaseManager.Instance.ExecuteScalar(db.DbSettings, sql);
      bool update = false;

      update = (ret == null || (ret != null && string.IsNullOrWhiteSpace((string) ret)));

      return update;
    }
  }

  public class TablePatchInfo : OverloadedPatchInfo
  {
    /// <summary>
    /// Metoda sprawdzająca czy wymagana jest aktualizacja obiektów pomocniczych SH na wskazanej bazie.
    /// </summary>
    /// <param name="db">Docelowa baza, na której sprawdzany jest stan obiektów</param>
    /// <returns></returns>
    public override bool NeedPatch(Database db)
    {
      TableFactory tf = new TableFactory() { GetConnection = delegate { return db.DbSettings; } };
      tf.IncludeTrHist();
      tf.IncludeHashCodes();
      tf.IncludeTrTemp();
      bool update = false;

      var res = tf.CreateCollection(Signature);
      update |= res.Count == 0;

      return update;
    }
  }

  public class HashCodeTablePatchInfo : TablePatchInfo
  {
    public bool updateHashesAfterPatch
    {
      get;
      set;
    }

    public override void Patch(Database db, ProgressInfo pi = null)
    {
      base.Patch(db);
      //if (updateHashesAfterPatch)
      //  me.UpdateAllHashesFromDb(pi);
    }
  }
  public static class ProcedurePatch
  {
    public static readonly PatchInfo[] PatchedOutputDatabaseObjects = new PatchInfo[] {
       new TablePatchInfo() {
        Name="sys_trhist",
        Signature="BS89147",
        PatchDDL=ProcedurePatch.SYS_TRHIST_DDL_PATCH
      },
      new HashCodeTablePatchInfo() {
        Name="sys_hashcodes",
        Signature="BS87678",
        PatchDDL=ProcedurePatch.SYS_HASHCODES_DDL_PATCH
      },
      new HashCodeTablePatchInfo() {
        Name="sys_tr_temp",
        Signature="PR88509",
        PatchDDL=ProcedurePatch.SYS_TR_TEMP
      }
    };

    public static readonly PatchInfo[] PatchedInputDatabaseObjects = new PatchInfo[] {
      new ProcedurePatchInfo() { Name = "sys_quote_reserved",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_QUOTE_RESERVED_DDL_PATCH

      },
      new ProcedurePatchInfo() { Name = "sys_constraint_ddl",
                        Signature = "BS106224",
                        PatchDDL = ProcedurePatch.SYS_CONSTRAINT_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_domain_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_DOMAIN_DLL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_exception_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_EXCEPTION_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_field_ddl",
                        Signature = "BS99942",
                        PatchDDL = ProcedurePatch.SYS_FIELD_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_appsection_ddl",
                        Signature = "PR89881",
                        PatchDDL = ProcedurePatch.SYS_APPSECTION_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_getcomment",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_GETCOMMENT_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_grant_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_GRANT_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_index_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_INDEX_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_procedure_parameters",
                        Signature = "BS104013",
                        PatchDDL = ProcedurePatch.SYS_PROCEDURE_PARAMETERS_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_procedure_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_PROCEDURE_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_sequence_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_SEQUENCE_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_table_or_view_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_TABLE_OR_VIEW_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_trigger_parameters",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_TRIGGER_PARAMETERS_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_trigger_ddl",
                        Signature = "BS87678",
                        PatchDDL = ProcedurePatch.SYS_TRIGGER_DDL_PATCH
      },
      new FieldPatchInfo()
       {
         Name = "rp_replicat.signature",
         Signature = "PR96748",
         PatchDDL = FieldPatchInfo.RP_REPLICAT_SIGNATURE
       },
       new ProcedurePatchInfo() { Name = "sys_replicat_ddl",
                        Signature = "PR96748",
                        PatchDDL = ProcedurePatch.SYS_REPLICAT_DDL_PATCH
      }
       ,
      new ProcedurePatchInfo() { Name = "sys_check_ddl",
                        Signature = "PR88488",
                        PatchDDL = ProcedurePatch.SYS_CHECK_DDL_PATCH
      },
      new ProcedurePatchInfo() { Name = "sys_role_ddl",
                        Signature = "BS104035",
                        PatchDDL = ProcedurePatch.SYS_ROLE_DDL_PATCH
      }
    };

    public const string SYS_PROCEDURE_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_procedure_ddl (
  nazwa           varchar(50),
  create_or_alter smallint)
returns (
  ddl blob sub_type 1 segment size 80)
as
declare variable coa      varchar(80);
declare variable source   blob sub_type 1        segment size 80;
declare variable params   blob sub_type 1        segment size 80;
declare variable descript blob sub_type 1        segment size 80;
declare variable transfer integer;
declare variable eol      varchar(2);
begin
 eol = '
';
  coa = 'SET TERM ^;'||:eol;
  coa = :coa || 'CREATE OR ALTER PROCEDURE '||:nazwa;
  select rdb$procedure_source from rdb$procedures
    where rdb$procedure_name = :nazwa
  into source;
  if(create_or_alter = 1) then
  begin
    source = :source || 'begin'||:eol||'  suspend;'||:eol||'end^'||:eol||'SET TERM ; ^';
    suspend;
  end
  execute procedure SYS_PROCEDURE_PARAMETERS(:nazwa) returning_values :params;
  ddl = :coa||:params||' as'||:eol||:source;
  ddl = :ddl || '^'||:eol||'SET TERM ; ^';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_PROCEDURE_DDL IS
'PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO PROCEDURE SYS_PROCEDURE_DDL;

GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_DDL TO SYSDBA;

COMMIT WORK;";

    public const string SYS_DOMAIN_DLL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_DOMAIN_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable PARAMETER_TYPE_NAME varchar(80);
declare variable PARAMETER_LENGTH integer;
declare variable PARAMETER_PRECISION integer;
declare variable PARAMETER_FIELD_SUB_TYPE integer;
declare variable PARAMETER_SCALE integer;
declare variable PARAMETER_NULL_FLAG integer;
declare variable PARAMETER_COLLATION_NAME varchar(80);
declare variable PARAMETER_CHARACTER_SET_NAME varchar(80);
declare variable PARAMETER_FIELD_TYPE smallint;
declare variable PARAMETER_FIELD_LENGTH smallint;
declare variable PARAMETER_CHARACTER_SET_ID smallint;
declare variable PARAMETER_COLLATION_ID smallint;
declare variable PARAMETER_FLAG_NULL smallint;
declare variable PARAMETER_CHARACTER_LENGTH smallint;
declare variable PARAMETER_FIELD_SCALE smallint;
declare variable PARAMETER_SEGMENT_LENGTH smallint;
declare variable PARAMETER_DEFAULT_SOURCE blob sub_type 1 segment size 80;
declare variable PARAMETER_VALIDATION_SOURCE blob sub_type 1 segment size 80;
begin
  if(:create_or_alter = 0) then
  begin
    ddl = 'CREATE DOMAIN ' || :nazwa || ' AS ';
    select t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision, f.rdb$field_sub_type,
           f.rdb$field_scale, f.rdb$null_flag, c.rdb$collation_name, ch.rdb$character_set_name, f.rdb$segment_length, f.rdb$default_source, f.rdb$validation_source
      from rdb$fields f
      join rdb$types t on (f.rdb$field_type = t.rdb$type)
      left join rdb$collations c on (c.rdb$collation_id = f.rdb$collation_id and c.rdb$character_set_id = f.rdb$character_set_id)
      left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
      where f.rdb$field_name = :nazwa and t.rdb$field_name = 'RDB$FIELD_TYPE'
      into :parameter_type_name, :parameter_length, :parameter_precision, :parameter_field_sub_type,
           :parameter_scale, :parameter_null_flag, :parameter_collation_name, :parameter_character_set_name,
           :parameter_segment_length, :parameter_default_source, :parameter_validation_source;

    if(parameter_collation_name is not null) then
      parameter_collation_name = substr(:parameter_collation_name, 1, strpos(' ', :parameter_collation_name) - 1);
    if(parameter_character_set_name is not null) then
      parameter_character_set_name = substr(:parameter_character_set_name, 1, strpos(' ', :parameter_character_set_name) - 1);
  
    parameter_type_name = substr(:parameter_type_name, 1, strpos(' ', :parameter_type_name) - 1);
    if(parameter_segment_length is null) then
      parameter_segment_length = 80;
    if(parameter_type_name = 'DATE') then
      ddl = :ddl || 'DATE';
    else if(parameter_type_name = 'DOUBLE') then
      ddl = :ddl || 'DOUBLE PRECISION';
    else if(parameter_type_name = 'FLOAT') then
      ddl = :ddl || 'FLOAT';
    else if(parameter_type_name = 'INT64' and (parameter_field_sub_type = 1 or parameter_scale<0 or parameter_precision>0)) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'INT64' and parameter_field_sub_type = 0) then
      ddl = :ddl || 'BIGINT';
    else if(parameter_type_name = 'INT64' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'LONG' and (parameter_field_sub_type = 0 or parameter_field_sub_type is null)) then
      ddl = :ddl || 'INTEGER';
    else if(parameter_type_name = 'LONG' and parameter_field_sub_type = 1) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'LONG' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 0) then
      ddl = :ddl || 'SMALLINT';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 1) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'TEXT') then
      ddl = :ddl || 'CHAR(' || :parameter_length || ')';
    else if(parameter_type_name = 'TIMESTAMP') then
      ddl = :ddl || 'TIMESTAMP';
    else if(parameter_type_name = 'VARYING') then begin
      if(parameter_character_set_name is not null and
         parameter_character_set_name='UTF8') then
         parameter_length = parameter_length / 4;
      ddl = :ddl || 'VARCHAR(' || :parameter_length || ')';
    end
    else if(parameter_type_name = 'TIME') then
      ddl = :ddl || 'TIME';
    else if(parameter_type_name = 'BLOB') then
      ddl = :ddl || 'BLOB SUB_TYPE '||parameter_field_sub_type||' SEGMENT SIZE '||parameter_segment_length;

    if(parameter_character_set_name is not null) then
      ddl = :ddl || ' CHARACTER SET ' || :parameter_character_set_name;

    if(parameter_default_source is not null) then
      ddl = :ddl || ' ' || :parameter_default_source;

    if(parameter_null_flag is not null) then
      ddl = :ddl || ' NOT NULL';
    if(parameter_collation_name is not null and :create_or_alter = 0) then
      ddl = :ddl || ' COLLATE ' || :parameter_collation_name;
    if(parameter_validation_source is not null) then
      ddl = :ddl || ' ' || :parameter_validation_source;
  end

  if(:create_or_alter = 1) then
  begin
    select f.rdb$field_type, f.rdb$field_length, f.rdb$character_set_id, f.rdb$collation_id, f.rdb$null_flag,
           f.rdb$character_length, f.rdb$field_precision, f.rdb$field_scale
      from rdb$fields f
      where rdb$field_name = :nazwa
      into :parameter_field_type, :parameter_field_length, :parameter_character_set_id, :parameter_collation_id, :parameter_flag_null,
           :parameter_character_length, :parameter_precision, :parameter_field_scale;
      ddl = 'update rdb$fields set rdb$field_type = '   || coalesce(:parameter_field_type, 'null')        ||
            ', rdb$field_length = '                     || coalesce(:parameter_field_length, 'null')      ||
            ', rdb$character_set_id = '                 || coalesce(:parameter_character_set_id, 'null')  ||
            ', rdb$collation_id = '                     || coalesce(:parameter_collation_id, 'null')      ||
            ', rdb$null_flag = '                        || coalesce(:parameter_flag_null, 'null')         ||
            ', rdb$character_length = '                 || coalesce(:parameter_character_length, 'null')  ||
            ', rdb$field_precision = '                  || coalesce(:parameter_precision, 'null')         ||
            ', rdb$field_scale = '                      || coalesce(:parameter_field_scale, 'null')       ||
            ', rdb$field_sub_type = '                   || coalesce(:parameter_field_sub_type, 'null');
      ddl = :ddl || ' where rdb$field_name = ''' || :nazwa || '''';
  end
  ddl = :ddl || ';';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_DOMAIN_DDL IS
'PR16135;PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_DOMAIN_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_DOMAIN_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_DOMAIN_DDL TO SYSDBA;

COMMIT WORK;";

    public const string SYS_TABLE_OR_VIEW_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_TABLE_OR_VIEW_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable FIELD_NAME varchar(80);
declare variable FIELD_DDL varchar(1024);
declare variable VIEW_SELECT_DDL blob sub_type 1 segment size 80;
declare variable EOL varchar(2);
declare variable TAB varchar(4);
declare variable WHITESPACE varchar(64);
declare variable MAX_LEN integer;
declare variable MAX_POS integer;
declare variable FIELD_POS integer;
declare variable I integer;
declare variable TABLE_OR_VIEW smallint;
declare variable PRIMARY_KEY_NAME varchar(255);
declare variable PRIMARY_KEY_FIELD_NAME varchar(255);
declare variable PRIMARY_KEY_FIELDS_NUMBER integer;
declare variable PRIMARY_KEY_CURRENT_ROW integer;
declare variable PRIMARY_KEY_CONSTRAINT varchar(255);
begin
/* create_or_alter - czy widok był zmieniony, jeżeli 1 to stosuje konstrukcje create or alter view */
  eol ='
  ';
  tab = '    ';
  table_or_view = 1;   -- 0 - widok, 1 - zwykla tabela, 2 - temporary delete, 3 - temporary preserve 4 - virtualna

  select
    case
      when rdb$relation_type = 0 then 1  --persistent
      when rdb$relation_type = 1 then 0  --view
      when rdb$relation_type = 3 then 4  --virtual
      when rdb$relation_type = 4 then 3  --global_temporary_preserve
      when rdb$relation_type = 5 then 2  --global_temporary_delete
      when rdb$relation_type is null and rdb$view_blr is null then 1 --treated as persistent based on rdb$view_blr
      when rdb$relation_type is null and rdb$view_blr is not null then 0 --treated as view based on rdb$view_blr
      else -1
    end
    from rdb$relations
    where rdb$relation_name = :nazwa
    into :table_or_view;

  ddl = case
    when table_or_view = 0 and create_or_alter = 0 then 'CREATE VIEW ' || :nazwa || ' (' || :eol
    when table_or_view = 0 and create_or_alter = 1 then 'CREATE OR ALTER VIEW ' || :nazwa || ' (' || :eol
    when table_or_view = 1 then 'CREATE TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 2 then 'CREATE GLOBAL TEMPORARY TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 3 then 'CREATE GLOBAL TEMPORARY TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 4 then 'CREATE TABLE ' || :nazwa || ' (' || :eol
    else 'CREATE TABLE ' || :nazwa || ' (' || :eol  -- domyslnie zwykla tabela
  end;

  if (table_or_view = 0) then
    select rl.rdb$view_source
    from rdb$relations rl
    where rl.rdb$relation_name = :nazwa
    into :view_select_ddl;

  if (table_or_view = 1) then
    select rc.rdb$constraint_name, rc.rdb$index_name
    from rdb$relation_constraints rc
    where rc.rdb$constraint_type = 'PRIMARY KEY' and rc.rdb$relation_name = :nazwa
    into :primary_key_constraint, :primary_key_name;

  select max(char_length(trim(r.rdb$field_name))),
      max(r.rdb$field_position)
    from rdb$relation_fields r
    where r.rdb$relation_name = :nazwa
    into :max_len, :max_pos;

  if (table_or_view = 1) then
      begin
         select count(s.rdb$field_name)
         from rdb$index_segments s
         where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
         into :primary_key_fields_number;
         primary_key_current_row = 1;

         if(primary_key_fields_number = 0) then begin
           select first 1 trim(r.rdb$field_name)
           from rdb$relation_fields r
           where rdb$relation_name = :nazwa 
           order by rdb$field_position
           into :field_name;
           execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
           ddl = :ddl || :tab || :field_ddl || :eol;
         end
         else
         for select trim(s.rdb$field_name)
         from rdb$index_segments s
         where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
         order by s.rdb$field_position
         into :PRIMARY_KEY_FIELD_NAME
         do begin
           execute procedure sys_field_ddl(:nazwa||'.'||:PRIMARY_KEY_FIELD_NAME, 1) returning_values :field_ddl;
           ddl = :ddl || :tab || :field_ddl;
           if(primary_key_current_row < primary_key_fields_number) then
             ddl = :ddl || ',';
           ddl = :ddl || :eol;
           primary_key_current_row = :primary_key_current_row + 1;
         end
         --ddl = substr(ddl, -1) || :eol;
      end
  else
  for select trim(r.rdb$field_name), r.rdb$field_position
    from rdb$relation_fields r
    where rdb$relation_name = :nazwa
    order by r.rdb$field_position
    into :field_name, :field_pos
  do begin
    whitespace = '';
    i = 0;
    while(i < (max_len - char_length(field_name))) do
    begin
      whitespace = :whitespace || ' ';
      i = :i + 1;
    end
    field_name = :field_name || :whitespace;

    if(table_or_view = 0) then
      field_ddl = field_name;
    else
    begin
       execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
      field_ddl = ' ' || field_ddl;
    end

    field_name = trim(:field_name);
    if(field_pos <> max_pos) then
      ddl = :ddl || :tab || :field_ddl || ',';
    else
      ddl = :ddl || :tab || :field_ddl;

    if(table_or_view = 0) then
    begin
      execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
      field_ddl = substring(:field_ddl from position(' ', :field_ddl));
      ddl = :ddl || '--' || coalesce(:field_ddl, '');
    end
    ddl = :ddl || :eol;
  end

  ddl = case
    when table_or_view = 0 then :ddl || ')' || :eol || 'AS ' || :view_select_ddl || eol || ';'
    when table_or_view = 1 then :ddl || ');'
    when table_or_view = 2 then :ddl || ') ON COMMIT DELETE ROWS;'
    when table_or_view = 3 then :ddl || ') ON COMMIT PRESERVE ROWS;'
    when table_or_view = 4 then :ddl || ');'
    else :ddl || ');'  -- domyslnie zwykla tabela
  end;

  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_TABLE_OR_VIEW_DDL IS
'PR16135;PR39086;PR65161;BS87678;PR89881';

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_TABLE_OR_VIEW_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_TABLE_OR_VIEW_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_TABLE_OR_VIEW_DDL TO SENTELOGIN;
GRANT EXECUTE ON PROCEDURE SYS_TABLE_OR_VIEW_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_FIELD_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_FIELD_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable FIELD_SOURCE varchar(80);
declare variable FIELD_TYPE_NAME varchar(80);
declare variable FIELD_LENGTH integer;
declare variable FIELD_PRECISION integer;
declare variable FIELD_SCALE integer;
declare variable FIELD_NULL_FLAG integer;
declare variable FIELD_SUB_TYPE integer;
declare variable FIELD_COLLATION_NAME varchar(80);
declare variable FIELD_CHARACTER_SET_NAME varchar(80);
declare variable FIELD_CHECK smallint;
declare variable FIELD_COLLATION_ID smallint;
declare variable FIELD_DEFAULT_SRC blob sub_type 1 segment size 80;
declare variable DOMAIN_DDL varchar(1024);
declare variable EOL varchar(2);
declare variable TABELA varchar(64);
declare variable POLE varchar(64);
declare variable POLE_ESCAPED varchar(64);
declare variable FIELD_NULL integer;
declare variable RELATION_NULL integer;
declare variable DEFAULT_COLLATE_NAME varchar(64);
begin
  eol ='
  ';
  tabela = substring(:nazwa from 1 for position('.' in :nazwa)-1);
  pole = substring(:nazwa from position('.' in :nazwa)+1);

  pole_escaped = (select outname from sys_quote_reserved(:pole));

  if (:create_or_alter <= 1) then
  begin
    field_check = 0;
    if(create_or_alter = 1) then
      ddl = :pole_escaped || ' ';
    else
      ddl = 'ALTER TABLE ' || :tabela || ' ADD ' || :pole_escaped || ' ';
    select trim(r.rdb$field_source), coalesce(nullif(r.rdb$null_flag,0),f.rdb$null_flag),
         t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision,
         f.rdb$field_scale, c.rdb$collation_name, ch.rdb$character_set_name,
         f.rdb$field_sub_type, r.rdb$default_source, f.rdb$null_flag, r.rdb$null_flag,
         trim(ch.rdb$default_collate_name)
      from rdb$relation_fields r
      join rdb$fields f on (r.rdb$field_source = f.rdb$field_name)
      join rdb$types t on (f.rdb$field_type = t.rdb$type)
      left join rdb$collations c on (c.rdb$collation_id = r.rdb$collation_id and c.rdb$character_set_id = f.rdb$character_set_id)
      left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
      where r.rdb$field_name = trim(:pole) and r.rdb$relation_name = :tabela and t.rdb$field_name = 'RDB$FIELD_TYPE'
      into :field_source, :field_null_flag, :field_type_name, :field_length,
           :field_precision, :field_scale, :field_collation_name, :field_character_set_name,
           :field_sub_type, :field_default_src , :field_null, :relation_null, :default_collate_name;

    if(field_type_name is null) then
      exception universal('Błąd generowania DDL. Nieznany typ pola.');

    if(position('RDB$' in field_source) = 0) then
      ddl = :ddl || :field_source;
    else
    begin
      field_type_name = trim(:field_type_name);
      if(field_type_name = 'DATE') then
        ddl = :ddl || 'DATE';
      else if(field_type_name = 'DOUBLE') then
        ddl = :ddl || 'DOUBLE PRECISION';
      else if(field_type_name = 'FLOAT') then
        ddl = :ddl || 'FLOAT';
      else if(field_type_name = 'INT64' and field_sub_type = 0) then
        ddl = :ddl || 'BIGINT';
      else if(field_type_name = 'INT64' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || coalesce(:field_precision,18) || ',' || (-field_scale) || ')';
      else if(field_type_name = 'INT64' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'LONG' and field_sub_type = 0) then
        ddl = :ddl || 'INTEGER';
      else if(field_type_name = 'LONG' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'LONG' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'SHORT' and field_sub_type = 0) then
        ddl = :ddl || 'SMALLINT';
      else if(field_type_name = 'SHORT' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'SHORT' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'TEXT') then
        ddl = :ddl || 'CHAR(' || :field_length || ')';
      else if(field_type_name = 'TIMESTAMP') then
        ddl = :ddl || 'TIMESTAMP';
      else if(field_type_name = 'VARYING') then begin
        if(field_character_set_name is not null and
           field_character_set_name='UTF8') then
           field_length = field_length / 4;
        ddl = :ddl || 'VARCHAR(' || :field_length || ')';
      end
      else if(field_type_name = 'TIME') then
        ddl = :ddl || 'TIME';
      else if(field_type_name = 'BLOB') then
        ddl = :ddl || 'BLOB SUB_TYPE '||field_sub_type;
      if(field_character_set_name is not null) then
        ddl = :ddl || ' CHARACTER SET ' || :field_character_set_name;
    end

    if(field_default_src is not null) then
      ddl = :ddl || ' ' || :field_default_src;

    if(relation_null is not null) then
         ddl = :ddl || ' NOT NULL';

    if(position('RDB$' in field_source) > 0) then
    begin
      if(field_collation_name is not null and field_collation_name<>default_collate_name) then
        ddl = :ddl || ' COLLATE ' || :field_collation_name;
    end
  end else
  if(:create_or_alter = 2) then
  begin
    select r.rdb$field_source, r.rdb$null_flag, r.rdb$collation_id
      from rdb$relation_fields r
      where rdb$field_name = :pole and rdb$relation_name = :tabela
      into  :field_source, :field_null_flag, field_collation_id;
      field_source = substr(:field_source, 1, strpos(' ', :field_source) - 1);
      ddl = 'update rdb$relation_fields set rdb$field_source = '''   || coalesce(:field_source, 'null')     ||
            ''', rdb$null_flag = '                                   || coalesce(:field_null_flag, 'null')  ||
            ', rdb$collation_id = '                                  || coalesce(:field_collation_id, 'null');
      ddl = :ddl || ' where rdb$field_name = ''' || trim(:pole) || ''' and rdb$relation_name = ''' || :tabela || '''';
  end
  if(create_or_alter <> 1) then
  ddl = :ddl || ';';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_FIELD_DDL IS
'PR16135;PR65161;BS87678;PR89882;PR89881;BS99942';

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO PROCEDURE SYS_TABLE_OR_VIEW_DDL;
GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO SENTELOGIN;
GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO SYSDBA;
GRANT EXECUTE ON PROCEDURE SYS_FIELD_DDL TO SYS_TABLE_OR_VIEW_DDL;

COMMIT WORK;";

    public const string SYS_QUOTE_RESERVED_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_QUOTE_RESERVED (
    NAME varchar(255))
returns (
    OUTNAME varchar(255))
as
declare variable KEYWORDS varchar(2526);
declare variable RES_WORD smallint;
declare variable SPACEBETWEEN integer;
declare variable NOTVALIDIDENT smallint;
begin
  keywords = ';ADD;ADMIN;ALL;ALTER;AND;ANY;AS;AT;AVG;BEGIN;BETWEEN;BIGINT;BIT_LENGTH;BLOB;BOTH;BY;CASE;CAST;CHAR;CHAR_LENGTH;CHARACTER;CHARACTER_LENGTH;CHECK;CLOSE;COLLATE;COLUMN;COMMIT;CONNECT;CONSTRAINT;COUNT;CREATE;CROSS;CURRENT;CURRENT_CONNECTION;CURRENT_DATE;CURRENT_ROLE;CURRENT_TIME;CURRENT_TIMESTAMP;CURRENT_TRANSACTION;CURRENT_USER;CURSOR;DATE;DAY;DEC;DECIMAL;DECLARE;DEFAULT;DELETE;DISCONNECT;DISTINCT;DOUBLE;DROP;ELSE;END;ESCAPE;EXECUTE;EXISTS;EXTERNAL;EXTRACT;FETCH;FILTER;FLOAT;FOR;FOREIGN;FROM;FULL;FUNCTION;GDSCODE;GLOBAL;GRANT;GROUP;HAVING;HOUR;IN;INDEX;INNER;INSENSITIVE;INSERT;INT;INTEGER;INTO;IS;JOIN;LEADING;LEFT;LIKE;LONG;LOWER;MAX;MAXIMUM_SEGMENT;MERGE;MIN;MINUTE;MONTH;NATIONAL;NATURAL;NCHAR;NO;NOT;NULL;NUMERIC;OCTET_LENGTH;OF;ON;ONLY;OPEN;OR;ORDER;OUTER;PARAMETER;PLAN;POSITION;POST_EVENT;PRECISION;PRIMARY;PROCEDURE;RDB$DB_KEY;REAL;RECORD_VERSION;RECREATE;RECURSIVE;REFERENCES;RELEASE;RETURNING_VALUES;RETURNS;REVOKE;RIGHT;ROLLBACK;ROW_COUNT;ROWS;SAVEPOINT;SECOND;SELECT;SENSITIVE;SET;SIMILAR;SMALLINT;SOME;SQLCODE;SQLSTATE;START;SUM;TABLE;THEN;TIME;TIMESTAMP;TO;TRAILING;TRIGGER;TRIM;UNION;UNIQUE;UPDATE;UPPER;USER;USING;VALUE;VALUES;VARCHAR;VARIABLE;VARYING;VIEW;WHEN;WHERE;WHILE;WITH;YEAR;';
  outname=trim(name);
  res_word = position(';'||upper(outname)||';' in keywords);
  spacebetween = position(' ' in outname);
  notvalidident = iif(outname similar to '[[:ALPHA:]][[:ALNUM:]]*',0,1);
  if(res_word>0 or spacebetween>0 or notvalidident>0) then --obsluga escapowania slow zastrzezonych
    outname = '""'||outname||'""';
  suspend;
end^

SET TERM; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_QUOTE_RESERVED IS
'BS87678;PR89881;BS120353';

GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO PROCEDURE SYS_FIELD_DDL;
GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_APPSECTION_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_APPSECTION_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable EOL varchar(2);
declare variable IDENT varchar(50);
declare variable VAL varchar(2048);
declare variable PREFIX varchar(32);
declare variable NAMESPACE varchar(100);
begin
  eol = '
';
  ddl = '['||:nazwa||']'||:eol;
  for
    select trim(ident), trim(val), trim(prefix), trim(namespace) from s_appini where section = :nazwa and (prefix is null or upper(prefix) not starting with 'U')
    order by 1, 3 into :ident, :val, :prefix, :namespace
  do begin
    ddl = ddl || iif(prefix is null or prefix = '', '', :prefix || '#');
    if(namespace is null or namespace = '') then
      ddl = ddl || 'NULL~';
    if(namespace <> 'SENTE') then
      ddl = ddl || :namespace||'~';
    --eskejpowanie kluczy i wartości, konieczne
    val = replace(val, '\', '\\');
    val = replace(val, '~', '\~');
    val = replace(val, '#', '\#');
    val = replace(val, '=', '\=');
    ident = replace(ident, '\', '\\');
    ident = replace(ident, '~', '\~');
    ident = replace(ident, '#', '\#');
    ident = replace(ident, '=', '\=');

    ddl = ddl || :ident||'='||:val||:eol;
  end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_APPSECTION_DDL IS
'PR65161;BS87678;PR89881;BS102241';

/* Following GRANT statements are generated automatically */

GRANT SELECT ON S_APPINI TO PROCEDURE SYS_APPSECTION_DDL;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_APPSECTION_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_APPSECTION_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_APPSECTION_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_EXCEPTION_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_exception_ddl (
  nazwa           varchar(255),
  create_or_alter smallint)
returns (
  ddl blob sub_type 1 segment size 80)
as
declare variable msg varchar(255);
begin
  SELECT RDB$MESSAGE FROM RDB$EXCEPTIONS WHERE RDB$EXCEPTION_NAME = :nazwa
  into :msg;
  ddl = 'CREATE OR ALTER EXCEPTION '||:nazwa||' '''||:msg||''';';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_EXCEPTION_DDL IS
'PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_EXCEPTION_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_EXCEPTION_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_EXCEPTION_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_REPLICAT_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_REPLICAT_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint = 0)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable SOURCE_DBASE varchar(10);
declare variable DEST_DBASE varchar(15);
declare variable NAMEREP varchar(40);
declare variable REPLICAT_TYPE smallint;
declare variable PARAMSREP varchar(1024);
declare variable SD_TABLE varchar(31);
declare variable EI_TYPE smallint;
declare variable OVERWRITE char(1);
declare variable SD_FILEPATH varchar(255);
declare variable S_OBJ integer;
declare variable D_OBJ integer;
declare variable REF_ID integer;
declare variable STRANSBLOCK smallint;
declare variable DTRANSBLOCK smallint;
declare variable RSIGNATURE varchar(255);
declare variable PR_ITEM_REF_ID integer;
declare variable PROGRAM integer;
declare variable NUMER integer;
declare variable REPLICAT integer;
declare variable COMMITTRANSACTS smallint;
declare variable ORD smallint;
declare variable COMMITAFTEREACH smallint;
declare variable ID_OBJ integer;
declare variable ITEM_REF_ID integer;
declare variable PARENT_ID integer;
declare variable STABLE varchar(60);
declare variable FIELD varchar(64);
declare variable DEPENDS varchar(30);
declare variable SOURCE_ITEM integer;
declare variable TYP smallint;
declare variable NAME varchar(60);
declare variable TABLESET varchar(250);
declare variable MSTAFTERSET varchar(150);
declare variable MSTDOAFTER smallint;
declare variable DFCONST varchar(255);
declare variable REPKIND smallint;
declare variable DFAFTERVAL varchar(150);
declare variable DEPENDSNAME varchar(80);
declare variable DELDESTTABLE smallint;
declare variable SORDER varchar(255);
declare variable ITEMNUMER integer;
declare variable DTUPDATESQL varchar(255);
declare variable DFNOTEMPTY smallint;
declare variable DFCONDITION varchar(255);
declare variable ITEMORD smallint;
declare variable DTKEYFIELDS varchar(255);
declare variable SAFEPROCNAME varchar(32);
declare variable FROMCHARS varchar(20);
declare variable TOCHARS varchar(20);
declare variable PROCNUMBER integer;
declare variable I integer;
declare variable J integer;
declare variable POS integer;
declare variable MAXCHUNKSIZE integer;
declare variable NL varchar(2);
declare variable CH varchar(1);
declare variable sourcedbase varchar(20);
declare variable destdbase varchar(20);
begin
  nl = '
';
  FROMCHARS = 'ąęćłńóżźĄĆŁŃÓŻŹ() ';
  TOCHARS   = 'aeclnozzACLNOZZ___';

  pos = position('~', nazwa);
  if(pos>0) then
  begin
    sourcedbase = substring(nazwa from 1 for pos-1);
    nazwa = substring(nazwa from pos+1);
  end
  else exception universal 'Brak wymaganej nazwy bazy źródłowej';

  pos = position('~', nazwa);
  if(pos>0) then
  begin
    destdbase = substring(nazwa from 1 for pos-1);
    nazwa = substring(nazwa from pos+1);
  end
  else exception universal 'Brak wymaganej nazwy bazy docelowej';

  --generowanie jakiejs poprawnej nazwy procedurek transferowych
  i = 1;j = 0;
  safeprocname = 'RP_';
  while(i <= char_length(nazwa) and j < 27) do
  begin
    ch = substring(nazwa from i for 1);
    pos = position(ch, fromchars);
    if(pos>0) then
      ch = substring(tochars from pos for 1);
    if (ch similar to '[[:ALNUM:]_]') then
    begin
      safeprocname = safeprocname || ch;
      j = j + 1;
    end
    i = i + 1;
  end

  procnumber = 0;
  --uwaga, rozmiar musi być dłuższy niż dlugosc pojedynczej instrukcji dodawanej do ddl
  --inaczej numery nie beda mialy wlasnosci ciaglosci, z drugiej strony
  --podanie wartości zbyt bliskiej limitowi 65535 bajtów może spowodować przekroczenie zakresu
  --z trzeciej strony podanie zbyt malej wartosci spowoduje przekroczenie limitu 100 procedur czesciowych
  maxchunksize = 50000;

  DDL = 'SET TERM ^ ;';
  ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
  ||'as'||nl
  ||'begin';

  for
    select r.source_dbase, r.dest_dbase, r.name, r.replicat_type, r.params, r.sd_table, r.ei_type, r.overwrite,
        r.sd_filepath, r.s_obj, r.d_obj, r.ref_id, r.stransblock, r.dtransblock, r.signature
      from RP_REPLICAT r
        where r.name=:nazwa and r.source_dbase=:sourcedbase and r.dest_dbase=:destdbase
    into :source_dbase, :dest_dbase, :NAMEREP, :replicat_type, :PARAMSREP, :sd_table, :ei_type, :overwrite,
        :sd_filepath, :s_obj, :d_obj, :ref_id, :stransblock, :dtransblock, :rsignature
  do begin
    -- aktualizacja reguly replikacji
    DDL = ddl || nl || '    UPDATE OR INSERT INTO RP_REPLICAT (SOURCE_DBASE, DEST_DBASE, NAME, REPLICAT_TYPE, PARAMS, SD_TABLE, EI_TYPE, OVERWRITE, SD_FILEPATH,
    S_OBJ, D_OBJ, REF_ID, STRANSBLOCK, DTRANSBLOCK, signature)
    VALUES ('||coalesce(''''||:source_dbase||'''','null')||', '||coalesce(''''||:dest_dbase||'''','null')||', '||coalesce(''''||:NAMEREP||'''','null')||
    ', '||coalesce(:replicat_type,'null')||', '||coalesce(''''||:PARAMSREP||'''','null')||', '||coalesce(''''||:sd_table||'''','null')||', '||coalesce(:ei_type,'null')||
    ', '||coalesce(''''||:overwrite||'''','null')||', '||coalesce(''''||:sd_filepath||'''','null')||', '||coalesce(:s_obj,'null')||
    ', '||coalesce(:d_obj,'null')||', '||coalesce(:ref_id,'null')||', '||coalesce(:stransblock,'null')||', '||coalesce(:dtransblock,'null')||
    ','||coalesce(''''||:rsignature||'''','null')||') matching(SOURCE_DBASE, DEST_DBASE, NAME);';

    -- usuniecie wpisow z RP_PR_ITEM dla reguly
    ddl = ddl || nl || '    delete from RP_PR_ITEM where REPLICAT = 0'||:ref_id||';';

    -- zakladanie wpisow RP_PR_ITEM
    for
      select i.ref_id, i.program, i.numer, i.replicat, i.committransacts, i.ord, i.commitaftereach
        from RP_PR_ITEM i
          where i.replicat = :ref_id
          order by i.ref_id
      into :PR_ITEM_REF_ID, :program, :numer, :replicat, :committransacts, :ord, :commitaftereach
    do begin
      ddl = ddl || nl || '    INSERT INTO RP_PR_ITEM (REF_ID, PROGRAM, NUMER, REPLICAT, COMMITTRANSACTS, ORD, COMMITAFTEREACH)
        VALUES ('||coalesce(:PR_ITEM_REF_ID,'null')||', '||coalesce(:program,'null')||', '||coalesce(:numer,'null')||', '||coalesce(:replicat,'null')||
        ', '||coalesce(:committransacts,'null')||', '||coalesce(:ord,'null')||
        ', '||coalesce(:commitaftereach,'null')||');';

      i = div(char_length(ddl),maxchunksize);
      if(i > procnumber) then
      begin
        ddl = ddl || nl || 'end^';
        procnumber = i;
        ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
              ||'as'||nl
              ||'begin';
      end
    end
    -- usuniecie RP_ITEM
    ddl = ddl || nl ||  '    delete from RP_ITEM where ID_OBJ in ('||:s_obj||','||:d_obj||');';
    -- zakladanie RP_ITEM
    for
      select i.id_obj, i.ref_id, i.parent_id, i.stable, i.field, i.depends, i.source_item, i.typ, i.name, replace(i.tableset,'''',''''''), replace(i.mstafterset,'''','''''') ,
          i.mstdoafter, i.dfconst, i.repkind, i.dfafterval, i.dependsname, i.deldesttable, i.sorder, i.numer, replace(i.dtupdatesql,'''',''''''),
          i.dfnotempty, i.dfcondition, i.ord, i.dtkeyfields
        from RP_ITEM i
          where i.id_obj in (:s_obj, :d_obj)
          order by i.id_obj, i.ref_id
      into :id_obj, :ITEM_REF_ID, :parent_id, :stable, :field, :depends, :source_item, :typ, :name, :tableset, :mstafterset,
          :mstdoafter, :dfconst, :repkind, :dfafterval, :dependsname, :deldesttable, :sorder, :ITEMnumer, :dtupdatesql,
          :dfnotempty, :dfcondition, :ITEMord, :dtkeyfields
    do begin
        
      ddl = ddl || nl || '    INSERT INTO RP_ITEM (ID_OBJ, REF_ID, PARENT_ID, STABLE, FIELD, DEPENDS, SOURCE_ITEM, TYP, NAME, TABLESET, MSTAFTERSET, MSTDOAFTER, DFCONST, REPKIND,
        DFAFTERVAL, DEPENDSNAME, DELDESTTABLE, SORDER, NUMER, DTUPDATESQL, DFNOTEMPTY, DFCONDITION, ORD, DTKEYFIELDS)
        VALUES ('||coalesce(:id_obj,'null')||', '||coalesce(:ITEM_REF_ID,'null')||', '||coalesce(:parent_id,'null')||
          ', '||coalesce(''''||:stable||'''','null')||', '||coalesce(''''||:field||'''','null')||
          ', '||coalesce(''''||:depends||'''','null')||', '||coalesce(:source_item,'null')||', '||coalesce(:typ,'null')||
          ', '||coalesce(''''||:name||'''','null')||', '||coalesce(''''||:tableset||'''','null')||
          ', '||coalesce(''''||:mstafterset||'''','null')||', '||coalesce(:mstdoafter,'null')||
          ', '||coalesce(''''||:dfconst||'''','null')||', '||coalesce(:repkind,'null')||', '||coalesce(''''||:dfafterval||'''','null')||
          ', '||coalesce(''''||:dependsname||'''','null')||', '||coalesce(:deldesttable,'null')||', '||coalesce(''''||:sorder||'''','null')||
          ', '||coalesce(:ITEMnumer,'null')||', '||coalesce(''''||:dtupdatesql||'''','null')||
          ', '||coalesce(:dfnotempty,'null')||', '||coalesce(''''||:dfcondition||'''','null')||', '||coalesce(:ITEMord,'null')||
          ', '||coalesce(''''||:dtkeyfields||'''','null')||');';

      i = div(char_length(ddl),maxchunksize);
      if(i > procnumber) then
      begin
        ddl = ddl || nl || 'end^';
        procnumber = i;
        ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
              ||'as'||nl
              ||'begin';
      end
    end

    i = div(char_length(ddl),maxchunksize);
    if(i > procnumber) then
    begin
      ddl = ddl || nl || 'end^';
      procnumber = i;
      ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
            ||'as'||nl
            ||'begin';
    end
  end
  ddl = ddl || nl || 'end^';


  ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||nl
              ||'as'||nl
              ||'begin';
  i = 0;
  while (i<=procnumber) do
  begin
    ddl = ddl || nl || '    execute procedure '||safeprocname||(cast(i as varchar(2)))||';';
    i = i + 1;
  end
  ddl = ddl || nl || 'end^';


  ddl = ddl || nl || nl || 'SET TERM ; ^';

  ddl = ddl || nl || nl || 'execute procedure ' || safeprocname || ';' || nl || nl;

  ddl = ddl || 'drop procedure ' || safeprocname || ';';

   i = procnumber;
  while (i>=0) do
  begin
    ddl = ddl || nl || 'drop procedure '||safeprocname||(cast(i as varchar(2)))||';';
    i = i - 1;
  end

  ddl = ddl || nl || nl || 'commit work;' || nl;

  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_REPLICAT_DDL IS
'PR96748';

/* Following GRANT statements are generated automatically */

GRANT SELECT ON RP_REPLICAT TO PROCEDURE SYS_REPLICAT_DDL;
GRANT SELECT ON RP_PR_ITEM TO PROCEDURE SYS_REPLICAT_DDL;
GRANT SELECT ON RP_ITEM TO PROCEDURE SYS_REPLICAT_DDL;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_REPLICAT_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_REPLICAT_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_SEQUENCE_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_sequence_ddl (
  nazwa           varchar(255),
  create_or_alter smallint)
returns (
  ddl blob sub_type 1 segment size 80)
as
begin
  ddl = 'CREATE SEQUENCE '||:nazwa||';';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_SEQUENCE_DDL IS
'BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE sys_sequence_ddl TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE sys_sequence_ddl TO SENTE;
GRANT EXECUTE ON PROCEDURE sys_sequence_ddl TO SYSDBA;

COMMIT WORK;";

    public const string SYS_TRIGGER_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_trigger_ddl (
  nazwa           varchar(255),
  create_or_alter smallint)
returns (
  ddl blob sub_type 1 segment size 80)
as
declare variable coa      varchar(80);
declare variable source   blob sub_type 1        segment size 80;
declare variable params   blob sub_type 1        segment size 80;
declare variable descript blob sub_type 1        segment size 80;
declare variable qnazwa    varchar(255);
declare variable eol      varchar(2);
begin
eol = '
';
  coa = 'SET TERM ^;'||:eol;
  qnazwa=(select outname from sys_quote_reserved(:nazwa));
  coa = :coa || 'CREATE OR ALTER TRIGGER '||:qnazwa;
  select rdb$trigger_source, rdb$description from rdb$triggers
      where rdb$trigger_name = :nazwa
  into source, descript;
  if(create_or_alter = 1) then
  begin
    source = :source || 'begin'||:eol||'  suspend;'||:eol||'end^'||:eol||'SET TERM ; ^';
    suspend;
  end
  execute procedure SYS_TRIGGER_PARAMETERS(:nazwa) returning_values :params;
  ddl = :coa||' '||:params||:eol||:source;
  ddl = :ddl || '^'||:eol||'SET TERM ; ^';
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_TRIGGER_DDL IS
'PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO PROCEDURE SYS_TRIGGER_DDL;

GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_PARAMETERS TO PROCEDURE SYS_TRIGGER_DDL;

GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_INDEX_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_index_ddl (
  nazwa           varchar(255),
  create_or_alter smallint)
returns (
  ddl blob sub_type 1 segment size 80)
as
declare variable index_relation_name varchar(80);
declare variable index_unique_flag   smallint;
declare variable index_type          smallint;
declare variable index_inactive      smallint;
declare variable index_field         varchar(1024);
declare variable eol                 varchar(2);
declare variable index_expression    varchar(1024);
begin
  eol ='
  ';
  ddl = '';
  nazwa = substring(:nazwa from position('.' in :nazwa)+1);
  if(exists(select first 1 1 from rdb$relation_constraints where rdb$constraint_name = :nazwa)) then
    execute procedure sys_constraint_ddl(:nazwa, :create_or_alter) returning_values :ddl;
  else
  begin
    if(:create_or_alter = 1) then
      ddl = :ddl || 'DROP INDEX ' || :nazwa || ';' || :eol;

    ddl = :ddl || 'CREATE ';
    select i.rdb$relation_name, i.rdb$unique_flag, i.rdb$index_type, i.rdb$index_inactive, i.rdb$expression_source
      from rdb$indices i
      where i.rdb$index_name = :nazwa
      into :index_relation_name, :index_unique_flag, :index_type, :index_inactive, :index_expression;
    index_relation_name = trim(:index_relation_name);

    if(index_unique_flag = 1) then
      ddl = :ddl || 'UNIQUE ';

    if(index_type = 1) then
      ddl = :ddl || 'DESCENDING ';

    ddl = :ddl || 'INDEX ' || :nazwa || ' ON ' || :index_relation_name;

    /* Różne ddl dla indeksu na formule ... */
    if(:index_expression is not null) then
    begin
      ddl = :ddl || ' COMPUTED BY ' || :index_expression || ';';
    end
    else
    /* ... i dla indeksu na pola */
    begin
      ddl = :ddl || '(';
      for select s.rdb$field_name
        from rdb$index_segments s
        where s.rdb$index_name = :nazwa
        order by s.rdb$field_position
        into :index_field
      do begin
        index_field = substr(:index_field, 1, strpos(' ', :index_field) - 1);
        ddl = :ddl || :index_field ||', ';
      end
      ddl = substr(:ddl, 1, strlen(:ddl)-2);
      ddl = :ddl || ');';
    end
  end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_INDEX_DDL IS
'PR16135;BS46225;PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO PROCEDURE SYS_INDEX_DDL;

GRANT EXECUTE ON PROCEDURE SYS_INDEX_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_INDEX_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_INDEX_DDL TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_GETCOMMENT_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_getcomment (
  objectname varchar(255),
  objecttype integer)
returns (
  result blob sub_type 1 segment size 80)
as
declare variable tablename varchar(100);
declare variable descript  blob sub_type 1         segment size 80;
declare variable quotedobjectname varchar(255);
begin
  if(position('.' in :objectname) > 0) then
  begin
    tablename = substring(:objectname from 1 for position('.' in :objectname)-1);
    objectname = substring(:objectname from position('.' in :objectname)+1);
  end
  quotedobjectname = (select outname from sys_quote_reserved(:objectname));
  result = 'COMMENT ON ';
  if(objecttype = 1) then
  begin
    select rdb$description from rdb$fields where rdb$field_name = :objectname into :descript;
    result = result||'DOMAIN '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 2) then
  begin
    select rdb$description from rdb$generators where rdb$generator_name = :objectname into :descript;
    result = result||'SEQUENCE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 3) then
  begin
    select rdb$description from rdb$exceptions where rdb$exception_name = :objectname into :descript;
    result = result||'EXCEPTION '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 4) then
  begin
    select rdb$description from rdb$relations where rdb$relation_name = :objectname and (rdb$relation_type is NULL or rdb$relation_type in (0,4,5)) into :descript;
    result = result||'TABLE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 5) then
  begin
    select rdb$description from rdb$relation_fields where rdb$relation_name = :tablename
                                                          and rdb$field_name = :objectname and rdb$view_context is null into :descript;
    result = result||'COLUMN '||:tablename||'.'||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 6) then
  begin
    select rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
    result = result||'PROCEDURE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 7) then
  begin
    select rdb$description from rdb$relations where rdb$relation_name = :objectname and rdb$relation_type = 1 into :descript;
    result = result||'VIEW '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 8) then
  begin
    select rdb$description from rdb$triggers where rdb$trigger_name = :objectname into :descript;
    result = result||'TRIGGER '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 9) then
  begin
    select rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
    result = result||'PROCEDURE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 10 or objecttype = 11 or objecttype = 12 or objecttype = 13)  then
  begin
    select rdb$description from rdb$indices where rdb$index_name = :objectname and rdb$relation_name = :tablename into :descript;
    result = result||'INDEX '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 14) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 15) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 16) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 17) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 18) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;

  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_GETCOMMENT IS
'PR74079;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO PROCEDURE SYS_GETCOMMENT;

GRANT SELECT ON S_APPINI TO PROCEDURE SYS_GETCOMMENT;

GRANT EXECUTE ON PROCEDURE SYS_GETCOMMENT TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_GETCOMMENT TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_GETCOMMENT TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_PROCEDURE_PARAMETERS_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_PROCEDURE_PARAMETERS (
    NAZWA varchar(80))
returns (
    PARAMETERS blob sub_type 0 segment size 80)
as
declare variable PARAMETER_NAME varchar(80);
declare variable PARAMETER_DEFAULT varchar(80);
declare variable PARAMETER_TYPE smallint; /* 0-input, 1-output */
declare variable PARAMETER_NUMBER smallint;
declare variable PARAMETER_NULL_FLAG smallint;
declare variable PARAMETER_TYPE_NAME varchar(80); /* systemowa nazwa typu, np. DOUBLE, INT64 */
declare variable PARAMETER_LENGTH integer;
declare variable PARAMETER_CHARACTER_SET_NAME varchar(80);
declare variable PARAMETER_PRECISION integer;
declare variable PARAMETER_FIELD_SUB_TYPE integer;
declare variable PARAMETER_SCALE integer;
declare variable MAX0 integer; /* maksymalna pozycja parametru wejsciowego (licząc od 0) */
declare variable MAX1 integer; /* maksymalna pozycja parametru wyjsciowego (licząc od 0) */
declare variable P integer; /* flaga pozycji parametrow 1-pierwszy parametr in, 2-pierwszy parametr out */
declare variable EOL varchar(2);
declare variable COUNTIN smallint; /* liczba parametrów wejsciowych */
declare variable COUNTOUT smallint; /* liczba parametrow wyjsciowych */
declare variable FIELDS_FIELD_NAME varchar(31); /* nazwa typu użyta w definicji parametru (np.INTEGER_ID, jeżeli RDB$... to typ wbudowany) */
declare variable PARAMETER_MECHANISM smallint; /* 1-type of column*/
declare variable PARAMETER_SEGMENT_LENGTH smallint;
declare variable PARAMETER_RELATION_NAME varchar(31);
declare variable PARAMETER_FIELD_NAME varchar(31);
begin
  parameters = '';
  eol = '
  ';
  p = 0;

  if(exists (select first 1 1 from rdb$procedure_parameters where rdb$procedure_name = :nazwa)) then
  begin
    select coalesce(r.rdb$procedure_inputs,0), coalesce(r.rdb$procedure_outputs,0)
      from rdb$procedures r
      where r.rdb$procedure_name = :nazwa
    into :countin, :countout;

    max0 = :countin - 1;
    max1 = :countout - 1;

    for
      select p.rdb$parameter_name, p.rdb$parameter_type, p.rdb$parameter_number, f.rdb$field_name, coalesce(p.rdb$parameter_mechanism,0),
        t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision, f.rdb$field_scale,
        case when f.rdb$field_name like 'RDB$%' then b_longsubstr(f.rdb$default_source, 0, 80) else b_longsubstr(p.rdb$default_source, 0, 80) end,
        ch.rdb$character_set_name,  f.rdb$field_sub_type, f.rdb$segment_length, p.rdb$null_flag, p.rdb$relation_name, p.rdb$field_name
        from rdb$procedure_parameters p
          left join rdb$fields f on (p.rdb$field_source = f.rdb$field_name)
          left join rdb$types t on (f.rdb$field_type = t.rdb$type)
          left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
        where p.rdb$procedure_name = :nazwa
          and t.rdb$field_name = 'RDB$FIELD_TYPE'                 -- ograniczamy wynik dla typów danych
        order by p.rdb$parameter_type, p.rdb$parameter_number     -- kolejnosc parametrow: in przed out oraz wg nr pozycji
      into :parameter_name, :parameter_type, :parameter_number, :fields_field_name, :parameter_mechanism,
        :parameter_type_name, :parameter_length, :parameter_precision, :parameter_scale,
        :parameter_default,:parameter_character_set_name, :parameter_field_sub_type, :parameter_segment_length, :parameter_null_flag, :parameter_relation_name, :parameter_field_name
    do begin
      if(:p = 0) then
      begin
        if(:parameter_type = 0) then
          parameters = '(' || :eol;
        if(:countin = 0) then
          parameters = :parameters || :eol;
        p = 1;
      end

      if(:p = 1 and :parameter_type = 1) then
      begin
        parameters = :parameters || 'returns (' || :eol;
        p = 2;
      end

      parameter_name = trim(:parameter_name);
      parameter_name = (select outname from sys_quote_reserved(:parameter_name));

      if(parameter_character_set_name is not null and
        parameter_character_set_name='UTF8') then
        parameter_length = parameter_length / 4;
      if(parameter_character_set_name is not null and
        parameter_character_set_name='UNICODE_FSS') then
        parameter_length = parameter_length / 3;

      if(:parameter_mechanism = 1 and :parameter_relation_name is not NULL) then
      begin
        parameters = :parameters || '    ' || :parameter_name || ' type of column ' || trim(:parameter_relation_name) || '.' || trim(:parameter_field_name);
        if(parameter_null_flag = 1) then
          parameters = :parameters || ' NOT NULL';
      end
      else if(:fields_field_name like 'RDB$%') then -- typy wbudowane
      begin
        if(parameter_type_name = 'DATE') then
          parameters = :parameters || '    ' || :parameter_name || ' date';
        else if(parameter_type_name = 'DOUBLE') then
          parameters = :parameters || '    ' || :parameter_name || ' double precision';
        else if(parameter_type_name = 'FLOAT') then
          parameters = :parameters || '    ' || :parameter_name || ' float';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' bigint';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 1) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 2) then
          parameters = :parameters || '    ' || :parameter_name || ' decimal(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' integer';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 1) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 2) then
          parameters = :parameters || '    ' || :parameter_name || ' decimal(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'SHORT' and :parameter_precision = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' smallint';
        else if(parameter_type_name = 'SHORT' and :parameter_precision > 0) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'TEXT') then
          parameters = :parameters || '    ' || :parameter_name || ' char(' || :parameter_length || ')';
        else if(parameter_type_name = 'TIMESTAMP') then
          parameters = :parameters || '    ' || :parameter_name || ' timestamp';
        else if(parameter_type_name = 'VARYING') then
          parameters = :parameters || '    ' || :parameter_name || ' varchar(' || :parameter_length || ')';
        else if(parameter_type_name = 'TIME') then
          parameters = :parameters || '    ' || :parameter_name || ' time';
        else if(parameter_type_name = 'BLOB') then
          begin
            parameters = :parameters || '    ' || :parameter_name || ' blob sub_type ' || :parameter_field_sub_type;
            if(parameter_segment_length is not null) then
            parameters = :parameters || ' SEGMENT SIZE ' || :parameter_segment_length;
          end
        if(parameter_character_set_name is not null and parameter_character_set_name <> 'WIN1250') then
            parameters = :parameters || ' CHARACTER SET ' || :parameter_character_set_name;
        if(parameter_null_flag = 1) then
            parameters = :parameters || ' NOT NULL';
      end else                -- typy definiowane przez użytkownika
      begin
        fields_field_name = substr(:fields_field_name, 1, strpos(' ', :fields_field_name) - 1);
        parameters = :parameters || '    ' || :parameter_name || ' ';
        if(:parameter_mechanism = 1 and :parameter_relation_name is not NULL) then
          parameters = :parameters || ' type of column ' || trim(:parameter_relation_name) || '.' || trim(:parameter_field_name);
        parameters = :parameters || :fields_field_name;
        if(parameter_null_flag = 1) then
          parameters = :parameters || ' NOT NULL';
      end

      if(:parameter_default > '') then
        parameter_default = ' ' || :parameter_default;

      if((:parameter_type = 0 and :parameter_number = :max0) or (:parameter_type = 1 and :parameter_number = :max1)) then
        parameters = :parameters || :parameter_default || ')' || :eol;
      else
        parameters = :parameters || :parameter_default || ',' || :eol;
    end
  end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_PROCEDURE_PARAMETERS IS
'PR16135;BS46793;PR65161;BS87678;PR89881;BS104013;BS120353';

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO PROCEDURE SYS_PROCEDURE_DDL;
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO SENTELOGIN;
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO SYSDBA;
GRANT EXECUTE ON PROCEDURE SYS_PROCEDURE_PARAMETERS TO SYS_PROCEDURE_DDL;

COMMIT WORK;
";

    public const string SYS_TRIGGER_PARAMETERS_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_trigger_parameters (
  nazwa varchar(50))
returns (
  parameters blob sub_type 0 segment size 80)
as
declare variable trigger_type     integer;
declare variable trigger_relation char(31);
declare variable trigger_sequence integer;
declare variable trigger_inactive integer;
declare variable eol              varchar(2);
begin

/*
trigger_type:
  1  PRE_STORE (BEFORE INSERT)
  2  POST_STORE (AFTER INSERT)
  3  PRE_MODIFY (BEFORE UPDATE)
  4  POST_MODIFY  (AFTER UPDATE)
  5  PRE_ERASE  (BEFORE DELETE)
  6  POST_ERASE (AFTER DELETE)
  8192  CONNECT (ON CONNECT)
  8193  DISCONNECT (ON DISCONNECT)
  8194  TRANSACTION_START (ON TRANSACTION START)
  8195  TRANSACTION_COMMIT (ON TRANSACTION COMMIT)
  8196  TRANSACTION_ROLLBACK (ON TRANSACTION ROLLBACK)
*/

  parameters = '';
  eol = '
  ';

  if(exists (select first 1 1 from RDB$TRIGGERS where RDB$TRIGGER_NAME = :nazwa)) then
  begin
    select RDB$TRIGGER_TYPE, RDB$RELATION_NAME, RDB$TRIGGER_SEQUENCE, RDB$TRIGGER_INACTIVE
      from RDB$TRIGGERS
      where RDB$TRIGGER_NAME = :nazwa
      into :trigger_type, :trigger_relation, :trigger_sequence, :trigger_inactive;

      if(:trigger_type < 8192) then
        parameters = 'FOR ' || :trigger_relation;
      parameters = :parameters  || :eol;

      if(trigger_inactive = 1) then
        parameters = :parameters || 'INACTIVE ';
      else
        parameters = :parameters || 'ACTIVE ';

      if(trigger_type = 1) then
        parameters = :parameters || 'BEFORE INSERT ';
      else if(trigger_type = 2) then
        parameters = :parameters || 'AFTER INSERT ';
      else if(trigger_type = 3) then
        parameters = :parameters || 'BEFORE UPDATE ';
      else if(trigger_type = 4) then
        parameters = :parameters || 'AFTER UPDATE ';
      else if(trigger_type = 5) then
        parameters = :parameters || 'BEFORE DELETE ';
      else if(trigger_type = 6) then
        parameters = :parameters || 'AFTER DELETE ';
      else if(trigger_type = 17) then
        parameters = :parameters || 'BEFORE INSERT OR UPDATE ';
      else if(trigger_type = 18) then
        parameters = :parameters || 'AFTER INSERT OR UPDATE ';
      else if(trigger_type = 25) then
        parameters = :parameters || 'BEFORE INSERT OR DELETE ';
      else if(trigger_type = 26) then
        parameters = :parameters || 'AFTER INSERT OR DELETE ';
      else if(trigger_type = 27) then
        parameters = :parameters || 'BEFORE UPDATE OR DELETE ';
      else if(trigger_type = 28) then
        parameters = :parameters || 'AFTER UPDATE OR DELETE ';
      else if(trigger_type = 113) then
        parameters = :parameters || 'BEFORE INSERT OR UPDATE OR DELETE ';
      else if(trigger_type = 114) then
        parameters = :parameters || 'AFTER INSERT OR UPDATE OR DELETE ';
      else if(trigger_type = 8192) then
        parameters = :parameters || 'ON CONNECT ';
      else if(trigger_type = 8193) then
        parameters = :parameters || 'ON DISCONNECT ';
      else if(trigger_type = 8194) then
        parameters = :parameters || 'ON TRANSACTION START ';
      else if(trigger_type = 8195) then
        parameters = :parameters || 'ON TRANSACTION COMMIT ';
      else if(trigger_type = 8196) then
        parameters = :parameters || 'ON TRANSACTION ROLLBACK ';

      parameters = :parameters || 'POSITION ' || :trigger_sequence || ' ';
    end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_TRIGGER_PARAMETERS IS
'PR16135;BS46225;PR65161;BS87678;PR89881';

GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_PARAMETERS TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_PARAMETERS TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_TRIGGER_PARAMETERS TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_CONSTRAINT_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_CONSTRAINT_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
AS
declare variable CONSTRAINT_RELATION_NAME varchar(80);
declare variable CONSTRAINT_RELATION_PK_NAME varchar(80);
declare variable CONSTRAINT_RELATION_FK_NAME varchar(80);
declare variable CONSTRAINT_RELATION_FK_REF varchar(80);
declare variable CONSTRAINT_TYPE varchar(80);
declare variable CONSTRAINT_PK varchar(80);
declare variable CONSTRAINT_PK_INDEX varchar(80);
declare variable CONSTRAINT_NAME varchar(80);
declare variable CONSTRAINT_PK_TYPE smallint;
declare variable CONSTRAINT_FIELD varchar(1024);
declare variable CONSTRAINT_UPDATE_RULE varchar(80);
declare variable CONSTRAINT_DELETE_RULE varchar(80);
declare variable EOL varchar(2);
declare variable WYGENEROWANO smallint;
declare variable POS integer;
begin
    eol ='
    ';
    ddl = '';
    if(position('.' in :nazwa) > 0)then
    begin
    nazwa = substring(:nazwa from position('.' in :nazwa)+1);
    end
    select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name), trim(c.rdb$foreign_key)
      from rdb$relation_constraints r left join rdb$indices c on c.rdb$index_name=r.rdb$index_name
      where (r.rdb$constraint_name = :nazwa or r.rdb$index_name = :nazwa)
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name, constraint_relation_fk_ref;

    if(constraint_name is null and upper(nazwa) starting with 'PK_') then
    begin
      CONSTRAINT_RELATION_PK_NAME = substring(nazwa from 4);
       select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name)
      from rdb$relation_constraints r
      where r.rdb$relation_name = :CONSTRAINT_RELATION_PK_NAME and r.rdb$constraint_type='PRIMARY KEY'
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name;
    end

    if(constraint_name is null and upper(nazwa) starting with 'FK_') then
    begin
      CONSTRAINT_RELATION_FK_NAME = substring(nazwa from 4);
      pos = position('_',CONSTRAINT_RELATION_FK_NAME);
      if(pos<1) then
        exception universal 'Nie mozna wygenerować ddl dla wiezu '||NAZWA||', ponieważ' ||
          ' nie znaleziono pasującego';
      constraint_relation_fk_ref = substring(CONSTRAINT_RELATION_FK_NAME from pos+1);
      CONSTRAINT_RELATION_FK_NAME = substring(CONSTRAINT_RELATION_FK_NAME from 1 for pos-1);

      select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name), trim(c.rdb$foreign_key)
      from rdb$relation_constraints r left join rdb$indices c on c.rdb$index_name=r.rdb$index_name
      where r.rdb$relation_name = :CONSTRAINT_RELATION_FK_NAME
        and c.rdb$foreign_key starting with :constraint_relation_fk_ref
        and r.rdb$constraint_type='FOREIGN KEY'
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name, constraint_relation_fk_ref;
    end

    if(constraint_name is null) then exception universal 'Nie mozna wygenerować ddl dla więzu '
      ||NAZWA||', ponieważ go nie znaleziono';

    if(:create_or_alter = 1) then
      ddl = :ddl || 'ALTER TABLE ' || :constraint_relation_name || ' DROP CONSTRAINT ' || :nazwa || ';' || :eol;

    ddl = :ddl || 'ALTER TABLE ';
    constraint_type = substr(:constraint_type, 1, 11);
    if(constraint_type = 'UNIQUE') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || :constraint_name || ' UNIQUE (';
    else if(constraint_name starting with 'INTEG_' and nazwa starting with 'PK_') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || 'PK_' || :constraint_relation_name || ' ' || :constraint_type || ' (';
    else if(constraint_name starting with 'INTEG_' and nazwa starting with 'FK_') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' ||
        (iif(char_length('FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref)>31,
        substring(('FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref) from 1 for 31),
        'FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref
        )) || ' ' || :constraint_type || ' (';
    else
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || :constraint_name || ' ' || :constraint_type || ' (';

    for select trim(s.rdb$field_name)
      from rdb$index_segments s
      where s.rdb$index_name = :constraint_pk_index
      order by s.rdb$field_position
    into :constraint_field
    do begin
      constraint_field = (select outname from sys_quote_reserved(:constraint_field));
      ddl = :ddl || :constraint_field ||', ';
    end
    ddl = substr(:ddl, 1, strlen(:ddl)-2);

    select ref.rdb$const_name_uq, ref.rdb$update_rule, ref.rdb$delete_rule
      from rdb$ref_constraints ref
      where ref.rdb$constraint_name = :nazwa or ref.rdb$constraint_name = :constraint_name
      into :constraint_pk, :constraint_update_rule, constraint_delete_rule;

    if(constraint_type = 'FOREIGN KEY') then
    begin
      select r.rdb$relation_name
        from rdb$relation_constraints r
        where r.rdb$constraint_name = :constraint_pk
        into :constraint_relation_name;
      constraint_relation_name = substr(:constraint_relation_name, 1, strpos(' ', :constraint_relation_name) - 1);
      ddl = :ddl || ') REFERENCES ' || :constraint_relation_name || '(';

      wygenerowano = 0;
      for select trim(s.rdb$field_name)
        from rdb$index_segments s
        where s.rdb$index_name = :constraint_pk
        order by s.rdb$field_position
      into :constraint_field
      do begin
        constraint_field = (select outname from sys_quote_reserved(:constraint_field));
        ddl = :ddl || :constraint_field ||', ';
        wygenerowano = 1;
      end

      if(wygenerowano=0) then
      begin
        for select s.rdb$field_name
        from rdb$indices w
        join rdb$index_segments s on w.rdb$foreign_key=s.rdb$index_name
        where w.rdb$index_name = :nazwa or (w.rdb$index_name = :constraint_pk_index)
        order by s.rdb$field_position
        into :constraint_field
        do begin
          constraint_field = trim(:constraint_field);
          constraint_field = (select outname from sys_quote_reserved(:constraint_field));
          ddl = :ddl || :constraint_field ||', ';
          wygenerowano = 1;
        end
      end

      if(wygenerowano=0) then
      begin
        for select s.rdb$field_name
        from rdb$relation_constraints w
        join rdb$indices i on w.rdb$index_name=i.rdb$index_name
        join rdb$index_segments s on i.rdb$foreign_key=s.rdb$index_name
        where w.rdb$constraint_name = :nazwa
        order by s.rdb$field_position
        into :constraint_field
        do begin
          constraint_field = trim(:constraint_field);
          constraint_field = (select outname from sys_quote_reserved(:constraint_field));
          ddl = :ddl || :constraint_field ||', ';
          wygenerowano = 1;
        end
      end

      ddl = substr(:ddl, 1, strlen(:ddl)-2);
      ddl = :ddl || ')';

      constraint_delete_rule = substr(:constraint_delete_rule, 1, strpos('  ', :constraint_delete_rule) - 1);
      constraint_update_rule = substr(:constraint_update_rule, 1, strpos('  ', :constraint_update_rule) - 1);
      if(constraint_delete_rule <> 'RESTRICT') then
        ddl = :ddl || ' ON DELETE ' || constraint_delete_rule;
      if(constraint_update_rule <> 'RESTRICT') then
        ddl = :ddl || ' ON UPDATE ' ||  constraint_update_rule;

      if(constraint_name <> constraint_pk_index) then
      begin
          if(position('RDB$' in :constraint_pk_index)=0) then
            if(constraint_pk_type = 1) then
              ddl = :ddl || ' USING DESCENDING INDEX ' || :constraint_pk_index;
            else
              ddl = :ddl || ' USING INDEX ' || :constraint_pk_index;
      end
    end
    else if(constraint_type = 'PRIMARY KEY' or constraint_type = 'UNIQUE') then
    begin
      select i.rdb$index_type
        from rdb$relation_constraints r
        join rdb$indices i on r.rdb$index_name = i.rdb$index_name
        where r.rdb$index_name = :nazwa
        into :constraint_pk_type;

      constraint_pk_index = trim(constraint_pk_index);

      if(constraint_name <> constraint_pk_index) then
      begin
        if(position('RDB$' in :constraint_pk_index)>0) then
          ddl = :ddl || ') ';
        else
          if(constraint_pk_type = 1) then
            ddl = :ddl || ') USING DESCENDING INDEX ' || :constraint_pk_index;
          else
            ddl = :ddl || ') USING INDEX ' || :constraint_pk_index;
      end else
        ddl = :ddl || ')';
    end
    ddl = :ddl || ';';
  suspend;end^

SET TERM ; ^

COMMENT ON PROCEDURE SYS_CONSTRAINT_DDL IS
'PR16135;PR65161;BS87678;PR89881;BS99942;BS106224;';

/* Following GRANT statements are generated automatically */

GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO PROCEDURE SYS_CONSTRAINT_DDL;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO PROCEDURE SYS_INDEX_DDL;
GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO SENTELOGIN;
GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO SYSDBA;
GRANT EXECUTE ON PROCEDURE SYS_CONSTRAINT_DDL TO SYS_INDEX_DDL;";

    public const string SYS_GRANT_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure sys_grant_ddl (
  nazwa varchar(255),
  typ integer,
  grantfilter integer)
returns (
  ddl blob sub_type 1 segment size 80)
as
declare variable username       varchar(255);
declare variable grantor        varchar(255);
declare variable grantname      varchar(10);
declare variable eol            varchar(32);
declare variable objecttype     varchar(10);
declare variable usertype       smallint;
begin
    eol =';
';
    ddl = '';
    if(TYP=4 or TYP=6 or TYP=7) then begin -- 4 - tabele, 6-procedury, 7-widoki
        for select
           trim(rdb$user),
           trim(rdb$grantor),
           case rdb$privilege
               when 'S' then 'SELECT'
               when 'U' then 'UPDATE'
               when 'D' then 'DELETE'
               when 'I' then 'INSERT'
               when 'X' then 'EXECUTE'
               when 'R' then 'REFERENCES'
           end,
           case :TYP
               when 4 then ''
               when 6 then 'PROCEDURE'
               when 7 then ''
           end,
           rdb$user_type
       from rdb$user_privileges
       where rdb$relation_name = upper(:nazwa)
        into :username, :grantor, :grantname, :objecttype, :usertype
        do begin
            select outname from sys_quote_reserved(:username) into :username;

            if (grantfilter=0 or (grantfilter=1 and :usertype = 8 and (username='""SYSDBA""' or username='""SENTE""' or username='""SENTELOGIN""')) ) then
            ddl = :ddl || 'GRANT ' || :grantname || ' ON ' || :objecttype || ' ' || :nazwa || ' TO ' || :username || ' GRANTED by ' || :grantor || :eol;
        end
    end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_GRANT_DDL IS
'PR87682;BS87678;PR89881;PR97467';

GRANT EXECUTE ON PROCEDURE SYS_QUOTE_RESERVED TO PROCEDURE SYS_GRANT_DDL;

GRANT EXECUTE ON PROCEDURE SYS_GRANT_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_GRANT_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_GRANT_DDL TO SYSDBA;

COMMIT WORK;
";
    public const string SYS_HASHCODES_DDL_PATCH = @"
/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/


CREATE GENERATOR GEN_SYS_HASHCODES;

CREATE TABLE SYS_HASHCODES (
    ID        INTEGER NOT NULL,
    NAME      VARCHAR(255) NOT NULL,
    ""TYPE""    SMALLINT NOT NULL,
    HASHCODE  INTEGER NOT NULL
);


/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE SYS_HASHCODES ADD CONSTRAINT PK_SYS_HASHCODES PRIMARY KEY (ID);


/******************************************************************************/
/****                               Indices                                ****/
/******************************************************************************/

CREATE INDEX SYS_HASHCODES_NAME_TYPE ON SYS_HASHCODES (NAME, ""TYPE"");


/******************************************************************************/
/****                               Triggers                               ****/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/****                         Triggers for tables                          ****/
/******************************************************************************/



/* Trigger: SYS_HASHCODES_BI */
CREATE OR ALTER TRIGGER SYS_HASHCODES_BI FOR SYS_HASHCODES
ACTIVE BEFORE INSERT POSITION 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_sys_hashcodes,1);
end
^


SET TERM ; ^



/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

COMMENT ON TABLE SYS_HASHCODES IS 
'BS87678';



/******************************************************************************/
/****                         Fields descriptions                          ****/
/******************************************************************************/

COMMENT ON COLUMN SYS_HASHCODES.""TYPE"" IS 
'Zgodnie z enumem SHlib.DatabaseExtraction.DbType
None = 0,
    Domain,
    Sequence,
    Exception,
    Table,
    Field,
    Procedure,
    View,
    Trigger,
    TransferProcedure,
    PrimaryKey,
    UniqueKey,
    ForeignKey,
    Index,
    AppSection,
    XkProcedure';



/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/


/* Privileges of users */
GRANT ALL ON SYS_HASHCODES TO SENTE;

/* Privileges of triggers */
GRANT UPDATE, REFERENCES ON SYS_HASHCODES TO TRIGGER SYS_HASHCODES_BI;

COMMIT WORK;
";
    public const string SYS_TRHIST_DDL_PATCH = @"
/******************************************************************************/
/****                               Domains                                ****/
/******************************************************************************/

CREATE DOMAIN SYS_TRHIST_ID AS
INTEGER;

CREATE DOMAIN SYS_TRHIST_NAZWA AS
VARCHAR(32)
COLLATE PXW_PLK;

CREATE DOMAIN SYS_TRHIST_STAN AS
SMALLINT
DEFAULT 0
NOT NULL;

CREATE DOMAIN SYS_TRHIST_TEMAT AS
VARCHAR(32)
COLLATE PXW_PLK;

CREATE DOMAIN SYS_TRHIST_TIMESTAMP AS
TIMESTAMP;



/******************************************************************************/
/****                              Generators                              ****/
/******************************************************************************/

CREATE GENERATOR GEN_SYS_TRHIST;
SET GENERATOR GEN_SYS_TRHIST TO 0;



/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/



CREATE TABLE SYS_TRHIST (
    ID      SYS_TRHIST_ID NOT NULL,
    TEMAT   SYS_TRHIST_TEMAT,
    NAZWA   SYS_TRHIST_NAZWA,
    STAN    SYS_TRHIST_STAN,
    TIMEST  SYS_TRHIST_TIMESTAMP
);




/******************************************************************************/
/****                          Unique Constraints                          ****/
/******************************************************************************/

ALTER TABLE SYS_TRHIST ADD CONSTRAINT UNQ_SYS_TRHIST_NAZWA UNIQUE (NAZWA);


/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE SYS_TRHIST ADD CONSTRAINT PK_SYS_TRHIST PRIMARY KEY (ID);


/******************************************************************************/
/****                               Indices                                ****/
/******************************************************************************/

CREATE INDEX SYS_TRHIST_STAN ON SYS_TRHIST (STAN);
CREATE INDEX SYS_TRHIST_TEMAT ON SYS_TRHIST (TEMAT);
CREATE INDEX SYS_TRHIST_TIMEST ON SYS_TRHIST (TIMEST);


/******************************************************************************/
/****                               Triggers                               ****/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/****                         Triggers for tables                          ****/
/******************************************************************************/



/* Trigger: SYS_TRHIST_BI */
CREATE OR ALTER TRIGGER SYS_TRHIST_BI FOR SYS_TRHIST
ACTIVE BEFORE INSERT POSITION 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_sys_trhist,1);
end
^


SET TERM ; ^



/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

COMMENT ON DOMAIN SYS_TRHIST_ID IS 
'BS89147';

COMMENT ON DOMAIN SYS_TRHIST_NAZWA IS 
'BS89147';

COMMENT ON DOMAIN SYS_TRHIST_STAN IS 
'BS89147
0 - założona
1 - wgrana';

COMMENT ON DOMAIN SYS_TRHIST_TEMAT IS 
'BS89147';

COMMENT ON DOMAIN SYS_TRHIST_TIMESTAMP IS 
'BS89147';



/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

COMMENT ON TABLE SYS_TRHIST IS 
'BS89147';



/******************************************************************************/
/****                         Fields descriptions                          ****/
/******************************************************************************/

COMMENT ON COLUMN SYS_TRHIST.ID IS 
'BS89147';

COMMENT ON COLUMN SYS_TRHIST.TEMAT IS 
'BS89147';

COMMENT ON COLUMN SYS_TRHIST.NAZWA IS 
'BS89147';

COMMENT ON COLUMN SYS_TRHIST.STAN IS 
'BS89147
0 - założona
1 - wgrana';

COMMENT ON COLUMN SYS_TRHIST.TIMEST IS 
'BS89147';



/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/


/* Privileges of triggers */
GRANT UPDATE, REFERENCES ON SYS_TRHIST TO TRIGGER SYS_TRHIST_BI;

COMMIT WORK;
";
    public const string EMPTY_SCRIPT_DB = @"      
DECLARE EXTERNAL FUNCTION ""ABS""
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'IB_UDF_abs' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""ASCII_VAL""
    CHAR(1)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IB_UDF_ascii_val' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""BIN_AND""
    INTEGER,
    INTEGER
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IB_UDF_bin_and' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""BIN_OR""
    INTEGER,
    INTEGER
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IB_UDF_bin_or' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION B_LONGSUBSTR
    BLOB,
    INTEGER,
    INTEGER
    RETURNS CSTRING(16383) FREE_IT
    ENTRY_POINT 'fn_b_longsubstr' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION DATETOSTR
    TIMESTAMP,
    CSTRING(255)
    RETURNS CSTRING(255)
    ENTRY_POINT 'fn_datetostr' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION DIV
    INTEGER,
    INTEGER
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'IB_UDF_div' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""FLOOR""
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'IB_UDF_floor' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION I64ROUND
    NUMERIC(18,4) BY DESCRIPTOR,
    NUMERIC(18,4) BY DESCRIPTOR
    RETURNS PARAMETER 2
    ENTRY_POINT 'fbround' MODULE_NAME 'fbudf';


DECLARE EXTERNAL FUNCTION INCDATETIME
    TIMESTAMP,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER
    RETURNS TIMESTAMP
    ENTRY_POINT 'fn_incdatetime' MODULE_NAME 'rfunc';

DECLARE EXTERNAL FUNCTION INCDATE
   TIMESTAMP,
   INTEGER,
   INTEGER,
   INTEGER
RETURNS TIMESTAMP FREE_IT
ENTRY_POINT 'fn_incdate' MODULE_NAME 'rfunc';

DECLARE EXTERNAL FUNCTION LONGSTRREPLACE
    CSTRING(16384),
    CSTRING(16384),
    CSTRING(16384)
    RETURNS CSTRING(16384) FREE_IT
    ENTRY_POINT 'fn_longstrreplace' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION LONGSUBSTR
    CSTRING(16383),
    INTEGER,
    INTEGER
    RETURNS CSTRING(16383)
    ENTRY_POINT 'fn_substr' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION MD5SUM
    CSTRING(16383)
    RETURNS CSTRING(32) FREE_IT
    ENTRY_POINT 'fn_md5sum' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION ""MOD""
    INTEGER,
    INTEGER
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'IB_UDF_mod' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""POWER""
    DOUBLE PRECISION,
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'fn_power' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION ""RAND""

    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT 'IB_UDF_rand' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION ""ROUND""
    INTEGER BY DESCRIPTOR,
    INTEGER BY DESCRIPTOR
    RETURNS PARAMETER 2
    ENTRY_POINT 'fbround' MODULE_NAME 'fbudf';


DECLARE EXTERNAL FUNCTION RTRIM
    CSTRING(255)
    RETURNS CSTRING(255) FREE_IT
    ENTRY_POINT 'IB_UDF_rtrim' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION SHASH_HMAC
    CSTRING(6),
    CSTRING(25),
    CSTRING(1024),
    CSTRING(512)
    RETURNS CSTRING(512) FREE_IT
    ENTRY_POINT 'fb_shash_hmac' MODULE_NAME 'fb_shash';


DECLARE EXTERNAL FUNCTION SHASH_HMAC_BLOB
    CSTRING(6),
    CSTRING(25),
    BLOB,
    CSTRING(512)
    RETURNS CSTRING(512) FREE_IT
    ENTRY_POINT 'fb_shash_hmac_blob' MODULE_NAME 'fb_shash';


DECLARE EXTERNAL FUNCTION STRFUZZYCMP
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'SENTE_UDF_strfuzzycmp' MODULE_NAME 'sente_udf';


DECLARE EXTERNAL FUNCTION STRFUZZYCMP2
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(100),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'SENTE_UDF_strfuzzycmp2' MODULE_NAME 'sente_udf';


DECLARE EXTERNAL FUNCTION STRFUZZYCMP5
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(100),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'SENTE_UDF_strfuzzycmp5' MODULE_NAME 'sente_udf';


DECLARE EXTERNAL FUNCTION STRLEN
    CSTRING(32767)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IB_UDF_strlen' MODULE_NAME 'ib_udf';


DECLARE EXTERNAL FUNCTION STRMULTICMP
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'SENTE_UDF_strmulticmp' MODULE_NAME 'sente_udf';


DECLARE EXTERNAL FUNCTION STRMULTICMPAND
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'SENTE_UDF_strmulticmpand' MODULE_NAME 'sente_udf';


DECLARE EXTERNAL FUNCTION STRPOS
    CSTRING(16384),
    CSTRING(16384)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'fn_strpos' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION STRREPLACE
    CSTRING(256),
    CSTRING(256),
    CSTRING(256)
    RETURNS CSTRING(256) FREE_IT
    ENTRY_POINT 'fn_strreplace' MODULE_NAME 'rfunc';


DECLARE EXTERNAL FUNCTION SUBSTR
    CSTRING(1024),
    SMALLINT,
    SMALLINT
    RETURNS CSTRING(1024) FREE_IT
    ENTRY_POINT 'IB_UDF_substr' MODULE_NAME 'ib_udf';

DECLARE EXTERNAL FUNCTION R_ROUND
    INTEGER BY DESCRIPTOR,
    INTEGER BY DESCRIPTOR
    RETURNS PARAMETER 2
    ENTRY_POINT 'fbround' MODULE_NAME 'fbudf';

DECLARE EXTERNAL FUNCTION EXTRACTWEEKDAY
    DATE
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'fn_weekday' MODULE_NAME 'rfunc';


/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

DESCRIBE FUNCTION DATETOSTR
'PR45221';

DESCRIBE FUNCTION LONGSTRREPLACE
'PR20279';

DESCRIBE FUNCTION LONGSUBSTR
'PR22974';

DESCRIBE FUNCTION SHASH_HMAC
'PR37632';

DESCRIBE FUNCTION SHASH_HMAC_BLOB
'PR37632';

DESCRIBE FUNCTION STRFUZZYCMP
'PR67927';

DESCRIBE FUNCTION STRFUZZYCMP2
'PR67927';

DESCRIBE FUNCTION STRFUZZYCMP5
'PR67927';

/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/


CREATE GENERATOR IBE$VERSION_HISTORY_ID_GEN;

CREATE TABLE IBE$VERSION_HISTORY (
    IBE$VH_ID           INTEGER NOT NULL,
    IBE$VH_MODIFY_DATE  TIMESTAMP NOT NULL,
    IBE$VH_USER_NAME    VARCHAR(67),
    IBE$VH_OBJECT_TYPE  SMALLINT NOT NULL,
    IBE$VH_OBJECT_NAME  VARCHAR(67) NOT NULL,
    IBE$VH_HEADER       VARCHAR(32000),
    IBE$VH_BODY         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    IBE$VH_DESCRIPTION  BLOB SUB_TYPE 1 SEGMENT SIZE 80
);

CREATE TABLE IBE$SCRIPTS (
    IBE$SCRIPT_NAME           VARCHAR(64) CHARACTER SET UNICODE_FSS NOT NULL,
    IBE$SCRIPT_TYPE           VARCHAR(15) CHARACTER SET UNICODE_FSS NOT NULL,
    IBE$SCRIPT_SOURCE         BLOB SUB_TYPE 1 SEGMENT SIZE 1024,
    IBE$SCRIPT_BLR            BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_DESCRIPTION    BLOB SUB_TYPE 1 SEGMENT SIZE 1024,
    IBE$SCRIPT_STATE          VARCHAR(15) CHARACTER SET UNICODE_FSS DEFAULT 'INVALID' NOT NULL,
    IBE$SCRIPT_ACTION_ID      INTEGER,
    IBE$SCRIPT_FORM           BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_PARAM_HISTORY  BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_ADD_DATA       BLOB SUB_TYPE 1 SEGMENT SIZE 1024
);


/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE IBE$VERSION_HISTORY ADD PRIMARY KEY (IBE$VH_ID);


/******************************************************************************/
/****                               Indices                                ****/
/******************************************************************************/

CREATE INDEX IBE$SCRIPTS_BY_ACTION_ID ON IBE$SCRIPTS (IBE$SCRIPT_ACTION_ID);
CREATE UNIQUE INDEX IBE$SCRIPTS_BY_NAME ON IBE$SCRIPTS (IBE$SCRIPT_NAME);


/******************************************************************************/
/****                               Triggers                               ****/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/****                         Triggers for tables                          ****/
/******************************************************************************/



/* Trigger: IBE$VERSION_HISTORY_BI */
CREATE OR ALTER TRIGGER IBE$VERSION_HISTORY_BI FOR IBE$VERSION_HISTORY
ACTIVE BEFORE INSERT POSITION 0
AS
BEGIN
  IF (NEW.IBE$VH_ID IS NULL) THEN
    NEW.IBE$VH_ID = GEN_ID(IBE$VERSION_HISTORY_ID_GEN,1);
  NEW.IBE$VH_USER_NAME = USER;
  NEW.IBE$VH_MODIFY_DATE = 'NOW';
END
^


SET TERM ; ^

/******************************************************************************/
/****                              Procedures                              ****/
/******************************************************************************/

SET TERM ^;
CREATE OR ALTER PROCEDURE EALGORITHM_RUN(
    ID varchar(10),
    EM integer,
    PR integer,
    ABLOCK varchar(4096) = null)
returns (
    RET numeric(14,2))
 as
 begin
 execute statement :ablock into :ret;
 suspend; 
end^
SET TERM ; ^

/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/


/* Privileges of users */
GRANT ALL ON IBE$VERSION_HISTORY TO ""ADMIN"";
GRANT ALL ON IBE$VERSION_HISTORY TO SENTE;
GRANT ALL ON IBE$VERSION_HISTORY TO SENTELOGIN;
GRANT ALL ON IBE$SCRIPTS TO ""ADMIN"";
GRANT ALL ON IBE$SCRIPTS TO SENTE;
GRANT ALL ON IBE$SCRIPTS TO SENTELOGIN;
GRANT EXECUTE ON PROCEDURE EALGORITHM_RUN TO ""SENTE"" GRANTED by SYSDBA;
GRANT EXECUTE ON PROCEDURE EALGORITHM_RUN TO ""SYSDBA"" GRANTED by SYSDBA;
GRANT EXECUTE ON PROCEDURE EALGORITHM_RUN TO ""ADMIN"" GRANTED by SYSDBA;

COMMIT WORK;
";

    public const string IBE_COMPARE_SCRIPT = @"
execute ibeblock
as
begin
  cbb = 'execute ibeblock (LogMessage variant)
         as
         begin
           ibec_progress(LogMessage);
         end';

  SourceDB = ibec_CreateConnection(__ctInterBase, 'DBName=""{0}"";
                                   ClientLib=gds32.dll;
                                   User={5}; Password={6}; Names={7}; SqlDialect=3');
  TargetDB = ibec_CreateConnection(__ctInterBase, 'DBName=""{1}""; ClientLib=gds32.dll; User=""{2}""; Password=""{3}""; Names={8}; SqlDialect=3');
  try
    ibec_CompareMetadata(SourceDB, TargetDB, '{4}',
                         'ServerVersion=FB25;
						  OmitUDFs;
                          OmitChecks;
                          IgnoreColumnPositions;
                          IgnoreIBEObjects',
                         cbb);
  finally
    ibec_CloseConnection(TargetDB);
    ibec_CloseConnection(SourceDB);
  end;
end;
";

    public const string IBE_RECOMPILE_ALL = @"
execute ibeblock  
returns (ErrMessage varchar(8000))
as
begin
   ErrMessage = ibec_RecompileProcedure(0, '');
   if (ErrMessage <> '') then
     suspend;
   ErrMessage = ibec_RecompileTrigger(0, '');
   if (ErrMessage <> '') then
     suspend;
end;
";

    public const string SYS_TR_DATA_RESTORE = @"
SET TERM ^ ;

create or alter procedure SYS_TR_DATA_RESTORE (
    UUID char(16) character set OCTETS not null)
as
declare variable RELATION_KEY_NAMES varchar(255);
declare variable RELATION_KEY_VALUES varchar(255);
declare variable RELATION_NAME varchar(255);
declare variable RELATION_FIELD_SOURCE varchar(255);
declare variable RELATION_FIELD_TARGET varchar(255);
declare variable ROW_VALUE blob sub_type 1 segment size 8092;
declare variable RELATION_FIELD_TARGET_TYPE varchar(255);
declare variable UPDATE_QUERY_WHERE varchar(255) = '';
declare variable QUERY_STATEMENT blob sub_type 1 segment size 8092;
begin
    for select t.relation_key_names,
                t.relation_key_values,
                t.relation_name,
                t.relation_field_source,
                t.relation_field_target,
                t.""VALUE""
    from sys_tr_temp t where t.uuid = :uuid
    into :relation_key_names, :relation_key_values, :relation_name, :relation_field_source, :relation_field_target, :row_value
    do begin
        update_query_where = '';
        if (relation_field_target_type is null) then
            if (relation_field_target is not null) then
                select r.rdb$field_source from rdb$relation_fields r where r.rdb$relation_name = :relation_name and r.rdb$field_name = :relation_field_target into :relation_field_target_type;
            else
                select r.rdb$field_source from rdb$relation_fields r where r.rdb$relation_name = :relation_name and r.rdb$field_name = :relation_field_source into :relation_field_target_type;
    begin
            while (position('|', :relation_key_names) > 0) do begin
                update_query_where = :update_query_where || substring(:relation_key_names from 1 for (position('|', :relation_key_names)-1)) || '=';
                relation_key_names = substring(:relation_key_names from (position('|', :relation_key_names)+1));
                update_query_where = :update_query_where || '''' || substring(:relation_key_values from 1 for (position('|', :relation_key_values)-1)) || ''' and ';
                relation_key_values = substring(:relation_key_values from (position('|', :relation_key_values)+1));
            end
            if(position('|', :relation_key_names) = 0) then
                update_query_where = :update_query_where||:relation_key_names||'='''||:relation_key_values||'''';
        end

        if(update_query_where<> '') then
            if (relation_field_target is not null) then
                query_statement = 'update ' ||:relation_name||' set '||:relation_field_target||'=cast('''||:row_value||''' as '||:relation_field_target_type||') where '||:update_query_where;
            else
                query_statement = 'update '||:relation_name||' set '||:relation_field_source||'=cast('''||:row_value||''' as '||:relation_field_target_type||') where '||:update_query_where;
        else
            exception;
        execute statement :query_statement;
        delete from sys_tr_temp t where t.uuid = :uuid;
    end
end^

SET TERM; ^

COMMIT WORK;

GRANT SELECT, DELETE ON SYS_TR_TEMP TO PROCEDURE SYS_TR_DATA_RESTORE;

GRANT EXECUTE ON PROCEDURE SYS_TR_DATA_RESTORE TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_TR_DATA_BACKUP = @"
SET TERM ^ ;

create or alter procedure SYS_TR_DATA_BACKUP (
    RELATION_NAME varchar(255) not null,
    RELATION_FIELD_SOURCE varchar(255) not null,
    RELATION_FIELD_TARGET varchar(255))
returns (
    UUID char(16) character set OCTETS)
as
declare variable PRIMARY_KEY_FIELDS_NUMBER integer;
declare variable PRIMARY_KEY_CURRENT_ROW integer;
declare variable PRIMARY_KEY_FIELD_NAME varchar(255);
declare variable PRIMARY_KEY_NAMES varchar(255) = '';
declare variable PRIMARY_KEY_NAME varchar(255);
declare variable PRIMARY_KEY_CONSTRAINT varchar(255);
declare variable PRIMARY_KEY_VALUES varchar(255) = '';
declare variable ROW_VALUE blob sub_type 1 segment size 8092;
declare variable PRIMARY_KEY_QUERY varchar(255) = '';
begin
    select gen_uuid() from rdb$database into :uuid;

    begin
        select rc.rdb$constraint_name, rc.rdb$index_name
        from rdb$relation_constraints rc
        where rc.rdb$constraint_type = 'PRIMARY KEY' and rc.rdb$relation_name = :relation_name
        into :primary_key_constraint, :primary_key_name;

        select count(s.rdb$field_name)
        from rdb$index_segments s
        where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
        into :primary_key_fields_number;
        PRIMARY_KEY_CURRENT_ROW = 1;

        if(primary_key_fields_number = 0) then begin
            exception;
        end
        else begin
            for select s.rdb$field_name
            from rdb$index_segments s
            where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
            order by s.rdb$field_position
            into :PRIMARY_KEY_FIELD_NAME
            do begin
                PRIMARY_KEY_FIELD_NAME = trim(PRIMARY_KEY_FIELD_NAME);
                PRIMARY_KEY_NAMES = :PRIMARY_KEY_NAMES || :PRIMARY_KEY_FIELD_NAME;
                PRIMARY_KEY_QUERY = :PRIMARY_KEY_QUERY || :PRIMARY_KEY_FIELD_NAME;
                if(primary_key_current_row < primary_key_fields_number) then begin
                    PRIMARY_KEY_NAMES = :PRIMARY_KEY_NAMES || '|';
                    PRIMARY_KEY_QUERY = :PRIMARY_KEY_QUERY || '||''|''||';
                end
                primary_key_current_row = :primary_key_current_row + 1;
            end
        end
    end

    begin
        for execute statement 'select '||:relation_field_source||','||:primary_key_query||' from '||:relation_name
        into :row_value, :primary_key_values
        do begin
         insert into sys_tr_temp (UUID,RELATION_KEY_NAMES,RELATION_KEY_VALUES,RELATION_NAME,RELATION_FIELD_SOURCE,RELATION_FIELD_TARGET,""VALUE"")
         values(:uuid,:primary_key_names,:primary_key_values,:relation_name,:relation_field_source,:relation_field_target,:row_value);
    end
end
  suspend;
end^

SET TERM; ^

COMMIT WORK;

GRANT INSERT ON SYS_TR_TEMP TO PROCEDURE SYS_TR_DATA_BACKUP;

GRANT EXECUTE ON PROCEDURE SYS_TR_DATA_BACKUP TO SYSDBA;

COMMIT WORK;
";

    public const string SYS_TR_TEMP = @"
/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/


CREATE GENERATOR GEN_SYS_TR_TEMP;

CREATE TABLE SYS_TR_TEMP (
    REF          BIGINT NOT NULL,
    TRANSFER_ID  VARCHAR(32),
    IDX_INT      BIGINT,
    IDX_STRING   VARCHAR(255),
    DATA         VARCHAR(4000)
);




/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE SYS_TR_TEMP ADD CONSTRAINT PK_SYS_TR_TEMP PRIMARY KEY (REF);


/******************************************************************************/
/****                               Indices                                ****/
/******************************************************************************/

CREATE INDEX SYS_TR_TEMP_IDX_ID ON SYS_TR_TEMP (TRANSFER_ID);
CREATE INDEX SYS_TR_TEMP_IDX_INT ON SYS_TR_TEMP (IDX_INT);
CREATE INDEX SYS_TR_TEMP_IDX_STRING ON SYS_TR_TEMP (IDX_STRING);


/******************************************************************************/
/****                               Triggers                               ****/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/****                         Triggers for tables                          ****/
/******************************************************************************/



/* Trigger: SYS_TR_TEMP_BI */
CREATE OR ALTER TRIGGER SYS_TR_TEMP_BI FOR SYS_TR_TEMP
ACTIVE BEFORE INSERT POSITION 0
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_sys_tr_temp,1);
end
^


SET TERM ; ^



/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

COMMENT ON TABLE SYS_TR_TEMP IS 
'PR88509';



/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/

COMMENT ON TRIGGER SYS_TR_TEMP_BI IS 
'PR88509';

COMMIT WORK;
";

    public const string SYS_CHECK_DDL_PATCH = @"
SET TERM ^ ;

create or alter procedure SYS_CHECK_DDL (
    NAZWA varchar(255),
    CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable CONSTRAINT_RELATION_NAME varchar(80);
declare variable TRIGGER_SOURCE blob sub_type 1 segment size 80;
declare variable EOL varchar(2);
begin
    eol ='
    ';
    ddl = '';
    for select distinct cast(t.rdb$trigger_source as varchar(8191)), trim(rc.rdb$relation_name)
    from rdb$relation_constraints rc
      join rdb$check_constraints cc on rc.rdb$constraint_name = cc.rdb$constraint_name
      join rdb$triggers t on cc.rdb$trigger_name = t.rdb$trigger_name
    where rc.rdb$constraint_type = 'CHECK'
      and t.rdb$trigger_type = 1
      and rc.rdb$constraint_name not starting with 'ibe$'
      and rc.rdb$relation_name = :nazwa
    order by cast(t.rdb$trigger_source as varchar(8191))
    into :trigger_source, :CONSTRAINT_RELATION_NAME
    do begin
      if(ddl<>'') then
        ddl = ddl || eol;
      ddl = ddl || 'ALTER TABLE '||CONSTRAINT_RELATION_NAME||' ADD '||trigger_source||';';
    end

    suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_CHECK_DDL IS
'PR88488';

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SYS_CHECK_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_CHECK_DDL TO SENTE;
GRANT EXECUTE ON PROCEDURE SYS_CHECK_DDL TO SYSDBA;

COMMIT WORK;
";


    public const string SYS_ROLE_DDL_PATCH = @"
SET TERM ^ ;

CREATE OR ALTER PROCEDURE SYS_ROLE_DDL (
  NAZWA varchar(255),
  CREATE_OR_ALTER smallint)
returns (
    DDL blob sub_type 1 segment size 80)
as
declare variable ROLENAME varchar(31);
declare variable OWNERNAME varchar(31);
declare variable FLAG smallint;
declare variable EOL varchar(2);
begin
  eol ='
    ';
  ddl = '';
  for select r.rdb$role_name, r.rdb$owner_name, r.rdb$system_flag
  from rdb$roles r
  where r.rdb$role_name = :nazwa
    and r.rdb$role_name NOT STARTING WITH 'RDB$'
  into :rolename, :ownername, :flag
  do begin
   if(ddl<>'') then
        ddl = ddl || eol;
   ddl = ddl || 'update or insert into rdb$roles (RDB$ROLE_NAME, RDB$OWNER_NAME, RDB$DESCRIPTION, RDB$SYSTEM_FLAG)
   values ('''||TRIM(:rolename)||''', '''||TRIM(:ownername)||''', '||'null'||', '||:flag||') matching(RDB$ROLE_NAME);';
  end
  suspend;
end^

SET TERM ; ^

COMMIT WORK;

COMMENT ON PROCEDURE SYS_ROLE_DDL IS
'BS104035';

GRANT EXECUTE ON PROCEDURE SYS_ROLE_DDL TO SYSDBA;
GRANT EXECUTE ON PROCEDURE SYS_CHECK_DDL TO ""ADMIN"";
GRANT EXECUTE ON PROCEDURE SYS_CHECK_DDL TO SENTE;

COMMIT WORK;
";
  }
}
