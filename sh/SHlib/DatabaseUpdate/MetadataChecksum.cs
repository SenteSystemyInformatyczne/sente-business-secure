﻿using FirebirdSql.Data.FirebirdClient;
using SHDataContracts.Enums;
using SHlib.DataExtraction;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SHlib.DatabaseUpdate
{
  public class MetadataChecksum
  {
    public static void UpdateHashCodeSelectedObject(DatabaseExtractor de, List<Metadata> filteredScripts, ProgressInfo pi, Action<string> AppendMessage)
    {
      AppendMessage("Aktualizacja haszy zmienionych obiektów");
      AppendMessage("Pobieranie listy obiektów z db");
      var list = de.SearchAll(pi);

      AppendMessage("Wyfiltrowanie tylko interesujących nasz obiektów");
      List<Metadata> result = list.Where(x => filteredScripts.Contains(x, new ScriptComparer())).ToList();

      AppendMessage("Wylicznie hashcode i aktualizacja SYS_Hashcode");
      UpdateHashes(de.GetConnectionType(), CalculateHashes(pi, result), pi);

      AppendMessage("Koniec");
    }
    internal static IEnumerable<Metadata> CalculateAllHashes(DatabaseExtractor de, ProgressInfo pi, Action<string> AppendMessage)
    {
      DatabasePatch.CheckSHStructuresIntegrity(pi, AppendMessage, updateHashes: false);
      AppendMessage("Obliczanie hashy wszystkich obiektów z bazy danych "+de.GetConnection(true).Name+" (jednorazowo)");
      var res = de.ExtractAllObjects(pi, false);

      return CalculateHashes(pi, res);
    }

    public static string PrepareDDL(string ddl)
    {
      return Regex.Replace(ddl, @"\s+", "").ToLower();
    }

    internal static IEnumerable<Metadata> CalculateHashes(ProgressInfo pi, List<Metadata> res)
    {
      pi.ReportCount(res.Count, "Obliczanie hashy");
      var crc32 = new Crc32();
      List<Metadata> result = new List<Metadata>();
      try
      {
        var processorCount = Environment.ProcessorCount;
        int maxDegreeOfParallelism;
        if (!int.TryParse(Connection.Instance.GetConnection(ConnectionType.Source).ConnectionPoolSize, out maxDegreeOfParallelism))
          maxDegreeOfParallelism = processorCount;

        result = res.AsParallel().WithDegreeOfParallelism(maxDegreeOfParallelism).Select(me =>
        {
          me.CRC = crc32.ComputeChecksum(Encoding.UTF8.GetBytes(PrepareDDL(me.DDL)));

          //if (me.DbType == DbTypes.AppSection)
          //{
          //  Console.WriteLine(">>>" + MetadataChecksum.PrepareDDL(me.DDL) + "<<<");
          //  Console.WriteLine(">>>" + Encoding.UTF8.GetBytes(MetadataChecksum.PrepareDDL(me.DDL)).Aggregate("", (acc, e) => acc + " " + string.Format("{0:x}", e)) + "<<<");
          //  Console.WriteLine(">>>" + me.CRC + "<<<");
          //}

          pi.ReportProgress();
          return me;
        }).ToList();

      }catch (AggregateException ae)
      {
        foreach (var ex in ae.InnerExceptions)
        {
          pi.ReportError(ex);
        }
      }
      catch (Exception e)
      {
        pi.ReportError(e);
      }

      pi.ReportEnd();

      return result;
    }

    internal static void UpdateHashes(ConnectionType connection, IEnumerable<Metadata> res, ProgressInfo pi, bool clearAllHash = false)
    {
      const string INSERT_OR_UPDATE_HASH = @"
UPDATE OR INSERT INTO SYS_HASHCODES (NAME, ""TYPE"", HASHCODE)
                             VALUES (@name, @type, @hashcode)
                           MATCHING (NAME, ""TYPE"");
";
      const string UPDATE_HASH = @"
UPDATE SYS_HASHCODES SET HASHCODE=@hashcode WHERE ID=@id;
";

      const string DELETE_HASH = @"DELETE FROM SYS_HASHCODES;";
      pi.ReportCount(res.Count(), "Update haszy w tabeli SYS_HASHCODES");

      using (var db = new Database(Connection.Instance.GetConnection(connection)))
      {

        Dictionary<string, object> paramz = new Dictionary<string, object>();
        using (FbConnection fbc = db.GetConnection(true))
        {
          fbc.Open();
          using (FbCommand fbcom = fbc.CreateCommand())
          {
            if (clearAllHash)
            {
              db.ExecuteStatement(DELETE_HASH, paramz, fbcom);
            }

            foreach (var me in res)
            {
              paramz["id"] = me.ID;
              paramz["name"] = me.Name;
              paramz["type"] = (int) me.DbType;
              if (me.CRC == null)
                throw new ArgumentException("Nie policzone crc");
              paramz["hashcode"] = (int) me.CRC;

              if (me.ID != null)
                db.ExecuteStatement(UPDATE_HASH, paramz, fbcom);
              else
                db.ExecuteStatement(INSERT_OR_UPDATE_HASH, paramz, fbcom);
              pi.ReportProgress();
            }
          }
        }
        pi.ReportEnd();
      }
    }

    /// <summary>
    /// Metoda wywołująca przelcizenie i zapisanie sum kontrolnych wszystkich obiektów na podanej bazie danych.
    /// </summary>
    /// <param name="connection">Połączenie do bazy danych</param>
    /// <param name="pi">Obiekt raportowania postępu</param>
    /// <param name="AppendMessage">Funkcja wypisywania komunikatów</param>
    /// <param name="ErrorMessage">Funckja wypisywania błędów</param>
    public static void UpdateAllHashesInDatabase(ConnectionType connection, ProgressInfo pi, Action<string> AppendMessage, Action<Exception> ErrorMessage, bool grantFilter)
    {
      var res = CalculateAllHashes(new DatabaseExtractor(connection, AppendMessage, ErrorMessage, grantFilter), pi , AppendMessage);
      UpdateHashes(connection, res, pi, true);
    }
  }
}
