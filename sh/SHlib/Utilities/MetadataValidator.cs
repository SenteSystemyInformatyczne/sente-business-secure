﻿using SHlib.DataExtraction;
using SHlib.DataExtraction.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static SHlib.DataExtraction.MetadataExtractor;

namespace SHlib.Utilities
{
  // Klasa wykorzystywana do validacji metadanych wczytanych z plików jak i z DB
  public class MetadataValidator
  {
    private ProgressInfo pi;

    public MetadataValidator(ProgressInfo pi)
    {
      this.pi = pi;
    }

    /// <summary>
    /// Metoda uruchamia walidację dla widoków pochodzących z BD
    /// </summary>
    /// <param name="dbe"></param>
    public bool ValidateViewsDBMetadata(DatabaseExtractor dbe)
    {
      bool result = true;

      pi.Write("Rozpoczynam weryfikację widoków w bazie danych.");
      List<Metadata> viewList = dbe.SearchAllForTypeObject(SHDataContracts.Enums.DbTypes.View, pi);

      foreach (Metadata view in viewList)
      {
        if (!view.ValidateDDL())
        {
          pi.Write($"#warn#Widok {view.Name} jest błędny");
          result = false;
        }
        // Validacja znanych błędów w widokach.
        string msg = "";
        if (!ValidateCustomViews(view, ref msg))
        {
          pi.Write($"#warn#{msg}");
          result = false;
        }
      }

      if (!result)
        pi.Write(@"#warn#Weryfikacja widoków zakończona błędem. Powyższe widoki zawierają w kodzie 'Select * from' lub 'alias.*' co może powodować błędy podczas aktualizacji bazy danych.
Zaloguj się do BD i popraw powyższe widoki.");

      return result;
    }

    public async Task<bool> ValidateViewsFileMetadata(FilesystemExtractor fe, string srcPath)
    {
      bool result = true;

      pi.Write($"Rozpoczynam weryfikację skryptów: {srcPath}");

      srcPath = srcPath + Path.DirectorySeparatorChar;

      var CurrentCancellationToken = new CancellationTokenSource();
      List<Metadata> viewList = await Task.Run(() => fe.ParseFiles("", srcPath, pi, TrMode.Default), CurrentCancellationToken.Token);

      viewList = viewList.Where(m => m.DbType == SHDataContracts.Enums.DbTypes.View).ToList();

      foreach (Metadata view in viewList)
      {
        if (!view.ValidateDDL())
        {
          pi.Write($"#warn#Widok {view.Name} jest błędny");
          result = false;
        }
        // Validacja znanych błędów w widokach.
        string msg = "";
        if (!ValidateCustomViews(view, ref msg))
        {
          pi.Write($"#warn#{msg}");
          result = false;
        }
      }

      if (!result)
        pi.Write(@"#warn#Weryfikacja widoków zakończona błędem. Powyższe widoki zawierają w kodzie 'Select * from' lub 'alias.*' co może powodować błędy podczas aktualizacji bazy danych.
#warn#Popraw widoki w skryptach a następnie uruchom proces ponownie.");

      return result;
    }

    /// <summary>
    /// Metoda weryfikuje zduplikowane pola bez aliasów w znanych widokach.
    /// SZKOLY, ZUS, VIEW_STANREJ
    /// </summary>
    /// <param name="view"></param>
    /// <returns></returns>
    private bool ValidateCustomViews(Metadata view, ref string msg)
    {
      bool result = true;

      if (view.Name == "SZKOLY")
      {
        msg = "Widok SZKOLY zawiera następujące pola bez aliasu: ";
        if (CheckExistString(view.DDL, ",fromdate"))
        {
          msg += "FROMDATE ";
          result = false;
        }
        if (CheckExistString(view.DDL, ",todate"))
        {
          msg += "TODATE ";
          result = false;
        }
      }
      else if (view.Name == "ZUS")
      {
        msg = "Widok ZUS zawiera następujące pola bez aliasu: ";
        if (CheckExistString(view.DDL, ",fromdate"))
        {
          msg += "FROMDATE";
          result = false;
        }
      }
      else if (view.Name == "VIEW_STANREJ")
      {
        msg = "Widok VIEW_STANREJ zawiera następujące pola bez aliasu: ";
        if (CheckExistString(view.DDL, ",operator"))
        {
          msg += "OPERATOR";
          result = false;
        }
      }
                 
      return result;
    }

    /// <summary>
    /// Metoda weryfikuje czy w przekazanym ddl występuje ciąg znaków str.
    /// </summary>
    /// <param name="ddl"></param>
    /// <param name="str"></param>
    /// <returns></returns>
    private bool CheckExistString(string ddl, string str)
    {
      var script = ddl.ToLower();
      // Wycinam wszystkie teksty
      script = Tools.RemoveTextBetween(script, "'", "'");
      // Analizujemy tylko tekst od pierwszego selecta.
      script = script.Substring(script.IndexOf("select "));
      // Wycinam wszystkie białe znaki
      script = new string(script.Where(c => !char.IsWhiteSpace(c)).ToArray());

      return script.Contains(str.ToLower());
    }

  }
}
