﻿using SHlib.DataExtraction;
using SHlib.DataExtraction.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.Utilities
{
  /// <summary>
  /// Komparator Metadanych
  /// </summary>
  class ScriptComparer : IEqualityComparer<Metadata>
  {

    #region IEqualityComparer<Metadata> Members

    public bool Equals(Metadata x, Metadata y)
    {
      return x.Name.Replace(":", "_").Replace("/", "^") == y.Name.Replace(":", "_").Replace("/", "^") && x.DbType == y.DbType;
    }

    public int GetHashCode(Metadata obj)
    {
      return (obj.DbType.ToString() + "#" + obj.Name.Replace(":", "_").Replace("/", "^")).GetHashCode();
    }

    #endregion
  }

  /// <summary>
  /// Komparator plików Metadanych
  /// </summary>
  class FileComparer : IEqualityComparer<Metadata>
  {

    #region IEqualityComparer<Metadata> Members

    public bool Equals(Metadata x, Metadata y)
    {
      return x.FilePath == y.FilePath;
    }

    public int GetHashCode(Metadata obj)
    {
      return obj.FilePath.GetHashCode();
    }

    #endregion
  }
}
