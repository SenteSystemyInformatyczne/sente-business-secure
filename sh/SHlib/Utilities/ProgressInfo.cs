﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.Utilities
{
  /// <summary>
  /// Obiekt informujący o postępie w procesach
  /// </summary>
  public class ProgressInfo
  {
    public const double REFRESHFACTOR = 0.01;

    /// <summary>
    /// Wrapper na MessageBox.Show z Window.Forms
    /// bez typów stamtąd aby nie mieszać na linuksie
    /// string text,
    /// string caption,
    /// MessageBoxButtons buttons,
    /// MessageBoxIcon icon,
    /// MessageBoxDefaultButton defaultButton
    /// </summary>
    public Func<string, string, int, int, int, int> MessageBoxForUUIDMismatch
    {
      get;
      set;
    }

    public Action<string> Write
    {
      get;
      set;
    }

    public Action<int> WriteCount
    {
      get;
      set;
    }

    public Action<int, int, string> WriteProgress
    {
      get;
      set;
    }

    public int RefreshStep
    {
      get;
      set;
    }

    public string Phase
    {
      get;
      set;
    }

    private DateTime lastReport;
    private int lastStep;
    private object _lock = 1;

    public void ReportCount(int cnt, string phase)
    {
      Count = cnt;
      if (WriteCount != null)
        WriteCount(cnt);
      lastReport = DateTime.MinValue;
      lastStep = int.MinValue;
      RefreshStep = (int) (Count * REFRESHFACTOR);
      Phase = phase;
      Counter = 0;
    }

    public void ReportProgress()
    {
      lock (_lock)
      {
        Counter++;
        if (Counter > lastStep + RefreshStep)
        {
          var now = DateTime.Now;
          if ((now - lastReport).TotalMilliseconds > RefreshRate)
          {
            if (WriteProgress != null)
              WriteProgress(Counter, Count, Phase);
            lastReport = now;
          }
          lastStep = Counter;
        }
      }
    }

    public void ReportEnd()
    {
      if (WriteProgress != null && Count > 0)
        WriteProgress(Count, Count, Phase);
    }

    public int Count
    {
      get;
      set;
    }

    public int Counter
    {
      get;
      set;
    }

    public int RefreshRate
    {
      get;
      set;
    }

    public ProgressInfo()
    {
      RefreshRate = 1000;
    }

    internal void ReportError(Exception e)
    {
      if (Write != null)
      {
        var msg = "";
        if (e.Data.Contains("message"))
          msg = e.Data["message"] + "\n";
        msg += e.Message;
        Write(msg);
      }
    }
  }

}
