﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace SHlib
{
  /// <summary>
  /// Klasa do szyfrowania haseł
  /// </summary>
    public class Crypt
    {
        private string _key;

        public string Key
        {
            get { return _key; }
            set 
            { 
                _key = value; 
                _bytes= ASCIIEncoding.ASCII.GetBytes(_key);
            }
        }

        private byte[] _bytes;

        private static Crypt _instance;
        public static Crypt Instance
        {
            get
            {
                return _instance;
            }
        }
        static Crypt()
        {
            _instance = new Crypt();
        }

        private Crypt()
        {
            Key = "ZeroCool";
        }


        public string Encrypt(string originalString)
        {
            try
            {
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(_bytes, _bytes), CryptoStreamMode.Write);
                StreamWriter writer = new StreamWriter(cryptoStream);
                writer.Write(originalString);
                writer.Flush();
                cryptoStream.FlushFinalBlock();
                writer.Flush();
                return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string Decrypt(string cryptedString)
        {
            try
            {
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(_bytes, _bytes), CryptoStreamMode.Read);
                StreamReader reader = new StreamReader(cryptoStream);
                return reader.ReadToEnd();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
