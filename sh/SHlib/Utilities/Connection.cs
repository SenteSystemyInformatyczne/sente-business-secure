﻿using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHlib.DataExtraction;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SHlib.Utilities
{
  public class Connection
  {
    private Dictionary<ConnectionType, IDbSettings> connectionSettings;

    private static Connection _instance;

    private Connection()
    {
      connectionSettings = new Dictionary<ConnectionType, IDbSettings>();
      foreach (var ct in Enum.GetValues(typeof(ConnectionType)).Cast<ConnectionType>())
      {
        connectionSettings.Add(ct, new DbSettings(ct));
      }
    }

    public IDbSettings GetConnection(ConnectionType connection)
    {
      return connectionSettings[connection];
    }

    public IDbSettings SetConnection(ConnectionType connection, IDbSettings settings)
    {
      connectionSettings[connection] = settings;
      return connectionSettings[connection];
    }

    public static Connection Instance
    {
      get
      {
        if (_instance == null)
          _instance = new Connection();
        return _instance;
      }
    }
  }
}
