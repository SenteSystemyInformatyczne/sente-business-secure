﻿using Newtonsoft.Json;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHDataContracts.Model.AppSection;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SHlib.DataExtraction.Utilities
{
  public static class AppSectionUpgrader
  {
    private static Regex matchAppSectionName = new Regex(@"\[(.+)\]", RegexOptions.Compiled);
    private static Regex matchAppSectionElements = new Regex(@"(.+?)(?<!\\)=(\n?|[\S\s]+?)(?=\n.+=|$)", RegexOptions.Compiled);

    /// <summary>
    /// Metoda wywołująca walidację wszystkich sekcji appini.
    /// </summary>
    /// <param name="workdir"></param>
    /// <param name="writeMessage"></param>
    public static void ValidateAppSectionScripts(string workdir, Action<string> writeMessage)
    {
      if (Directory.Exists(Path.Combine(workdir, "appini")))
      {
        var files = Directory.EnumerateFiles(Path.Combine(workdir, "appini"), "*", SearchOption.AllDirectories);
        foreach (var f in files)
        {
          AppSection section = new AppSection(Path.GetFileNameWithoutExtension(f), "", File.ReadAllText(f));
          if (section.AppSectionObject?.Type == AppSectionType.Action)
          {
            ValidateAppSectionAction(section, writeMessage);
          }
        }
      }
      else
      {
        writeMessage?.Invoke("Brak skryptów APPINI w " + Path.Combine(workdir, "appini"));
      }
    }

    /// <summary>
    /// Metoda walidująca zgodność akcji appini z jej modelem.
    /// </summary>
    /// <param name="appSection"></param>
    /// <param name="writeMessage"></param>
    private static void ValidateAppSectionAction(AppSection appSection, Action<string> writeMessage)
    {
      var sectionName = appSection.AppSectionObject?.Name;
      var actionObject = appSection.AppSectionObject?.ActionDetails;
      if (actionObject != null && actionObject.Type != SHDataContracts.Model.AppSection.Subtypes.Enums.AppSectionSubtype.Unknown)
      {
        var errors = actionObject.Parameters.Where(f => !f.IsValid);
        if (errors.Count() > 0 || actionObject.HasNonModelFields)
        {
          StringBuilder validationMsg = new StringBuilder();
          validationMsg.AppendLine($"#warn#Błędy walidacji obiektu {sectionName}:");
          if (errors.Count() > 0)
          {
            validationMsg.AppendLine("Wartości spoza zakresu dla elementów:");
            foreach (var e in errors)
            {
              validationMsg.AppendLine($"(ns:{e.Namespace},p:{e.Prefix}) {e.Name} = {e.Value} <expected: [{string.Join(", ", e.AllowedValues)}]>");
            }
          }

          if (actionObject.HasNonModelFields)
          {
            validationMsg.AppendLine($"Elmenty spoza modelu akcji {actionObject.Type}:");
            foreach (var nm in actionObject.NonModelFields)
            {
              validationMsg.AppendLine($"(ns:{nm.NS},p:{nm.Prefix}) {nm.Identifier} = {nm.Value}");
            }
          }
          writeMessage(validationMsg.ToString());
        }
      }
      else if (actionObject == null)
      {
        writeMessage($"#warn#Niespodziewany błąd podczas walidacji akcji {sectionName}!");
      }
    }

    public static readonly object upgradeLock = new object();

    /// <summary>
    /// Metoda konwertująca wszystki pliki starego formatu appini
    /// </summary>
    /// <param name="workdir"></param>
    /// <param name="progress"></param>
    /// <param name="writeMessage"></param>
    public static bool UpgradeAppSection(string workdir, ProgressInfo progress, Action<string> writeMessage)
    {
      bool changes = false;
      if (Directory.Exists(workdir) && Directory.Exists(Path.Combine(workdir, "appini")))
      {
        var files = Directory.EnumerateFiles(Path.Combine(workdir, "appini")).Where(f => f.EndsWith(".app")).ToList();
        if (files.Count > 0)
        {
          changes = true;
          writeMessage("Wykryto niekompatybilny format APPINI. Wykonywanie migracji.");
          progress.ReportCount(files.Count, "Migracja formatu APPINI");
          foreach (var f in files)
          {
            try
            {
              var description = string.Empty;
              var jsonObj = BuildAppSectionObject(File.ReadAllText(f), out description);
              AppSection appSection = new AppSection(jsonObj, description);

              if (Path.GetFileName(f).Contains("#"))
              {
                appSection.NeedEscaping = true;
              }

              var fullPath = Path.Combine(workdir, appSection.FilePath);
              var dir = fullPath.Replace(Path.GetFileName(fullPath), "");
              if (!Directory.Exists(dir))
              {
                Directory.CreateDirectory(dir);
              }

              // Weryfikacja czy istnieje plik json dla danej sekcji s_appini
              if (File.Exists(fullPath))
              {
                var currectDescription = string.Empty;
                //Pobieramy istniejący plik json
                AppSection currectAppSection = new AppSection(appSection.Name, appSection.Description, File.ReadAllText(fullPath));

                if (currectAppSection.Equals(appSection))
                {
                  File.WriteAllText(fullPath, appSection.SaveObject());
                }
                else
                {
                  // Mergujemy zmiany
                  writeMessage("Merge sekcji .app z json: " + appSection.Name);
                  var mergedAppSection = MergeAppSection(appSection, currectAppSection);
                  File.WriteAllText(fullPath, mergedAppSection.SaveObject());
                }
              }
              else
              {
                File.WriteAllText(fullPath, appSection.SaveObject());
              }

              File.Delete(f);
              progress.ReportProgress();
            }
            catch (Exception e)
            {
              progress.ReportError(e);
            }
          }
          progress.ReportEnd();
          writeMessage("Zakończono migrację.");
        }
      }
      return changes;
    }

    /// <summary>
    /// Metoda budująca obiekt AppSection na podstawie nowego parsera APPINI.
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static AppSectionRoot BuildAppSectionObject(string ddl, out string description)
    {
      Dictionary<string, string> kvmap = GetAppSectionMap(ddl);
      if (kvmap.ContainsKey("SectionDescription"))
      {
        description = kvmap["SectionDescription"];
      }
      else
      {
        description = string.Empty;
      }
      if (kvmap.ContainsKey("Section"))
      {
        AppSectionRoot obj = new AppSectionRoot(kvmap["Section"]);
        foreach (var e in kvmap.Where(k => k.Key != "Section"))
        {
          string ident = e.Key;
          string val = e.Value;
          string @namespace = "SENTE";
          string prefix = null;

          int pos = ContainsEscaped(ident, '#');
          if (pos >= 0)
          {
            prefix = ident.Substring(0, pos);
            ident = ident.Substring(pos + 1);
          }

          pos = ContainsEscaped(ident, '~');
          if (pos >= 0)
          {
            @namespace = ident.Substring(0, pos);
            ident = ident.Substring(pos + 1);
          }

          obj.Elements.Add(new AppElement()
          {
            Identifier = ident.Replace("\\~", "~").Replace("\\#", "#").Replace("\\=", "=").Replace("\\\\", "\\"),
            NS = @namespace,
            Prefix = prefix,
            Value = val.Replace("\\~", "~").Replace("\\#", "#").Replace("\\=", "=").Replace("\\\\", "\\")
          });
        }
        obj.Elements.Sort();
        return obj;
      }
      return null;
    }

    /// <summary>
    /// Metoda pomocnicza do tworzenia słownika z wpisów starego formatu APPINI
    /// </summary>
    /// <param name="ddl"></param>
    /// <param name="writeMessage"></param>
    /// <returns></returns>
    private static Dictionary<string, string> GetAppSectionMap(string ddl, Action<string> writeMessage = null)
    {
      Dictionary<string, string> kvmap = new Dictionary<string, string>();
      if (ddl.StartsWith(CustomMetadata.SKEY))
      {
        ddl = ddl.Remove(0, CustomMetadata.SKEY.Length);
        if (ddl.IndexOf('\r') > 0)
        {
          string description = ddl.Substring(0, ddl.IndexOf('\r'));
          kvmap.Add("SectionDescription", description);
          ddl = ddl.Substring(description.Length);
        }
        ddl = ddl.Trim();
      }
      string sectionName = string.Empty;
      while (!string.IsNullOrWhiteSpace(ddl))
      {
        if (!kvmap.ContainsKey("Section"))
        {
          var match = matchAppSectionName.Match(ddl);
          if (match.Success)
          {
            sectionName = match.Groups[1].Value.Trim();
            kvmap.Add("Section", sectionName);
            ddl = ddl.Replace(match.Groups[0].Value, "").Trim();
          }
          else
          {
            Console.WriteLine(ddl);
            throw new InvalidOperationException();
          }
        }
        else
        {
          var match = matchAppSectionElements.Match(ddl);
          if (match.Success)
          {
            var key = match.Groups[1].Value.Trim();
            var val = match.Groups[2].Value.Trim();
            if (!kvmap.ContainsKey(key))
            {
              kvmap.Add(key, val);
            }
            else if (kvmap[key] != val)
            {
              writeMessage?.Invoke($"#warn#Wykryto zduplikowane wpisy o różnych wartościach w sekcji {sectionName}!");
            }
            ddl = ddl.Substring(match.Groups[0].Value.Length).Trim();
          }
          else
          {
            Console.WriteLine(ddl);
            throw new InvalidOperationException();
          }
        }
      }
      return kvmap;
    }

    /// <summary>
    /// Metoda budująca obiekt AppSection na podstawie starego parsera APPINI.
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static AppSectionRoot BuildAppSectionObject(string[] s)
    {
      var section = new AppSectionRoot(s[1].Trim(new Char[] { '[', ']' }));

      for (int i = 2; i < s.Length; i++)
      {
        if (!string.IsNullOrEmpty(s[i]) && ContainsEscaped(s[i], '=') < 0)
        {
          s[i - 1] += "\r\n" + s[i];
          s[i] = "";
        }
      }

      bool multiline = false;
      var cnt = s.Count();
      string val = "", ident = "", prefix = "", @namespace = "";
      for (int i = 2; i < cnt; i++)
        if (!string.IsNullOrWhiteSpace(s[i]) || multiline)
        {
          int eqPos = ContainsEscaped(s[i], '=');
          int eqPosAhead = 0;
          if (i < cnt - 1)
          {
            eqPosAhead = ContainsEscaped(s[i + 1], '=');
          }

          if (!multiline && eqPosAhead < 0)
          {
            ParseLine(s[i], out val, out ident, out prefix, out @namespace, eqPos);
            multiline = true;
            continue;
          }
          else
          {
            if (multiline && eqPos > -1)
            {
              section.Elements.Add(EmitInsertRecord(ref val, ref ident, ref prefix, ref @namespace));
              multiline = false;
            }
          }

          if (eqPos >= 0 && eqPosAhead >= 0)
          {
            ParseLine(s[i], out val, out ident, out prefix, out @namespace, eqPos);

            if (!multiline)
            {
              section.Elements.Add(EmitInsertRecord(ref val, ref ident, ref prefix, ref @namespace));
            }
          }
          else
          {
            val += s[i];
          }
        }
      if (multiline)
      {
        section.Elements.Add(EmitInsertRecord(ref val, ref ident, ref prefix, ref @namespace));
      }

      return section;
    }

    /// <summary>
    /// Metoda pomocnicza do sprawdzania czy ciąg zawiera wyescapowane znaki.
    /// </summary>
    /// <param name="string"></param>
    /// <param name="pattern"></param>
    /// <returns>Pozycja znaku escapowania</returns>
    private static int ContainsEscaped(string @string, char pattern)
    {
      bool ignore = false;
      if (pattern == '\\')
        throw new ArgumentOutOfRangeException("Pattern cannot be " + pattern);
      for (int i = 0; i < @string.Length; i++)
      {
        if (ignore)
        {
          ignore = false;
          continue;
        }

        char c = @string[i];
        ignore = c == '\\';
        if (c == pattern)
          return i;
      }
      return -1;
    }

    /// <summary>
    /// Metoda pomocnicza zwracająca podzielony wpis appini na Identyfiaktor, Wartość, Prefix i Namespace.
    /// </summary>
    /// <param name="val"></param>
    /// <param name="ident"></param>
    /// <param name="prefix"></param>
    /// <param name="namespace"></param>
    /// <returns></returns>
    private static AppElement EmitInsertRecord(ref string val, ref string ident, ref string prefix, ref string @namespace)
    {
      val = val.Replace("\\~", "~").Replace("\\#", "#").Replace("\\=", "=").Replace("\\\\", "\\");
      ident = ident.Replace("\\~", "~").Replace("\\#", "#").Replace("\\=", "=").Replace("\\\\", "\\");
      var element = new AppElement()
      {
        Identifier = ident,
        NS = @namespace,
        Prefix = prefix,
        Value = val
      };
      val = "";
      ident = "";
      prefix = "";
      @namespace = "";
      return element;
    }


    /// <summary>
    /// Pomocnicza metoda parsująca kolejne linijki starego foramtu APPINI.
    /// </summary>
    /// <param name="s">Linia z wpisu appini</param>
    /// <param name="val">Wartość klucza</param>
    /// <param name="ident">Nazwa klucza</param>
    /// <param name="prefix">Prefix klucza</param>
    /// <param name="namespace">PRzestrzeń klucza</param>
    /// <param name="eqPos">Aktualna pozycja w wyescapowanym ciągu</param>
    private static void ParseLine(string s, out string val, out string ident, out string prefix, out string @namespace, int eqPos)
    {
      ident = s.Substring(0, eqPos);
      val = s.Substring(eqPos + 1).Replace("'", "''");
      prefix = "NULL";
      @namespace = "'SENTE'";

      int pos = ContainsEscaped(ident, '#');
      if (pos >= 0)
      {
        prefix = "'" + ident.Substring(0, pos) + "'";
        ident = ident.Substring(pos + 1);
      }

      pos = ContainsEscaped(ident, '~');
      if (pos >= 0)
      {
        @namespace = "'" + ident.Substring(0, pos) + "'";
        ident = ident.Substring(pos + 1);
      }
    }

    /// <summary>
    /// Metoda merguje AppSecion pochodzące z poprzedniego formatu APP z sekcjami w formatem JSON
    /// </summary>
    /// <param name="appFromApp"></param>
    /// <param name="appFromJson"></param>
    /// <returns></returns>
    public static AppSection MergeAppSection(AppSection appFromApp, AppSection appFromJson)
    {
      foreach (AppElement el in appFromApp.AppSectionObject.Elements)
      {
        if (appFromJson.AppSectionObject.Elements.Contains(el, new AppElementEqualityComparer()))
        {
          var element = appFromJson.AppSectionObject.Elements.SingleOrDefault(e => e.Identifier == el.Identifier && e.NS == el.NS && e.Prefix == el.Prefix && e.Value != el.Value);
          if (element != null)
          {
            element.Value = el.Value;
          }
        }
        else
        {
          appFromJson.AppSectionObject.Elements.Add(el);
        }
        appFromJson.AppSectionObject.Elements.Sort();
        appFromJson.DDLNeedsRefresh = true;
      }

      return appFromJson;
    }

  }
}
