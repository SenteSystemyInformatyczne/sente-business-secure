﻿using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Services;
using SHDataContracts.Interfaces;
using SHlib.DataExtraction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SHlib.Utilities
{
  public class DatabaseManager
  {
    private static DatabaseManager _instance;

    private DatabaseManager()
    {
    }

    public static DatabaseManager Instance
    {
      get
      {
        if (_instance == null)
          _instance = new DatabaseManager();
        return _instance;
      }
    }

    /// <summary>
    /// Tworzy backup bazy
    /// </summary>
    /// <param name="dbsettings"></param>
    /// <param name="fbkname"></param>
    public void CreateBackup(IDbSettings dbsettings, string fbkname)
    {
      using (var sw = File.Create(fbkname))
      {
        FbStreamingBackup fbb = new FbStreamingBackup(dbsettings.ConnectionString);
        fbb.OutputStream = sw;
        fbb.Options = FbBackupFlags.IgnoreLimbo;
        fbb.ServiceOutput += fbb_ServiceOutput;
        try
        {
          fbb.Execute();
        }
        catch (Exception e)
        {
          throw new Exception("Wystąpił błąd podczas gbaka bazy: " + e.Message);
        }
      }
      //FbBackupFile fbf = new FbBackupFile(fbkname);
      //fbb.BackupFiles.Add(new FbBackupFile(fbkname));
      //fbb.Verbose = true;//bez tego nie dowiesz się, kiedy koniec
    }

    /// <summary>
    /// Odtwarza bazę z backupu
    /// </summary>
    /// <param name="dbsettings"></param>
    /// <param name="fbkname"></param>
    public void RestoreBackup(IDbSettings dbsettings, string fbkname)
    {
      FbRestore restoreSvc = new FbRestore(dbsettings.ConnectionString);

      restoreSvc.BackupFiles.Add(new FbBackupFile(fbkname));
      restoreSvc.Verbose = true;
      restoreSvc.PageSize = 16384;

      restoreSvc.ServiceOutput += fbb_ServiceOutput;
      try
      {
        restoreSvc.Execute();
      }
      catch (Exception e)
      {
        throw new Exception("Wystąpił błąd podczas odgbaka bazy: " + e.Message);
      }
    }

    void fbb_ServiceOutput(object sender, ServiceOutputEventArgs e)
    {
      //AppendMessage(e.Message);
    }

    /// <summary>
    /// Metoda zmieniająca stan bazy danych w dół - wyłączanie
    /// </summary>
    /// <param name="dbsettings">Dane do połączenia z bazą danych</param>
    /// <param name="mode">Docelowy tryb pracy bazy danych</param>
    public void ShutdownDb(IDbSettings dbsettings, Action<string> WriteMessage , FbShutdownOnlineMode mode = FbShutdownOnlineMode.Multi)
    {
      FbConfiguration fbc = new FbConfiguration(dbsettings.ConnectionString);
      try
      {
        if (WriteMessage != null)
          WriteMessage("Próba ustawienia bazy "+dbsettings.Name+" w tryb " + mode);
        try
        {
          DatabaseManager.Instance.ExecuteStatement(dbsettings, "delete from mon$attachments");
        } catch (Exception e)
        {
          WriteMessage(e.Message);
      }
        
        var task = Task.Run(() => {
          fbc.DatabaseShutdown2(mode, FbShutdownType.ForceShutdown, 1);
          FbConnection.ClearAllPools();
          WriteMessage("Baza " + dbsettings.Name + " jest w trybie " + mode);
        });
        while (!task.IsCompleted)
        {
          task.Wait(5000);

          if (!task.IsCompleted)
          {
            if (WriteMessage != null)
              WriteMessage("Shutdown nie powiódł się, próba usunięcia połączeń do bazy...");
            try
            {
              DatabaseManager.Instance.ExecuteStatement(dbsettings, "delete from mon$attachments");
            }
      catch (Exception e)
      {
              WriteMessage(e.Message);
            }
          }
        }
        if (task.Exception != null)
          throw task.Exception;
      }
      catch (Exception e)
      {
        if (!e.Message.ToLower().Contains("shutdown"))
          throw;
      }
    }

    /// <summary>
    /// Metoda zmieniająca stan bazy danych w górę - włączanie
    /// </summary>
    /// <param name="dbsettings">Dane do połączenia z bazą danych</param>
    /// <param name="mode">Docelowy tryb pracy bazy danych</param>
    public void OnlineDb(IDbSettings dbsettings, FbShutdownOnlineMode mode = FbShutdownOnlineMode.Normal)
    {
      FbConfiguration fbc = new FbConfiguration(dbsettings.ConnectionString);
      try
      {
        fbc.DatabaseOnline2(mode);
        FbConnection.ClearAllPools();
      }
      catch (Exception e)
      {
        if (!e.Message.ToLower().Contains("online") && !e.Message.ToLower().Contains("invalid"))
          throw;
      }
    }

    /// <summary>
    /// Metoda oczekująca na uruchomienie bazy danych
    /// </summary>
    /// <param name="dbsettings">Dane do połączenia z bazą danych</param>
    /// <param name="wait">Maksymalny czas oczekiwania na odpowiedź bazy danych w sekundach</param>
    /// <param name="interval">Odstęp wykonywania zapytań do bazy danych w sekundach</param>
    public void WaitForDb(IDbSettings dbsettings, int wait = 60, int interval = 1)
    {
      DateTime end = DateTime.Now.AddSeconds(wait);
      while (DateTime.Now < end && !DbStarted(dbsettings))
      {
        Thread.Sleep(interval * 1000);
      }
    }

    /// <summary>
    /// Metoda wykonująca zapytanie do bazy danych o połączenie
    /// </summary>
    /// <param name="dbsettings"></param>
    /// <returns></returns>
    private bool DbStarted(IDbSettings dbsettings)
    {
      FbConnection conn = new FbConnection(dbsettings.ConnectionString);
      bool result = false;
      try
      {
        conn.Open();
        conn.BeginTransaction();
        result = true;
      }
      catch (Exception e)
      {
        return false;
      }
      finally
      {
        conn.Close();
      }
      return result;
    }

    /// <summary>
    /// Metoda wykonująca zapytanie do bazy danych
    /// </summary>
    /// <param name="dbsettings"></param>
    /// <returns></returns>
    public void ExecuteStatement(IDbSettings dbsettings, string sql)
    {
      using (FbConnection conn = new FbConnection(dbsettings.ConnectionString))
      {
        conn.Open();
        using (var cmd = conn.CreateCommand())
        {
          cmd.CommandText = sql;
          cmd.ExecuteNonQuery();
        }
      }

    }

    /// <summary>
    /// Metoda wykonująca zapytanie do bazy danych i zwracająca pojedynczą wartość
    /// </summary>
    /// <param name="dbsettings"></param>
    /// <returns></returns>
    public object ExecuteScalar(IDbSettings dbsettings, string sql, bool forceRefresh = true)
    {
      object res = null;
      if(forceRefresh)
        FbConnection.ClearAllPools();
      using (FbConnection conn = new FbConnection(dbsettings.ConnectionString))
      {
        conn.Open();
        using (var cmd = conn.CreateCommand())
        {
          cmd.CommandText = sql;
          res = cmd.ExecuteScalar();
        }
        conn.Close();
      }
      return res;
    }

    //public void CopyDb(DbSettings dbsettings, string copyName)
    //{
    //  File.Copy(dbsettings.DatabaseFilePath, copyName);
    //}
  }
}
