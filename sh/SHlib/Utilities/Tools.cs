﻿using SHDataContracts.Interfaces;
using SHlib.DataExtraction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHlib.Utilities
{
  static public class Tools
  {
    /// <summary>
    /// Sprawdza połączenie z bazą
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="kind"></param>
    /// <returns></returns>
    public static string CheckDatabaseConnection(IDbSettings settings, string kind)
    {
      string errorMsg = "";
      using (Database db = new Database(settings))
      {
        if (!db.TestConnection(out errorMsg))
        {
          return "Błąd połączenia ze " + kind + " bazą danych " + settings.Name + "\r\n" + errorMsg;
        }
      }
      return "";
    }

    public static string CheckDirectoryExistence(string path)
    {

      if (!Directory.Exists(path))
      {
        return "Nie odnaleziono katalogu: " + path;
      }
      return "";
    }

    public static string CheckFileExistence(string filepath)
    {
      if (!File.Exists(filepath))
      {
        return "Nie odnaleziono pliku: " + filepath;
      }
      return "";
    }

    /// <summary>
    /// Wydaje się, że metoda bierze dwa stringi i odcina z pierwszego prefiks i sufiks wspólny z drugim stringiem
    /// </summary>
    /// <param name="txt1"></param>
    /// <param name="txt2"></param>
    /// <returns></returns>
    public static string RemoveIntersectionFromHashDDL(string txt1, string txt2)
    {
      int i = 0;
      int j = 0;
      if (txt1.Length > 0 && txt2.Length > 0)
      {
        while (txt1[i] == txt2[i])
        {
          i++;
          if (i >= txt1.Length || i >= txt2.Length)
            break;
        }

        txt1 = txt1.Remove(0, i);
        txt2 = txt2.Remove(0, i);
      }
      if (txt1.Length > 0 && txt2.Length > 0)
      {
        while (txt1[txt1.Length - 1 - j] == txt2[txt2.Length - 1 - j])
        {
          j++;
          if (txt2.Length - 1 - j < 0 || txt1.Length - 1 - j < 0)
            break;
        }

        txt1 = txt1.Remove(txt1.Length - j, j);
        txt2 = txt2.Remove(txt2.Length - j, j);
      }
      return txt1;
    }
    /// <summary>
    /// Funkcja weryfikuje czy katalogu z skryptami nie jest pusty, sprawdzając czy
    /// znajduje się co najmniej jeden plik json i sql w tym katalogu.
    /// </summary>
    /// <param name="scriptPath">Ściężka do katalogu w którym są skrypty</param>
    /// <returns></returns>
    public static List<string> CheckIfDBScriptFolderIsEmpty(string scriptPath)
    {
      List<string> msgs = new List<string>();

      string[] files = Directory.GetFiles(scriptPath, "*.sql", SearchOption.AllDirectories);
      if (files.Length == 0)
        msgs.Add("W folderze z skryptami nie znaleziono plików sql.");
      
      files = Directory.GetFiles(scriptPath, "*.json", SearchOption.AllDirectories);
      if (files.Length == 0)
        msgs.Add("W folderze z skryptami nie znaleziono plików json.");

      if(msgs.Count > 0)
        msgs.Add("Z powodu braku skryptów w katalogu proces został przerwany.");

      return msgs;
    }

    /// <summary>
    /// Usuwa text pomiędzi przekazanymi ciągami tekstu "start" ... "stop" wraz z tymi znacznikami.
    /// </summary>
    /// <param name="strSource"></param>
    /// <param name="strStart"></param>
    /// <param name="strEnd"></param>
    /// <returns></returns>
    public static string RemoveTextBetween(string strSource, string strStart, string strEnd)
    {
      string result = "";

      if (strSource.Contains(strStart) && strSource.Contains(strEnd))
      {
        int startIdx = 0;
        int endIdx = 0;
        int idx = 0;

        while ((startIdx = strSource.IndexOf(strStart, idx)) >= 0)
        {
          endIdx = strSource.IndexOf(strEnd, startIdx + 1);

          if (endIdx > 0)
          {
            result += strSource.Substring(idx, startIdx - idx);
            idx = endIdx + strEnd.Length;
          }
          else
            break;
          startIdx = 0;
          endIdx = 0;
        }

        result += strSource.Substring(idx, strSource.Length - idx);
      }
      else
        result = strSource;
      return result;
    }
  }
}
