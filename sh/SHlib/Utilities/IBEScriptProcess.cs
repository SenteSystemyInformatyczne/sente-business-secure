﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SHlib
{
  public class IBEScriptProcess
  {
    public const string IBESCRIPT_NAME = @"\IBEScript.exe";

    ProcessStartInfo _process;
    string[] filterOutput;
    int progress = 0;
    bool progressAvalible = false;

    public IBEScriptProcess(string arguments, string[] filterOutput = null, string ibexpertPath = @"C:\Program Files (x86)\HK-Software\IBExpert")
    {
      //tablica do filtrowania wiadomości do wyświetlenia
      this.filterOutput = filterOutput ?? new string[0];

      //tworzenie process info dla uruchomienia IBEScript
      _process = new ProcessStartInfo();
      _process.FileName = ibexpertPath + IBESCRIPT_NAME;
      _process.WorkingDirectory = Directory.GetCurrentDirectory();
      _process.Arguments = arguments;
      _process.UseShellExecute = false;
      _process.CreateNoWindow = true;
      _process.WindowStyle = ProcessWindowStyle.Hidden;
      _process.RedirectStandardOutput = true;
      _process.RedirectStandardError = true;
    }

    /// <summary>
    /// Sprawdza czy wersja IBEScripta nie jest "zbyt stara"
    /// </summary>
    /// <param name="pathIBEScript">ścieżka do IBEScript exe</param>
    public static bool isIBScriptVersionOk(string pathIBEScript)
    {
      try
      {
        var versionInfo = FileVersionInfo.GetVersionInfo(pathIBEScript);
        int year = Int32.Parse(versionInfo.FileVersion.Substring(0, 4));
        return year >= 2014;
      }
      catch
      {
        return false;
      }
    }

    /// <summary>
    /// Pobiera pierwszą część wersji IBScript (rok)
    /// </summary>
    /// <param name="versionString">Tekst w którym jest zapisana wersja IBScript</param>
    public static int GetIBScriptVersion(string versionString)
    {
      return Int32.Parse(versionString.Substring("IBEScript Version ".Length, 4));
    }

    /// <summary>
    /// Sprawdza czy zmienna progres się uaktualniła. Jeżeli tak to aktualizuje ją i zwraca true. W przeciwnym wypadku zwraca false
    /// </summary>
    /// <param name="progressMessage">Linia tekstu, zwrócona przez parametr '-A' IBScript</param>
    /// <returns>true/false</returns>
    private bool CheckAndUpdateProgress(string progressMessage)
    {
      var match = Int32.Parse(Regex.Match(progressMessage, @"POC:\ ([\d]{1,2})").Groups[1].ToString());
      if (match <= progress)
        return false;
      progress = match;
      return true;
    }

    /// <summary>
    /// Pobiera przewidywany czas do zakończenia.
    /// </summary>
    /// <param name="progressMessage">Linia tekstu, zwrócona przez parametr '-A' IBScript</param>
    /// <returns>Przewidywany czas do zakończenia w formacie MM:SS</returns>
    private string GetEstimatedTime(string progressMessage)
    {
      return Regex.Match(progressMessage, @"ETL:\ ([0-9:]*)").Groups[1].ToString();
    }

    public int Execute(Action<string> AppendMessage, CancellationToken ct)
    {
      int exitCode;
      bool errorExists = false;
      using (Process process = Process.Start(_process))
      {
        process.OutputDataReceived += (sender, args) =>
        {
          if (ct.IsCancellationRequested && !process.HasExited)
            process.Kill();

          if (!string.IsNullOrWhiteSpace(args.Data))
          {
            var lower = args.Data.ToLower();
            errorExists |= (lower.Contains("error in script"));

            if (filterOutput.Any(k => args.Data.StartsWith(k)) || errorExists)
            {
              if ((!progressAvalible) && args.Data.StartsWith("IBEScript Version"))
              {
                if (IBEScriptProcess.GetIBScriptVersion(args.Data) >= 2016)
                  progressAvalible = true;
                return;
              }

              if ((progressAvalible) && (args.Data.StartsWith(" SC:")))
                if (CheckAndUpdateProgress(args.Data))
                {
                  if (errorExists && args.Data.StartsWith("(Line:"))
                    errorExists = false;
                  else
                    AppendMessage("Wgrano " + progress.ToString() + "%. Pozostało około: " + GetEstimatedTime(args.Data) + " min");    
                }
                else
                  return;
              else
              {
                if (errorExists && args.Data.StartsWith("(Line:"))
                  errorExists = false;
                else
                  AppendMessage(args.Data);

              }
            }
          }
        };

        process.ErrorDataReceived += (sender, args) =>
        {
          if (!string.IsNullOrWhiteSpace(args.Data))
          {
            var lower = args.Data.ToLower();
            errorExists |= (lower.Contains("error in script"));
            AppendMessage(args.Data);
          }
        };
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        process.WaitForExit();
        exitCode = process.ExitCode;
      }
      return exitCode;
    }


  }
}
