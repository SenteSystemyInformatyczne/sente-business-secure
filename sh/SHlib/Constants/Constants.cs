﻿using SHDataContracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SHlib.Constants
{
  public static class Constants
  {
    public static Dictionary<DbTypes, Regex[]> IgnoredPatternsInDiffs
    {
      get;
    } = new Dictionary<DbTypes, Regex[]>()
    {
      [DbTypes.Domain] = new Regex[] { new Regex("^collate[a-z0-9_]+$", RegexOptions.Compiled) },
      [DbTypes.Table] = new Regex[] { new Regex("^(collate[a-z0-9_]+(,?[\\d\\w\"_]+collate[a-z0-9_]+)*)$", RegexOptions.Compiled) },
      [DbTypes.Field] = new Regex[] { new Regex("^collate[a-z0-9_]+$", RegexOptions.Compiled) }
    };

    public static Dictionary<string, Encoding> Encodings
    {
      get;
    } = new Dictionary<string, Encoding>()
    {
      ["WIN1250"] = Encoding.GetEncoding(1250),
      ["UTF8"] = new UTF8Encoding(false)
    };
  }
}
