﻿using Microsoft.CSharp.RuntimeBinder;
using NLog;
using SenteUpdateTool.Code;
using SenteUpdateTool.Code.Steps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenteUpdateTool
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();
//#if DEBUG
//      Debugger.Launch();//żeby się dało odpalać z .exe i debugować
//#endif
      Execute();
    }

    private dynamic input = new ExpandoObject();
    private static Logger logger = LogManager.GetCurrentClassLogger();
    private void Execute()
    {
        Random rnd = new Random();
        MDC.Set("id", "_" + rnd.Next(1, 100) + "_");
        ReadCommandLineArguments(input);
        UpdateStep step = null;
       // try
      //  {
          if (string.IsNullOrWhiteSpace(input.stepToStart))
          step = new CheckIfVersionRepositoryExists();
          else
          {
            try
            {
              var type = Type.GetType("SenteUpdateTool.Code.Steps." + ((string)input.stepToStart));
              step = (UpdateStep)Activator.CreateInstance((Type)type);
            }
            catch (Exception e)
            {
              var ex = new Exception("Nie można znaleźć kroku o nazwie " + input.stepToStart, e);
              string inputval = "";
              foreach (KeyValuePair<string, object> kvp in input) // enumerating over it exposes the Properties and Values as a KeyValuePair
                inputval += kvp.Key + "=" + kvp.Value + " ";

              logger.Error("Nie można znaleźć kroku o nazwie " + input.stepToStart + "\r\n" + "Zawartość zmiennej input:" + inputval);
              ShowTroubles(ex);
            }
          }
       // }
       // catch (RuntimeBinderException e)
       // {

     //   }

        while (step != null)
        {
          PrepareProgressBar(step);
          try
          {
            SetStatus(step.StepName);
            step.Do(input);
            step = step.NextStep;
          }
          catch (Exception e)
          {
            string inputval = "";
            foreach (KeyValuePair<string, object> kvp in input) // enumerating over it exposes the Properties and Values as a KeyValuePair
              inputval += kvp.Key + "=" + kvp.Value + "\r\n";

            logger.Error("Message:" + e.Message + "\r\n" + "Exception:" + e.InnerException + "\r\n" 
                                                   + "Stack Trace:" + e.StackTrace + "\r\n" + "Zawartość zmiennej input:" + inputval+ "\r\n" 
                                                   + "Step:" + step.StepName + "\r\n" + "NextStep:" + step.NextStep);
            ShowTroubles(e);
            try
            {
              new RunNewVersionOfApp().Do(input);
            }
            catch (Exception ex)
            {
              foreach (KeyValuePair<string, object> kvp in input) // enumerating over it exposes the Properties and Values as a KeyValuePair
                inputval += kvp.Key + "=" + kvp.Value + " ";

              logger.Error("Message:" + e.Message + "\r\n" + "Exception:" + e.InnerException + "\r\n" + "Stack Trace:" + e.StackTrace + "\r\n" + "Zawartość zmiennej input:" + inputval);
              ShowTroubles(ex, exit: true);
              step = null;
            }
          }
        }
        MDC.Remove("id");
        CloseForm();
    }

    private void PrepareProgressBar(UpdateStep step)
    {
      if (progressBar.InvokeRequired)
      {
        progressBar.BeginInvoke((MethodInvoker)delegate()
        {
          PrepareProgressBar(step);
        }, step);
        return;
      }
      if (step.Kind == UpdateStep.ProgressKind.Determinate)
      {
        progressBar.Style = ProgressBarStyle.Continuous;
        step.ProgressMaxValue = ProgressMaxValue;
        step.UpdateProgress = UpdateProgress;
        UpdateProgress(0);
      }
      else
      {
        progressBar.Style = ProgressBarStyle.Marquee;
      }
    }

    private void UpdateProgress(int obj)
    {
      if (this.progressBar.InvokeRequired)
      {
        this.progressBar.BeginInvoke((MethodInvoker) delegate()
        {
          this.progressBar.Value = obj;
        });
      }
      else
      {
        this.progressBar.Value = obj;
      }
    }

    private void ProgressMaxValue(int obj)
    {
      if (this.progressBar.InvokeRequired)
      {
        this.progressBar.BeginInvoke((MethodInvoker) delegate()
        {
          this.progressBar.Maximum = obj;
        });
      }
      else
      {
        this.progressBar.Maximum = obj;
      }
    }

    private void CloseForm()
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke((MethodInvoker) delegate()
        {
          this.Close();
        });
      }
      else
      {
        this.Close();
      }
    }

    private void SetStatus(string message)
    {
      if (this.labelStatus.InvokeRequired)
      {
        this.labelStatus.BeginInvoke((MethodInvoker) delegate()
        {
          this.labelStatus.Text = message;
        });
      }
      else
      {
        this.labelStatus.Text = message;
      }
    }

    private void ShowTroubles(Exception e, bool exit = false)
    {
      var instance = new TroublesForm()
      {
        Cause = e
      };
      this.Hide();
      instance.ShowDialog();
      this.CloseForm();
    }

    private void ReadCommandLineArguments(dynamic input)
    {
      string[] args = Environment.GetCommandLineArgs();

      logger.Info("Uruchomienie aplikacji z parametrami: " + String.Join(" ", args));
      input.appToRun = null;
      input.stepToStart = null;
      foreach(var arg in args)
      {
        if(arg.StartsWith("--appToRun") || arg.StartsWith("-a"))
        {
          input.appToRun = ParseArgument(arg);
        }
        else if (arg.StartsWith("--stepToStart") || arg.StartsWith("-s"))
        {
          input.stepToStart = ParseArgument(arg);
        }
        else
        {
          switch (Array.IndexOf(args, arg))
          {
            case 1:
              input.appToRun = arg;
              break;
            case 2:
              input.stepToStart = arg;
              break;
          }
          
        }
      }
      /*if (args.Length > 1)
        input.appToRun = args[1];
      if (args.Length > 2) 
        input.stepToStart = args[2];
      else
        input.stepToStart = null;*/
    }

    private string ParseArgument(string arg)
    {
      return arg.Substring(arg.IndexOf("=")+1);
    }
  }
}
