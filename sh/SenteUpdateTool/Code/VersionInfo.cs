﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool
{
  public class VersionInfo
  {
    const int DESCRIPTORSIZE = 4;
    private int[] descriptor;

    public int Major
    {
      get
      {
        return descriptor[0];
      }
      set
      {
        descriptor[0] = value;
      }
    }

    public int Minor
    {
      get
      {
        return descriptor[1];
      }
      set
      {
        descriptor[1] = value;
      }
    }

    public int Revision
    {
      get
      {
        return descriptor[2];
      }
      set
      {
        descriptor[2] = value;
      }
    }

    public int Build
    {
      get
      {
        return descriptor[3];
      }
      set
      {
        descriptor[3] = value;
      }
    }

    public bool IsOlderThan(VersionInfo v)
    {
      return Major < v.Major ||
        (Major == v.Major && Minor < v.Minor) ||
        (Major == v.Major && Minor == v.Minor && Revision < v.Revision) ||
        (Major == v.Major && Minor == v.Minor && Revision == v.Revision && Build < v.Build);
    }

    public VersionInfo()
    {
      descriptor = new int[DESCRIPTORSIZE];
    }

    public VersionInfo(List<int> numbers)
      : this()
    {
      for (int i = 0; i < Math.Min(numbers.Count, DESCRIPTORSIZE); i++)
        descriptor[i] = numbers[i];
    }

    public static VersionInfo Parse(string fname)
    {
      //pierwsze 3 człony jakie da się sparsować na int utworzą numer wersji
      //np cos.1.2.aa.3.4.sud będzie miało wersję 1.2.3
      var tokens = fname.Split('.');

      List<int> numbers = new List<int>();
      foreach (var token in tokens)
      {
        int number;
        if (int.TryParse(token, out number))
          numbers.Add(number);
      }

      return new VersionInfo(numbers);
    }
  }
}
