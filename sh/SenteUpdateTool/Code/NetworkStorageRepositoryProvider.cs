﻿using SenteUpdateTool.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool
{
  public class NetworkStorageRepositoryProvider : RepositoryProvider
  {
    protected const string PATTERN = "*.sud";

    protected string PATH
    {
      get
      {
        return Settings.Default.RepositoryPath;
      }
    }

    public override bool Exists
    {
      get
      {
        return Directory.Exists(PATH);//co najmniej dostęp read-only
      }
    }

    public override string Name
    {
      get
      {
        return PATH;
      }
    }

    protected IEnumerable<VersionDataDescriptor> GetAllVersions()
    {
      foreach (var file in Directory.EnumerateFiles(PATH, PATTERN, SearchOption.AllDirectories))
      {
        var vdd = new VersionDataDescriptor();
        vdd.FileName = file;
        vdd.Size = new FileInfo(file).Length;
        vdd.Version = VersionInfo.Parse(Path.GetFileNameWithoutExtension(file));
        yield return vdd;
      }
    }

    private VersionDataDescriptor currentVersion = null;
    public override VersionInfo CurrentVersion
    {
      get
      {
        if (currentVersion == null)
          currentVersion = GetCurrentVersion();
        return currentVersion == null ? null : currentVersion.Version;
      }
    }

    private VersionDataDescriptor GetCurrentVersion()
    {
      VersionDataDescriptor currvdd = null;
      foreach (var verd in GetAllVersions())
      {
        if (currvdd == null || currvdd.Version.IsOlderThan(verd.Version))
          currvdd = verd;
      }
      return currvdd;
    }

    public override VersionDataDescriptor GetCurrentVersionDescriptor()
    {
      if (CurrentVersion != null)
      {
        currentVersion.DataStream = new FileStream(currentVersion.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
        return currentVersion;
      }
      return null;
    }
  }
}
