﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool
{
  public abstract class RepositoryProvider
  {
    public abstract string Name
    {
      get;
    }

    public abstract bool Exists
    {
      get;
    }

    public abstract VersionInfo CurrentVersion
    {
      get;
    }

    public abstract VersionDataDescriptor GetCurrentVersionDescriptor();
  }
}
