﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public class RunNewVersionOfApp : UpdateStep
  {

    public override string StepName
    {
      get
      {
        return "ponowne uruchamianie aplikacji";
      }
    }

    protected override void Execute(dynamic input)
    {
      Process.Start(input.appToRun);
    }

    public override UpdateStep NextStep
    {
      get
      {
        return null;
      }
    }
  }
}
