﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public abstract class UpdateStep
  {
    public enum ProgressKind
    {
      Determinate,
      Indeterminate
    }

    public virtual ProgressKind Kind
    {
      get
      {
        return ProgressKind.Indeterminate;
      }
    }

    public Action<int> ProgressMaxValue
    {
      get;
      set;
    }

    public Action<int> UpdateProgress
    {
      get;
      set;
    }

    public abstract string StepName
    {
      get;
    }

    protected virtual void Prepare(dynamic input)
    {
    }

    protected abstract void Execute(dynamic input);

    protected virtual void Confirm(dynamic input)
    {
    }

    public abstract UpdateStep NextStep
    {
      get;
    }

    public void Do(dynamic input)
    {
      Exception exc = null;
      LogManager.GetCurrentClassLogger().Info("Wykonywanie kroku: " + this.GetType());
      try
      {
        Prepare(input);
        try
        {
          Execute(input);
          try
          {
            Confirm(input);
          }
          catch (Exception exx)
          {
            exc = new Exception("Błąd podczas sprawdzania poprawności kroku " + StepName, exx);
          }
        }
        catch (Exception ex)
        {
          exc = new Exception("Błąd podczas wykonywania kroku " + StepName, ex);
        }
      }
      catch (Exception e)
      {
        exc = new Exception("Błąd podczas przygotowywania kroku " + StepName, e);
      }
      if (exc != null)
        throw exc;
    }
  }
}
