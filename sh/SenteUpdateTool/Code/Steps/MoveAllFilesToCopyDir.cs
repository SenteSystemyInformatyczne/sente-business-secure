﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public class MoveAllFilesToCopyDir : UpdateStep
  {
    const string MAINAPPPATH = "..";
    const string COPYDIR = "Update";

    public override UpdateStep.ProgressKind Kind
    {
      get
      {
        return ProgressKind.Determinate;
      }
    }

    public override string StepName
    {
      get
      {
        return "kopia zapasowa plików";
      }
    }

    protected override void Prepare(dynamic input)
    {
      input.copydir = Path.Combine(MAINAPPPATH, COPYDIR);
      if (Directory.Exists(input.copydir))
        Directory.Delete(input.copydir, true);
    }

    protected override void Execute(dynamic input)
    {
      Directory.CreateDirectory(input.copydir);
      var files = Directory.GetFiles(MAINAPPPATH, "*.*", SearchOption.TopDirectoryOnly).Where(file => file.ToLower().EndsWith(".bpl") || file.ToLower().EndsWith(".exe")).ToArray();
      ProgressMaxValue(files.Length);
      int i = 0;
      foreach (var file in files)
      {
        //string x = Path.GetDirectoryName(file).TrimStart('.','\\','/');
        File.Copy(file, Path.Combine(input.copydir, Path.GetFileName(file)));
        i++;
        UpdateProgress(i);
      }
      //CopyDir.CopyAll(new DirectoryInfo(MAINAPPPATH),new DirectoryInfo(Path.Combine(MAINAPPPATH,COPYDIR)));
    }

    protected override void Confirm(dynamic input)
    {
      foreach (var file in Directory.GetFiles(MAINAPPPATH, "*.*", SearchOption.TopDirectoryOnly).Where(file => file.ToLower().EndsWith(".bpl") || file.ToLower().EndsWith(".exe")))
        if (!File.Exists(Path.Combine(input.copydir, Path.GetFileName(file))))
          throw new Exception("Nie znaleziono kopii pliku " + file);
      /*foreach (var file in Directory.GetFiles(MAINAPPPATH, "*.*", SearchOption.TopDirectoryOnly))
      {
        FileTryDelete(file);
      }*/
    }

    public override UpdateStep NextStep
    {
      get
      {
        return new ExtractUpdate();
      }
    }

    const int TRYOUTS = 3;
    const int COOLDOWN = 500;

    private void FileTryDelete(string file)
    {
      bool deleted = false;
      int tryout = 0;
      while (!deleted && tryout < TRYOUTS)
      {
        try
        {
          File.Delete(file);
          deleted = true;
        }
        catch (Exception e)
        {
          Thread.Sleep(COOLDOWN);
          if (!deleted && tryout == TRYOUTS-1)
            throw new Exception("Nie można usunąć pliku " + file, e);
        }
        tryout++;
      }
    }
  }
}
