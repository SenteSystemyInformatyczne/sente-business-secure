﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public class ExtractUpdate : UpdateStep
  {
    const string MAINAPPPATH = "..";

    public override string StepName
    {
      get
      {
        return "rozpakowywanie aktualizacji";
      }
    }

    protected override void Prepare(dynamic input)
    {
      if (!File.Exists(input.tempfile))
      {
        throw new Exception(string.Format("Local update file {0} not found", input.tempfile));
      }
    }

    protected override void Execute(dynamic input)
    {
      using (ZipFile zf = ZipFile.Read(input.tempfile))
      {
        zf.ExtractAll(MAINAPPPATH);
      }
    }

    public override UpdateStep NextStep
    {
      get
      {
        return new RunNewVersionOfApp();
      }
    }
  }
}
