﻿using SenteUpdateTool.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public class CheckIfVersionRepositoryExists : UpdateStep
  {
    public override string StepName
    {
      get
      {
        return "sprawdzanie dostępu do zdalnego repozytorium";
      }
    }
    protected override void Execute(dynamic input)
    {
      RepositoryProvider repo = null;
      if (Settings.Default.RepositoryType == 0)
      {
        repo = new NetworkStorageRepositoryProvider();
      }
      else
      {
        repo = new XmlManifestNSRepositoryProvider();
      }
        if (!repo.Exists)
          throw new Exception(string.Format("Repozytorium {0} jest nieosiągalne.",repo.Name));

        input.repo=repo;
    }

    public override UpdateStep NextStep
    {
      get
      {
        return new CheckCurrentLocalVersion();
      }
    }
  }
}
