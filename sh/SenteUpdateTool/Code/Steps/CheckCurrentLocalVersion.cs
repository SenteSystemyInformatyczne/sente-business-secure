﻿using Microsoft.CSharp.RuntimeBinder;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenteUpdateTool.Code.Steps
{
  public class CheckCurrentLocalVersion : UpdateStep
  {
    const string MAINAPPPATH = "..";
    private bool needUAC = false;
    public override string StepName
    {
      get
      {
        return "sprawdzanie zainstalowanej wersji";
      }
    }

    protected override void Prepare(dynamic input)
    {
      try
      {
        needUAC = hasWriteAccess(Application.StartupPath);
        LogManager.GetCurrentClassLogger().Info("Ścieżka StartupPath: " + Application.StartupPath);
        LogManager.GetCurrentClassLogger().Info("Ścieżka appToRun: " + Path.GetFullPath(Path.Combine(Application.StartupPath,input.appToRun)));
        if (!File.Exists(Path.GetFullPath(Path.Combine(Application.StartupPath,input.appToRun))))
          throw new Exception("Nie znaleziono pliku " + input.appToRun);
        input.mainexe = input.appToRun;
        WaitForProcessToEnd(input.appToRun);
      }
      catch (RuntimeBinderException ex)
      {
        FindExeOldMethod(input);
      }
        
    }

    const int COOLDOWN = 500;
    const int KILLTIME = 3;
    const int ENDTIME = 6;

    private void WaitForProcessToEnd(string exe)
    {
      List<Process> runningApps = new List<Process>();
      int tryout = 0;
      do
      {
        var processes = Process.GetProcesses();
        runningApps.Clear();
        foreach(var p in processes)
        {
          try {
            if (p.MainModule.FileName == exe)
              runningApps.Add(p);
          } catch {
          }
        }
        if (tryout == KILLTIME)
        {
          foreach (var app in runningApps)
            app.Kill();
        }
        tryout++;
        if (runningApps.Count > 0)
          Thread.Sleep(COOLDOWN);
      } while (tryout < ENDTIME || runningApps.Count != 0);
    }

    private static void FindExeOldMethod(dynamic input)
    {
      try
      {
        var mainexe = Directory.EnumerateFiles(MAINAPPPATH, "*.exe", SearchOption.TopDirectoryOnly).Single();
        input.mainexe = mainexe;
      }
      catch (Exception e)
      {
        throw new Exception("Jest więcej niż jeden plik .exe w katalogu " + Path.GetFullPath(MAINAPPPATH), e);
      }
    }

    private bool hasWriteAccess(string folderPath)
    {
      try
      {
        var writeAllow = false;
        var writeDeny = false;
        var accessControlList = Directory.GetAccessControl(folderPath);
        if (accessControlList == null)
          return false;
        var accessRules = accessControlList.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
        if (accessRules == null)
          return false;

        foreach (FileSystemAccessRule rule in accessRules)
        {
          if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write) continue;

          if (rule.AccessControlType == AccessControlType.Allow)
            writeAllow = true;
          else if (rule.AccessControlType == AccessControlType.Deny)
            writeDeny = true;
        }

        return writeAllow && !writeDeny;
      }
      catch (UnauthorizedAccessException)
      {
        return false;
      }
    }

    protected override void Execute(dynamic input)
    {
      //string version = AssemblyName.GetAssemblyName(input.mainexe).Version.ToString();
      string version = FileVersionInfo.GetVersionInfo(Path.GetFullPath(Path.Combine(Application.StartupPath, input.mainexe))).FileVersion;
      input.localVersion = VersionInfo.Parse(version);
      input.currentVersion = input.repo.CurrentVersion;
      input.installNeeded = input.localVersion.IsOlderThan(input.currentVersion);
      if (input.installNeeded)
      {
        DialogResult r = MessageBox.Show("Aktualizacja oprogramowania jest dostępna. Czy pobrać nową wersję?", "Aktualizacja", MessageBoxButtons.YesNo);
        if (r == DialogResult.No)
          input.installNeeded = false;
      }
    }

    protected override void Confirm(dynamic input)
    {
      if (input.installNeeded && !needUAC)
      {
        var pi = new ProcessStartInfo()
        {
          Arguments = "-a="+input.mainexe+" -s=DownloadUpdateFile",
          FileName = Application.ExecutablePath,
          WorkingDirectory = Application.StartupPath,
          Verb="runas",
          UseShellExecute=true
        };
        LogManager.GetCurrentClassLogger().Info("Parametry aplikacji do odpalenia: " + pi.Arguments + "\r\n" + "Ścieżka:" + pi.FileName + "\r\n" + "WD:" + pi.WorkingDirectory);
        Process.Start(pi);
      }
    }

    public override UpdateStep NextStep
    {
      get
      {
        if(!needUAC)
          return null;//przerywamy tu, bo jeżeli dalej to potrzebujemy admina, więc musimy się sami zrestartować
        return new DownloadUpdateFile();
      }
    }
  }
}
