﻿using SenteUpdateTool.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool.Code.Steps
{
  public class DownloadUpdateFile : UpdateStep
  {
    public override string StepName
    {
      get
      {
        return "pobieranie aktualizacji";
      }
    }

    protected override void Prepare(dynamic input)
    {
      RepositoryProvider repo = null;
      if (Settings.Default.RepositoryType == 0)
      {
        repo = new NetworkStorageRepositoryProvider();
      }
      else
      {
        repo = new XmlManifestNSRepositoryProvider();
      }
      input.repo = repo;
    }

    public override UpdateStep.ProgressKind Kind
    {
      get
      {
        return ProgressKind.Determinate;
      }
    }

    const int BUFSIZE = 128 * 1024;

    protected override void Execute(dynamic input)
    {
      VersionDataDescriptor data = input.repo.GetCurrentVersionDescriptor();

      ProgressMaxValue((int)data.Size/BUFSIZE);

      using (var stream = data.DataStream)
      {
        input.tempfile = Path.GetTempFileName();
        using (var localstream = File.OpenWrite(input.tempfile))
        {
          byte[] buffer = new byte[BUFSIZE];

          int read;
          long written = 0;
          while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
          {
            localstream.Write(buffer, 0, read);
            written += read;
            UpdateProgress((int)written/BUFSIZE);
          }
        }
      }
      input.currentVersionDescriptor = data;
    }

    protected override void Confirm(dynamic input)
    {
      long size = input.currentVersionDescriptor.Size;
      long currsize = new FileInfo(input.tempfile).Length;
      if (currsize != size)
        throw new Exception("Pobrany plik ma inną wielkość ({0}) niż plik na repozytorium ({1}).".Format(currsize, size));
    }

    public override UpdateStep NextStep
    {
      get
      {
        return new MoveAllFilesToCopyDir();
      }
    }
  }
}
