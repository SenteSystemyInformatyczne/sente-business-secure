﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SenteUpdateTool
{
  public class XmlManifestNSRepositoryProvider : NetworkStorageRepositoryProvider
  {
    readonly string MANIFESTFILE = "patches.xml";

    public override bool Exists
    {
      get
      {
        return File.Exists(Path.Combine(PATH,MANIFESTFILE));//co najmniej dostęp read-only
      }
    }

    public override string Name
    {
      get
      {
        return PATH;
      }
    }

    protected IEnumerable<VersionDataDescriptor> GetAllVersions()
    {
      var xml = XElement.Load(Path.Combine(PATH, MANIFESTFILE));
      var query = xml.Elements();
      foreach (var patch in query)
      {
        var vdd = new VersionDataDescriptor();
        vdd.FileName = Path.Combine(PATH,patch.Element("filename").Value);
        FileInfo zipfile = new FileInfo(vdd.FileName);
        if (zipfile.Exists)
          vdd.Size = zipfile.Length;
        else
          vdd.Size = 0;
        vdd.Version = VersionInfo.Parse(patch.Element("version").Value);
        yield return vdd;
      }
    }

    private VersionDataDescriptor currentVersion = null;
    public override VersionInfo CurrentVersion
    {
      get
      {
        if (currentVersion == null)
          currentVersion = GetCurrentVersion();
        return currentVersion == null ? null : currentVersion.Version;
      }
    }

    private VersionDataDescriptor GetCurrentVersion()
    {
      VersionDataDescriptor currvdd = null;
      foreach (var verd in GetAllVersions())
      {
        if ((currvdd == null || currvdd.Version.IsOlderThan(verd.Version)) && verd.Size > 0)
          currvdd = verd;
      }
      return currvdd;
    }

    public override VersionDataDescriptor GetCurrentVersionDescriptor()
    {
      if (CurrentVersion != null)
      {
        currentVersion.DataStream = new FileStream(currentVersion.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
        return currentVersion;
      }
      return null;
    }
  }
}
