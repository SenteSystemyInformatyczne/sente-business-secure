﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenteUpdateTool
{
  public class VersionDataDescriptor
  {
    public long Size
    {
      get;
      set;
    }

    public Stream DataStream
    {
      get;
      set;
    }

    public string FileName
    {
      get;
      set;
    }

    public VersionInfo Version
    {
      get;
      set;
    }
  }
}
