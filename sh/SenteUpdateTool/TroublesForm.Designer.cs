﻿namespace SenteUpdateTool
{
  partial class TroublesForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.lblMessage = new System.Windows.Forms.Label();
      this.textBoxDetails = new System.Windows.Forms.TextBox();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.lblMessage);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(562, 34);
      this.panel1.TabIndex = 2;
      // 
      // lblMessage
      // 
      this.lblMessage.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblMessage.Location = new System.Drawing.Point(0, 0);
      this.lblMessage.Name = "lblMessage";
      this.lblMessage.Size = new System.Drawing.Size(562, 31);
      this.lblMessage.TabIndex = 2;
      this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // textBoxDetails
      // 
      this.textBoxDetails.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxDetails.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.textBoxDetails.Location = new System.Drawing.Point(0, 34);
      this.textBoxDetails.Multiline = true;
      this.textBoxDetails.Name = "textBoxDetails";
      this.textBoxDetails.ReadOnly = true;
      this.textBoxDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.textBoxDetails.Size = new System.Drawing.Size(562, 433);
      this.textBoxDetails.TabIndex = 3;
      // 
      // TroublesForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(562, 467);
      this.Controls.Add(this.textBoxDetails);
      this.Controls.Add(this.panel1);
      this.DoubleBuffered = true;
      this.Name = "TroublesForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Wystąpił błąd";
      this.TopMost = true;
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label lblMessage;
    private System.Windows.Forms.TextBox textBoxDetails;
  }
}