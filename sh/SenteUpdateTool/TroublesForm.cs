﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenteUpdateTool
{
  public partial class TroublesForm : Form
  {
    public Exception Cause
    {
      set
      {
        SetValues(value);
      }
    }

    private void SetValues(Exception value)
    {
      lblMessage.Text = value.Message;

      StringBuilder details = new StringBuilder();
      while (value != null)
      {
        details.AppendLine(value.Message);
        details.AppendLine();
        details.AppendLine(value.StackTrace);
        details.AppendLine();
        value = value.InnerException;
      }
      textBoxDetails.Text = details.ToString();
    }

    public TroublesForm()
    {
      InitializeComponent();
    }
  }
}
