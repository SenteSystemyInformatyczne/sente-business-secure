﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SHDataContracts.Model.AppSection;
using System.Collections.Generic;
using SHlib.DataExtraction.Model;
using Newtonsoft.Json.Linq;
using System.Text;
using SHlib.DataExtraction.Utilities;
using System.Linq;
using Newtonsoft.Json;
using SHlib.DataExtraction;
using System.Text.RegularExpressions;
using SHDataContracts.Model.AppSection.Subtypes.Enums;
using SHDataContracts.Model.AppSection.Subtypes;

namespace SHTests
{
  [TestClass]
  public class AppSectionJsonTests
  {
    const string actionName = "Action:TESTACTION";
    Dictionary<string, string> keyValueMap;

    string jsonSection;
    string appSection;
    [TestInitialize]
    public void Init()
    {
      keyValueMap = new Dictionary<string, string>
      {
        { "SINGLELINE", "SINGLELINEVALUE" },
        { "MULTILINE", "MULTI\r\n\r\nLINE"},
        { "SPECIAL", "SPE\\CI%qfind='sadf'%\r\n VAL\\UES \\IN #TEST#"}
      };

      JObject jsonObj = new JObject();
      jsonObj.Add(new JProperty("Name", actionName));
      jsonObj.Add(new JProperty("Elements", new JArray(
          keyValueMap.Select(e =>
          {
            return new JObject(
              new JProperty("Identifier", e.Key),
              new JProperty("Value", e.Value));
          }).ToArray()
         )));
      jsonSection = CustomMetadata.SKEY + "\r\n" + jsonObj.ToString();

      StringBuilder appObj = new StringBuilder(CustomMetadata.SKEY + "\r\n");
      appObj.AppendLine($"[{actionName}]");
      foreach (var e in keyValueMap)
      {
        appObj.AppendLine($"{e.Key}={e.Value}");
      }
      appSection = appObj.ToString();
    }

    [TestMethod]
    [TestProperty("AppSection", "Comparer")]
    public void AppSection_AppElement_Comparer_Test()
    {
      //given
      var e1 = new AppElement();
      e1.Identifier = "Action";

      var e2 = new AppElement();
      e2.Identifier = "Action";
      e2.NS = "SENTE";

      var e3 = new AppElement();
      e3.Identifier = "2Action";
      e3.NS = "SENTE";
      e3.Prefix = "A";

      var e4 = new AppElement();
      e4.Identifier = "1Action";
      e4.NS = "SENTE";
      e4.Prefix = "A";

      List<AppElement> elements = new List<AppElement> { e1, e2, e3, e4 };

      //when
      elements.Sort();

      //then
      Assert.AreEqual(e4, elements[0]);
      Assert.AreEqual(e3, elements[1]);
      Assert.AreEqual(e1, elements[2]);
      Assert.AreEqual(e2, elements[3]);
    }

    [TestMethod]
    [TestProperty("AppSection", "Build")]
    public void AppSection_Build_AppSectionObject_FromJsonFile_Test()
    {
      //given
      AppSection section = new AppSection(actionName, "", jsonSection);

      //when

      //then
      Assert.IsNotNull(section.AppSectionObject);
      Assert.AreEqual(actionName, section.AppSectionObject.Name);
      foreach (var e in section.AppSectionObject.Elements)
      {
        Assert.IsTrue(keyValueMap.ContainsKey(e.Identifier));
        Assert.AreEqual(keyValueMap[e.Identifier], e.Value);
      }
    }

    [TestMethod]
    [TestProperty("AppSection", "Build")]
    public void AppSection_Build_AppSectionObject_FromAppFile_Test()
    {
      //given
      string desc;
      var obj = AppSectionUpgrader.BuildAppSectionObject(appSection, out desc);
      AppSection section = new AppSection(obj, desc);

      //when

      //then
      Assert.IsNotNull(section.AppSectionObject);
      Assert.AreEqual(actionName, section.AppSectionObject.Name);
      foreach (var e in section.AppSectionObject.Elements)
      {
        Assert.IsTrue(keyValueMap.ContainsKey(e.Identifier));
        Assert.AreEqual(keyValueMap[e.Identifier], e.Value);
      }
    }

    [TestMethod]
    [TestProperty("AppSection", "Serializer")]
    public void AppSection_Serialize_Deserialize_Test()
    {
      //given
      string desc;
      var obj = AppSectionUpgrader.BuildAppSectionObject(appSection, out desc);

      //when
      string serialized = JsonConvert.SerializeObject(obj);
      var deserialized = JsonConvert.DeserializeObject<AppSectionRoot>(serialized);

      //then
      Assert.AreEqual(obj, deserialized);
    }

    [TestMethod]
    [TestProperty("AppSection", "SQL")]
    public void AppSection_CreateQuery_Test()
    {
      //given
      string desc;
      var obj = AppSectionUpgrader.BuildAppSectionObject(appSection, out desc);
      PrivateType creator = new PrivateType(typeof(MetadataExtractor));

      //when
      string query = (string)creator.InvokeStatic("AppIniSqlCreateSection", new AppSection(obj, ""), false, null);

      //then
      Assert.IsNotNull(query);
      Assert.IsFalse(string.IsNullOrWhiteSpace(query));
      Assert.IsTrue(query.StartsWith($"DELETE FROM S_APPINI WHERE SECTION = '{actionName}'"));

      var inserts = Regex.Split(query, @"INSERT INTO S_APPINI \(SECTION, IDENT, VAL, NAMESPACE, PREFIX\) VALUES", RegexOptions.Compiled).Skip(1);
      Assert.IsTrue(inserts.Count() > 0);
      foreach (var i in inserts)
      {
        var split = Regex.Split(i, ",");
        Assert.AreEqual(5, split.Length);
        var aName = split[0].Trim('(', '\'', ' ');
        var ident = split[1].Trim(' ', '\'');
        var val = split[2].Trim(' ', '\'');
        var ns = split[3].Trim(' ', '\'');
        var prefix = split[4].Trim(' ', '\'', ')', ';', '\'', '\r', '\n');
        Assert.AreEqual(actionName, aName);
        Assert.IsTrue(keyValueMap.ContainsKey(ident));
        Assert.AreEqual(keyValueMap[ident].Replace("'", "''"), val);
        Assert.AreEqual("SENTE", ns);
        Assert.AreEqual("NULL", prefix);
      }
    }

    [TestMethod]
    [TestProperty("AppSection", "ActionSubtypes")]
    public void AppSection_Action_Subtype_Parser_Test()
    {
      //given
      var json = "{\"Name\":\"Action:displaycrmklient\",\"Elements\":[{\"Identifier\":\"FormName\",\"Value\":\"CRMForm\",\"NS\":\"SENTE\",\"Prefix\":null},{\"Identifier\":\"Function\",\"Value\":\"DisplayWindow\",\"NS\":\"SENTE\",\"Prefix\":null},{\"Identifier\":\"If\",\"Value\":\"%if(%CKONTRAKTY.CPODMIOT%)%1%else%%msg(Brak określonego klienta w sprawie,Brak parametru,WARNING)%%end%\",\"NS\":\"SENTE\",\"Prefix\":null},{\"Identifier\":\"Param0:Name\",\"Value\":\"CRMPODMIOT\",\"NS\":\"SENTE\",\"Prefix\":null},{\"Identifier\":\"Param1:Value\",\"Value\":\"%CKONTRAKTY.CPODMIOT%\",\"NS\":\"SENTE\",\"Prefix\":null},{\"Identifier\":\"Signature\",\"Value\":\"PR12098\",\"NS\":\"SENTE\",\"Prefix\":null}]}";
      var obj = JsonConvert.DeserializeObject<AppSectionRoot>(json);

      //when
      ActionBase parsed = obj.ParseAction();

      //then
      Assert.IsInstanceOfType(parsed, typeof(ActionDisplayWindow));
      Assert.AreEqual("CRMForm", parsed.Parameters.Single(f => f.Name == "FormName").Value);
      Assert.AreEqual("CRMPODMIOT", parsed.Parameters.Single(f => f.Name == "Param0:Name").Value);
    }

    [TestMethod]
    [TestProperty("AppSection", "ActionSubtypes")]
    public void AppSection_Action_Detect_Subtype_Test()
    {
      //given
      var functions = new Dictionary<string, AppSectionSubtype>
      {
        { "BrowseTable", AppSectionSubtype.BrowseTable },
        { "SimpleBlank", AppSectionSubtype.SimpleBlank },
        { "DisplayWindow", AppSectionSubtype.DisplayWindow },
        { "EditRecord", AppSectionSubtype.EditRecord },
        { "NewRecord", AppSectionSubtype.NewRecord },
        { "PrintReport", AppSectionSubtype.PrintReport },
        { "DisplayRecord", AppSectionSubtype.DisplayRecord },
        { "RunSQL", AppSectionSubtype.RunSQL },
        { "BrowseQuery", AppSectionSubtype.BrowseQuery },
        { "Parser", AppSectionSubtype.Parser },
        { "DeleteRecord", AppSectionSubtype.DeleteRecord },
        { "Refresh", AppSectionSubtype.Refresh },
        { "Zapytanie", AppSectionSubtype.Query },
        { "ShowRights", AppSectionSubtype.ShowRights },
        { "ShowDefines", AppSectionSubtype.ShowDefines },
        { "Return", AppSectionSubtype.Return },
        { "MultiAction", AppSectionSubtype.MultiAction },
        { "ShowNotificationForm", AppSectionSubtype.ShowNotificationForm },
        { "RunNotification", AppSectionSubtype.RunNotification },
        { "DefineMediums", AppSectionSubtype.DefineMediums },
        { "ShowMediumsForm", AppSectionSubtype.ShowMediumsForm },
        { "ShowImportDefinesForm", AppSectionSubtype.ShowImportDefinesForm },
        { "ShowAction", AppSectionSubtype.ShowAction },
        { "ShowReplicatDefinition", AppSectionSubtype.ShowReplicatDefinition },
        { "ShowNavigatorAction", AppSectionSubtype.ShowNavigatorAction },
        { "ShowAppForm", AppSectionSubtype.ShowAppForm },
        { "EditCopy", AppSectionSubtype.EditCopy },
        { "DesignReport", AppSectionSubtype.DesignReport },
        { "EditDefines", AppSectionSubtype.EditDefines },
        { "PopupSQLu", AppSectionSubtype.PopupSQLu },
        { "TEST1" , AppSectionSubtype.Unknown },
        { "--Run SQL--" , AppSectionSubtype.Unknown }
      };

      //when

      //then
      foreach (var f in functions)
      {
        Assert.AreEqual(f.Value, AppSectionUtils.GetActionSubtype(f.Key));
      }
    }
  }
}
