﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SHlib.DataExtraction.Utilities;
using SHDataContracts.Model.AppSection;
using SHlib.DataExtraction.Model;

namespace SHTests
{
  [TestClass]
  public class AppSectionMigrationTests
  {
    string test;

    [TestInitialize]
    public void Init()
    {
      test = @"--@@@***SKEY***@@@--
[Action:TEST]
Define2:Type=Memo
Define2:Value=Szanowny Kliencie.

We wniosku o limit kredytowy załączony skan umowy z operatorem sieci telefonicznej jest nieprawdłowy.Prosimy o podesłanie prawidłowego skanu umowy.


Zespół RAW - POL
Function = ShowDefines";
    }

    [TestMethod]
    public void AppSection_Migration_OldFormatReader_Test()
    {
      //given
      PrivateType upgrader = new PrivateType(typeof(AppSectionUpgrader));
      Dictionary<string, string> kvmap = (Dictionary<string, string>) upgrader.InvokeStatic("GetAppSectionMap", test, null);

      //when

      //then
      Assert.AreEqual("Action:TEST", kvmap["Section"]);
      Assert.AreEqual(4, kvmap.Count);
      Assert.AreEqual("Memo", kvmap["Define2:Type"]);
      Assert.AreEqual("ShowDefines", kvmap["Function"]);
      var expected = @"Szanowny Kliencie.

We wniosku o limit kredytowy załączony skan umowy z operatorem sieci telefonicznej jest nieprawdłowy.Prosimy o podesłanie prawidłowego skanu umowy.


Zespół RAW - POL";
      var actual = kvmap["Define2:Value"];
      Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void AppSection_Migration_ParseOldAppSectionFile_Test()
    {
      PrivateType upgrader = new PrivateType(typeof(AppSectionUpgrader));
      Dictionary<string, string> kvmap = (Dictionary<string, string>) upgrader.InvokeStatic("GetAppSectionMap", test, null);
      string desc;
      AppSectionRoot obj = AppSectionUpgrader.BuildAppSectionObject(test, out desc);
      Assert.IsNotNull(obj);
      Assert.AreEqual(kvmap["Section"], obj.Name);
      foreach (var e in obj.Elements)
      {
        Assert.AreEqual(kvmap[e.Identifier], e.Value);
      }
    }


    [TestMethod]
    [TestProperty("AppSection", "MergeAppSection")]
    public void AppSection_Migration_MergeAppSection_AddNewItem_Test()
    {
      //given
      var e1 = new AppElement();
      e1.Identifier = "Action";
      e1.Value = "%.DOKUMNAGREF%";
      e1.NS = "SENTE";

      var e2 = new AppElement();
      e2.Identifier = "DOKUMPOZREF";
      e2.Value = "%.REF%";
      e2.NS = "SENTE";

      var e3 = new AppElement();
      e3.Identifier = "Function";
      e3.Value = "AddParowanie2";
      e3.NS = "SENTE";

      string desc;

      AppSectionRoot obj1 = AppSectionUpgrader.BuildAppSectionObject(test, out desc);
      AppSection appSection1 = new AppSection(obj1, desc);

      AppSectionRoot obj2 = AppSectionUpgrader.BuildAppSectionObject(test, out desc);
      AppSection appSection2 = new AppSection(obj2, desc);

      appSection1.AppSectionObject.Elements.Add(e1);
      appSection1.AppSectionObject.Elements.Add(e2);
      appSection1.AppSectionObject.Elements.Add(e3);

      //when
      AppSection result = AppSectionUpgrader.MergeAppSection(appSection1, appSection2);

      //then      
      Assert.AreEqual(result.AppSectionObject.Elements.Count,5);

      Assert.AreEqual(appSection1.AppSectionObject.Elements.Contains(e1), true);
      Assert.AreEqual(appSection1.AppSectionObject.Elements.Contains(e2), true);
      Assert.AreEqual(appSection1.AppSectionObject.Elements.Contains(e3), true);

      Assert.AreEqual(result.AppSectionObject.Elements.Contains(e1), true);
      Assert.AreEqual(result.AppSectionObject.Elements.Contains(e2), true);
      Assert.AreEqual(result.AppSectionObject.Elements.Contains(e3), true);

    }

    [TestMethod]
    [TestProperty("AppSection", "MergeAppSection")]
    public void AppSection_Migration_MergeAppSection_EditValueItem_Test()
    {
      //given
      var e1 = new AppElement();
      e1.Identifier = "Function";
      e1.Value = "AddParowanieNew";
      e1.NS = "SENTE";

      var e2 = new AppElement();
      e2.Identifier = "Function";
      e2.Value = "AddParowanie";
      e2.NS = "SENTE";

      string desc = "test";

      AppSectionRoot obj1 = new AppSectionRoot("Action:AddPAROWANIE");
      AppSection appSection1 = new AppSection(obj1, desc);

      AppSectionRoot obj2 = new AppSectionRoot("Action:AddPAROWANIE");
      AppSection appSection2 = new AppSection(obj2, desc);

      appSection1.AppSectionObject.Elements.Add(e1);
      appSection2.AppSectionObject.Elements.Add(e2);

      //when
      AppSection result = AppSectionUpgrader.MergeAppSection(appSection1, appSection2);

      //then      
      Assert.AreEqual(result.AppSectionObject.Elements.Count, 1);
      Assert.AreEqual(result.AppSectionObject.Elements[0], e2);

    }

  }
}
