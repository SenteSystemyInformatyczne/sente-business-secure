﻿using System;
using SHlib.DataExtraction;
using SHlib.DataExtraction.Model;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SHTests
{
  [TestClass]
  public class MetadataExtractorTests
  {
    [TestMethod]
    public void MetadataExtractor_SortTransferProcedures_Test()
    {
      var testList = new List<TransferProcedure>
        {
          new TransferProcedure("TRA_PR100_2", "", ""),
          new TransferProcedure("TRA_PR100", "", ""),
          new TransferProcedure("TRA_PR99_2", "", ""),
          new TransferProcedure("TRA_TEST", "", ""),
          new TransferProcedure("TRA_PR99", "", ""),
          new TransferProcedure("TRA_PR99_B", "", ""),
          new TransferProcedure("TRA_PR99_A", "", ""),
          new TransferProcedure("TRA_PR99_1", "", ""),
          new TransferProcedure("TRA_PR100_1", "", ""),
        };

      var sortedList = new List<TransferProcedure>
        {
          new TransferProcedure("TRA_PR99", "", ""),
          new TransferProcedure("TRA_PR99_1", "", ""),
          new TransferProcedure("TRA_PR99_2", "", ""),
          new TransferProcedure("TRA_PR99_A", "", ""),
          new TransferProcedure("TRA_PR99_B", "", ""),
          new TransferProcedure("TRA_PR100", "", ""),
          new TransferProcedure("TRA_PR100_1", "", ""),
          new TransferProcedure("TRA_PR100_2", "", ""),
          new TransferProcedure("TRA_TEST", "", ""),
        };

      var random = new Random();
      testList = testList.OrderBy(tr => random.Next()).ToList(); // tasowanie listy
      var resultList = MetadataExtractor.SortTransferProcedures(testList).ToList();

      Assert.AreEqual(sortedList.Count, resultList.Count);
      for (int i = 0; i < resultList.Count; i++)
      {
        Assert.AreEqual(sortedList[i].Name, resultList[i].Name);
      }
    }
  }
}
