﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignatureHelper
{
  public partial class UpdateProgress : Form
  {
    public UpdateProgress()
    {
      InitializeComponent();
    }

    public void ShowForm()
    {
      if (this.InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(() => ShowForm()));
        return;
      }
      this.Show();
    }

    public void CloseForm()
    {
      if (this.InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(() => CloseForm()));
        return;
      }
      this.Close();
    }

    public void SetProgress(int value)
    {
      if (progressBar1.InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(() => SetProgress(value)));
        return;
      }
      progressBar1.Value = value;
    }

    public void SetText(string value)
    {
      if (label1.InvokeRequired)
      {
        BeginInvoke(new MethodInvoker(() => SetText(value)));
        return;
      }
      label1.Text = value;
      SetProgress(0);
    }
  }
}
