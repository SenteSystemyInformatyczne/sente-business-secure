﻿using SignatureHelper.SettingsHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper
{
  /// <summary>
  /// Konfiguracja globalna Signature Helper
  /// </summary>
  class GlobalConfigs
  {
    static BindingList<SignatureHelper.SettingsForm.ProfileConfig> profiles = new BindingList<SignatureHelper.SettingsForm.ProfileConfig>();
    static BindingList<SignatureHelper.SettingsForm.DatabaseConfig> dbConfigs = new BindingList<SignatureHelper.SettingsForm.DatabaseConfig>();
    static bool initialized = false;

    static public BindingList<SignatureHelper.SettingsForm.ProfileConfig> Profiles
    {
      get
      {
        if (!initialized)
          Load();
        return profiles;
      }
    }

    static public BindingList<SignatureHelper.SettingsForm.DatabaseConfig> DBConfigs
    {
      get
      {
        if (!initialized)
          Load();
        return dbConfigs;
      }
    }

    public static void Refresh()
    {
      profiles.ResetBindings();
      dbConfigs.ResetBindings();
    }

    public static void Sort()
    {
      dbConfigs = new BindingList<SignatureHelper.SettingsForm.DatabaseConfig>(dbConfigs.OrderBy(db => db.Alias).ToList());
      profiles = new BindingList<SignatureHelper.SettingsForm.ProfileConfig>(profiles.OrderBy(p => p.Id).ToList());
    }

    /// <summary>
    /// Metoda ładująca konfigurację SH z pliku config
    /// </summary>
    public static void Load()
    {
      dbConfigs.Clear();
      foreach (Database item in DatabaseSettings.Default.Databases)
      {
        SignatureHelper.SettingsForm.DatabaseConfig dc = new SignatureHelper.SettingsForm.DatabaseConfig();
        dc.Alias = item.Alias;
        dc.Address = item.Address;
        dc.Port = item.Port;
        dc.Path = item.Path;
        dc.User = item.User;
        dc.Pass = item.Password;
        dc.Role = item.Role;
        dc.Id = item.Id;
        dc.Encoding = item.Encoding;
        dc.ConnectionPoolSize = item.ConnectionPool;
        dbConfigs.Add(dc);
      };
      dbConfigs = new BindingList<SignatureHelper.SettingsForm.DatabaseConfig>(dbConfigs.OrderBy(db => db.Alias).ToList());

      profiles.Clear();
      foreach (Profile item in ProfileSettings.Default.Profiles)
      {
        SignatureHelper.SettingsForm.ProfileConfig pc = new SignatureHelper.SettingsForm.ProfileConfig();
        pc.Id = item.Id;
        pc.Name = item.Name;
        pc.SourceDB_Id = item.SourceDB_Id;
        pc.DestDB_Id = item.DestDB_Id;
        pc.SourceDB_Name = item.SourceDB_Name;
        pc.DestDB_Name = item.DestDB_Name;
        pc.ScriptPath = item.ScriptPath;

        profiles.Add(pc);
      }
      profiles = new BindingList<SignatureHelper.SettingsForm.ProfileConfig>(profiles.OrderBy(p => p.Id).ToList());

      initialized = true;
    }
  }
}
