﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Diagnostics;
using System.Reflection;
using SHlib.DataExtraction;
using SHlib;
using System.Threading.Tasks;
using FirebirdSql.Data.Services;
using System.Threading;
using SHlib.Utilities;
using SHlib.DatabaseUpdate;
using System.Text.RegularExpressions;
using SignatureHelper.TxtEdit;
using NLog;
using SHDataContracts.Enums;
using SHDataContracts.Interfaces;
using SHlib.DataExtraction.Utilities;
using Squirrel;
using SignatureHelper.Analyzer;

namespace SignatureHelper
{
  public partial class MainForm : Form
  {
    public string dbver;
    public string lcver;
    public string srcver;
    private static Logger logger;
    private bool clearLog;

    public SignatureHelper.SettingsForm.ProfileConfig currentProfile;
    public SignatureHelper.SettingsForm.DatabaseConfig currentSourceDb;
    public SignatureHelper.SettingsForm.DatabaseConfig currentDestDb;

    private void createLogger()
    {
      logger = LogManager.GetLogger("MainLogger");
      LogManager.Configuration.Variables["log_path"] = Settings.Default.log_path;
    }

    RotatedMessageBuffer message;
    bool appending = true;

    public void AppendMessage(string text)
    {
      if (appending)
      {
        var norm = text.ToLower();
        if (norm.StartsWith("przetwarzam") || norm.StartsWith("wygenerowano plik"))
          return;
        if (textBox.InvokeRequired)
          textBox.BeginInvoke((MethodInvoker) (() =>
          {
            AppendMessage(text);
          }));
        else
        {
          logger.Log(LogLevel.Info, text);
          string timestamp = DateTime.Now.ToString();
          message.Append(String.Format("{0} {1}\r\n", timestamp, text));

        }
      }
    }

    public MainForm()
    {
      InitializeComponent();
      InitializeRegexManager();
      ApplySettings();

      createLogger();
      UpgradeSettings();
      SetCurrentVersion();

      Environment.SetEnvironmentVariable("PATH", Directory.GetCurrentDirectory());
      this.message = new RotatedMessageBuffer();

      SQLRepository.Instance.AppendMessage = AppendMessage;

      refresher.Tick += (s,a) => RefreshLog();
      refresher.Interval = 1000;
      refresher.Start();
    }

    /// <summary>
    /// Metoda zastosowująca ustawienia okna głównego.
    /// </summary>
    public void ApplySettings()
    {
      GenerateScriptMenuItem.Visible = Settings.Default.show_gen_script;
    }

    /// <summary>
    /// Metoda inicjalizująca kolekcję podstawowych regexów jeśli kolekcja jest pusta
    /// </summary>
    private void InitializeRegexManager()
    {
      RegexManager rm = Settings.Default.RegexManager;

      if (rm == null || (rm != null && rm.Count == 0))
      {
        rm = new RegexManager();
        RegexSettingItem regexSettingsItem = new RegexSettingItem()
        {
          Name = "Wyszukiwanie --XXX",
          Value = @"(?i)(?:((?:-{2,}\s*x{3,}[^>])(?:[^\n])*))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Wyszukiwanie -->XXX",
          Value = @"(?i)(?:(?:(?:-{2,}\s*(?:>|<)\s*x{3,})(?:(?:(?!x{3,})(?:\S|\w|\W))*)(?:\s*x{3,})))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Wyszukiwanie /* XXX */",
          Value = @"(?i)(((?:\/\*\s*x{3,}\s*\w*\*\/)(?:\S|\w|\W[^\*\/])*(?:\/\*\s*x{3,}\s*\w*\*\/)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "S_APPINI usuwanie akcji X-owych",
          Value = @"(?i)(DELETE\s*FROM\s*S_APPINI\s*WHERE\s*SECTION\s*[=]\s*(\'(\w*[\:]*[x]+\w*)\'))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych trigerów",
          Value = @"(?i)(?!(DROP\s+TRIGER\s+(\w*[x]+\w*)))(DROP\s+TRIGER\s+[\w*^x]*)"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych trigerów",
          Value = @"(?i)(DROP\s*TRIGER\s+(\w*[x]+\w*))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych procedur",
          Value = @"(?i)(DROP\s+procedure\s+(?!(RP|TRA|TRB|[x]))\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)$))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych procedur",
          Value = @"(?i)(DROP\s+procedure\s+(?!(RP|TRA|TRB))[x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)$))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych pól z tabel",
          Value = @"(?i)(?!ALTER\s*TABLE\s*\w*\s*DROP\s*(\w*[x]+\w*))([^']ALTER\s*TABLE\s*\w*\s*DROP\s*(\w*))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych pól z tabel",
          Value = @"(?i)(ALTER\s*TABLE\s*\w*\s*DROP\s*(\w*[x]+\w*))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych tabel",
          Value = @"(?i)(DROP\s+TABLE\s+[^x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych tabel",
          Value = @"(?i)(DROP\s+TABLE\s+[x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych indeksów",
          Value = @"(?i)(DROP\s+INDEX\s+[^x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych indeksów",
          Value = @"(?i)(DROP\s+INDEX\s+[x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie standardowych widoków",
          Value = @"(?i)(DROP\s+VIEW\s+[^x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        regexSettingsItem = new RegexSettingItem()
        {
          Name = "Usuwanie X-owych widoków",
          Value = @"(?i)(DROP\s+VIEW\s+[x]\w*[;])(?=(?:[^\^])*(?:set term \^\s*;))|(?=(^(SET TERM \;\s*\^)(\W|\w[^\^])*(SET TERM \^\s*;)))"
        };
        rm.Add(regexSettingsItem);

        Settings.Default.RegexManager = rm;
        Settings.Default.Save();
      }
    }

    /// <summary>
    /// Metoda odświeżająca 
    /// </summary>
    private void RefreshLog()
    {
      if (!message.Refresh())
        return;
      if (textBox.InvokeRequired)
        Invoke(new MethodInvoker(() =>
        {
          UpdateLog();
        }));
      else
      {
        UpdateLog();
      }
    }

    /// <summary>
    /// Metoda aktualizująca zawartość kontroli wyświetlającej logi aplikacji.
    /// </summary>
    private void UpdateLog()
    {
      var messages = message.GetMessages();
      var textBoxMsg = textBox.Text.Split('\n');

      var msgList = messages.Item1.AsEnumerable();
      if (messages.Item2 || clearLog)
      {
        textBox.Text = "";
        clearLog = false;
      }
      else
      {
        msgList = messages.Item1.Skip(textBoxMsg.Length - 1);
      }

      foreach (var m in msgList)
      {
        if (m.Contains("#warn#"))
        {
          textBox.AppendText(m.Replace("#warn#", "") + "\n", Color.Red);
        }
        else
        {
          textBox.AppendText(m + "\n");
        }
      }

      textBox.SelectionStart = textBox.TextLength;
      textBox.ScrollToCaret();
    }

    private void UpgradeSettings()
    {
      if (Settings.Default.call_update)
      {
        Settings.Default.Upgrade();
        Settings.Default.call_update = false;
        Settings.Default.Save();
      }
    }

    private void SetCurrentVersion()
    {
      toolStripStatusVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }

    private void CheckForUpdates()
    {
      if ((DateTime.Now - Settings.Default.last_update_check).Days > 1)
      {
        Settings.Default.last_update_check = DateTime.Today;
        Settings.Default.Save();

        if (File.Exists(@"updatetool\SenteUpdateTool.exe"))
        {
          ProcessStartInfo pi = new ProcessStartInfo()
          {
            Arguments = Application.ExecutablePath,
            UseShellExecute = true,
            FileName = @"SenteUpdateTool.exe",
            WorkingDirectory = "updatetool"
          };
          Process.Start(pi);

          Close();
        }
      }
    }

    private void settingsButton_Click(object sender, EventArgs e)
    {
      SettingsForm settingsForm = new SettingsForm(this.message, this);
    }

    private async void DbExport_Click(object sender, EventArgs e)
    {
      Clear();

      if (!CheckSettings())
        return;

      ChangeVisibilityIfProcessIsRunning(false);
      string scriptContent = "";
      string signature = Interaction.InputBox("Wpisz sygnaturę", "Szukaj sygnatury", "");
      if (signature != "")
      {
        PrepareConnections();
        var pi = ProgressInfoFactory();
        DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Source, AppendMessage, ErrorMessage, Settings.Default.grant_filter);
        try
        {
          IsBusy = true;
          CurrentCancellationToken = new CancellationTokenSource();
          scriptContent = await Task.Run(() => de.ExportSignature(currentProfile.ScriptPath, signature, pi, true), CurrentCancellationToken.Token);
        }
        catch (Exception ex)
        {
          ExpandException(ex);
          MessageBox.Show(ex.Message, "Błąd eksportu", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        finally
        {
          IsBusy = false;
        }
      }
      ChangeVisibilityIfProcessIsRunning(true);
      AppendMessage("Zakończono");
      //if (!string.IsNullOrWhiteSpace(scriptContent))
      //{
      //  var scriptForm = new ScriptForm()
      //  {
      //    ExecuteScriptCallback = ExecuteScript,
      //    Script = scriptContent,
      //    ShowUpdateHashCodeButton = false,
      //    mf = this
      //  };
      //  scriptForm.Show();
      //}   
    }

    public ProgressInfo ProgressInfoFactory()
    {
      ProgressInfo pi = new ProgressInfo();
      pi.WriteCount = WriteCount;
      pi.WriteProgress = WriteProgress;
      pi.Write = AppendMessage;
      pi.MessageBoxForUUIDMismatch = MessageBoxShow;
      return pi;
    }

    public void ErrorMessage(Exception obj)
    {
      while (obj != null)
      {
        AppendMessage(obj.Message);
        AppendMessage(obj.StackTrace);
        obj = obj.InnerException;
      }
    }

    /// <summary>
    /// Pobiera ustawienia bazy loklanej
    /// </summary>
    /// <returns></returns>
    public IDbSettings GetLocalSettings()
    {
      var settings = new DbSettings(ConnectionType.Local)
      {
        DatabaseFilePath = "",
        ServerAddress = "localhost",
        Name = "lokalna",
        User = Settings.Default.local_user,
        Password = Crypt.Instance.Decrypt(Settings.Default.local_password),
        Port = Settings.Default.local_port.ToString(),
        Role = Settings.Default.local_role,
        Encoding = Settings.Default.local_encoding,
        ConnectionPoolSize = Settings.Default.connection_pool
      };
      return settings;
    }

    /// <summary>
    /// Pobiera ustawienia bazy źródłowej
    /// </summary>
    /// <returns></returns>
    public IDbSettings GetSourceSettings()
    {
      if (currentDestDb == null)
        throw new ArgumentNullException("Nie ustawiono bazy źródłowej w bieżacym profilu");

      var settings = new DbSettings(ConnectionType.Source)
      {
        DatabaseFilePath = currentSourceDb.Path,
        ServerAddress = currentSourceDb.Address,
        Name = currentSourceDb.Alias,
        User = currentSourceDb.User,
        Password = Crypt.Instance.Decrypt(currentSourceDb.Pass),
        Port = currentSourceDb.Port,
        Role = currentSourceDb.Role,
        Encoding = currentSourceDb.Encoding,
        ConnectionPoolSize = currentSourceDb.ConnectionPoolSize
      };
      return settings;
    }

    /// <summary>
    /// Pobiera ustawienia bazy docelowej
    /// </summary>
    /// <returns></returns>
    public IDbSettings GetDestSettings()
    {
      if (currentDestDb == null)
        throw new ArgumentNullException("Nie ustawiono bazy docelowej w bieżacym profilu");

      var settings = new DbSettings(ConnectionType.Remote)
      {
        DatabaseFilePath = currentDestDb.Path,
        ServerAddress = currentDestDb.Address,
        Name = currentDestDb.Alias,
        User = currentDestDb.User,
        Password = Crypt.Instance.Decrypt(currentDestDb.Pass),
        Port = currentDestDb.Port,
        Role = currentDestDb.Role,
        Encoding = currentDestDb.Encoding,
        ConnectionPoolSize = currentDestDb.ConnectionPoolSize
      };
      return settings;
    }

    /// <summary>
    /// Sprawdza poprawność ustawień pod kątem ostrzeżeń niekrytycznych.
    /// </summary>
    /// <returns>pusty string lub komunikaty ostrzeżeń, jeśli jakieś były</returns>
    private string CheckSoftSettings()
    {
      StringBuilder sb = new StringBuilder();
      sb.Append(IBEScriptProcess.isIBScriptVersionOk(Settings.Default.ibexpert_path + IBEScriptProcess.IBESCRIPT_NAME) ? "" : "#warn#Nieaktualna wersja programu: " + Settings.Default.ibexpert_path + IBEScriptProcess.IBESCRIPT_NAME);
      return sb.ToString();
    }

    /// <summary>
    /// Sprawdza poprawność ustawień istotnych.
    /// </summary>
    /// <returns>pusty string lub komunikaty błędów, jeśli jakieś były</returns>
    private string CheckHardSettings()
    {
      StringBuilder sb = new StringBuilder();
      var src_settings = GetSourceSettings();
      var dest_settings = GetDestSettings();
      sb.Append(Tools.CheckDatabaseConnection(src_settings, "źródłową"));
      sb.Append(Tools.CheckDatabaseConnection(dest_settings, "źródłową"));
      sb.Append(Tools.CheckFileExistence(Settings.Default.ibexpert_path + IBEScriptProcess.IBESCRIPT_NAME));
      sb.Append(Tools.CheckDirectoryExistence(currentProfile.ScriptPath));
      sb.Append(Tools.CheckDirectoryExistence(Settings.Default.tempFile_path));
      if (Settings.Default.backup_mode == "lock")
      {
        object ret = DatabaseManager.Instance.ExecuteScalar(dest_settings, CHECKDELTAMODE);
        if (ret != null && (Convert.ToInt32(ret) != 0))
        {
          sb.Append("Baza docelowa jest już zalockowana. Zmerguj zmiany lub zrollbackuj je nbackupem");
          ShowInfoAboutUnlockDatabase();
        }
      }
      return sb.ToString();
    }

    /// <summary>
    /// Wyświetla messageboxa z informacją jak odlokować bazę danych.
    /// </summary>
    private void ShowInfoAboutUnlockDatabase()
    {
      var dest_settings = GetDestSettings();
      string destPath = dest_settings.DatabaseFilePath;

      if (String.IsNullOrEmpty(destPath))
        destPath = "ścieżkaDoBazy";

      MessageBox.Show($@"Baza docelowa ma założonego lock'a. Aby wykonać rollback wszystkich zmian na bazie, wykonaj następujące polecenia:
1. Uruchom wiersz poleceń jako administrator.
2. Przejdź do katalogu w którym znajduje się program nbackup.
  * w windows: c:\Program Files(x86)\Firebird\Firebird_2_5\bin 
  * w linux: /opt/firebird/bin
3. Wykonaj polecenie: nbackup -F {destPath} 
4. Baza powinna być odlockowana.", 
        "Baza docelowa jest już zalockowana.", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    /// <summary>
    /// Sprawdza poprawność ustawień i wyświetla ostrzeżenia oraz błędy.
    /// </summary>
    /// <returns>true, jeśli nie ma błędów istotnych, w przeciwnym razie false</returns>
    private bool CheckSettings()
    {
      var msg = CheckHardSettings();
      if (msg == "")
      {
        AppendMessage("Ustawienia prawidłowe.");
      }
      else
      {
        AppendMessage(msg);
        return false;
      }
      msg = CheckSoftSettings();
      if (msg != "")
      {
        AppendMessage(msg);
      }
      return true;
    }

    const string CANCELTEXT = "Zatrzymaj";
    const string CANCELINGTEXT = "Zatrzymywanie...";

    /// <summary>
    /// Wydarzenie po kliknięciu. Zależnie od tekstu na przycisku, zaczyna lub anuluje procesu generowania skryptu dla pełnej aktualizacji bazy. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void GenScript_Click(object sender, EventArgs e)
    {
      if (!CheckSettings())
        return;

      var msgs = Tools.CheckIfDBScriptFolderIsEmpty(currentProfile.ScriptPath);
      if (msgs.Count > 0)
      {
        foreach (var msg in msgs)
          AppendMessage(msg);

        return;
      }

      // Validacja plików dbscripts
      bool result = await ValidateDLLOnDbscripts();
      if (!result)
      {
        AppendMessage("#warn#Pełny proces został przerwany ponieważ skrypty zawierają błędy!");
        return;
      }
      // Koniec validacji dbscirptów.

      if (zaktualizujBazęToolStripMenuItem1.Text == CANCELTEXT && CurrentCancellationToken != null)
      {
        CurrentCancellationToken.Cancel(false);
        zaktualizujBazęToolStripMenuItem1.Text = CANCELINGTEXT;
        AppendMessage("Trwa zatrzymywanie procesu. Może to potrwać kilka minut, zachowaj cierpliwość!");
        appending = false;
        zaktualizujBazęToolStripMenuItem1.Enabled = false;
        return;
      }

      Clear();
      string srcpath = currentProfile.ScriptPath;
      PrepareConnections();
      var destDb = Connection.Instance.GetConnection(ConnectionType.Remote);
      var srcDb = Connection.Instance.GetConnection(ConnectionType.Source);
      var localDb = Connection.Instance.GetConnection(ConnectionType.Local);

      var tempFolder = Path.Combine(Settings.Default.tempFile_path, "sh_" + Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
      Directory.CreateDirectory(tempFolder);

      AppendMessage("Aktualizowanie bazy " + destDb.ToString());

      //DatabaseExtractor source_de = new DatabaseExtractor(ConnectionTypes.Source, AppendMessage, ErrorMessage);
      //DatabaseExtractor remote_de = new DatabaseExtractor(ConnectionTypes.Remote, AppendMessage, ErrorMessage);

      var pi = ProgressInfoFactory();

      var scriptForm = new ScriptForm()
      {
        ExecuteScriptCallback = ExecuteScript
      };

      var notr = Settings.Default.no_tr;
      var fname = Path.Combine(tempFolder,
        Path.GetFileNameWithoutExtension(destDb.DatabaseFilePath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"));

      var fdbEmptyName = Path.Combine(tempFolder,
        "meta_" + Path.GetFileNameWithoutExtension(destDb.DatabaseFilePath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss")) + ".fdb";

      var fbkName = Path.ChangeExtension(fname, "fbk");
      var fdbName = Path.ChangeExtension(fname, "fdb");
      var copySettings = (DbSettings) localDb.Clone();
      copySettings.DatabaseFilePath = fdbName;
      var emptySettings = (DbSettings) localDb.Clone();
      emptySettings.DatabaseFilePath = fdbEmptyName;

      ScriptsResult scripts = null;

      var orgscript = zaktualizujBazęToolStripMenuItem1.Text;
      try
      {
        zaktualizujBazęToolStripMenuItem1.Text = CANCELTEXT;
        ChangeVisibilityIfProcessIsRunning(false);

        IsBusy = true;
        CurrentCancellationToken = new CancellationTokenSource();
        UpdateDatabaseTask = Task.Run(() => UpdateDatabase(srcpath, tempFolder, pi, fbkName, fdbName, srcDb, emptySettings, copySettings, destDb), CurrentCancellationToken.Token);
        scripts = await (Task<ScriptsResult>)UpdateDatabaseTask;
        if (scripts != null)
        {
          FastUpdate_Click(sender, e);
        }
      }
      finally
      {
        IsBusy = false;
        UpdateDatabaseTask = null;
        zaktualizujBazęToolStripMenuItem1.Text = orgscript;
        if (CurrentCancellationToken != null)
          CurrentCancellationToken = null;
        ChangeVisibilityIfProcessIsRunning(true);
      }
      AppendMessage("Zakończono");
    }

    /// <summary>
    /// obsługa wstrzymania procesu do czasu komunikatu
    /// </summary>
    static AutoResetEvent PauseUpdateResetEvent = new AutoResetEvent(false);

    /// <summary>
    /// Metoda przeprowadzająca pełną aktualizację bazy danych
    /// </summary>
    /// <param name="srcpath">Ścieżka do skryptów bazodanoych</param>
    /// <param name="tempPath">Ścieżka dla plików tymczasowych</param>
    /// <param name="pi">Obiekt do zwracania komunikatów o postępie</param>
    /// <param name="fbkName">Nazwa pliku bazy danych</param>
    /// <param name="fdbName">Nazwa pliku backupu bazy danych</param>
    /// <param name="srcSettings">Dane do połączenia do bazy źródłowej</param>
    /// <param name="emptySettings">Dane do połączenia do bazy metadanych</param>
    /// <param name="copySettings">Dane do połączenia do lokalnej kopii bazy docelowej</param>
    /// <param name="destSettings">Dane do połączenia do bazy docelowej</param>
    /// <returns></returns>
    private ScriptsResult UpdateDatabase(string srcpath, string tempPath, ProgressInfo pi,
      string fbkName, string fdbName,
       IDbSettings srcSettings, IDbSettings emptySettings, IDbSettings copySettings, IDbSettings destSettings)
    {
      ScriptsResult scripts = null;
      bool ok = true;
      if (!CheckSettings())
        return null;

      bool usenbackup = Settings.Default.backup_mode == "lock";
      bool localdestdb = destSettings.ServerAddress.ToLower() == "localhost";
      if (usenbackup)
      {
        if (!localdestdb)
        {
          if (MessageBox.Show("Został wybrany tryb lockowania bazy danych i baza ta znajduje się na zdalnym serwerze. " +
            "W przypadku błędów w skrypcie różnicowym konieczny będzie dostęp do konta na serwerze bazy danych. " +
            "\n\nCzy kontynuować?", "Uwaga", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
          {
            AppendMessage("Proces został zatrzymany na życzenie użytkownika.");
            return null;
          }
        }
      }
      bool nbackuplocked = false;
      bool dblocked = false;
      var cts = new CancellationTokenSource();
      try
      {
        AppendMessage("Wszystkie pliki tymczasowe są tworzone w " + tempPath);

        DatabasePatch.CheckSHStructuresIntegrity(pi, AppendMessage, updateHashes: false);
        DatabasePatch.CheckDatabaseId(srcpath, destSettings, pi);

        CheckCancellation();

        FilesystemExtractor fe = new FilesystemExtractor(AppendMessage, ErrorMessage);
        scripts = fe.GenerateScript(srcpath, pi, ConnectionType.Remote, true, destpath: tempPath, alwaysGenerateHashes: false, checkIntegrity: false);
        CheckCancellation();

        List<Task> tasks = new List<Task>();

        if (!usenbackup)
        {
          dblocked = LockDatabase(destSettings);
          tasks.Add(CreateMetadataDatabase(tempPath, pi, emptySettings, scripts, cts));
          tasks.Add(
            CreateLocalDatabaseCopyThroughGbak(tempPath, pi, fbkName, fdbName, emptySettings,
              copySettings, destSettings, scripts, cts));
        }
        else
        {
          LockDestinationDatabase(destSettings, Path.GetFileName(tempPath));
          dblocked = LockDatabase(destSettings);
          tasks.Add(CreateMetadataDatabase(tempPath, pi, emptySettings, scripts, cts));
          nbackuplocked = true;
        }
        CheckCancellation();

        var t = Task.WhenAll(tasks.ToArray()).ContinueWith((_) =>
        {
          if (!cts.Token.IsCancellationRequested)
          {
            string diffpath;

            if (!usenbackup)
              diffpath = GenerateDifferentialScript(tempPath, emptySettings, copySettings, cts.Token, fe);
            else
              diffpath = GenerateDifferentialScript(tempPath, emptySettings, destSettings, cts.Token, fe);

            if (Settings.Default.show_scripts)
            {
              AppendMessage("Wybrano opcję pauzy w celu obejrzenia skryptów.");
              AppendMessage("Wgrane będą pliki before.esystem.sql, postdiffscritpt.sql i after.esystem.sql.");

              //Wczytanie skryptów z dysku
              string pathToBeforeSQL = tempPath + @"\before.esystem.sql";
              string pathToPostDiffSQL = tempPath + @"\postdiffscript.sql";
              string pathToAfterSQL = tempPath + @"\after.esystem.sql";

              string script = "";
              if(File.Exists(pathToBeforeSQL))
                script = File.ReadAllText(pathToBeforeSQL);

              if(File.Exists(pathToPostDiffSQL))
                script += File.ReadAllText(pathToPostDiffSQL);

              if(File.Exists(pathToAfterSQL))
                script += File.ReadAllText(pathToAfterSQL);

              this.Invoke(new MethodInvoker(() =>
              {
                var scriptForm = new ScriptForm()
                {
                  ShowPanelWithButton = false
                };
                scriptForm.Script = script;

                scriptForm.ShowDialog();
              }));

              PauseProcess();
              CheckCancellation();
            }

            if (!usenbackup)
            {
              AppendMessage("Procedury transferowe before");
              ExecuteScriptFile(destSettings, tempPath, "before.esystem.sql", cts.Token);
              CheckCancellation();
              AppendMessage("Skrypt różnicowy");
              ExecuteScriptFile(destSettings, tempPath, Path.GetFileName(diffpath), cts.Token);
              CheckCancellation();
              AppendMessage("Procedury transferowe after");
              ExecuteScriptFile(destSettings, tempPath, "after.esystem.sql", cts.Token);
              CheckCancellation();
              AppendMessage("Odblokowanie bazy docelowej " + destSettings);
              DatabaseManager.Instance.OnlineDb(destSettings);
              dblocked = false;
            }
            else
            {
              CheckCancellation();
              DatabaseManager.Instance.OnlineDb(destSettings);
              UnlockDestinationDatabase(destSettings);
              dblocked = false;
            }
            if (Settings.Default.recompile_all)
            {
              CheckCancellation();
              AppendMessage("Rekompilacja procedur i triggerów");
              RecompileAll(destSettings, tempPath, cts.Token);
            }
            CheckCancellation();
            AppendMessage("Przeliczanie i aktualizacja HashCodes");
            MetadataChecksum.UpdateAllHashesInDatabase(ConnectionType.Remote, pi, AppendMessage, ErrorMessage, Settings.Default.grant_filter);
          }
        }, cts.Token);

        t.Wait();

        if (t.IsFaulted)
          ok = HandleException(tempPath, destSettings, usenbackup, localdestdb, nbackuplocked, ref dblocked, t.Exception);
        else
          ok = true;
      }
      catch (Exception e)
      {
        cts.Cancel();
        ok = HandleException(tempPath, destSettings, usenbackup, localdestdb, nbackuplocked, ref dblocked, e);
      }
      finally
      {
        cts.Dispose();
      }

      if (!ok)
      {
        bool forcedelete = false;
        appending = true;
        AppendMessage("Zatrzymano proces aktualizacji bazy.");
        forcedelete =
          MessageBox.Show("Pozostawić pliki tymczasowe?", "Błąd aktualizacji", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            == System.Windows.Forms.DialogResult.No;

        scripts = null;//nie generuj appini

        if (forcedelete)
        {
          if (Directory.Exists(tempPath))
          {
            if (File.Exists(emptySettings.DatabaseFilePath))
              DatabaseManager.Instance.ShutdownDb(emptySettings, AppendMessage, FbShutdownOnlineMode.Full);
            Directory.Delete(tempPath, true);
          }
        }
        AppendMessage("Zakończono z błędem.");
      }
      else
        AppendMessage("Zakończono poprawnie.");

      return scripts;
    }

    /// <summary>
    /// Obsługa wyjątków. Odblokowanie bazy po wystąpieniu.
    /// </summary>
    /// <param name="tempPath"></param>
    /// <param name="destSettings"></param>
    /// <param name="usenbackup"></param>
    /// <param name="localdestdb"></param>
    /// <param name="nbackuplocked"></param>
    /// <param name="dblocked"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    private bool HandleException(string tempPath, IDbSettings destSettings, bool usenbackup, bool localdestdb, bool nbackuplocked, ref bool dblocked, Exception e)
    {
      bool ok;
      AppendMessage(e.Message);
      if (usenbackup && nbackuplocked)
      {
        if (localdestdb)
        {
          UnlockAndFixupDatabase(destSettings, tempPath, localdestdb, ref dblocked);
        }
        else
        {
          if (!dblocked)
          {
            DatabaseManager.Instance.ShutdownDb(destSettings, AppendMessage, FbShutdownOnlineMode.Single);
            dblocked = true;
          }
          WaitForSomeoneToFixupDatabase(destSettings, localdestdb, ref dblocked);
        }
      }
      ok = false;
      if (dblocked)
      {
        AppendMessage("Odblokowanie bazy docelowej " + destSettings);
        DatabaseManager.Instance.OnlineDb(destSettings);
      }

      return ok;
    }


    /// <summary>
    /// Zablokowanie bazy
    /// </summary>
    /// <param name="destSettings"></param>
    /// <returns></returns>
    private bool LockDatabase(IDbSettings destSettings)
    {
      bool dblocked;
      AppendMessage("Zamykanie bazy " + destSettings);
      DatabaseManager.Instance.ShutdownDb(destSettings, AppendMessage, FbShutdownOnlineMode.Multi);
      dblocked = true;
      CheckCancellation();
      return dblocked;
    }

    /// <summary>
    /// Odblokowanie i próba zmergowania pliku delta
    /// </summary>
    /// <param name="destSettings"></param>
    /// <param name="tempPath"></param>
    /// <param name="localdestdb"></param>
    /// <param name="dblocked"></param>
    private void UnlockAndFixupDatabase(IDbSettings destSettings, string tempPath, bool localdestdb, ref bool dblocked)
    {
      AppendMessage("Zdjęcie locka z bazy bez mergowania pliku .delta");
      var tool = Path.Combine(Settings.Default.local_fb_bin_path, "nbackup");
      ProcessStartInfo psi = new ProcessStartInfo(tool, "-F " + destSettings.DatabaseFilePath);
      psi.CreateNoWindow = true;
      psi.WindowStyle = ProcessWindowStyle.Hidden;
      psi.RedirectStandardError = true;
      psi.RedirectStandardInput = true;
      psi.RedirectStandardOutput = true;
      psi.UseShellExecute = false;
      var pi = new Process();
      pi.StartInfo = psi;
      bool error = false;
      pi.OutputDataReceived += (s, e) =>
      {
        AppendMessage(e.Data);
      };
      pi.ErrorDataReceived += (s, e) =>
      {
        AppendMessage(e.Data);
        error = true;
      };

      pi.Start();
      pi.WaitForExit();
      error |= pi.ExitCode != 0;

      if (error)
      {
        WaitForSomeoneToFixupDatabase(destSettings, localdestdb, ref dblocked);
      }
      else
      {
        if (dblocked)
        {
          DatabaseManager.Instance.OnlineDb(destSettings);
          dblocked = false;
        }
        var mode = DatabaseManager.Instance.ExecuteScalar(destSettings, CHECKDELTAMODE);
        if (mode != null && (Convert.ToInt32(mode) == 0))
          AppendMessage("Operacja zdjęcia locka z bazy bez mergowania pliku .delta przebiegła poprawnie");
        else
        {
          if (!dblocked)
          {
            DatabaseManager.Instance.ShutdownDb(destSettings, AppendMessage, FbShutdownOnlineMode.Full);
            dblocked = true;
          }
          WaitForSomeoneToFixupDatabase(destSettings, localdestdb, ref dblocked);
        }

        AppendMessage("Usuwanie tworzenia specjalnego pliku .delta " + destSettings.DeltaFilePath);
        DatabaseManager.Instance.ExecuteStatement(destSettings, RESETDELTAFILENAME);

        if (!string.IsNullOrWhiteSpace(PreviousDeltaFileName) && !PreviousDeltaFileName.Contains("sh_"))
        {
          AppendMessage("Ustawianie poprzednio zdefiniowanego pliku .delta " + PreviousDeltaFileName);
          DatabaseManager.Instance.ExecuteStatement(destSettings, string.Format(SETDELTAFILENAME, PreviousDeltaFileName));
        }

        try
        {
          AppendMessage("Usuwanie pliku .delta z dysku " + destSettings.DeltaFilePath);

          try
          {
            DatabaseManager.Instance.ShutdownDb(destSettings, AppendMessage, FbShutdownOnlineMode.Full);
            dblocked = true;

            File.Delete(destSettings.DeltaFilePath);
          }
          finally
          {
            DatabaseManager.Instance.OnlineDb(destSettings);
            dblocked = false;
          }

        }
        catch (Exception e)
        {
          AppendMessage(e.Message);
        }
      }
    }

    /// <summary>
    /// Oczekiwanie na naprawę bazy po nieudanym mergowaniu pliku delta
    /// </summary>
    /// <param name="destSettings"></param>
    /// <param name="localdestdb"></param>
    /// <param name="dblocked"></param>
    private void WaitForSomeoneToFixupDatabase(IDbSettings destSettings, bool localdestdb, ref bool dblocked)
    {
      string server = string.Empty;
      if (!localdestdb)
        server = "zaloguj się na serwerze " + destSettings.ServerAddress + " ";
      AppendMessage(string.Format("Błąd podczas mergowania pliku delta. Samodzielnie " +
        "{0}popraw przyczyny i wywołaj nbackup -F {1}", server, destSettings.DatabaseFilePath));
      AppendMessage("Potem możesz usunąć plik " + destSettings.DeltaFilePath);
      // Wyswietlam messageboxa z informacją jak odlockować DB.
      ShowInfoAboutUnlockDatabase();

      var dbOk = false;
      var quitAnyway = false;
      while (!dbOk)
      {
        var res = MessageBox.Show("Baza nie została jeszcze odlockowana.", "Potwierdzenie", MessageBoxButtons.RetryCancel, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
        quitAnyway = res == DialogResult.Cancel;
        if (quitAnyway)
          break;

        try
        {
          DatabaseManager.Instance.OnlineDb(destSettings, FbShutdownOnlineMode.Normal);
          var ret = DatabaseManager.Instance.ExecuteScalar(destSettings, CHECKDELTAMODE, forceRefresh: true);
          dbOk = (ret != null) && (Convert.ToInt32(ret) == 0);
          if (dbOk)
            AppendMessage("Baza została poprawnie odlockowana");
        }
        catch (Exception e)
        {
          AppendMessage(e.Message);
        }
      }
    }

    public string UNLOCKDBSCRIPT = "ALTER DATABASE END BACKUP;";

    /// <summary>
    /// Odblokowanie bazy
    /// </summary>
    /// <param name="destSettings"></param>
    private void UnlockDestinationDatabase(IDbSettings destSettings)
    {
      AppendMessage("Zdjęcie locka z bazy " + destSettings);
      DatabaseManager.Instance.ExecuteStatement(destSettings, UNLOCKDBSCRIPT);
    }

    /// <summary>
    /// Task tworzący bazę metadanych
    /// </summary>
    /// <param name="tempPath"></param>
    /// <param name="pi"></param>
    /// <param name="emptySettings"></param>
    /// <param name="scripts"></param>
    /// <param name="cts"></param>
    /// <returns></returns>
    private Task CreateMetadataDatabase(string tempPath, ProgressInfo pi, IDbSettings emptySettings, ScriptsResult scripts, CancellationTokenSource cts)
    {
      // Task tworzący pusta bazę danych i wypełniający ją metadanymi
      return Task.Run(() =>
      {
        try
        {
          AppendMessage("Tworzenie bazy metadanych");
          using (var db = new Database(emptySettings))
          {
            emptySettings.CreateDatabase(16384);
            CheckCancellation();

            AppendMessage("Tworzenie bazy metadanych - inicjalizacja");
            ExecuteScript(emptySettings, tempPath, "emptyInitDb.sql", ProcedurePatch.EMPTY_SCRIPT_DB, cts.Token);
            CheckCancellation();

            DatabasePatch.PatchOutputDatabase(emptySettings, pi, AppendMessage, updateHashes: false);
            CheckCancellation();
            AppendMessage("Tworzenie bazy metadanych - wgrywanie metadanych");
            ExecuteScript(emptySettings, tempPath, "emptyMetaDb.sql", scripts.Metadata, cts.Token);
            CheckCancellation();
            AppendMessage("Zakonczono tworzenie bazy metadanych");
          }
        }
        catch (Exception e)
        {
          AppendMessage(e.Message);
          cts.Cancel();
        }
      }, cts.Token);
    }


    /// <summary>
    /// Metoda tworząca skrypt różnicowy
    /// </summary>
    /// <param name="tempPath"></param>
    /// <param name="emptySettings"></param>
    /// <param name="copySettings"></param>
    /// <param name="ct"></param>
    /// <returns></returns>
    private string GenerateDifferentialScript(string tempPath, IDbSettings emptySettings, IDbSettings copySettings, CancellationToken ct, FilesystemExtractor fe = null)
    {
      AppendMessage("Procedury transferowe na bazie " + copySettings.Name);
      ExecuteScriptFile(copySettings, tempPath, "before.esystem.sql", ct);
      CheckCancellation();

      AppendMessage("Generowanie skryptu różnicowego");
      var diffpath = GenerateDifferentialScript(emptySettings, copySettings, tempPath, ct);
      CheckCancellation();
      AppendMessage("Postprocessing skryptu różnicowego");
      diffpath = DifferentialFile.Postprocess(diffpath, scriptPath: currentProfile.ScriptPath, fe: fe);
      CheckCancellation();

      AppendMessage("Testowe wgrywanie na bazę " + copySettings.Name);
      AppendMessage("Skrypt różnicowy");
      ExecuteScriptFile(copySettings, tempPath, Path.GetFileName(diffpath), ct);
      CheckCancellation();
      AppendMessage("Procedury transferowe after");
      ExecuteScriptFile(copySettings, tempPath, "after.esystem.sql", ct);
      CheckCancellation();

      AppendMessage("Wszystko ok, aktualizacja bazy docelowej");
      return diffpath;
    }

    public string LOCKDBSCRIPT = "ALTER DATABASE BEGIN BACKUP;";
    public string DELTAFILENAME = "select first 1 rdb$file_name from rdb$files where bin_and(RDB$FILE_FLAGS,32)=32";
    public string RESETDELTAFILENAME = "alter database drop difference file";
    public string SETDELTAFILENAME = "alter database add difference file '{0}'";
    public string CHECKDELTAMODE = "select mon$backup_state from mon$database";

    private string PreviousDeltaFileName;

    /// <summary>
    /// Zablokowanie bazy docelowej
    /// </summary>
    /// <param name="destSettings"></param>
    /// <param name="deltaName"></param>
    private void LockDestinationDatabase(IDbSettings destSettings, string deltaName)
    {
      var dirpos = destSettings.DatabaseFilePath.LastIndexOfAny(new char[] { '\\', '/' });
      string dir = "";
      if (dirpos >= 0)
        dir = destSettings.DatabaseFilePath.Substring(0, dirpos + 1);
      dir += deltaName + ".delta";

      AppendMessage("Założenie locka na bazę " + destSettings);
      object prevDeltaName = DatabaseManager.Instance.ExecuteScalar(destSettings, DELTAFILENAME);
      if (prevDeltaName != null)
      {
        PreviousDeltaFileName = prevDeltaName.ToString();
        if (!prevDeltaName.ToString().Contains("sh_"))
          AppendMessage("Baza miała ustawiony plik .delta " + PreviousDeltaFileName);
        DatabaseManager.Instance.ExecuteStatement(destSettings, RESETDELTAFILENAME);
      }

      AppendMessage("Na potrzeby procesu zostanie ustawiony nowy plik .delta " + dir);
      DatabaseManager.Instance.ExecuteStatement(destSettings, string.Format(SETDELTAFILENAME, dir));
      destSettings.DeltaFilePath = dir;

      AppendMessage("Baza przełączona w tryb pliku .delta");
      DatabaseManager.Instance.ExecuteStatement(destSettings, LOCKDBSCRIPT);
    }

    /// <summary>
    /// Utworzenie kopii bazy z użyciem gbaka
    /// </summary>
    /// <param name="tempPath"></param>
    /// <param name="pi"></param>
    /// <param name="fbkName"></param>
    /// <param name="fdbName"></param>
    /// <param name="emptySettings"></param>
    /// <param name="copySettings"></param>
    /// <param name="destSettings"></param>
    /// <param name="scripts"></param>
    /// <param name="cts"></param>
    /// <returns></returns>
    private Task CreateLocalDatabaseCopyThroughGbak(string tempPath, ProgressInfo pi, string fbkName, string fdbName, IDbSettings emptySettings, IDbSettings copySettings, IDbSettings destSettings, ScriptsResult scripts, CancellationTokenSource cts)
    {
      // Taks tworzący kopię bazy danych za pomocą gbak i odgbak 
      return Task.Run(() =>
      {
        try
        {
          AppendMessage("Tworzenie kopii bazy docelowej");
          AppendMessage("Gbak " + Path.GetFileName(fbkName));
          DatabaseManager.Instance.CreateBackup(destSettings, fbkName);
          CheckCancellation();

          AppendMessage("Odgbak kopii " + Path.GetFileName(fdbName));
          DatabaseManager.Instance.RestoreBackup(copySettings, fbkName);
          CheckCancellation();
          AppendMessage("Zakonczono tworzenie kopii bazy docelowej");
        }
        catch (Exception e)
        {
          AppendMessage(e.Message);
          cts.Cancel();
        }
      }, cts.Token);
    }

    /// <summary>
    /// Metoda zleca wątkowi UI wykonanie messageboxa i wstrzymuje działanie wątku do czasu przetworzenia odpowiedzi
    /// </summary>
    private void PauseProcess()
    {
      this.BeginInvoke((MethodInvoker) (() =>
      {
        if (MessageBox.Show("Proces wgrania skryptów na bazę testową przebiegł poprawnie. Czy kontynuować?",
          "Pauza w procesie"
          , MessageBoxButtons.YesNo,
          MessageBoxIcon.Question,
          MessageBoxDefaultButton.Button1) != DialogResult.Yes)
        {
          CurrentCancellationToken.Cancel();
        }
        PauseUpdateResetEvent.Set();
      }));
      PauseUpdateResetEvent.WaitOne();
    }

    private void CheckCancellation()
    {
      if (CurrentCancellationToken != null && CurrentCancellationToken.IsCancellationRequested)
        CurrentCancellationToken.Token.ThrowIfCancellationRequested();
    }



    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private void ExecuteScript(IDbSettings dbsettings, string path, string scriptName, string content, CancellationToken ct, bool continueOnErrors = true)
    {
      var ppath = Path.Combine(path, scriptName);
      //konfiguracja skryptu porównującego metadane
      System.IO.File.WriteAllText(ppath, content, SHlib.Constants.Constants.Encodings[dbsettings.Encoding]);
      ExecuteScriptFile(dbsettings, path, scriptName, ct, continueOnErrors);
    }

    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private void ExecuteScript(IDbSettings dbsettings, string path, string scriptName, string content, bool continueOnErrors = true)
    {
      ExecuteScript(dbsettings, path, scriptName, content, CancellationToken.None, continueOnErrors);
    }

    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private void ExecuteScriptFile(IDbSettings dbsettings, string path, string scriptFileName, CancellationToken ct, bool continueOnErrors = true)
    {
      string fname = Path.Combine(path, scriptFileName);
      //konfiguracja skryptu porównującego metadane
      string ibeargs = $"-D\"{dbsettings.ServerAddress}:{dbsettings.DatabaseFilePath}\" -U\"{dbsettings.User}\" -P\"{dbsettings.Password}\" -L3 -C\"{dbsettings.Encoding}\" \"{fname}\" -A 5 {(continueOnErrors ? " -N" : "")}";
      IBEScriptProcess ibescript = new IBEScriptProcess(ibeargs,
        new string[] { "Script executed", "Total", " SC:", "IBEScript" },
        Settings.Default.ibexpert_path);
      int exitcode = ibescript.Execute(AppendMessage, ct);

      if (exitcode > 0)
        throw new Exception("Wystąpił błąd przy wykonywaniu skryptu " + scriptFileName);
    }

    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private void ExecuteScriptFile(IDbSettings dbsettings, string path, string scriptFileName)
    {
      ExecuteScriptFile(dbsettings, path, scriptFileName, CancellationToken.None);
    }

    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private string GenerateDifferentialScript(IDbSettings local, IDbSettings dest, string tempPath, CancellationToken ct)
    {
      const string diffScriptName = "diffscript.sql";
      const string resultDiffName = "diff.sql";
      string diffpath = Path.Combine(tempPath, diffScriptName);
      string respath = Path.Combine(tempPath, resultDiffName);
      //konfiguracja skryptu porównującego metadane
      string compareScript = String.Format(ProcedurePatch.IBE_COMPARE_SCRIPT,
        local.ServerAddress + ":" + local.DatabaseFilePath, //ścieżka do bazy źródłowej
        dest.ServerAddress + ":" + dest.DatabaseFilePath, //ścieżka do bazy docelowej
        dest.User, //użytkownik bazy docelowej
        dest.Password, //hasło użytkownika bazy docelowej
        respath,
        local.User,
        local.Password,
        local.Encoding,
        dest.Encoding);
      File.WriteAllText(diffpath, compareScript, SHlib.Constants.Constants.Encodings[dest.Encoding]);

      IBEScriptProcess ibescript = new IBEScriptProcess(diffpath,
        new string[] { "Extracting", "Parsing", "Linking", "Comparing", "Script executed", "Total" },
        Settings.Default.ibexpert_path);
      int exitcode = ibescript.Execute(AppendMessage, ct);

      if (exitcode > 0)
        throw new Exception("Wystąpił błąd przy generowaniu skrytpu różnicowego!");
      return respath;
    }

    /// <summary>
    /// Rekompiluje wszystkie procedury i trigery
    /// </summary>
    public void RecompileAll(IDbSettings dbsettings, string tempPath, CancellationToken ct)
    {
      const string filename = "recompile_all.sql";
      string filepath = Path.Combine(tempPath, filename);
      //konfiguracja skryptu porównującego metadane
      File.WriteAllText(filepath, ProcedurePatch.IBE_RECOMPILE_ALL, SHlib.Constants.Constants.Encodings[dbsettings.Encoding]);

      string ibeargs = $"-D\"{dbsettings.ServerAddress}:{dbsettings.DatabaseFilePath}\" -U\"{dbsettings.User}\" -P\"{dbsettings.Password}\" -L3 -C\"{dbsettings.Encoding}\" \"{filepath}\"";
      IBEScriptProcess ibescript = new IBEScriptProcess(ibeargs,
        new string[] { "Script executed", "Total", " SC:", "IBEScript" },
        Settings.Default.ibexpert_path);
      int exitcode = ibescript.Execute(AppendMessage, ct);

      if (exitcode > 0)
        throw new Exception("Wystąpił błąd w trakcie rekompilacji procedur lub triggerów");
    }

    /// <summary>
    /// Generowanie skryptu różnicowego
    /// </summary>
    private string GenerateDifferentialScript(IDbSettings local, IDbSettings dest, string tempPath)
    {
      return GenerateDifferentialScript(local, dest, tempPath, CancellationToken.None);
    }

    CancellationTokenSource CurrentCancellationToken;

    /// <summary>
    /// Obiekt reprezentujący zadanie pełnej aktualizacji bazy
    /// <para /> Potrzebny aby bezpiecznie anulować zadanie przy zamykaniu okna.
    /// </summary>
    Task UpdateDatabaseTask { get; set; }
    bool IsBusy { get; set; }

    private void Clear()
    {
      message.Clear();
      clearLog = true;
    }

    private void ExecuteScript()
    {
      this.BringToFront();
      Clear();
      string srcpath = currentProfile.ScriptPath;
      var pi = ProgressInfoFactory();

      FilesystemExtractor fe = new FilesystemExtractor(AppendMessage, ErrorMessage);
      Task.Run(() =>
      {
        Scripts.ExecuteScript(srcpath, true, fe, pi);
      });
    }

    private async void toolStripButton2_Click(object sender, EventArgs e)
    {
      Clear();
      if (!CheckSettings())
        return;

      ChangeVisibilityIfProcessIsRunning(false);
      PrepareConnections();
      var pi = ProgressInfoFactory();

      DatabaseExtractor me = new DatabaseExtractor(ConnectionType.Source, AppendMessage, ErrorMessage, Settings.Default.grant_filter); //omijamy standardowe komunikaty

      try
      {
        if (!ValidateDLLOnSourceDatabase())
        {
          throw new Exception("Walidacja bazy danych zakończona z błędem!");
        }

        IsBusy = true;
        CurrentCancellationToken = new CancellationTokenSource();
        await Task.Run(() => me.ExportAll(currentProfile.ScriptPath, pi, true, true, true), CurrentCancellationToken.Token);
      }
      catch (Exception ex)
      {
        ExpandException(ex);
        MessageBox.Show(ex.Message, "Błąd eksportu", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        IsBusy = false;
        ChangeVisibilityIfProcessIsRunning(true);
        AppendMessage("Zakończono");
      }
    }

    private int MessageBoxShow(string title, string message, int buttons, int icon, int defbuttons)
    {
      return (int) MessageBox.Show(message, title,
        (MessageBoxButtons) buttons,
        (MessageBoxIcon) icon,
        (MessageBoxDefaultButton) defbuttons);
    }

    private void ExpandException(Exception ex)
    {
      if (!string.IsNullOrWhiteSpace(ex.Message))
        AppendMessage(ex.Message);
      if (ex.InnerException != null)
        ExpandException(ex.InnerException);
      return;
    }

    private void WriteCount(int cnt)
    {
      AppendMessage("Przetwarzam " + cnt + " obiektów");
    }

    private int lastprogress = 0;

    private const int CURRENT_SETTINGS_VERSION = 4;

    private void WriteProgress(int i, int cnt, string phase)
    {
      int proc = (int) (1.0 * i / cnt * 100);
      if (proc != lastprogress)
      {
        AppendMessage(phase + " " + proc + " %...");
        lastprogress = proc;
      }
    }

    private async void FastUpdate_Click(object sender, EventArgs e)
    {
      Clear();

      if (!CheckSettings())
        return;
      AppendMessage("Wczytanie wszystkich plików w dbscripts i wygenerowanie hashy w pamięci");
      ChangeVisibilityIfProcessIsRunning(false);

      string srcpath = currentProfile.ScriptPath;
      PrepareConnections();

      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Remote, AppendMessage, ErrorMessage, Settings.Default.grant_filter);
      var pi = ProgressInfoFactory();

      string scriptContent = "";
      try
      {
        IsBusy = true;
        CurrentCancellationToken = new CancellationTokenSource();
        scriptContent = await Task.Run(() => de.FastUpdate(srcpath, pi, Settings.Default.grant_filter), CurrentCancellationToken.Token);
      }
      catch (Exception exp)
      {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(exp.Message);
        if (exp.InnerException != null)
          sb.AppendLine(exp.InnerException.Message);

        AppendMessage(sb.ToString());
      }
      finally
      {
        IsBusy = false;
      }
      if (!string.IsNullOrWhiteSpace(scriptContent))
      {
        var scriptForm = new ScriptForm()
        {
          ExecuteScriptCallback = ExecuteScript,
          Script = scriptContent,
          ShowUpdateHashCodeButton = true,
          mf = this,
          ChangeList = de.mList
        };
        scriptForm.Show();
      }
      else
      {
        AppendMessage("Wygenerowany skrypt jest pusty, brak zmian.");
      }

      AppendMessage("Zakończono");
      ChangeVisibilityIfProcessIsRunning(true);
    }

    private async void wygenerujHasheToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Clear();

      if (!CheckSettings())
        return;

      AppendMessage("Aktualizacja hashy wszystkich obiektów w bazie danych");
      ChangeVisibilityIfProcessIsRunning(false);

      string srcpath = currentProfile.ScriptPath;
      PrepareConnections();

      var pi = ProgressInfoFactory();
      try
      {
        IsBusy = true;
        CurrentCancellationToken = new CancellationTokenSource();
        await Task.Run(() => MetadataChecksum.UpdateAllHashesInDatabase(ConnectionType.Remote, pi, AppendMessage, ErrorMessage, Settings.Default.grant_filter),CurrentCancellationToken.Token);
      }
      catch (Exception ex)
      {
        AppendMessage(ex.Message);
      }
      finally
      {
        IsBusy = false;
      }
      AppendMessage("Zakończono");
      ChangeVisibilityIfProcessIsRunning(true);
    }

    private void PrepareConnections()
    {
      Connection.Instance.SetConnection(ConnectionType.Remote, GetDestSettings());
      Connection.Instance.SetConnection(ConnectionType.Source, GetSourceSettings());
      Connection.Instance.SetConnection(ConnectionType.Local, GetLocalSettings());
    }

    private void pokażFolderToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (Directory.Exists(Settings.Default.tempFile_path))
        Process.Start("explorer.exe", Settings.Default.tempFile_path);
      else
        MessageBox.Show("W ustawieniach nie podano ścieżki do folderu plików tymczasowych lub ta ścieżka jest błędna", "Brak konfiguracji");
    }

    private void usuńWszystkiePlikiToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (Directory.Exists(Settings.Default.tempFile_path))
      {

        DirectoryInfo di = new DirectoryInfo(Settings.Default.tempFile_path);
        var backupFolders = di.GetDirectories();

        if (MessageBox.Show("Napewno usunąć wszystkie pliki tymczasowe?", "Potwierdzenie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
        {
          foreach (DirectoryInfo dir in di.GetDirectories())
          {
            dir.Delete(true);
          }
        }
      }
      else
        MessageBox.Show("W ustawieniach nie podano ścieżki do folderu plików tymczasowych lub ta ścieżka jest błędnah", "Brak ustawień");
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      SetCurrentProfile();
    }

    /// <summary>
    /// Metoda przy inicjalizacji MainForm ustawia wartosci w combo profili. Ustawia aktywny profil.
    /// </summary>
    private void SetCurrentProfile()
    {
      var current = GlobalConfigs.Profiles.Where(p => p.Name == Settings.Default.current_profile).FirstOrDefault();

      toolStripComboBoxProfil.ComboBox.DataSource = GlobalConfigs.Profiles;
      toolStripComboBoxProfil.ComboBox.DisplayMember = "Name";
      toolStripComboBoxProfil.ComboBox.ValueMember = "Name";
      //toolStripComboBoxProfil.ComboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;

      if (current != null)
        toolStripComboBoxProfil.ComboBox.SelectedItem = current;
      else if (GlobalConfigs.Profiles.Count > 0)
      {
        toolStripComboBoxProfil.ComboBox.SelectedIndex = 0;
        ComboBox_SelectedValueChanged(this, EventArgs.Empty);
      }
      else
        ChangeProcessesVisibility(false);
    }

    /// <summary>
    /// Ustawia domyslny profil, na aktualnie wybrany.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ComboBox_SelectedValueChanged(object sender, EventArgs e)
    {
      var selectedProfil = (SignatureHelper.SettingsForm.ProfileConfig) toolStripComboBoxProfil.ComboBox.SelectedItem;

      if (selectedProfil != null)
      {
        Clear();
        Task.Run(() =>
        {
          lock (AppSectionUpgrader.upgradeLock)
          {
            Invoke(new MethodInvoker(() =>
            {
              ChangeVisibilityIfProcessIsRunning(false);
            }));

            RunAppIniMigration(selectedProfil);

            Invoke(new MethodInvoker(() =>
            {
              ChangeVisibilityIfProcessIsRunning(true);
            }));
          }
        });

        toolStripLabelSources.Text = selectedProfil.ScriptPath;
        var db1 = GlobalConfigs.DBConfigs.FirstOrDefault(d => d.Id == selectedProfil.SourceDB_Id);
        var db2 = GlobalConfigs.DBConfigs.FirstOrDefault(d => d.Id == selectedProfil.DestDB_Id);
        if (db1 != null)
        {
          toolStripLabelFrom.Text = db1.Alias;
          if (db2 != null)
          {
            ChangeProcessesVisibility(true);
            toolStripLabelTo.Text = db2.Description;
            ChangeProfile(selectedProfil, db1, db2);
            Settings.Default.current_profile = selectedProfil.Name;
            Settings.Default.Save();
          }
          else
          {
            AppendMessage("Nie znaleziono docelowej bazy danych o aliasie " + selectedProfil.DestDB_Name + " dla profilu " + selectedProfil.Name);
            ChangeProcessesVisibility(false);
          }
        }
        else
        {
          AppendMessage("Nie znaleziono źródłowej bazy danych o aliasie " + selectedProfil.SourceDB_Name + " dla profilu " + selectedProfil.Name);
          ChangeProcessesVisibility(false);
        }
      }
      else
      {
        AppendMessage("Nie znaleziono wybranego profilu");
        ChangeProcessesVisibility(false);
      }
    }

    private void ChangeProfile(SignatureHelper.SettingsForm.ProfileConfig selectedProfil, SignatureHelper.SettingsForm.DatabaseConfig db1, SignatureHelper.SettingsForm.DatabaseConfig db2)
    {
      currentProfile = selectedProfil;
      currentSourceDb = db1;
      currentDestDb = db2;
    }

    private void toolStripComboBoxProfil_SelectedIndexChanged(object sender, EventArgs e)
    {
      ComboBox_SelectedValueChanged(sender, e);
    }

    /// <summary>
    /// Zmiana widoczności kontrolek procesów, jeżeli proifil jest niepełny lub nie ma wybranego profilu
    /// </summary>
    /// <param name="value"></param>
    private void ChangeProcessesVisibility(bool value)
    {
      szybkiProcesToolStripMenuItem.Enabled = value;
      pełnyProcesToolStripMenuItem.Enabled = value;
    }

    /// <summary>
    /// Zmiana widocznosci kontrolek, zależnie od tego, czy jakiś proces jest uruchomiony
    /// </summary>
    /// <param name="value"></param>
    public void ChangeVisibilityIfProcessIsRunning(bool value)
    {
      zaktualizujBazęToolStripMenuItem.Enabled = value;
      eksportujTematToolStripMenuItem.Enabled = value;
      wygenerujHasheToolStripMenuItem.Enabled = value;
      eksportujWszystkoToolStripMenuItem.Enabled = value;
      zaktualizujBazęToolStripMenuItem1.Enabled = zaktualizujBazęToolStripMenuItem1.Text == CANCELTEXT ? true : value;
      plikiTymczasoweToolStripMenuItem.Enabled = value;
      toolStrip1.Enabled = value;
      ustawieniaToolStripMenuItem.Enabled = value;
      walidacjaSekcjiAPPINIToolStripMenuItem.Enabled = value;
    }

    /// <summary>
    /// Ustawia aktuany profil na null
    /// </summary>
    public void SetEmptyProfilAsCurrent()
    {
      //Brzydkie, do zmiany w przyszłości
      toolStripComboBoxProfil.ComboBox.SelectedItem = null;
      toolStripComboBoxProfil.ComboBox.SelectedItem = null;
      toolStripLabelTo.Text = "";
      toolStripLabelSources.Text = "";
      toolStripLabelFrom.Text = "";
      Settings.Default.current_profile = null;
    }

    /// <summary>
    /// Metoda wywołująca migrację starego formatu appini do json
    /// </summary>
    /// <param name="profile"></param>
    private void RunAppIniMigration(SettingsForm.ProfileConfig profile)
    {
      var pi = ProgressInfoFactory();
      AppSectionUpgrader.UpgradeAppSection(profile.ScriptPath, pi, AppendMessage);
    }

    /// <summary>
    /// Akcja wywołująca sprwadzanie spójności akcji APPINI
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void walidacjaSekcjiAPPINIToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Clear();
      var selectedProfil = (SignatureHelper.SettingsForm.ProfileConfig) toolStripComboBoxProfil.ComboBox.SelectedItem;
      ChangeVisibilityIfProcessIsRunning(false);
      CurrentCancellationToken = new CancellationTokenSource();
      IsBusy = true;
      await Task.Run(() => AppSectionUpgrader.ValidateAppSectionScripts(selectedProfil.ScriptPath, AppendMessage), CurrentCancellationToken.Token);
      IsBusy = false;
      ChangeVisibilityIfProcessIsRunning(true);
    }



    private async void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (IsBusy)
      {
        Task udt = UpdateDatabaseTask;
        bool closing = false;
        bool updating = udt != null && !udt.IsCompleted;
        if (updating)
        {
          closing = MessageBox.Show("SignatureHelper jest w trakcie aktualizacji bazy. Czy napewno chcesz przerwać prace programu?",
                                     "SignatureHelper",
                                      MessageBoxButtons.YesNo,
                                      MessageBoxIcon.Warning) == DialogResult.Yes;

        }
        else
        {
          closing = MessageBox.Show("Czy napewno chcesz zamknąć aplikację?",
                                     "SignatureHelper",
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Information) == DialogResult.Yes;
        }
        if (closing)
        {
          CurrentCancellationToken?.Cancel(false);
          if (updating) // Chcemy odczekać aż zadanie zostanie anulowane
          {
            AppendMessage("Trwa zatrzymywanie procesu. Może to potrwać kilka minut, zachowaj cierpliwość!");
            appending = false;
            e.Cancel = true; // tymczasowo nie zamykaj aplikacji
            await udt; // kontynuuj prace programu i wróć tu gdy zadanie się anuluje
            this.FormClosing -= MainForm_FormClosing; // ponów próbę zamknięcia aplikacji tym razem bez komunikatów
            Close();
          }
          refresher.Stop();
        }
        else
        {
          e.Cancel = true;
        }
      }
    }

    private async void GenerateScriptMenuItem_Click(object sender, EventArgs e)
    {
      Clear();

      if (!CheckSettings())
        return;
      ChangeVisibilityIfProcessIsRunning(false);

      string srcpath = currentProfile.ScriptPath;
      PrepareConnections();

      FilesystemExtractor fe = new FilesystemExtractor(AppendMessage, ErrorMessage);
      var pi = ProgressInfoFactory();

      string scriptContent = "";
      string signature = Interaction.InputBox("Wpisz sygnaturę", "Szukaj sygnatury", "");
      if (!string.IsNullOrWhiteSpace(signature))
      {
        try
        {
          IsBusy = true;
          CurrentCancellationToken = new CancellationTokenSource();
          scriptContent = await Task.Run(() => fe.FastUpdateFromSignature(srcpath, pi, signature), CurrentCancellationToken.Token);
        }
        catch (Exception exp)
        {
          StringBuilder sb = new StringBuilder();
          sb.AppendLine(exp.Message);
          if (exp.InnerException != null)
            sb.AppendLine(exp.InnerException.Message);

          AppendMessage(sb.ToString());
        }
        finally
        {
          IsBusy = false;
        }
        if (!string.IsNullOrWhiteSpace(scriptContent))
        {
          var scriptForm = new ScriptForm()
          {
            ExecuteScriptCallback = ExecuteScript,
            Script = scriptContent,
            ShowUpdateHashCodeButton = true,
            mf = this,
            ChangeList = fe.mList
          };
          scriptForm.Show();
        }
        else
        {
          AppendMessage("Wygenerowany skrypt jest pusty, brak zmian.");
        }
      }
      AppendMessage("Zakończono");
      ChangeVisibilityIfProcessIsRunning(true);
    }
    
    private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      Application.Exit();
    }

    private async void weryfikacjaBDToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Clear();
      if (!CheckSettings())
        return;
     
      ValidateDLLOnSourceDatabase();
      await ValidateDLLOnDbscripts();
    }

    /// <summary>
    /// Metoda weryfikuje poprawność skryptów utworzonych w BD
    /// Na razie tylko widoki.
    /// </summary>
    /// <returns></returns>
    private bool ValidateDLLOnSourceDatabase()
    {
      if (!CheckSettings())
        return false;

      bool result = true;
      ChangeVisibilityIfProcessIsRunning(false);
      PrepareConnections();

      var pi = ProgressInfoFactory();
      MetadataValidator mv = new MetadataValidator(pi);
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Source, AppendMessage, ErrorMessage, Settings.Default.grant_filter);
      AppendMessage("Walidacja bazy danych: " + currentSourceDb.Alias);
      result = mv.ValidateViewsDBMetadata(de);
      AppendMessage("Zakończono walidację bazy danych: " + currentSourceDb.Alias);

      ChangeVisibilityIfProcessIsRunning(true);

      return result;
    }
    
    /// <summary>
    /// Validacja dbscriptow czy są poprawne.
    /// </summary>
    /// <returns></returns>
    private async Task<bool> ValidateDLLOnDbscripts()
    {
      if (!CheckSettings())
        return false;

      bool result = true;
      var pi = ProgressInfoFactory();

      ChangeVisibilityIfProcessIsRunning(false);
      MetadataValidator mv = new MetadataValidator(pi);
      
      string srcpath = currentProfile.ScriptPath;
      AppendMessage("Walidacja skryptów: " + srcpath);
      FilesystemExtractor fe = new FilesystemExtractor(AppendMessage, ErrorMessage);
      try
      {
        result = await mv.ValidateViewsFileMetadata(fe, srcpath);
      }
      catch (Exception exp)
      {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(exp.Message);
        if (exp.InnerException != null)
          sb.AppendLine(exp.InnerException.Message);

        AppendMessage(sb.ToString());
        result = false;
      }
      finally
      {
        AppendMessage("Walidacja skryptów została zakończona.");
        ChangeVisibilityIfProcessIsRunning(true);
      }

      return result;
    }

  }

  

  public static class RichTextBoxExtensions
  {
    public static void AppendText(this RichTextBox box, string text, Color color)
    {
      box.SelectionStart = box.TextLength;
      box.SelectionLength = 0;

      box.SelectionColor = color;
      box.AppendText(text);
      box.SelectionColor = box.ForeColor;
    }
  }
}
