﻿using ScintillaNET;
using SHDataContracts.Enums;
using SHlib;
using SHlib.DatabaseUpdate;
using SHlib.DataExtraction;
using SHlib.DataExtraction.Model;
using SHlib.Utilities;
using SignatureHelper.Analyzer;
using SignatureHelper.Analyzer.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SignatureHelper
{
  public partial class ScriptForm : Form
  {
    private const int BACK_COLOR = 0x2A211C;
    private const int FORE_COLOR = 0xB7B7B7;
    private const int NUMBER_MARGIN = 1;
    private const string FIREBIRD_KEYWORDS = @"ADD ADMIN ALL ALTER AND ANY AS AT AVG BEGIN BETWEEN BIGINT BIT_LENGTH BLOB BOTH BY CASE CAST CHAR CHAR_LENGTH CHARACTER CHARACTER_LENGTH CHECK CLOSE COLLATE COLUMN COMMIT CONNECT CONSTRAINT COUNT CREATE CROSS CURRENT CURRENT_CONNECTION CURRENT_DATE CURRENT_ROLE CURRENT_TIME CURRENT_TIMESTAMP CURRENT_TRANSACTION CURRENT_USER CURSOR DATE DAY DEC DECIMAL DECLARE DEFAULT DELETE DISCONNECT DISTINCT DOUBLE DROP ELSE END ESCAPE EXECUTE EXISTS EXTERNAL EXTRACT FETCH FILTER FLOAT FOR FOREIGN FROM FULL FUNCTION GDSCODE GLOBAL GRANT GROUP HAVING HOUR IN INDEX INNER INSENSITIVE INSERT INT INTEGER INTO IS JOIN LEADING LEFT LIKE LONG LOWER MAX MAXIMUM_SEGMENT MERGE MIN MINUTE MONTH NATIONAL NATURAL NCHAR NO NOT NULL NUMERIC OCTET_LENGTH OF ON ONLY OPEN OR ORDER OUTER PARAMETER PLAN POSITION POST_EVENT PRECISION PRIMARY PROCEDURE RDB$DB_KEY REAL RECORD_VERSION RECREATE RECURSIVE REFERENCES RELEASE RETURNING_VALUES RETURNS REVOKE RIGHT ROLLBACK ROW_COUNT ROWS SAVEPOINT SECOND SELECT SENSITIVE SET SIMILAR SMALLINT SOME SQLCODE SQLSTATE START SUM TABLE THEN TIME TIMESTAMP TO TRAILING TRIGGER TRIM UNION UNIQUE UPDATE UPPER USER USING VALUE VALUES VARCHAR VARIABLE VARYING VIEW WHEN WHERE WHILE WITH YEAR TYPE IF 
        add admin all alter and any as at avg begin between bigint bit_length blob both by case cast char char_length character character_length check close collate column commit connect constraint count create cross current current_connection current_date current_role current_time current_timestamp current_transaction current_user cursor date day dec decimal declare default delete disconnect distinct double drop else end escape execute exists external extract fetch filter float for foreign from full function gdscode global grant group having hour in index inner insensitive insert int integer into is join leading left like long lower max maximum_segment merge min minute month national natural nchar no not null numeric octet_length of on only open or order outer parameter plan position post_event precision primary procedure rdb$db_key real record_version recreate recursive references release returning_values returns revoke right rollback row_count rows savepoint second select sensitive set similar smallint some sqlcode sqlstate start sum table then time timestamp to trailing trigger trim union unique update upper user using value values varchar variable varying view when where while with year type if";

    bool UpdatedHashCode = false;
    public bool ShowUpdateHashCodeButton = false;
    public bool ShowPanelWithButton = true;
    public MainForm mf = null;
    private List<Metadata> changeList = null;
    private ScriptAnalyzer scriptAnalyzer = null;
    private RegexManager regexManager = null;
    private List<RegexFindItem> currentListFoundItem;

    public List<Metadata> ChangeList
    {
      set
      {
        changeList = value;
        scriptAnalyzer.ChangeList = value;
      }
      get
      {
        return changeList;
      }
    }

    public Action ExecuteScriptCallback
    {
      get;
      set;
    }

    public ScriptForm()
    {
      InitializeComponent();
      this.Text += " " + DateTime.Now.ToString();
      scriptAnalyzer = new ScriptAnalyzer();
      regexManager = Settings.Default.RegexManager;

      dataGridView1.AutoGenerateColumns = false;
      DataGridViewColumn column = new DataGridViewTextBoxColumn();
      column.DataPropertyName = "Name";
      column.Name = "Nazwa";
      column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dataGridView1.Columns.Add(column);

      column = new DataGridViewTextBoxColumn();
      column.DataPropertyName = "NumberLine";
      column.Name = "Nr";
      column.Width = 50;
      column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns.Add(column);
      
      this.textBoxScript = new ScintillaNET.Scintilla();
      panel2.Controls.Add(this.textBoxScript);

      // BASIC CONFIG
      textBoxScript.Dock = System.Windows.Forms.DockStyle.Fill;

      // INITIAL VIEW CONFIG
      textBoxScript.WrapMode = WrapMode.None;
      textBoxScript.IndentationGuides = IndentView.LookBoth;

      // STYLING
      InitColors();
      InitSyntaxColoring();

      // NUMBER MARGIN
      InitNumberMargin();

      // HOTKEYS
      InitHotkeys();

      InitComboBox();
    }

    private void InitHotkeys()
    {
      // register the hotkeys with the form
      HotKeyManager.AddHotKey(this, OpenSearch, Keys.F, true);   
      HotKeyManager.AddHotKey(this, ZoomIn, Keys.Oemplus, true);
      HotKeyManager.AddHotKey(this, ZoomOut, Keys.OemMinus, true);
      HotKeyManager.AddHotKey(this, ZoomDefault, Keys.D0, true);
      HotKeyManager.AddHotKey(this, CloseSearch, Keys.Escape);

      // remove conflicting hotkeys from scintilla
      textBoxScript.ClearCmdKey(Keys.Control | Keys.F);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.R);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.H);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.L);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.U);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.Z);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.X);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.OemMinus);
      textBoxScript.ClearCmdKey(Keys.Control | Keys.V);

    }

    #region Quick Search Bar

    bool SearchIsOpen = false;

    private void OpenSearch()
    {

      SearchManager.SearchBox = TxtSearch;
      SearchManager.TextArea = textBoxScript;

      if (!SearchIsOpen)
      {
        SearchIsOpen = true;
        InvokeIfNeeded(delegate () {
          PanelSearch.Visible = true;
          TxtSearch.Text = SearchManager.LastSearch;
          TxtSearch.Focus();
          TxtSearch.SelectAll();
        });
      }
      else
      {
        InvokeIfNeeded(delegate () {
          TxtSearch.Focus();
          TxtSearch.SelectAll();
        });
      }
    }
    private void CloseSearch()
    {
      if (SearchIsOpen)
      {
        SearchIsOpen = false;
        InvokeIfNeeded(delegate () {
          PanelSearch.Visible = false;
          //CurBrowser.GetBrowser().StopFinding(true);
        });
      }
    }

    private void InvokeIfNeeded(Action action)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(action);
      }
      else
      {
        action.Invoke();
      }
    }

    private void BtnClearSearch_Click(object sender, EventArgs e)
    {
      CloseSearch();
    }

    private void BtnPrevSearch_Click(object sender, EventArgs e)
    {
      SearchManager.Find(false, false);
    }
    private void BtnNextSearch_Click(object sender, EventArgs e)
    {
      SearchManager.Find(true, false);
    }
    private void TxtSearch_TextChanged(object sender, EventArgs e)
    {
      SearchManager.Find(true, true);
    }

    private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
    {
      if (HotKeyManager.IsHotkey(e, Keys.Enter))
      {
        SearchManager.Find(true, false);
      }
      if (HotKeyManager.IsHotkey(e, Keys.Enter, true) || HotKeyManager.IsHotkey(e, Keys.Enter, false, true))
      {
        SearchManager.Find(false, false);
      }
    }

    #endregion

    #region Zoom

    private void ZoomIn()
    {
      textBoxScript.ZoomIn();
    }

    private void ZoomOut()
    {
      textBoxScript.ZoomOut();
    }

    private void ZoomDefault()
    {
      textBoxScript.Zoom = 0;
    }


    #endregion

    private void InitComboBox()
    {
      comboBox1.DataSource = new BindingSource(regexManager.Values, null);

      comboBox1.DisplayMember = "Name";
      comboBox1.ValueMember = "Value";

      comboBox1.SelectedIndex = -1;
    }

    private Color IntToColor(int rgb)
    {
      return Color.FromArgb(255, (byte) (rgb >> 16), (byte) (rgb >> 8), (byte) rgb);
    }

    private void InitSyntaxColoring()
    {
      // Configure the default style
      textBoxScript.StyleResetDefault();
      textBoxScript.Styles[Style.Default].Font = "Courier New";
      textBoxScript.Styles[Style.Default].Size = 10;
      textBoxScript.Styles[Style.Default].BackColor = IntToColor(0xFFFFFF);
      textBoxScript.Styles[Style.Default].ForeColor = IntToColor(0x000000);
      textBoxScript.StyleClearAll();

      //String
      textBoxScript.Styles[Style.Sql.Character].ForeColor = IntToColor(0x000000);
      textBoxScript.Styles[Style.Sql.Character].BackColor = IntToColor(0x00FFFF);
      textBoxScript.Styles[Style.Sql.String].ForeColor = IntToColor(0x000000);
      textBoxScript.Styles[Style.Sql.String].BackColor = IntToColor(0x00FFFF);

      //Komentarze
      textBoxScript.Styles[Style.Sql.Comment].ForeColor = IntToColor(0x0000FF);
      textBoxScript.Styles[Style.Sql.CommentLine].ForeColor = IntToColor(0x0000FF);

      textBoxScript.Styles[Style.Sql.Default].ForeColor = IntToColor(0x000000);

      /*textBoxScript.Styles[Style.Sql.CommentDoc].ForeColor = IntToColor(0xFF0000);
      textBoxScript.Styles[Style.Sql.CommentDocKeyword].ForeColor = IntToColor(0xFF0000);
      textBoxScript.Styles[Style.Sql.CommentDocKeywordError].ForeColor = IntToColor(0x0000FF);
      textBoxScript.Styles[Style.Sql.CommentLineDoc].ForeColor = IntToColor(0xE95454);
      textBoxScript.Styles[Style.Sql.Identifier].ForeColor = IntToColor(0x77A7DB);
      textBoxScript.Styles[Style.Sql.Number].ForeColor = IntToColor(0x000000);
      textBoxScript.Styles[Style.Sql.Operator].ForeColor = IntToColor(0xF98906);
      textBoxScript.Styles[Style.Sql.QOperator].ForeColor = IntToColor(0xB3D991);
      textBoxScript.Styles[Style.Sql.QuotedIdentifier].ForeColor = IntToColor(0xFF0000);
      textBoxScript.Styles[Style.Sql.SqlPlus].ForeColor = IntToColor(0xE95454);
      textBoxScript.Styles[Style.Sql.SqlPlusComment].ForeColor = IntToColor(0xE0E0E0);
      textBoxScript.Styles[Style.Sql.SqlPlusPrompt].ForeColor = IntToColor(0x77A7DB);
      textBoxScript.Styles[Style.Sql.User1].ForeColor = IntToColor(0xFF0000);
      textBoxScript.Styles[Style.Sql.User2].ForeColor = IntToColor(0xB3D991);
      textBoxScript.Styles[Style.Sql.User3].ForeColor = IntToColor(0xFF0000);
      textBoxScript.Styles[Style.Sql.User4].ForeColor = IntToColor(0x000000);
      textBoxScript.Styles[Style.Sql.Word2].ForeColor = IntToColor(0xB3D991);
      */

      textBoxScript.Styles[Style.Sql.Word].Bold = true;
      textBoxScript.Styles[Style.Sql.Word].ForeColor = IntToColor(0x000000);

      textBoxScript.Lexer = Lexer.Sql;

      textBoxScript.SetKeywords(0, FIREBIRD_KEYWORDS);
    }

    private void InitColors()
    {
      textBoxScript.SetSelectionBackColor(true, Color.Yellow);
    }

    private void InitNumberMargin()
    {
      textBoxScript.Styles[Style.LineNumber].BackColor = IntToColor(BACK_COLOR);
      textBoxScript.Styles[Style.LineNumber].ForeColor = IntToColor(FORE_COLOR);
      textBoxScript.Styles[Style.IndentGuide].ForeColor = IntToColor(FORE_COLOR);
      textBoxScript.Styles[Style.IndentGuide].BackColor = IntToColor(BACK_COLOR);

      var nums = textBoxScript.Margins[NUMBER_MARGIN];
      nums.Width = 50;
      nums.Type = MarginType.Number;
      nums.Sensitive = true;
      nums.Mask = 0;
    }
    
    public string Script
    {
      set
      {
        if (textBoxScript.Text != value)
        {
          string tmp = value;
          // Wymagane do prezentacji. Skrypt nie zawsze zawiera poprawne przejście do nowej linii \r\n
          tmp = tmp.Replace("\r\n", "\r");
          tmp = tmp.Replace("\n", "\r");
          tmp = tmp.Replace("\r", "\r\n");

          textBoxScript.Text = tmp;
          scriptAnalyzer.Script = tmp;
          ShowRegexMatchItems();
          textBoxScript.ReadOnly = true;
        }
      }
    }
    
    private void CopyToClipboard(string value)
    {
      Clipboard.SetText(value);
    }

    private void button2_Click(object sender, EventArgs e)
    {
      CopyToClipboard(textBoxScript.Text);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (ExecuteScriptCallback != null)
        ExecuteScriptCallback();
    }

    private void ShowRegexMatchItems()
    {
      currentListFoundItem = scriptAnalyzer.ListMatchedItems;
      dataGridView1.DataSource = scriptAnalyzer.ListMatchedItems;
      dataGridView1.Refresh();
    }

    private async void button3_Click(object sender, EventArgs e)
    {
      if (mf != null)
        mf.Activate();

      UpdatedHashCode = true;
      string srcpath = Settings.Default.src_path;

      Connection.Instance.SetConnection(ConnectionType.Remote, mf.GetDestSettings());
      Connection.Instance.SetConnection(ConnectionType.Local, mf.GetLocalSettings());
      Connection.Instance.SetConnection(ConnectionType.Source, mf.GetSourceSettings());

      var srcDb = mf.GetLocalSettings();
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Remote, mf.AppendMessage, mf.ErrorMessage, Settings.Default.grant_filter);

      ProgressInfo pi = mf.ProgressInfoFactory();
      await Task.Run(() => MetadataChecksum.UpdateHashCodeSelectedObject(de, changeList, pi, mf.AppendMessage));
    }

    private void ScriptForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (ShowUpdateHashCodeButton &&
       !UpdatedHashCode &&
       MessageBox.Show(@"Nie przeliczono hashcode dla zmienionych obiektów! Po wgraniu skryptów na bazę przed zamknięciem okna należy użyć przyciku 'Aktualizuj hasze zmienionych obiektów'. Pomimo tego nadal chcesz zamknąć okno?", "Ostrzeżenie", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
        e.Cancel = true;
    }

    private void ScriptForm_Load(object sender, EventArgs e)
    {
      if (ShowUpdateHashCodeButton)
        button3.Visible = true;

      panel1.Visible = ShowPanelWithButton; 
    }

    /// <summary>
    /// Po zmianie indexu w comboBox który prezentuje wszystkie zdefiniowane regexy
    /// filtrujemy dziedzinę znalezionych regexów.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      ComboBox cb = (ComboBox) sender;

      if (cb.SelectedIndex < 0)
      {
        if (scriptAnalyzer.ListMatchedItems.Count > 0)
        {
          dataGridView1.DataSource = scriptAnalyzer.ListMatchedItems;
          currentListFoundItem = scriptAnalyzer.ListMatchedItems;
          dataGridView1.Refresh();
        }
        btn4Clear.Visible = false;
        return;
      }

      RegexSettingItem selectedItem = (RegexSettingItem) cb.SelectedItem;

      if (selectedItem != null)
      {
        List<RegexFindItem> filteredList = scriptAnalyzer.ListMatchedItems.Where(i => i.RegexItemID == selectedItem.ID).ToList();

        if (filteredList.Count > 0)
        {
          currentListFoundItem = filteredList;
          dataGridView1.DataSource = filteredList;
          dataGridView1.Refresh();
        }
        else
        {
          filteredList = null;
          dataGridView1.DataSource = null;
          textBoxScript.SetEmptySelection(0);
          dataGridView1.Refresh();
        }
        btn4Clear.Visible = true;
      }
    }

    private void btn4Clear_Click(object sender, EventArgs e)
    {
      comboBox1.SelectedIndex = -1;
    }

    /// <summary>
    /// Metoda wywoływana po zmianie wiersza w gridzie lub kliknięciu na ten sam wiersz. 
    /// Jest odpowiedzialna za zaznaczenie tekstu znalezionego przez regex'a 
    /// oraz odpowiednie przeskrolowanie skryptu SQL aby znaleziony tekst był "w miare na środku okna".
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex < 0)
        return;

      if (dataGridView1.Rows[e.RowIndex].Cells[1].Value != null && currentListFoundItem != null)
      {
        if (e.RowIndex >= 0 && e.RowIndex <= currentListFoundItem.Count)
        {
          RegexFindItem selectItem = currentListFoundItem[e.RowIndex];

          int selectedLine = selectItem.NumberLine - 1;

          Line line = textBoxScript.Lines[selectedLine];
          textBoxScript.SetSelection(selectItem.SelectFromIndex, selectItem.SelectToIndex);

          // Obliczamy 3 linie przed i 3 po aby poprawnie zrobić scrolla
          Line bLine, aLine;
          int linesOnScreen = textBoxScript.LinesOnScreen;
          int offset = linesOnScreen / 4;

          if (selectedLine - offset >= 0)
            bLine = textBoxScript.Lines[selectedLine - offset];
          else
            bLine = textBoxScript.Lines.First();

          int countLine = textBoxScript.Lines.Count;

          if (selectedLine + offset <= countLine)
            aLine = textBoxScript.Lines[selectedLine + offset];
          else
            aLine = textBoxScript.Lines.Last();

          textBoxScript.ScrollRange(bLine.Position, aLine.Position);
          textBoxScript.ScrollCaret();
        }
      }
    }
  }
}
