﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignatureHelper
{
    // struktura wiążaca sygnaturę z nazwą, typem i ddl'em elementu bazodanowego 
    public class SignatureContainer
    {
        public string signature;
        public string description;
        public string name;
        public DbTypes type;
        public string table;

        public SignatureContainer(string signature, string description, string name, DbTypes type, string table)
        {
            this.signature = signature;
            this.description = description;
            this.name = name;
            this.type = type;
            this.table = table;
        }

        public static int CompareSignatureContainer(SignatureContainer a, SignatureContainer b)
        {
            if (a.type < b.type)
                return -1;
            else if (a.type > b.type)
                return 1;
            else
                return 0;
        }
    }

    // typ wyliczeniowy - typy elementów bazodanowych
    public enum DbTypes
    {
        Domain = 0,
        Generator = 1,
        Exception = 2,
        Procedure = 3,
        Table = 4,
        Field = 5,
        Index = 6,
        Trigger = 7,
        TransferProcedure = 8
    }
}
