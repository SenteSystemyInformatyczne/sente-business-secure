﻿using SHlib;
using SHlib.Utilities;
using SignatureHelper.TxtEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NLog;
using SHDataContracts.Enums;
using SignatureHelper.SettingsHelpers;
using SignatureHelper.Analyzer;
using System.Collections;

namespace SignatureHelper
{
  public partial class SettingsForm : Form
  {
    /// <summary>
    /// Inicjalizacja okna ustawień
    /// </summary>
    public SettingsForm(RotatedMessageBuffer buff, MainForm main)
    {
      InitializeComponent();
      ReadDBSettings();
      ValidateChildren();
      rbuff = buff;
      mainForm = main;
      ShowDialog();
      //ResetProfileChange();
    }

    /// <summary>
    /// Referencja do menagera regexów.
    /// </summary>
    private RegexManager regexManager = null;

    /// <summary>
    /// Referencja do obiektu "messages" z klasy RotatedMessageBuffer z MainForm, odpowiadajacego za rotowanie komunikatow w oknie glownym.
    /// </summary>
    RotatedMessageBuffer rbuff;
    /// <summary>
    /// Referencja do okna głównego SignatureHelper
    /// </summary>
    MainForm mainForm;

    private void toolStripButton2_Click(object sender, EventArgs e)
    {
      Close();
    }

    /// <summary>
    /// Metoda ładująca ustawienia globalne
    /// </summary>
    private void ReadDBSettings()
    {
      tbLogPath.Text = Environment.ExpandEnvironmentVariables(Settings.Default.log_path);
      txtIbExpert.Text = Settings.Default.ibexpert_path;
      tbTempFilePath.Text = Environment.ExpandEnvironmentVariables(Settings.Default.tempFile_path);
      cb_ShowScripts.Checked = Settings.Default.show_scripts;
      textBox_LocalFBUser.Text = Settings.Default.local_user;
      maskedTextBox_LocalFBPass.Text = Settings.Default.local_password;
      textBox_LocalFBRole.Text = Settings.Default.local_role;
      rbBackupGbak.Checked = Settings.Default.backup_mode == "gbak";
      rbBackupLock.Checked = Settings.Default.backup_mode == "lock";
      linesTextBox.Text = Settings.Default.maxLines.ToString();
      txtFirebirdBinFolder.Text = Environment.ExpandEnvironmentVariables(Settings.Default.local_fb_bin_path);
      cb_GrantFilter.Checked = Settings.Default.grant_filter;
      localEncodingDropDown.SelectedItem = Settings.Default.local_encoding;
      cb_ShowGenScript.Checked = Settings.Default.show_gen_script;
      cb_RecompileAll.Checked = Settings.Default.recompile_all;
    }

    /// <summary>
    /// Metoda zapisująca ustawienia globalne
    /// </summary>
    private void SaveDBSettings()
    {
      Settings.Default.log_path = tbLogPath.Text;
      Settings.Default.ibexpert_path = txtIbExpert.Text;
      Settings.Default.tempFile_path = tbTempFilePath.Text;
      Settings.Default.local_user = textBox_LocalFBUser.Text;
      if (Settings.Default.local_password != maskedTextBox_LocalFBPass.Text)
        Settings.Default.local_password = Crypt.Instance.Encrypt(maskedTextBox_LocalFBPass.Text);
      Settings.Default.local_role = textBox_LocalFBRole.Text;
      Settings.Default.local_encoding = localEncodingDropDown.SelectedItem.ToString();
      Settings.Default.maxLines = Int32.Parse(linesTextBox.Text);
      Settings.Default.show_scripts = cb_ShowScripts.Checked;
      Settings.Default.backup_mode = (rbBackupGbak.Checked ? "gbak" : "lock");
      Settings.Default.local_fb_bin_path = txtFirebirdBinFolder.Text;
      Settings.Default.grant_filter = cb_GrantFilter.Checked;
      Settings.Default.show_gen_script = cb_ShowGenScript.Checked;
      Settings.Default.recompile_all = cb_RecompileAll.Checked;
      Settings.Default.Save();
    }

    private void buttonChooseSrcPath_Click(object sender, EventArgs e)
    {
      DialogResult dlgResult = folderBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in textbox1.
        textBoxSrcPath.Text = folderBrowserDialogSrcPath.SelectedPath;
      }
    }

    private void btnChangeIBExpertPath_Click(object sender, EventArgs e)
    {
      DialogResult dlgResult = folderBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in textbox1.
        txtIbExpert.Text = folderBrowserDialogSrcPath.SelectedPath;
      }
    }

    private void btnTempFilePath_Click(object sender, EventArgs e)
    {
      DialogResult dlgResult = folderBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in textbox1.
        tbTempFilePath.Text = folderBrowserDialogSrcPath.SelectedPath;
      }
    }

    /// <summary>
    /// Metoda walidująca poprawność konfiguracji.
    /// </summary>
    private void btnCheckSettings_Click(object sender, EventArgs e)
    {

      ProfileConfig profile = (ProfileConfig) listBox_profiles.SelectedItem;
      StringBuilder sb = new StringBuilder();

      if (profile == null)
        sb.AppendLine("Brak zdefiniowanego profilu");
      else
      {
        DatabaseConfig source = GlobalConfigs.DBConfigs.FirstOrDefault(db => db.Id == profile.SourceDB_Id);
        DatabaseConfig dest = GlobalConfigs.DBConfigs.FirstOrDefault(db => db.Id == profile.DestDB_Id);

        if (source == null)
          sb.AppendLine("Nie skonfigurowano źródłowej bazy '" + profile.SourceDB_Name + "'");
        else
        {
          var src_settings = new SHlib.DataExtraction.DbSettings(ConnectionType.Source)
          {
            DatabaseFilePath = source.Path,
            ServerAddress = source.Address,
            Name = source.Alias,
            User = source.User,
            Password = Crypt.Instance.Decrypt(source.Pass),
            Port = source.Port,
            Role = source.Role,
            Encoding = source.Encoding,
            ConnectionPoolSize = source.ConnectionPoolSize
          };

          sb.AppendLine(Tools.CheckDatabaseConnection(src_settings, "źródłową"));
        }

        if (dest == null)
          sb.AppendLine("Nie skonfigurowano docelowej bazy '" + profile.DestDB_Name + "'");
        else
        {
          var dest_settings = new SHlib.DataExtraction.DbSettings(ConnectionType.Remote)
          {
            DatabaseFilePath = dest.Path,
            ServerAddress = dest.Address,
            Name = dest.Alias,
            User = dest.User,
            Password = Crypt.Instance.Decrypt(dest.Pass),
            Port = dest.Port,
            Role = dest.Role,
            Encoding = dest.Encoding,
            ConnectionPoolSize = dest.ConnectionPoolSize
          };

          sb.AppendLine(Tools.CheckDatabaseConnection(dest_settings, "docelową"));
        }

        sb.AppendLine(Tools.CheckFileExistence(txtIbExpert.Text + IBEScriptProcess.IBESCRIPT_NAME));
        sb.AppendLine(Tools.CheckDirectoryExistence(tbTempFilePath.Text));

        if (sb.ToString().Trim().Length == 0)
        {
          sb.Append("Brak problemów z ustawieniami. SignatureHelper jest gotowy do pracy.");
        }
      }
      MessageBox.Show(sb.ToString());

    }

    /// <summary>
    /// Klasa definiująca obiekt konfiguracji bazy danych.
    /// </summary>
    public class DatabaseConfig
    {
      public string Id
      {
        get;
        set;
      }
      public string Alias
      {
        get;
        set;
      }
      public string Address
      {
        get;
        set;
      }
      public string Port
      {
        get;
        set;
      }
      public string Path
      {
        get;
        set;
      }
      public string User
      {
        get;
        set;
      }
      public string Pass
      {
        get;
        set;
      }
      public string Role
      {
        get;
        set;
      }
      public string Encoding
      {
        get;
        set;
      } = "WIN1250";
      public string ConnectionPoolSize
      {
        get;
        set;
      } = "20";

      /// <summary>
      /// Metoda zwracająca obiekt Database dla aktualnej konfiguracji DB.
      /// </summary>
      /// <returns>Obiekt Database</returns>
      public Database ToDatabase()
      {
        Database result = new Database(Id);
        result.Alias = Alias;
        result.Address = Address;
        result.Port = Port;
        result.Path = Path;
        result.User = User;
        result.Password = Pass;
        result.Role = Role;
        result.Encoding = Encoding;
        result.Id = Id;
        result.ConnectionPool = ConnectionPoolSize;
        return result;
      }

      /// <summary>
      /// Metoda budująca obiekt DatabaseConfig na podstawie obiektu Database.
      /// </summary>
      /// <returns>Obiekt DatabaseConfig</returns>
      public static DatabaseConfig FromDatabase(Database d)
      {
        DatabaseConfig dc = new DatabaseConfig();
        dc.Address = d.Address;
        dc.Alias = d.Alias;
        dc.Pass = d.Password;
        dc.Path = d.Path;
        dc.Port = d.Port;
        dc.Role = d.Role;
        dc.Encoding = d.Encoding;
        dc.User = d.User;
        dc.Id = d.Id;
        dc.ConnectionPoolSize = d.ConnectionPool;
        return dc;
      }

      public string Description
      {
        get
        {

          return (Alias + " (" + Address + ":" + Path + " jako " + User + " " + Role).Trim() + ")";
        }
      }
    }

    /// <summary>
    /// Klasa definiująca obiekt konfiguracji profilu.
    /// </summary>
    public class ProfileConfig
    {
      public string Id
      {
        get;
        set;
      }
      public string Name
      {
        get;
        set;
      }
      public string SourceDB_Name
      {
        get;
        set;
      }
      public string DestDB_Name
      {
        get;
        set;
      }
      public string SourceDB_Id
      {
        get;
        set;
      }
      public string DestDB_Id
      {
        get;
        set;
      }
      public string ScriptPath
      {
        get;
        set;
      }

      public string Description
      {
        get
        {
          return Name + " z " + SourceDB_Name + " do " + DestDB_Name + " skrypt " + ScriptPath;
        }
      }

      /// <summary>
      /// Metoda budująca obiekt ProfileConfig na podstawie obiektu Profile.
      /// </summary>
      /// <returns>Obiekt DatabaseConfig</returns>
      internal static ProfileConfig FromProfile(Profile profile)
      {
        ProfileConfig pc = new ProfileConfig();
        pc.Id = profile.Id;
        pc.Name = profile.Name;
        pc.ScriptPath = profile.ScriptPath;
        pc.SourceDB_Name = profile.SourceDB_Name;
        pc.DestDB_Name = profile.DestDB_Name;
        pc.SourceDB_Id = profile.SourceDB_Id;
        pc.DestDB_Id = profile.DestDB_Id;
        return pc;
      }

      /// <summary>
      /// Metoda zwracająca obiekt Profile dla aktualnej konfiguracji profilu.
      /// </summary>
      /// <returns>Obiekt Database</returns>
      internal Profile ToProfile(ProfileConfig pc)
      {
        Profile p = new Profile(pc.Id);
        p.Id = pc.Id;
        p.DestDB_Name = pc.DestDB_Name;
        p.SourceDB_Name = pc.SourceDB_Name;
        p.DestDB_Id = pc.DestDB_Id;
        p.SourceDB_Id = pc.SourceDB_Id;
        p.ScriptPath = pc.ScriptPath;
        p.Name = pc.Name;
        return p;
      }
    }

    private BindingList<DatabaseConfig> DestDBList;
    private BindingList<DatabaseConfig> SourceDBList;
    private DatabaseConfig currentDatabaseConfig;
    private ProfileConfig currentProfileConfig;
    private string dbId;
    private string profileId;
    private bool LoadingForm;

    enum SettingsModes
    {
      Default,
      DatabaseInsert,
      ProfileInsert
    }

    SettingsModes settingsMode = SettingsModes.Default;
    SettingsModes SettingsMode
    {
      get
      {
        return settingsMode;
      }
      set
      {
        if (settingsMode != value)
        {
          settingsMode = value;
          SetVisibility();
        }
      }
    }

    bool profileSectionEnabled = false;

    /// <summary>
    /// Ustawia widoczność sekcji edycji profilu
    /// </summary>
    bool ProfileSectionEnabled
    {
      get { return profileSectionEnabled; }
      set
      {
        profileSectionEnabled = value;
        textBox_profileName.Enabled = value;
        comboBox_DBSource.Enabled = value;
        comboBox_DBDest.Enabled = value;
        textBoxSrcPath.Enabled = value;
        buttonChooseSrcPath.Enabled = value;
      }
    }

    bool databaseSectionEnabled = false;

    /// <summary>
    /// Ustawia widoczność sekcji edycji bazy danych
    /// </summary>
    bool DatabaseSectionEnabled
    {
      get { return databaseSectionEnabled; }
      set
      {
        databaseSectionEnabled = value;
        textBox_DBAlias.Enabled = value;
        textBox_DBAddress.Enabled = value;
        textBox_DBPort.Enabled = value;
        textBox_DBPort.Enabled = value;
        textBox_DBPath.Enabled = value;
        btnChooseDBPath.Enabled = value;
        textBox_DBUser.Enabled = value;
        textBox_DBPass.Enabled = value;
        textBox_DBRole.Enabled = value;
      }
    }

    /// <summary>
    /// Metoda ładująca konfigurację baz
    /// </summary>
    public void LoadSettings()
    {
      DestDBList = new BindingList<DatabaseConfig>(GlobalConfigs.DBConfigs);
      SourceDBList = new BindingList<DatabaseConfig>(GlobalConfigs.DBConfigs);
    }

    public void FillDatabase(Database db)
    {
      db.Alias = textBox_DBAlias.Text;
      db.Address = textBox_DBAddress.Text;
      db.Port = textBox_DBPort.Text;
      db.Path = textBox_DBPath.Text;
      db.User = textBox_DBUser.Text;
      db.Encoding = encodingDropDown.SelectedItem.ToString();
      if (db.Password != textBox_DBPass.Text)
        db.Password = Crypt.Instance.Encrypt(textBox_DBPass.Text);
      db.Role = textBox_DBRole.Text;
      db.ConnectionPool = textBox_ConnectionPool.Text;
    }

    /// <summary>
    /// Metoda zapisująca zmiany konfiguracji bazy danych
    /// </summary>
    public void SaveDatabase()
    {
      var database = DatabaseSettings.Default.Databases.FirstOrDefault(d => d.Id == dbId);

      if (database == null)
      {
        database = new Database();
        FillDatabase(database);
        DatabaseSettings.Default.Databases.Add(database);
      }
      else
        FillDatabase(database);

      var idx = GlobalConfigs.DBConfigs.ToList().FindIndex(d => d.Id == currentDatabaseConfig.Id);
      GlobalConfigs.DBConfigs[idx] = DatabaseConfig.FromDatabase(database);

      DatabaseSettings.Default.Save();
    }

    /// <summary>
    /// Metoda usuwająca konfigurację danej bazy danych
    /// </summary>
    /// <param name="db">Obiekt bazy danych</param>
    public void DeleteDatabase(Database db)
    {
      var index = DatabaseSettings.Default.Databases.IndexOf(db);
      if (index >= 0)
        DatabaseSettings.Default.Databases.RemoveAt(index);

      DatabaseSettings.Default.Save();
    }

    private void SettingsForm_Load(object sender, EventArgs e)
    {
      LoadingForm = true;
      SetVisibility();
      LoadSettings();
      ControlBindings();
      ResetProfileChange();
      LoadingForm = false;

      regexManager = Settings.Default.RegexManager;
      RefreshSelectedListRegex();
    }

    private void ControlBindings()
    {
      BindingSource bsDbs = new BindingSource();
      BindingSource bsProfiles = new BindingSource();

      bsDbs.DataSource = GlobalConfigs.DBConfigs;
      bsProfiles.DataSource = GlobalConfigs.Profiles;

      listBox_databases.DataSource = GlobalConfigs.DBConfigs;
      listBox_databases.DisplayMember = "Alias";
      listBox_databases.ValueMember = "Id";

      comboBox_DBSource.DataSource = SourceDBList;
      comboBox_DBSource.DisplayMember = "Description";
      comboBox_DBSource.ValueMember = "Id";

      comboBox_DBDest.DataSource = DestDBList;
      comboBox_DBDest.DisplayMember = "Description";
      comboBox_DBDest.ValueMember = "Id";

      listBox_profiles.DataSource = GlobalConfigs.Profiles;
      listBox_profiles.DisplayMember = "Name";
      listBox_profiles.ValueMember = "Id";
    }

    /// <summary>
    /// Metoda 
    /// </summary>
    /// <param name="db"></param>
    public void FillDatabaseConfig(DatabaseConfig db)
    {
      db.Alias = textBox_DBAlias.Text;
      db.Address = textBox_DBAddress.Text;
      db.Port = textBox_DBPort.Text;
      db.Path = textBox_DBPath.Text;
      db.User = textBox_DBUser.Text;
      db.Pass = textBox_DBPass.Text;
      db.Role = textBox_DBRole.Text;
      db.ConnectionPoolSize = textBox_ConnectionPool.Text;
    }

    /// <summary>
    /// Metoda ustawiająca domyślne widoczności kontrolek
    /// </summary>
    public void SetVisibility()
    {
      DatabaseSectionEnabled = listBox_databases.SelectedItem != null;
      ProfileSectionEnabled = listBox_profiles.SelectedItem != null;

      button_DBAdd.Enabled = SettingsMode == SettingsModes.Default;
      button_DBDel.Enabled = SettingsMode == SettingsModes.Default;
      button_DBCancel.Visible = SettingsMode == SettingsModes.DatabaseInsert;
      button_DBSave.Visible = SettingsMode == SettingsModes.DatabaseInsert;
      panel_profileDetail.Enabled = SettingsMode != SettingsModes.DatabaseInsert;
      button_addProfil.Enabled = SettingsMode == SettingsModes.Default;
      button_delProfile.Enabled = SettingsMode == SettingsModes.Default;
      listBox_profiles.Enabled = SettingsMode == SettingsModes.Default;
      toolStrip1.Enabled = SettingsMode == SettingsModes.Default;
      button_addProfil.Enabled = settingsMode == SettingsModes.Default;
      button_delProfile.Enabled = settingsMode == SettingsModes.Default;
      btnProflieSave.Visible = settingsMode == SettingsModes.ProfileInsert;
      btnProfileCancel.Visible = settingsMode == SettingsModes.ProfileInsert;
      panel_DBDetail.Enabled = settingsMode != SettingsModes.ProfileInsert;
      listBox_databases.Enabled = settingsMode == SettingsModes.Default;
      setVisibilityTabControlPage(tabPage2, settingsMode == SettingsModes.Default);
    }

    public void setVisibilityTabControlPage(TabPage page, bool visible)
    {
      if (visible && !tabControl1.TabPages.Contains(page))
        tabControl1.TabPages.Add(page);

      if (!visible && tabControl1.TabPages.Contains(page))
        tabControl1.TabPages.Remove(page);
    }

    const string NewDBName = "Nowa";
    /// <summary>
    /// Akcja dodająca konfigurację dla nowej bazy danych
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button_DBAdd_Click(object sender, EventArgs e)
    {
      currentDatabaseConfig = new DatabaseConfig();
      currentDatabaseConfig.Alias = NewDBName;
      GlobalConfigs.DBConfigs.Add(currentDatabaseConfig);
      listBox_databases.SelectedItem = GlobalConfigs.DBConfigs.Last();
      ResetDBChange();
      SettingsMode = SettingsModes.DatabaseInsert;
    }
    /// <summary>
    /// Akcja zapisująca konfigurację bazy danych
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button_DBSave_Click(object sender, EventArgs e)
    {
      if (GlobalConfigs.DBConfigs.Where(d => d.Alias == textBox_DBAlias.Text && d.Id != dbId).Count() >= 1)
        MessageBox.Show("Istnieje już baza danych o aliasie '" + textBox_DBAlias.Text + "'");
      else if (ValidateNewDatabaseConfig())
      {
        SaveDatabase();
        SetVisibility();
        DestDBList.ResetBindings();
        SourceDBList.ResetBindings();
        ResetProfileChange();
        IfIsCurrentProfileDB();
        SettingsMode = SettingsModes.Default;
      }
      else
        MessageBox.Show("Nie uzupełniono wszystkich wymaganych pól");
    }
    /// <summary>
    /// Akcja anulująca edycję konfiguracji bazy danych.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button_DBCancel_Click(object sender, EventArgs e)
    {
      CancelEditDatabaseMode();
    }
    /// <summary>
    /// Akcja usuwająca konfigurację bazy danych.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button_DBDel_Click(object sender, EventArgs e)
    {
      DatabaseConfig obj = (DatabaseConfig) listBox_databases.SelectedItem;
      if (obj != null)
      {
        DeleteDatabase(obj.ToDatabase());
        GlobalConfigs.DBConfigs.Remove(obj);
        DestDBList.ResetBindings();
        SourceDBList.ResetBindings();
      }
      IfIsCurrentProfileDB();

      ResetProfileChange();
      ResetDBChange();
    }
    /// <summary>
    /// Sprawdza czy baza należy do aktywnego profilu. Jeżeli tak, wywołuje metodę czyszczącą wybór profilu i wyświetla komunikat.
    /// </summary>
    /// <returns></returns>
    private void IfIsCurrentProfileDB()
    {
      ProfileConfig currentProfile = GlobalConfigs.Profiles.Where(p => p.Name == Settings.Default.current_profile).FirstOrDefault();
      if (currentProfile != null && (currentProfile.SourceDB_Id == dbId || currentProfile.DestDB_Name == dbId))
      {
        MessageBox.Show("Edytowano bazę danych aktywnego profilu. Aby kontynuować pracę, ustaw profil.");
        mainForm.SetEmptyProfilAsCurrent();
      }
    }
    /// <summary>
    /// Metoda przeładowująca 
    /// </summary>
    public void ResetDBChange()
    {
      DatabaseConfig obj = (DatabaseConfig) listBox_databases.SelectedItem;

      dbId = obj != null ? obj.Id : "";
      textBox_DBAlias.Text = obj != null ? obj.Alias : "";
      textBox_DBAddress.Text = obj != null ? obj.Address : "";
      textBox_DBPort.Text = obj != null ? obj.Port : "";
      textBox_DBPath.Text = obj != null ? obj.Path : "";
      textBox_DBUser.Text = obj != null ? obj.User : "";
      textBox_DBPass.Text = obj != null ? obj.Pass : "";
      textBox_DBRole.Text = obj != null ? obj.Role : "";
      encodingDropDown.SelectedItem = obj != null ? obj.Encoding : "WIN1250";
      textBox_ConnectionPool.Text = obj != null ? obj.ConnectionPoolSize : "20";
    }

    private bool ValidateIfTextIsNotEmpty(Control ctrl, string msgIfEmpty)
    {
      if (ctrl != null)
      {
        return ctrl.Text == "" ? FailureValidate(ctrl, msgIfEmpty) : SuccessValidate(ctrl);
      }

      return true;
    }

    /// <summary>
    /// Metoda usuwająca komunkat błędu z kontrolki
    /// </summary>
    /// <param name="ctrl">Kontrolka</param>
    /// <returns></returns>
    private bool SuccessValidate(Control ctrl)
    {
      ctrl.BackColor = SystemColors.Window;
      errPrv.SetError(ctrl, "");
      return true;
    }

    /// <summary>
    /// Metoda wyświetlająca komunikat błędu na kontrolce
    /// </summary>
    /// <param name="ctrl">Kontrolka</param>
    /// <param name="msgIfEmpty">Komunikat błędu</param>
    /// <returns></returns>
    private bool FailureValidate(Control ctrl, string msgIfEmpty)
    {
      ctrl.BackColor = Color.PaleVioletRed;
      errPrv.SetError(ctrl, msgIfEmpty);
      return false;
    }

    /// <summary>
    /// Metoda walidująca konfigurację bazy danych
    /// </summary>
    /// <returns></returns>
    public bool ValidateNewDatabaseConfig()
    {
      int counter = 0;
      counter = ValidateIfTextIsNotEmpty(textBox_DBAlias, "Pole nie może być puste") ? counter + 1 : counter;
      if (counter == 1 && GlobalConfigs.DBConfigs.Where(d => d.Alias == textBox_DBAlias.Text).Count() > 1)
      {
        counter--;
        FailureValidate(textBox_DBAlias, "Istnieje już baza o aliasie '" + textBox_DBAlias.Text + "'");
      }
      counter = ValidateIfTextIsNotEmpty(textBox_DBAddress, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(textBox_DBPath, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(textBox_DBUser, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(textBox_DBPass, "Pole nie może być puste") ? counter + 1 : counter;

      return counter == 5;
    }

    /// <summary>
    /// Metoda walidująca konfigurację profilu
    /// </summary>
    /// <returns></returns>
    public bool ValidateNewProfileConfig()
    {
      int counter = 0;
      counter = ValidateIfTextIsNotEmpty(textBox_profileName, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfComboIsNotEmpty(comboBox_DBSource, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfComboIsNotEmpty(comboBox_DBDest, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(textBoxSrcPath, "Pole nie może być puste") ? counter + 1 : counter;

      return counter == 4;
    }

    /// <summary>
    /// Metoda walidująca konfigurację globalną SH
    /// </summary>
    /// <returns></returns>
    public bool ValidateOtherOptions()
    {
      int counter = 0;
      counter = ValidatePath(tbLogPath, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidatePath(txtIbExpert, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidatePath(tbTempFilePath, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(textBox_LocalFBUser, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateIfTextIsNotEmpty(maskedTextBox_LocalFBPass, "Pole nie może być puste") ? counter + 1 : counter;
      counter = ValidateMaxLines() ? counter + 1 : counter;

      return counter == 6;
    }

    /// <summary>
    /// Metoda walidująca poprawność scieżki
    /// </summary>
    /// <returns></returns>
    public bool ValidatePath(Control ctrl, string msgIfEmpty)
    {
      var result = true;
      if (ctrl != null)
      {
        if (string.IsNullOrWhiteSpace(ctrl.Text))
          result = FailureValidate(ctrl, "Pole nie może być puste");
        else if (!Directory.Exists(ctrl.Text))
          result = FailureValidate(ctrl, "Podana ścieżka jest nieprawidłowa");
        else
          result = SuccessValidate(ctrl);
      }


      return result;
    }

    private bool ValidateIfComboIsNotEmpty(ComboBox ctrl, string msgIfEmpty)
    {
      var result = true;
      if (ctrl != null)
      {
        if (ctrl.SelectedIndex < 0)
        {
          ctrl.BackColor = Color.PaleVioletRed;
          errPrv.SetError(ctrl, msgIfEmpty);
          result = false;
        }
        else
        {
          ctrl.BackColor = Color.White;
          errPrv.SetError(ctrl, "");
          result = true;
        }
      }

      return result;
    }

    /// <summary>
    /// Zdarzenie aktualizujące wartości kontrolek GUI dla konfiguracji bazy danych
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBox_databases_SelectedValueChanged(object sender, EventArgs e)
    {
      DatabaseConfig obj = (DatabaseConfig) listBox_databases.SelectedItem;
      if (obj != null)
      {
        dbId = obj.Id;
        textBox_DBAlias.Text = obj.Alias;
        textBox_DBAddress.Text = obj.Address;
        textBox_DBPort.Text = obj.Port;
        textBox_DBPath.Text = obj.Path;
        textBox_DBUser.Text = obj.User;
        textBox_DBPass.Text = obj.Pass;
        textBox_DBRole.Text = obj.Role;
        textBox_ConnectionPool.Text = obj.ConnectionPoolSize;
        encodingDropDown.SelectedItem = obj.Encoding;
        currentDatabaseConfig = obj;
      }
      DatabaseSectionEnabled = obj != null;
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBAlias_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBAlias.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Alias)
        EditDatabaseMode();
    }
    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBAddress_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBAddress.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Address)
        EditDatabaseMode();
    }
    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBPort_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBPort.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Port)
        EditDatabaseMode();
    }
    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBPath_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBPath.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Path)
        EditDatabaseMode();
    }
    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBUser_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBUser.Text != ((DatabaseConfig) listBox_databases.SelectedItem).User)
        EditDatabaseMode();
    }
    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBPass_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBPass.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Pass)
        EditDatabaseMode();
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_DBRole_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_DBRole.Text != ((DatabaseConfig) listBox_databases.SelectedItem).Role)
        EditDatabaseMode();
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji bazy danych, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void encodingDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && encodingDropDown.SelectedItem?.ToString() != ((DatabaseConfig) listBox_databases.SelectedItem).Encoding)
        EditDatabaseMode();
    }

    /// <summary>
    /// Metoda ustawiająca konfigurację bazy danych w tryb edycji.
    /// </summary>
    private void EditDatabaseMode()
    {
      SettingsMode = SettingsModes.DatabaseInsert;
    }

    /// <summary>
    /// Metoda wyłączająca tryb edycji w sekcji konfiguracji bazy danych.
    /// </summary>
    private void CancelEditDatabaseMode()
    {
      if (DatabaseSettings.Default.Databases.Exists(d => d.Id == dbId) == false)
      {
        // Usuwamy aktualnie edytowaną bazę danych, jeśli nie ma jej w pliku konfiguracyjnym
        GlobalConfigs.DBConfigs.Remove(currentDatabaseConfig);
      }

      ResetDBChange();
      SetDatabaseCtrlDefault();
      SettingsMode = SettingsModes.Default;
    }

    const string NEWProfileName = "Nowy";

    private void button_addProfil_Click(object sender, EventArgs e)
    {
      currentProfileConfig = new ProfileConfig();
      currentProfileConfig.Name = NEWProfileName;
      GlobalConfigs.Profiles.Add(currentProfileConfig);
      listBox_profiles.SelectedItem = GlobalConfigs.Profiles.Last();
      ResetProfileChange();
      SettingsMode = SettingsModes.ProfileInsert;
    }

    private void button_delProfile_Click(object sender, EventArgs e)
    {
      ProfileConfig obj = (ProfileConfig) listBox_profiles.SelectedItem;
      if (obj != null)
      {
        DeleteProfile(obj.ToProfile(obj));
        GlobalConfigs.Profiles.Remove(obj);
      }

      ResetProfileChange();
    }

    /// <summary>
    /// Metoda usuwająca profil
    /// </summary>
    /// <param name="p">Obiekt profilu do usunięcia</param>
    private void DeleteProfile(Profile p)
    {
      var index = ProfileSettings.Default.Profiles.IndexOf(p);
      if (index >= 0)
        ProfileSettings.Default.Profiles.RemoveAt(index);

      ProfileSettings.Default.Save();
    }

    /// <summary>
    /// Akcja zapisująca konfigurację profilu.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnProflieSave_Click(object sender, EventArgs e)
    {
      if (GlobalConfigs.Profiles.Where(d => d.Name == textBox_profileName.Text && d.Id != profileId).Count() >= 1)
        MessageBox.Show("Istnieje już profil o nazwie '" + textBox_profileName.Text + "'");
      else
      {
        if (ValidateNewProfileConfig())
        {
          SaveProfile();
          SettingsMode = SettingsModes.Default;
          if (textBox_profileName.Text == Settings.Default.current_profile)
          {
            MessageBox.Show("Edytowano aktywny profil. Aby kontynuować pracę, ustaw profil.");
            mainForm.SetEmptyProfilAsCurrent();
          }
        }
        else
          MessageBox.Show("Nie uzupełniono wszystkich wymaganych pól");
      }
    }

    /// <summary>
    /// Metoda zapisująca zmiany w profilu
    /// </summary>
    private void SaveProfile()
    {
      var profile = ProfileSettings.Default.Profiles.FirstOrDefault(d => d.Id == profileId);

      if (profile == null)
      {
        profile = new Profile();
        FillProfile(profile);
        ProfileSettings.Default.Profiles.Add(profile);
      }
      else
        FillProfile(profile);

      GlobalConfigs.Profiles[GlobalConfigs.Profiles.ToList().FindIndex(p => p.Id == currentProfileConfig.Id)] = ProfileConfig.FromProfile(profile);
      ProfileSettings.Default.Save();
    }

    /// <summary>
    /// Metoda ustawiająca własności profilu na podstawie danych z GUI
    /// </summary>
    /// <param name="profile">Obiekt profilu do zmodyfikowania</param>
    private void FillProfile(Profile profile)
    {
      profile.Name = textBox_profileName.Text;
      profile.ScriptPath = textBoxSrcPath.Text;
      profile.SourceDB_Name = (comboBox_DBSource.SelectedItem as DatabaseConfig).Alias;
      profile.DestDB_Name = (comboBox_DBDest.SelectedItem as DatabaseConfig).Alias;
      profile.SourceDB_Id = (comboBox_DBSource.SelectedItem as DatabaseConfig).Id;
      profile.DestDB_Id = (comboBox_DBDest.SelectedItem as DatabaseConfig).Id;
    }

    private void btnProfileCancel_Click(object sender, EventArgs e)
    {
      CancelEditProfileMode();
    }

    /// <summary>
    /// Metoda przeładowująca ustawienia wybranego profilu
    /// </summary>
    private void ResetProfileChange()
    {
      ProfileConfig obj = (ProfileConfig) listBox_profiles.SelectedItem;

      profileId = obj != null ? obj.Id : "";
      textBox_profileName.Text = obj != null ? obj.Name : "";
      textBoxSrcPath.Text = obj != null ? obj.ScriptPath : "";
      if (obj != null)
      {
        if (!string.IsNullOrEmpty(obj.SourceDB_Id))
          comboBox_DBSource.SelectedItem = GlobalConfigs.DBConfigs.FirstOrDefault(d => d.Id == obj.SourceDB_Id);
        else
          comboBox_DBSource.SelectedIndex = -1;
        if (!string.IsNullOrEmpty(obj.DestDB_Id))
          comboBox_DBDest.SelectedItem = GlobalConfigs.DBConfigs.FirstOrDefault(d => d.Id == obj.DestDB_Id);
        else
          comboBox_DBDest.SelectedIndex = -1;
      }
      else
      {
        comboBox_DBDest.SelectedIndex = -1;
        comboBox_DBSource.SelectedIndex = -1;
      }
    }

    private void textBox_profileName_TextChanged(object sender, EventArgs e)
    {
      if (listBox_profiles.SelectedItem != null && textBox_profileName.Text != ((ProfileConfig) listBox_profiles.SelectedItem).Name)
        EditProfileMode();
    }

    /// <summary>
    /// Metoda przełączająca sekcję profilu w tryb edycji
    /// </summary>
    private void EditProfileMode()
    {
      SettingsMode = SettingsModes.ProfileInsert;
    }

    /// <summary>
    /// Metoda wyłączająca tryb edycji w sekcji profilu
    /// </summary>
    private void CancelEditProfileMode()
    {
      if (ProfileSettings.Default.Profiles.Exists(d => d.Id == profileId) == false)
      {
        // Usuwamy aktualnie edytowany profil, jeśli nie ma go w pliku konfiguracyjnym
        GlobalConfigs.Profiles.Remove(currentProfileConfig);
      }
      ResetProfileChange();
      SetProfileCtrlDefault();
      SettingsMode = SettingsModes.Default;
    }

    private void textBoxSrcPath_TextChanged(object sender, EventArgs e)
    {
      if (listBox_profiles.SelectedItem != null && textBoxSrcPath.Text != ((ProfileConfig) listBox_profiles.SelectedItem).ScriptPath)
        EditProfileMode();
    }

    private void comboBox_DBSource_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (listBox_profiles.SelectedItem != null && comboBox_DBSource.SelectedItem != null && !LoadingForm &&
        ((DatabaseConfig) comboBox_DBSource.SelectedItem).Id != ((ProfileConfig) listBox_profiles.SelectedItem).SourceDB_Id)
        EditProfileMode();
    }

    private void comboBox_DBDest_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (listBox_profiles.SelectedItem != null && comboBox_DBDest.SelectedItem != null && !LoadingForm &&
        ((DatabaseConfig) comboBox_DBDest.SelectedItem).Id != ((ProfileConfig) listBox_profiles.SelectedItem).DestDB_Id)
        EditProfileMode();
    }

    private void button_OtherSave_Click(object sender, EventArgs e)
    {
      if (ValidateOtherOptions())
      {
        SaveDBSettings();
        LogManager.Configuration.Variables["log_path"] = Settings.Default.log_path;
        rbuff.MaxLines = Settings.Default.maxLines;
        mainForm.ApplySettings();
      }
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji globalnej, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tbTempFilePath_TextChanged(object sender, EventArgs e)
    {
      EditOtherOptionsMode();
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji globalnej, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtIbExpert_TextChanged(object sender, EventArgs e)
    {
      EditOtherOptionsMode();
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji globalnej, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_LocalFBUser_TextChanged(object sender, EventArgs e)
    {
      EditOtherOptionsMode();
    }

    /// <summary>
    /// Zdarzenie włączające tryb edycji konfiguracji globalnej, gdy zmieniła się wartość w kontrolce
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void maskedTextBox_LocalFBPass_TextChanged(object sender, EventArgs e)
    {
      EditOtherOptionsMode();
    }

    /// <summary>
    /// Metoda uruchamiająca walidację konfiguracji globalnej
    /// </summary>
    private void EditOtherOptionsMode()
    {
      ValidateOtherOptions();
    }

    private void Anuluj_Click(object sender, EventArgs e)
    {
      ReadDBSettings();
    }

    private void listBox_Profiles_SelectedValueChanged(object sender, EventArgs e)
    {
      ProfileConfig obj = (ProfileConfig) listBox_profiles.SelectedItem;
      if (obj != null)
      {
        profileId = obj.Id;
        textBox_profileName.Text = obj.Name;
        textBoxSrcPath.Text = obj.ScriptPath;

        var srcitem = SourceDBList.FirstOrDefault(s => s.Id == obj.SourceDB_Id);
        var destitem = DestDBList.FirstOrDefault(s => s.Id == obj.DestDB_Id);

        comboBox_DBSource.SelectedItem = srcitem;
        comboBox_DBDest.SelectedItem = destitem;
        currentProfileConfig = obj;
      }
      ProfileSectionEnabled = obj != null;
    }
    /// <summary>
    /// Metoda walidujaca wartość wprowadzaną jako maksymalną liczbę linii komunikatów
    /// </summary>
    private bool ValidateMaxLines()
    {
      const int MAX_LINES = 100000;
      int result;

      if (String.IsNullOrWhiteSpace(linesTextBox.Text))
      {
        linesTextBox.Text = Settings.Default.maxLines.ToString();
        return SuccessValidate(linesTextBox);
      }
      else if (Int32.TryParse(linesTextBox.Text, out result) && result > 0 && result < MAX_LINES)
      {
        return SuccessValidate(linesTextBox);
      }
      else
      {
        return FailureValidate(linesTextBox, String.Format("Nieudana próba ustawienia maksymalnej liczby linii z komunikatami, wprowadź liczbę naturalną nie większą niż {0}", MAX_LINES));
      }
    }

    private void tbLogPath_TextChanged(object sender, EventArgs e)
    {
      EditOtherOptionsMode();
    }

    private void btnChangeTbLogPath_Click(object sender, EventArgs e)
    {
      DialogResult dlgResult = folderBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in tbLogPath.
        tbLogPath.Text = folderBrowserDialogSrcPath.SelectedPath;
      }
    }

    /// <summary>
    /// Wydarzenie po kliknięciu przycisku, przy wyborze ścieżki do pliku bazy danych. Dodaje filtr do obiektu fileBrowserDialogSrcPath, uruchamia dialog z mozliwoscia wyboru pliku bazy danych.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnChooseDBPath_Click(object sender, EventArgs e)
    {
      fileBrowserDialogSrcPath.Filter = "Firebird DB files (*.fdb)|*.fdb|All files (*.*)|*.*";
      DialogResult dlgResult = fileBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in textbox1.
        textBox_DBPath.Text = fileBrowserDialogSrcPath.FileName;
      }
    }

    /// <summary>
    /// Usuwa komunikat błędu z kontrolek profilu
    /// </summary>
    private void SetProfileCtrlDefault()
    {
      SuccessValidate(textBox_profileName);
      SuccessValidate(comboBox_DBSource);
      SuccessValidate(comboBox_DBDest);
      SuccessValidate(textBoxSrcPath);
    }

    /// <summary>
    /// Usuwa komunikat błędu z kontrolek bazy danych
    /// </summary>
    private void SetDatabaseCtrlDefault()
    {
      SuccessValidate(textBox_DBAlias);
      SuccessValidate(textBox_DBAddress);
      SuccessValidate(textBox_DBPath);
      SuccessValidate(textBox_DBUser);
      SuccessValidate(textBox_DBPass);
    }

    private void btnSelectFirebirdBinPath_Click(object sender, EventArgs e)
    {
      DialogResult dlgResult = folderBrowserDialogSrcPath.ShowDialog();
      if (dlgResult.Equals(DialogResult.OK))
      {
        //Show selected folder path in textbox1.
        txtFirebirdBinFolder.Text = folderBrowserDialogSrcPath.SelectedPath;
      }
    }

    private void label8_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Sprawdza pole ze ścieżką do IBEScript-a i wyświetla pole z ostrzeżeniem jeśli wykryje jakiś problem
    /// </summary>
    private void refreshIBEScriptInfo()
    {
        string text = "";
        if (!File.Exists(txtIbExpert.Text + IBEScriptProcess.IBESCRIPT_NAME))
          text = "Brak pliku";
        if (text == "" && !IBEScriptProcess.isIBScriptVersionOk(txtIbExpert.Text + IBEScriptProcess.IBESCRIPT_NAME))
          text = "Wersja nieaktualna";
        lblWersjaNieaktualna.Text = text;
    }

    /// <summary>
    /// zdarzenie na zmianę zakładki w ustawieniach
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tabControl1_Selected(object sender, TabControlEventArgs e)
    {
      if (e.TabPage.Name == "tabPage2")
      {
        refreshIBEScriptInfo();
      }
    }

    /// <summary>
    /// zdarzenie na zmianę ścieżki do IBEScript
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtIbExpert_TextChanged_1(object sender, EventArgs e)
    {
      refreshIBEScriptInfo();
    }

    private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (SettingsMode == SettingsModes.DatabaseInsert)
      {
        CancelEditDatabaseMode();
      }

      if (SettingsMode == SettingsModes.ProfileInsert)
      {
        CancelEditProfileMode();
      }
    }

    private void cb_ShowGenScript_CheckedChanged(object sender, EventArgs e)
    {
      if (cb_ShowGenScript.Checked && Settings.Default.show_gen_script == false)
      {
        MessageBox.Show("Przed wygenerowaniem skryptu z tematu należy najpierw eksportować ten temat.\nZależności w obrębie tej funkcjonalności nie są obecnie obsługiwane - obiekty, które nie są oznaczone bezpośrednio sygnaturą, ale zostały wyeksportowane na podstawie zależności nie wchodzą w skład generowanego skryptu.\nOpcja znajduję się w Szybki proces -> Skrypt dla tematu.", "Uwaga", MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }

    #region Obsluga zakladki Analiza skryptu

    // Przechowuje aktuanie wybrany regex wykorzystywany do edycji
    private RegexSettingItem currentRegexItem;

    /// <summary>
    /// Zdarzenie czyści wprowadzoną nazwę oraz regex w kontrolki textBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn4Clear_Click(object sender, EventArgs e)
    {
      textBoxRegexName.Clear();
      textBoxRegexValue.Clear();
      currentRegexItem = null;
      groupBox6.Enabled = false;
      listBox_regex.SelectedIndex = -1;
    }

    /// <summary>
    /// Metoda robi refresh źródła danych do ListBoxa
    /// </summary>
    private void RefreshSelectedListRegex()
    {
      if (regexManager.Count > 0)
      {
        listBox_regex.DataSource = new BindingSource(regexManager.Values, null);
        listBox_regex.DisplayMember = "Name";
        listBox_regex.ValueMember = "Value";
        listBox_regex.Refresh();
        listBox_regex.SelectedIndex = -1;
      }
      else
      {
        listBox_regex.DataSource = null;
        listBox_regex.Refresh();
      }
      groupBox6.Enabled = false;
    }

    /// <summary>
    /// Metoda wyszukuje a następnie usuwa z ustawień zaznaczony regex.
    /// Zapisuje ustawienia.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn5Delete_Click(object sender, EventArgs e)
    {
      object obj = listBox_regex.SelectedItem;

      if (obj == null)
        return;

      RegexSettingItem selectedItem = (RegexSettingItem)obj;
      regexManager.Remove(selectedItem.ID);

      RefreshSelectedListRegex();
      SaveRegexSettings();
    }

    /// <summary>
    /// Metoda odświeża textboxy dla ustawień regexów. 
    /// Jesli jest zaznaczony regex to metoda wpisuje dane do pól tekstowych.
    /// Jesli nie to pola textowe zostają wyczyszczone.
    /// </summary>
    private void RefreshRegexTextBox()
    {
      if (currentRegexItem != null)
      {
        textBoxRegexName.Text = currentRegexItem.Name.ToString();
        textBoxRegexValue.Text = currentRegexItem.Value.ToString();
        groupBox6.Enabled = true;
      }
      else
      {
        textBoxRegexName.Clear();
        textBoxRegexValue.Clear();
        groupBox6.Enabled = false;
      }
    }

    /// <summary>
    /// Ustawia currentRegexItem. Wykonywana po zaznaczeniu rekordu w listBoxie. 
    /// Następnie wywołuje metodę RefershRegexItem w celu naliczenia textboxów.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBox_regex_SelectedIndexChanged(object sender, EventArgs e)
    {
      object obj = listBox_regex.SelectedItem;
      if (obj != null)
      {
        currentRegexItem = (RegexSettingItem) obj;
      }
      else
      {
        currentRegexItem = null;
      }
      RefreshRegexTextBox();
    }

    /// <summary>
    /// Dodanie nowego regexa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn4Add_Click(object sender, EventArgs e)
    {
      currentRegexItem = new RegexSettingItem()
      {
        Name = "Zmień nazwę regexu",
        Value = "regex"
      };
      RefreshRegexTextBox();
    }

    /// <summary>
    /// Metoda dodaje nowy regex. Weryfikuje czy zostały wprowadzone wymagane pola. 
    /// Tworzy oraz dodaje regex do kolekcji a następnie zapisuje ustawienia.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button3_Click(object sender, EventArgs e)
    {
      string nameRegex = textBoxRegexName.Text;
      string valueRegex = textBoxRegexValue.Text;

      if (String.IsNullOrEmpty(nameRegex))
      {
        MessageBox.Show("Nazwa nie może być pusta!!");
        return;
      }

      if (String.IsNullOrEmpty(valueRegex))
      {
        MessageBox.Show("Regex nie może być pusty!!");
        return;
      }

      if (currentRegexItem == null)
        currentRegexItem = new RegexSettingItem();

      currentRegexItem.Name = nameRegex;
      currentRegexItem.Value = valueRegex;

      if (regexManager.ContainsKey(currentRegexItem.ID))
      {
        regexManager[currentRegexItem.ID] = currentRegexItem;
      }
      else
      {
        regexManager.Add(currentRegexItem);
      }

      textBoxRegexName.Clear();
      textBoxRegexValue.Clear();

      RefreshSelectedListRegex();
      SaveRegexSettings();
    }

    /// <summary>
    /// Metoda zapisuje ustawienia
    /// </summary>
    private void SaveRegexSettings()
    {
      Settings.Default.Save();
    }

    #endregion

    private void textBox_ConnectionPool_TextChanged(object sender, EventArgs e)
    {
      if (listBox_databases.SelectedItem != null && textBox_ConnectionPool.Text != ((DatabaseConfig) listBox_databases.SelectedItem).ConnectionPoolSize)
        EditDatabaseMode();
    }
  }
}

