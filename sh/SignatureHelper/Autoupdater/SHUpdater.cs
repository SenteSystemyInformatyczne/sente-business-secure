﻿using Squirrel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignatureHelper.Autoupdater
{
  public class SHUpdater
  {

    private Action _onFinish;

    public SHUpdater(Action onFinish)
    {
      _onFinish = onFinish;
    }

    public async void Update()
    {
      bool restartAfterUpdate = false;
      try
      {
        using (var logger = new SetupLogLogger(false) { Level = Splat.LogLevel.Info })
        {
          using (var updater = new UpdateManager(Settings.Default.UpdateServer))
          {
            Splat.Locator.CurrentMutable.Register(() => logger, typeof(Splat.ILogger));
            ConfigureSquirrel(updater);
            var progress = new UpdateProgress();
            restartAfterUpdate = await UpdateImpl(updater, progress);
          }
        }
      }
      catch (Exception e)
      {
        //MessageBox.Show(e.Message);
      }
      if (restartAfterUpdate)
      {
        UpdateManager.RestartApp();
      }
      else
      {
        _onFinish();
      }
    }

    private async Task<bool> UpdateImpl(UpdateManager updater, UpdateProgress progress)
    {
      UpdateInfo info = null;
      var infoTask = updater.CheckForUpdate();
      if (await Task.WhenAny(infoTask, Task.Delay(TimeSpan.FromSeconds(5))) == infoTask) //timeout, gdy serwer aktualizacji jest nieosiągalny
      {
        info = await infoTask;
        if (info != null && info.CurrentlyInstalledVersion.Version < info.FutureReleaseEntry.Version)
        {
          MessageBox.Show("Wykryto nową wersję Signature Helper. Za chwilę wykonany zostanie proces aktualizacji.", "Dostępna nowa wersja");

          progress.ShowForm();

          progress.SetText("Pobieranie aktualizacji...");
          await updater.DownloadReleases(info.ReleasesToApply, progress.SetProgress);

          progress.SetText("Aktualizacja aplikacji...");
          await updater.ApplyReleases(info, progress.SetProgress);

          progress.CloseForm();

          return true;
        }
      }
      return false;
    }

    private void ConfigureSquirrel(UpdateManager updater)
    {
      SquirrelAwareApp.HandleEvents(
                  onInitialInstall: v => updater.CreateShortcutForThisExe(),
                  onAppUpdate: v => updater.CreateShortcutForThisExe(),
                  onAppUninstall: v => updater.RemoveShortcutForThisExe(),
                  onFirstRun: () =>
                  {
                    if (MessageBox.Show("Czy chcesz importować konfigurację profili i baz danych? (Operacja jest jednorazowa)", "Pierwsze uruchomienie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                      using (var fbd = new FolderBrowserDialog())
                      {
                        fbd.Description = "Wskaż folder z plikami konfiguracji:";
                        fbd.ShowNewFolderButton = false;

                        var result = fbd.ShowDialog();
                        if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        {
                          var files = Directory.GetFiles(fbd.SelectedPath).Where(f => Path.GetFileName(f) == "databases.config" || Path.GetFileName(f) == "profiles.config");
                          foreach (var f in files)
                          {
                            if (!Directory.Exists($"{Application.StartupPath}\\..\\config"))
                            {
                              Directory.CreateDirectory($"{Application.StartupPath}\\..\\config");
                            }
                            File.Copy(f, $"{Application.StartupPath}\\..\\config\\{Path.GetFileName(f)}");
                          }
                        }
                      }
                    }

                    UpdateManager.RestartApp();
                  }
                  );
    }
  }
}

public class SetupLogLogger : Splat.ILogger, IDisposable
{
  StreamWriter inner;
  readonly object gate = 42;
  public Splat.LogLevel Level
  {
    get; set;
  }

  public SetupLogLogger(bool saveInTemp)
  {
    var dir = saveInTemp ?
        Path.GetTempPath() :
        Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

    var file = Path.Combine(dir, "SquirrelSetup.log");
    if (File.Exists(file))
      File.Delete(file);

    inner = new StreamWriter(file, false, Encoding.UTF8);
  }

  public void Write(string message, Splat.LogLevel logLevel)
  {
    if (logLevel < Level)
    {
      return;
    }

    lock (gate)
      inner.WriteLine(message);
  }

  public void Dispose()
  {
    lock (gate)
      inner.Dispose();
  }
}

