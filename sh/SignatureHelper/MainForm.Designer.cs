﻿namespace SignatureHelper
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusSpacer = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolStripStatusVersion = new System.Windows.Forms.ToolStripStatusLabel();
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.szybkiProcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.zaktualizujBazęToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.eksportujTematToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.GenerateScriptMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.wygenerujHasheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.pełnyProcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.eksportujWszystkoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.zaktualizujBazęToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.plikiTymczasoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.pokażFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.usuńWszystkiePlikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.walidacjaSekcjiAPPINIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.weryfikacjaBDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.toolStripComboBoxProfil = new System.Windows.Forms.ToolStripComboBox();
      this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
      this.toolStripLabelTo = new System.Windows.Forms.ToolStripLabel();
      this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
      this.toolStripLabelSources = new System.Windows.Forms.ToolStripLabel();
      this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
      this.toolStripLabelFrom = new System.Windows.Forms.ToolStripLabel();
      this.textBox = new System.Windows.Forms.RichTextBox();
      this.refresher = new System.Windows.Forms.Timer(this.components);
      this.statusStrip1.SuspendLayout();
      this.menuStrip1.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusSpacer,
            this.toolStripStatusVersion});
      this.statusStrip1.Location = new System.Drawing.Point(0, 371);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(693, 22);
      this.statusStrip1.TabIndex = 1;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // toolStripStatusSpacer
      // 
      this.toolStripStatusSpacer.Name = "toolStripStatusSpacer";
      this.toolStripStatusSpacer.Size = new System.Drawing.Size(678, 17);
      this.toolStripStatusSpacer.Spring = true;
      // 
      // toolStripStatusVersion
      // 
      this.toolStripStatusVersion.Name = "toolStripStatusVersion";
      this.toolStripStatusVersion.Size = new System.Drawing.Size(0, 17);
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.szybkiProcesToolStripMenuItem,
            this.pełnyProcesToolStripMenuItem,
            this.ustawieniaToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
      this.menuStrip1.Size = new System.Drawing.Size(693, 24);
      this.menuStrip1.TabIndex = 3;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // szybkiProcesToolStripMenuItem
      // 
      this.szybkiProcesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zaktualizujBazęToolStripMenuItem,
            this.toolStripSeparator1,
            this.eksportujTematToolStripMenuItem,
            this.toolStripSeparator2,
            this.GenerateScriptMenuItem,
            this.wygenerujHasheToolStripMenuItem});
      this.szybkiProcesToolStripMenuItem.Name = "szybkiProcesToolStripMenuItem";
      this.szybkiProcesToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
      this.szybkiProcesToolStripMenuItem.Text = "Szybki proces";
      // 
      // zaktualizujBazęToolStripMenuItem
      // 
      this.zaktualizujBazęToolStripMenuItem.Name = "zaktualizujBazęToolStripMenuItem";
      this.zaktualizujBazęToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
      this.zaktualizujBazęToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
      this.zaktualizujBazęToolStripMenuItem.Text = "Zaktualizuj bazę";
      this.zaktualizujBazęToolStripMenuItem.Click += new System.EventHandler(this.FastUpdate_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
      // 
      // eksportujTematToolStripMenuItem
      // 
      this.eksportujTematToolStripMenuItem.Name = "eksportujTematToolStripMenuItem";
      this.eksportujTematToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
      this.eksportujTematToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
      this.eksportujTematToolStripMenuItem.Text = "Eksportuj temat";
      this.eksportujTematToolStripMenuItem.Click += new System.EventHandler(this.DbExport_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(181, 6);
      // 
      // GenerateScriptMenuItem
      // 
      this.GenerateScriptMenuItem.Name = "GenerateScriptMenuItem";
      this.GenerateScriptMenuItem.Size = new System.Drawing.Size(184, 22);
      this.GenerateScriptMenuItem.Text = "Skrypt dla tematu";
      this.GenerateScriptMenuItem.Visible = false;
      this.GenerateScriptMenuItem.Click += new System.EventHandler(this.GenerateScriptMenuItem_Click);
      // 
      // wygenerujHasheToolStripMenuItem
      // 
      this.wygenerujHasheToolStripMenuItem.Name = "wygenerujHasheToolStripMenuItem";
      this.wygenerujHasheToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
      this.wygenerujHasheToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
      this.wygenerujHasheToolStripMenuItem.Text = "Wygeneruj hashe";
      this.wygenerujHasheToolStripMenuItem.Click += new System.EventHandler(this.wygenerujHasheToolStripMenuItem_Click);
      // 
      // pełnyProcesToolStripMenuItem
      // 
      this.pełnyProcesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eksportujWszystkoToolStripMenuItem,
            this.zaktualizujBazęToolStripMenuItem1,
            this.plikiTymczasoweToolStripMenuItem,
            this.walidacjaSekcjiAPPINIToolStripMenuItem,
            this.weryfikacjaBDToolStripMenuItem});
      this.pełnyProcesToolStripMenuItem.Name = "pełnyProcesToolStripMenuItem";
      this.pełnyProcesToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
      this.pełnyProcesToolStripMenuItem.Text = "Pełny proces";
      // 
      // eksportujWszystkoToolStripMenuItem
      // 
      this.eksportujWszystkoToolStripMenuItem.Name = "eksportujWszystkoToolStripMenuItem";
      this.eksportujWszystkoToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
      this.eksportujWszystkoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
      this.eksportujWszystkoToolStripMenuItem.Text = "Eksportuj wszystko";
      this.eksportujWszystkoToolStripMenuItem.Click += new System.EventHandler(this.toolStripButton2_Click);
      // 
      // zaktualizujBazęToolStripMenuItem1
      // 
      this.zaktualizujBazęToolStripMenuItem1.Name = "zaktualizujBazęToolStripMenuItem1";
      this.zaktualizujBazęToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F12;
      this.zaktualizujBazęToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
      this.zaktualizujBazęToolStripMenuItem1.Text = "Zaktualizuj bazę";
      this.zaktualizujBazęToolStripMenuItem1.Click += new System.EventHandler(this.GenScript_Click);
      // 
      // plikiTymczasoweToolStripMenuItem
      // 
      this.plikiTymczasoweToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pokażFolderToolStripMenuItem,
            this.toolStripSeparator3,
            this.usuńWszystkiePlikiToolStripMenuItem});
      this.plikiTymczasoweToolStripMenuItem.Name = "plikiTymczasoweToolStripMenuItem";
      this.plikiTymczasoweToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
      this.plikiTymczasoweToolStripMenuItem.Text = "Pliki tymczasowe";
      // 
      // pokażFolderToolStripMenuItem
      // 
      this.pokażFolderToolStripMenuItem.Name = "pokażFolderToolStripMenuItem";
      this.pokażFolderToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
      this.pokażFolderToolStripMenuItem.Text = "Pokaż folder";
      this.pokażFolderToolStripMenuItem.Click += new System.EventHandler(this.pokażFolderToolStripMenuItem_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(175, 6);
      // 
      // usuńWszystkiePlikiToolStripMenuItem
      // 
      this.usuńWszystkiePlikiToolStripMenuItem.Name = "usuńWszystkiePlikiToolStripMenuItem";
      this.usuńWszystkiePlikiToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
      this.usuńWszystkiePlikiToolStripMenuItem.Text = "Usuń wszystkie pliki";
      this.usuńWszystkiePlikiToolStripMenuItem.Click += new System.EventHandler(this.usuńWszystkiePlikiToolStripMenuItem_Click);
      // 
      // walidacjaSekcjiAPPINIToolStripMenuItem
      // 
      this.walidacjaSekcjiAPPINIToolStripMenuItem.Name = "walidacjaSekcjiAPPINIToolStripMenuItem";
      this.walidacjaSekcjiAPPINIToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
      this.walidacjaSekcjiAPPINIToolStripMenuItem.Text = "Walidacja sekcji APPINI";
      this.walidacjaSekcjiAPPINIToolStripMenuItem.Click += new System.EventHandler(this.walidacjaSekcjiAPPINIToolStripMenuItem_Click);
      // 
      // weryfikacjaBDToolStripMenuItem
      // 
      this.weryfikacjaBDToolStripMenuItem.Name = "weryfikacjaBDToolStripMenuItem";
      this.weryfikacjaBDToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
      this.weryfikacjaBDToolStripMenuItem.Text = "Waliduj bazę i skrypty";
      this.weryfikacjaBDToolStripMenuItem.Click += new System.EventHandler(this.weryfikacjaBDToolStripMenuItem_Click);
      // 
      // ustawieniaToolStripMenuItem
      // 
      this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
      this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
      this.ustawieniaToolStripMenuItem.Text = "Ustawienia";
      this.ustawieniaToolStripMenuItem.Click += new System.EventHandler(this.settingsButton_Click);
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBoxProfil,
            this.toolStripLabel3,
            this.toolStripLabelTo,
            this.toolStripLabel4,
            this.toolStripLabelSources,
            this.toolStripLabel2,
            this.toolStripLabelFrom});
      this.toolStrip1.Location = new System.Drawing.Point(0, 24);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(693, 25);
      this.toolStrip1.TabIndex = 4;
      this.toolStrip1.Text = "toolStripMain";
      // 
      // toolStripComboBoxProfil
      // 
      this.toolStripComboBoxProfil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.toolStripComboBoxProfil.Name = "toolStripComboBoxProfil";
      this.toolStripComboBoxProfil.Size = new System.Drawing.Size(121, 25);
      this.toolStripComboBoxProfil.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxProfil_SelectedIndexChanged);
      // 
      // toolStripLabel3
      // 
      this.toolStripLabel3.Name = "toolStripLabel3";
      this.toolStripLabel3.Size = new System.Drawing.Size(24, 22);
      this.toolStripLabel3.Text = "do ";
      // 
      // toolStripLabelTo
      // 
      this.toolStripLabelTo.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.toolStripLabelTo.Name = "toolStripLabelTo";
      this.toolStripLabelTo.Size = new System.Drawing.Size(22, 22);
      this.toolStripLabelTo.Text = "     ";
      // 
      // toolStripLabel4
      // 
      this.toolStripLabel4.Name = "toolStripLabel4";
      this.toolStripLabel4.Size = new System.Drawing.Size(42, 22);
      this.toolStripLabel4.Text = "źródła ";
      // 
      // toolStripLabelSources
      // 
      this.toolStripLabelSources.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.toolStripLabelSources.Name = "toolStripLabelSources";
      this.toolStripLabelSources.Size = new System.Drawing.Size(22, 22);
      this.toolStripLabelSources.Text = "     ";
      // 
      // toolStripLabel2
      // 
      this.toolStripLabel2.Name = "toolStripLabel2";
      this.toolStripLabel2.Size = new System.Drawing.Size(15, 22);
      this.toolStripLabel2.Text = "z ";
      // 
      // toolStripLabelFrom
      // 
      this.toolStripLabelFrom.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.toolStripLabelFrom.Name = "toolStripLabelFrom";
      this.toolStripLabelFrom.Size = new System.Drawing.Size(25, 22);
      this.toolStripLabelFrom.Text = "      ";
      // 
      // textBox
      // 
      this.textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
      this.textBox.Location = new System.Drawing.Point(0, 49);
      this.textBox.Name = "textBox";
      this.textBox.Size = new System.Drawing.Size(693, 322);
      this.textBox.TabIndex = 6;
      this.textBox.Text = "";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(693, 393);
      this.Controls.Add(this.textBox);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.menuStrip1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.menuStrip1;
      this.MinimumSize = new System.Drawing.Size(420, 150);
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Signature Helper";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusSpacer;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusVersion;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem szybkiProcesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem zaktualizujBazęToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem eksportujTematToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem pełnyProcesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem eksportujWszystkoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem zaktualizujBazęToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem wygenerujHasheToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem plikiTymczasoweToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem pokażFolderToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem usuńWszystkiePlikiToolStripMenuItem;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripComboBox toolStripComboBoxProfil;
    private System.Windows.Forms.ToolStripLabel toolStripLabel2;
    private System.Windows.Forms.ToolStripLabel toolStripLabelFrom;
    private System.Windows.Forms.ToolStripLabel toolStripLabel3;
    private System.Windows.Forms.ToolStripLabel toolStripLabelTo;
    private System.Windows.Forms.ToolStripLabel toolStripLabel4;
    private System.Windows.Forms.ToolStripLabel toolStripLabelSources;
    private System.Windows.Forms.RichTextBox textBox;
    private System.Windows.Forms.ToolStripMenuItem walidacjaSekcjiAPPINIToolStripMenuItem;
    private System.Windows.Forms.Timer refresher;
    private System.Windows.Forms.ToolStripMenuItem GenerateScriptMenuItem;
    private System.Windows.Forms.ToolStripMenuItem weryfikacjaBDToolStripMenuItem;
  }
}

