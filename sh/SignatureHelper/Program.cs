﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Squirrel;
using System.IO;
using System.Threading.Tasks;
using System.Reflection;
using System.Text;
using System.Threading;
using System.ComponentModel;
using SignatureHelper.Autoupdater;

namespace SignatureHelper
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    internal static MainForm MainFormInstance
    {
      get;
      set;
    }

    internal static UpdateProgress IpdateFormInstance
    {
      get;
      set;
    }

    internal static Action RunMainProcess;

    [STAThread]
    static void Main(string[] args)
    {

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      RunMainProcess = () =>
      {
        MainFormInstance = new MainForm();
        MainFormInstance.Show();
      };

      //Uruchomienie procesu autoaktualizacji SH w trybie bez debugu
#if (!DEBUG)
      var updater = new SHUpdater(RunMainProcess);
      updater.Update();
      Application.Run();
#else
      Application.Run(new MainForm());
#endif
    }
  }
}
