﻿

namespace SignatureHelper
{
  partial class ScriptForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScriptForm));
      this.panel1 = new System.Windows.Forms.Panel();
      this.button3 = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.panel3 = new System.Windows.Forms.Panel();
      this.btn4Clear = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.panel2 = new System.Windows.Forms.Panel();
      this.PanelSearch = new System.Windows.Forms.Panel();
      this.BtnNextSearch = new System.Windows.Forms.Button();
      this.BtnPrevSearch = new System.Windows.Forms.Button();
      this.BtnCloseSearch = new System.Windows.Forms.Button();
      this.TxtSearch = new System.Windows.Forms.TextBox();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.panel3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.panel2.SuspendLayout();
      this.PanelSearch.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.button3);
      this.panel1.Controls.Add(this.button1);
      this.panel1.Controls.Add(this.button2);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(955, 30);
      this.panel1.TabIndex = 6;
      // 
      // button3
      // 
      this.button3.Dock = System.Windows.Forms.DockStyle.Right;
      this.button3.Location = new System.Drawing.Point(341, 0);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(206, 30);
      this.button3.TabIndex = 8;
      this.button3.Text = "Aktualizuj hasze zmienionych obiektów";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Visible = false;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // button1
      // 
      this.button1.Dock = System.Windows.Forms.DockStyle.Right;
      this.button1.Location = new System.Drawing.Point(547, 0);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(204, 30);
      this.button1.TabIndex = 7;
      this.button1.Text = "Wgraj na bazę";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Visible = false;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // button2
      // 
      this.button2.Dock = System.Windows.Forms.DockStyle.Right;
      this.button2.Location = new System.Drawing.Point(751, 0);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(204, 30);
      this.button2.TabIndex = 6;
      this.button2.Text = "Kopiuj do schowka";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
      this.label1.Size = new System.Drawing.Size(860, 30);
      this.label1.TabIndex = 5;
      this.label1.Text = "Treść skryptu została skopiowana do schowka";
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.panel2);
      this.splitContainer1.Size = new System.Drawing.Size(955, 453);
      this.splitContainer1.SplitterDistance = 300;
      this.splitContainer1.TabIndex = 8;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.comboBox1, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 2);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 3;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(300, 453);
      this.tableLayoutPanel1.TabIndex = 4;
      // 
      // comboBox1
      // 
      this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(3, 33);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(294, 21);
      this.comboBox1.TabIndex = 3;
      this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.btn4Clear);
      this.panel3.Controls.Add(this.label2);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel3.Location = new System.Drawing.Point(3, 3);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(294, 24);
      this.panel3.TabIndex = 4;
      // 
      // btn4Clear
      // 
      this.btn4Clear.Dock = System.Windows.Forms.DockStyle.Right;
      this.btn4Clear.Location = new System.Drawing.Point(219, 0);
      this.btn4Clear.Name = "btn4Clear";
      this.btn4Clear.Size = new System.Drawing.Size(75, 24);
      this.btn4Clear.TabIndex = 5;
      this.btn4Clear.Text = "Wyczyść";
      this.btn4Clear.UseVisualStyleBackColor = true;
      this.btn4Clear.Click += new System.EventHandler(this.btn4Clear_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(0, 8);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(117, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Wybierz regułę analizy:";
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToResizeRows = false;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(3, 58);
      this.dataGridView1.MultiSelect = false;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.Size = new System.Drawing.Size(294, 392);
      this.dataGridView1.TabIndex = 2;
      this.dataGridView1.TabStop = false;
      this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
      this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.PanelSearch);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(651, 453);
      this.panel2.TabIndex = 0;
      // 
      // PanelSearch
      // 
      this.PanelSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.PanelSearch.BackColor = System.Drawing.Color.White;
      this.PanelSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.PanelSearch.Controls.Add(this.BtnNextSearch);
      this.PanelSearch.Controls.Add(this.BtnPrevSearch);
      this.PanelSearch.Controls.Add(this.BtnCloseSearch);
      this.PanelSearch.Controls.Add(this.TxtSearch);
      this.PanelSearch.Location = new System.Drawing.Point(330, 3);
      this.PanelSearch.Name = "PanelSearch";
      this.PanelSearch.Size = new System.Drawing.Size(292, 40);
      this.PanelSearch.TabIndex = 11;
      this.PanelSearch.Visible = false;
      // 
      // BtnNextSearch
      // 
      this.BtnNextSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.BtnNextSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BtnNextSearch.ForeColor = System.Drawing.Color.White;
      this.BtnNextSearch.Image = ((System.Drawing.Image)(resources.GetObject("BtnNextSearch.Image")));
      this.BtnNextSearch.Location = new System.Drawing.Point(233, 4);
      this.BtnNextSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.BtnNextSearch.Name = "BtnNextSearch";
      this.BtnNextSearch.Size = new System.Drawing.Size(25, 30);
      this.BtnNextSearch.TabIndex = 9;
      this.BtnNextSearch.Tag = "Find next (Enter)";
      this.BtnNextSearch.UseVisualStyleBackColor = true;
      this.BtnNextSearch.Click += new System.EventHandler(this.BtnNextSearch_Click);
      // 
      // BtnPrevSearch
      // 
      this.BtnPrevSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.BtnPrevSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BtnPrevSearch.ForeColor = System.Drawing.Color.White;
      this.BtnPrevSearch.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrevSearch.Image")));
      this.BtnPrevSearch.Location = new System.Drawing.Point(205, 4);
      this.BtnPrevSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.BtnPrevSearch.Name = "BtnPrevSearch";
      this.BtnPrevSearch.Size = new System.Drawing.Size(25, 30);
      this.BtnPrevSearch.TabIndex = 8;
      this.BtnPrevSearch.Tag = "Find previous (Shift+Enter)";
      this.BtnPrevSearch.UseVisualStyleBackColor = true;
      this.BtnPrevSearch.Click += new System.EventHandler(this.BtnPrevSearch_Click);
      // 
      // BtnCloseSearch
      // 
      this.BtnCloseSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.BtnCloseSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.BtnCloseSearch.ForeColor = System.Drawing.Color.White;
      this.BtnCloseSearch.Image = ((System.Drawing.Image)(resources.GetObject("BtnCloseSearch.Image")));
      this.BtnCloseSearch.Location = new System.Drawing.Point(261, 4);
      this.BtnCloseSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.BtnCloseSearch.Name = "BtnCloseSearch";
      this.BtnCloseSearch.Size = new System.Drawing.Size(25, 30);
      this.BtnCloseSearch.TabIndex = 7;
      this.BtnCloseSearch.Tag = "Close (Esc)";
      this.BtnCloseSearch.UseVisualStyleBackColor = true;
      this.BtnCloseSearch.Click += new System.EventHandler(this.BtnClearSearch_Click);
      // 
      // TxtSearch
      // 
      this.TxtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TxtSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.TxtSearch.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.TxtSearch.Location = new System.Drawing.Point(10, 6);
      this.TxtSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.TxtSearch.Name = "TxtSearch";
      this.TxtSearch.Size = new System.Drawing.Size(189, 25);
      this.TxtSearch.TabIndex = 6;
      this.TxtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
      this.TxtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtSearch_KeyDown);
      // 
      // splitContainer2
      // 
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.panel1);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.splitContainer1);
      this.splitContainer2.Size = new System.Drawing.Size(955, 487);
      this.splitContainer2.SplitterDistance = 30;
      this.splitContainer2.TabIndex = 9;
      // 
      // ScriptForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(955, 487);
      this.Controls.Add(this.splitContainer2);
      this.Name = "ScriptForm";
      this.Text = "Wygenerowany skrypt";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScriptForm_FormClosing);
      this.Load += new System.EventHandler(this.ScriptForm_Load);
      this.panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.panel3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.panel2.ResumeLayout(false);
      this.PanelSearch.ResumeLayout(false);
      this.PanelSearch.PerformLayout();
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private ScintillaNET.Scintilla textBoxScript;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button btn4Clear;
    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.Panel PanelSearch;
    private System.Windows.Forms.Button BtnNextSearch;
    private System.Windows.Forms.Button BtnPrevSearch;
    private System.Windows.Forms.Button BtnCloseSearch;
    private System.Windows.Forms.TextBox TxtSearch;
  }
}