﻿using SHlib;
using SHlib.DataExtraction;
using SHlib.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignatureHelper;
using System.Collections.Concurrent;

namespace SignatureHelper.TxtEdit
{
  /// <summary>
  /// Klasa przechowująca 
  /// </summary>
  public class RotatedMessageBuffer
  {
    private ConcurrentQueue<string> buffer;
    private List<string> messages;
    private int lastIndex = 0;
    private int linesCount
    {
      get
      {
        return messages.Count;
      }
    }

    public RotatedMessageBuffer()
    {
      MaxLines = Settings.Default.maxLines;
      messages = new List<string>();
      buffer = new ConcurrentQueue<string>();
    }
    /// <summary>
    /// Wlasciwosc przyjmujaca wartosc maksymalnej liczby linii z komunikatami
    /// </summary>
    public int MaxLines
    {
      get;
      set;
    }

    /// <summary>
    /// Dodaje tekst to kolejki
    /// </summary>
    /// <param name="text"></param>
    public void Append(string text)
    {
      buffer.Enqueue(text);
    }

    /// <summary>
    /// Pobiera obiekty z kolejki, dodaje do bufora logów i zwiększa licznik linii.
    /// </summary>
    public bool Refresh()
    {
      string text;
      bool something = false;
      while (buffer.TryDequeue(out text))
      {
        something = true;
        foreach (var t in text.Trim('\r', '\n').Split('\n'))
        {
          messages.Add(t);
        }
      }
      return something;
    }

    public Tuple<List<string>, bool> GetMessages()
    {
      var clear = Cut();
      lastIndex = messages.Count - 1;
      return new Tuple<List<string>, bool>(messages, clear);
    }

    /// <summary>
    /// Zwraca string z komunikatami, gdzie liczba wierszy nie jest większa od ustawionej maksymalnej liczby linii z komunikatami 
    /// </summary>
    /// <returns></returns>
    public Tuple<string, bool> GetString()
    {
      var clear = Cut();
      var msgs = string.Join("\n", messages).ToString();
      return new Tuple<string, bool>(msgs, clear);
    }

    /// <summary>
    /// Czyści obiekt, usuwa cały tekst, który zawiera obiekt
    /// </summary>
    public void Clear()
    {
      messages.Clear();
    }

    /// <summary>
    /// Usuwa najstarszye linie, gdy ich ilość przekracza maksymalną ustawioną ilość
    /// </summary>
    private bool Cut()
    {
      bool result = false;
      while (linesCount - MaxLines > 0)
      {
        messages.RemoveAt(0);
        result = true;
      }
      return result;
    }
  }
}
