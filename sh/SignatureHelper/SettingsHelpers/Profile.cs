﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper.SettingsHelpers
{
  /// <summary>
  /// Klasa definiująca konfigurację profilu
  /// </summary>
  [Serializable]
  public class Profile : IEquatable<Profile>
  {
    public Profile()
    {
      Id = Guid.NewGuid().ToString();
    }

    public Profile(string id)
    {
      Id = id;
    }

    public string Id
    {
      get;
      set;
    }

    public string Name
    {
      get;
      set;
    }

    public string SourceDB_Name
    {
      get;
      set;
    }

    public string DestDB_Name
    {
      get;set;
    }

    public string SourceDB_Id
    {
      get;set;
    }

    public string DestDB_Id
    {
      get;set;
    }

    public string ScriptPath
    {
      get;set;
    }

    public bool Equals(Profile other)
    {
      var result = true;
      result &= Id == other.Id;
      result &= Name == other.Name;
      result &= SourceDB_Name == other.SourceDB_Name;
      result &= SourceDB_Id == other.SourceDB_Id;
      result &= DestDB_Name == other.DestDB_Name;
      result &= DestDB_Id == other.DestDB_Id;
      result &= ScriptPath == other.ScriptPath;
      return result;
    }
  }
}
