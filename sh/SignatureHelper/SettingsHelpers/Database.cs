﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper.SettingsHelpers
{
  /// <summary>
  /// Klasa definiująca konfigurację bazy danych
  /// </summary>
  [Serializable]
  public class Database : IEquatable<Database>
  {
    private const string DEFAULT_ENCODING = "WIN1250";

    public Database()
    {
      this.Id = Guid.NewGuid().ToString();
    }

    public Database(string id)
    {
      Id = id;
    }

    public string Id
    {
      get; set;
    }

    public string Alias
    {
      get; set;
    }

    public string Address
    {
      get; set;
    }

    public string Port
    {
      get; set;
    }

    public string Path
    {
      get; set;
    }

    public string User
    {
      get; set;
    }

    public string Password
    {
      get; set;
    }

    public string Role
    {
      get; set;
    }

    private string _connectionPool;
    public string ConnectionPool
    {
      get
      {
        if (string.IsNullOrWhiteSpace(_connectionPool))
        {
          _connectionPool = "20";
        }
        return _connectionPool;
      }
      set
      {
        _connectionPool = value;
      }
    }

    private string _encoding;
    public string Encoding
    {
      get
      {
        return _encoding;
      }
      set
      {
        if (string.IsNullOrWhiteSpace(value))
          value = DEFAULT_ENCODING;
        _encoding = value;
      }
    }

    public bool Equals(Database y)
    {
      var result = true;
      result &= Id == y.Id;
      result &= Alias == y.Alias;
      result &= Address == y.Address;
      result &= Port == y.Port;
      result &= Path == y.Path;
      result &= User == y.User;
      result &= Password == y.Password;
      result &= Role == y.Role;
      result &= Encoding == y.Encoding;
      result &= ConnectionPool == y.ConnectionPool;
      return result;
    }
  }
}
