﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SignatureHelper.SettingsHelpers.Providers
{
  public class ProfileSettingsProvider : SettingsProvider
  {
    const string ROOT = "profilesSettings";
    const string COLLECTION = "Profiles";
    const string PROFILE = "Profile";
    private string ProfileConfigPath
    {
      get
      {
        return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{ApplicationName}\\config\\profiles.config";
      }
    }

    /// <summary>
    /// Loads the file into memory.
    /// </summary>
    public ProfileSettingsProvider()
    {
      SettingsDictionary = new Dictionary<string, List<Profile>> { { "Profiles", new List<Profile>() } };

    }

    /// <summary>
    /// Override.
    /// </summary>
    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
      base.Initialize(ApplicationName, config);
    }

    public override string ApplicationName
    {
      get
      {
        var attributes = typeof(Program).GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute));
        var assemblyTitleAttribute = attributes.SingleOrDefault() as AssemblyTitleAttribute;
        return $"{assemblyTitleAttribute?.Title}";
      }

      set
      {

      }
    }

    /// <summary>
    /// Must override this, this is the bit that matches up the designer properties to the dictionary values
    /// </summary>
    /// <param name="context"></param>
    /// <param name="collection"></param>
    /// <returns></returns>
    public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
    {
      //load the file
      if (!_loaded)
      {
        _loaded = true;
        LoadValuesFromFile();
      }

      //collection that will be returned.
      SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();

      //iterate thought the properties we get from the designer, checking to see if the setting is in the dictionary
      foreach (SettingsProperty setting in collection)
      {
        SettingsPropertyValue value = new SettingsPropertyValue(setting);
        value.IsDirty = false;

        //need the type of the value for the strong typing
        var t = Type.GetType(setting.PropertyType.FullName);

        if (SettingsDictionary.ContainsKey(setting.Name))
        {
          value.SerializedValue = SettingsDictionary[setting.Name];
          value.PropertyValue = SettingsDictionary[setting.Name];
        }
        else //use defaults in the case where there are no settings yet
        {
          value.SerializedValue = setting.DefaultValue;
          value.PropertyValue = setting.DefaultValue;
        }

        values.Add(value);
      }
      return values;
    }

    /// <summary>
    /// Must override this, this is the bit that does the saving to file.  Called when Settings.Save() is called
    /// </summary>
    /// <param name="context"></param>
    /// <param name="collection"></param>
    public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
    {
      var profiles = ((List<Profile>) collection["Profiles"].PropertyValue).ToList();

      //grab the values from the collection parameter and update the values in our dictionary.
      foreach (Profile value in profiles)
      {
        var profile = SettingsDictionary["Profiles"].FirstOrDefault(p => p.Id == value.Id);
        if (profile != null)
        {
          SettingsDictionary["Profiles"].Remove(profile);
        }
        SettingsDictionary["Profiles"].Add(value);
      }

      //now that our local dictionary is up-to-date, save it to disk.
      SaveValuesToFile();
    }

    /// <summary>
    /// Loads the values of the file into memory.
    /// </summary>
    private void LoadValuesFromFile()
    {
      if (!File.Exists(ProfileConfigPath))
      {
        //if the config file is not where it's supposed to be create a new one.
        CreateEmptyConfig();
      }

      //load the xml
      var configXml = XDocument.Load(ProfileConfigPath);

      //get all of the <setting name="..." serializeAs="..."> elements.
      var profileElements = configXml.Element(ROOT).Element(COLLECTION).Elements(PROFILE);

      //iterate through, adding them to the dictionary, (checking for nulls, xml no likey nulls)
      //using "String" as default serializeAs...just in case, no real good reason.
      foreach (var element in profileElements)
      {
        var newSetting = new Profile(element.Attribute("id").Value)
        {
          Name = element.Attribute("name").Value,
          DestDB_Id = element.Attribute("destDB_Id").Value,
          DestDB_Name = element.Attribute("destDB_Name").Value,
          ScriptPath = element.Attribute("scriptPath").Value,
          SourceDB_Id = element.Attribute("sourceDB_Id").Value,
          SourceDB_Name = element.Attribute("sourceDB_Name").Value
        };

        SettingsDictionary["Profiles"].Add(newSetting);
      }
    }

    /// <summary>
    /// Creates an empty user.config file...looks like the one MS creates.  
    /// This could be overkill a simple key/value pairing would probably do.
    /// </summary>
    private void CreateEmptyConfig()
    {
      var doc = new XDocument();
      var declaration = new XDeclaration("1.0", "utf-8", "true");
      var root = new XElement(ROOT);
      var collection = new XElement(COLLECTION);
      root.Add(collection);
      doc.Add(root);
      doc.Declaration = declaration;

      var path = Path.GetDirectoryName(ProfileConfigPath);
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      doc.Save(ProfileConfigPath);
    }

    /// <summary>
    /// Saves the in memory dictionary to the user config file
    /// </summary>
    private void SaveValuesToFile()
    {
      //load the current xml from the file.
      var import = XDocument.Load(ProfileConfigPath);

      //get the settings group (e.g. <Company.Project.Desktop.Settings>)
      var settingsSection = import.Element(ROOT).Element(COLLECTION);

      //iterate though the dictionary, either updating the value or adding the new setting.
      foreach (var entry in SettingsDictionary["Profiles"])
      {
        var setting = settingsSection.Elements().FirstOrDefault(e => e.Attribute("id").Value == entry.Id);
        if (setting == null) //this can happen if a new setting is added via the .settings designer.
        {
          var newSetting = new XElement("Profile");
          newSetting.Add(new XAttribute("id", entry.Id));
          newSetting.Add(new XAttribute("name", entry.Name));
          newSetting.Add(new XAttribute("sourceDB_Name", entry.SourceDB_Name));
          newSetting.Add(new XAttribute("sourceDB_Id", entry.SourceDB_Id));
          newSetting.Add(new XAttribute("destDB_Name", entry.DestDB_Name));
          newSetting.Add(new XAttribute("destDB_Id", entry.DestDB_Id));
          newSetting.Add(new XAttribute("scriptPath", entry.ScriptPath));
          settingsSection.Add(newSetting);
        }
        else //update the value if it exists.
        {
          setting.Attribute("name").Value = entry.Name;
          setting.Attribute("sourceDB_Name").Value = entry.SourceDB_Name;
          setting.Attribute("sourceDB_Id").Value = entry.SourceDB_Id;
          setting.Attribute("destDB_Name").Value = entry.DestDB_Name;
          setting.Attribute("destDB_Id").Value = entry.DestDB_Id;
          setting.Attribute("scriptPath").Value = entry.ScriptPath;
        }
      }

      var toRemove = settingsSection.Elements().Where(e => !SettingsDictionary["Profiles"].Any(p => e.Attribute("id").Value == p.Id));
      foreach (var e in toRemove)
      {
        e.Remove();
      }


      import.Save(ProfileConfigPath);
    }

    /// <summary>
    /// In memory storage of the settings values
    /// </summary>
    private Dictionary<string, List<Profile>> SettingsDictionary
    {
      get; set;
    }

    bool _loaded;
  }
}
