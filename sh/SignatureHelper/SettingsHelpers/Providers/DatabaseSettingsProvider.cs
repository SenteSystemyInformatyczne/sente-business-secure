﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SignatureHelper.SettingsHelpers.Providers
{
  public class DatabaseSettingsProvider : SettingsProvider
  {
    const string ROOT = "databasesSettings";
    const string COLLECTION = "Databases";
    const string ELEMENT = "Database";

    private string DatabaseConfigPath
    {
      get
      {
        return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{ApplicationName}\\config\\databases.config";
      }
    }

    /// <summary>
    /// Loads the file into memory.
    /// </summary>
    public DatabaseSettingsProvider()
    {
      SettingsDictionary = new Dictionary<string, List<Database>> { { "Databases", new List<Database>() } };

    }

    /// <summary>
    /// Override.
    /// </summary>
    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
      base.Initialize(ApplicationName, config);
    }

    public override string ApplicationName
    {
      get
      {
        var attributes = typeof(Program).GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute));
        var assemblyTitleAttribute = attributes.SingleOrDefault() as AssemblyTitleAttribute;
        return $"{assemblyTitleAttribute?.Title}";
      }

      set
      {

      }
    }

    /// <summary>
    /// Must override this, this is the bit that matches up the designer properties to the dictionary values
    /// </summary>
    /// <param name="context"></param>
    /// <param name="collection"></param>
    /// <returns></returns>
    public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
    {
      //load the file
      if (!_loaded)
      {
        _loaded = true;
        LoadValuesFromFile();
      }

      //collection that will be returned.
      SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();

      //iterate thought the properties we get from the designer, checking to see if the setting is in the dictionary
      foreach (SettingsProperty setting in collection)
      {
        SettingsPropertyValue value = new SettingsPropertyValue(setting);
        value.IsDirty = false;

        //need the type of the value for the strong typing
        var t = Type.GetType(setting.PropertyType.FullName);

        if (SettingsDictionary.ContainsKey(setting.Name))
        {
          value.SerializedValue = SettingsDictionary[setting.Name];
          value.PropertyValue = SettingsDictionary[setting.Name];
        }
        else //use defaults in the case where there are no settings yet
        {
          value.SerializedValue = setting.DefaultValue;
          value.PropertyValue = setting.DefaultValue;
        }

        values.Add(value);
      }
      return values;
    }

    /// <summary>
    /// Must override this, this is the bit that does the saving to file.  Called when Settings.Save() is called
    /// </summary>
    /// <param name="context"></param>
    /// <param name="collection"></param>
    public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
    {
      var profiles = ((List<Database>) collection[COLLECTION].PropertyValue).ToList();

      //grab the values from the collection parameter and update the values in our dictionary.
      foreach (Database value in profiles)
      {
        var profile = SettingsDictionary[COLLECTION].FirstOrDefault(p => p.Id == value.Id);
        if (profile != null)
        {
          SettingsDictionary[COLLECTION].Remove(profile);
        }
        SettingsDictionary[COLLECTION].Add(value);
      }

      //now that our local dictionary is up-to-date, save it to disk.
      SaveValuesToFile();
    }

    /// <summary>
    /// Loads the values of the file into memory.
    /// </summary>
    private void LoadValuesFromFile()
    {
      if (!File.Exists(DatabaseConfigPath))
      {
        //if the config file is not where it's supposed to be create a new one.
        CreateEmptyConfig();
      }

      //load the xml
      var configXml = XDocument.Load(DatabaseConfigPath);

      //get all of the <setting name="..." serializeAs="..."> elements.
      var profileElements = configXml.Element(ROOT).Element(COLLECTION).Elements(ELEMENT);

      //iterate through, adding them to the dictionary, (checking for nulls, xml no likey nulls)
      //using "String" as default serializeAs...just in case, no real good reason.
      foreach (var element in profileElements)
      {
        var newSetting = new Database(element.Attribute("id").Value)
        {
          Alias = element.Attribute("name").Value,
          Address = element.Attribute("address").Value,
          Port = element.Attribute("port").Value,
          Path = element.Attribute("path").Value,
          User = element.Attribute("user").Value,
          Password = element.Attribute("password").Value,
          Role = element.Attribute("role").Value,
          Encoding = element.Attribute("encoding")?.Value,
          ConnectionPool = element.Attribute("connection_pool")?.Value
        };

        SettingsDictionary[COLLECTION].Add(newSetting);
      }
    }

    /// <summary>
    /// Creates an empty user.config file...looks like the one MS creates.  
    /// This could be overkill a simple key/value pairing would probably do.
    /// </summary>
    private void CreateEmptyConfig()
    {
      var doc = new XDocument();
      var declaration = new XDeclaration("1.0", "utf-8", "true");
      var root = new XElement(ROOT);
      var collection = new XElement(COLLECTION);
      root.Add(collection);
      doc.Add(root);
      doc.Declaration = declaration;

      var path = Path.GetDirectoryName(DatabaseConfigPath);
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      doc.Save(DatabaseConfigPath);
    }

    /// <summary>
    /// Saves the in memory dictionary to the user config file
    /// </summary>
    private void SaveValuesToFile()
    {
      //load the current xml from the file.
      var import = XDocument.Load(DatabaseConfigPath);

      //get the settings group (e.g. <Company.Project.Desktop.Settings>)
      var settingsSection = import.Element(ROOT).Element(COLLECTION);

      //iterate though the dictionary, either updating the value or adding the new setting.
      foreach (var entry in SettingsDictionary[COLLECTION])
      {
        var setting = settingsSection.Elements().FirstOrDefault(e => e.Attribute("id").Value == entry.Id);
        if (setting == null) //this can happen if a new setting is added via the .settings designer.
        {
          var newSetting = new XElement(ELEMENT);
          newSetting.Add(new XAttribute("id", entry.Id));
          newSetting.Add(new XAttribute("name", entry.Alias));
          newSetting.Add(new XAttribute("address", entry.Address));
          newSetting.Add(new XAttribute("port", entry.Port));
          newSetting.Add(new XAttribute("path", entry.Path));
          newSetting.Add(new XAttribute("user", entry.User));
          newSetting.Add(new XAttribute("password", entry.Password));
          newSetting.Add(new XAttribute("role", entry.Role));
          newSetting.Add(new XAttribute("encoding", entry.Encoding));
          newSetting.Add(new XAttribute("connection_pool", entry.ConnectionPool));
          settingsSection.Add(newSetting);
        }
        else //update the value if it exists.
        {
          setting.Attribute("name").Value = entry.Alias;
          setting.Attribute("address").Value = entry.Address;
          setting.Attribute("port").Value = entry.Port;
          setting.Attribute("path").Value = entry.Path;
          setting.Attribute("user").Value = entry.User;
          setting.Attribute("password").Value = entry.Password;
          setting.Attribute("role").Value = entry.Role;

          if (setting.Attribute("encoding") != null)
            setting.Attribute("encoding").Value = entry.Encoding;
          else
            setting.Add(new XAttribute("encoding", entry.Encoding));

          if (setting.Attribute("connection_pool") != null)
            setting.Attribute("connection_pool").Value = entry.ConnectionPool;
          else
            setting.Add(new XAttribute("connection_pool", entry.ConnectionPool));
        }
      }

      var toRemove = settingsSection.Elements().Where(e => !SettingsDictionary[COLLECTION].Any(p => e.Attribute("id").Value == p.Id));
      foreach (var e in toRemove)
      {
        e.Remove();
      }

      import.Save(DatabaseConfigPath);
    }

    /// <summary>
    /// In memory storage of the settings values
    /// </summary>
    private Dictionary<string, List<Database>> SettingsDictionary
    {
      get; set;
    }

    bool _loaded;
  }
}
