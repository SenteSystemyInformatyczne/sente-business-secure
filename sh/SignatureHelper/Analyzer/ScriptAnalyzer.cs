﻿using SHlib.DataExtraction.Model;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignatureHelper.Analyzer
{
  /// <summary>
  /// Klasa analizująca skrypt końcowy. Wykorzystywana do naliczenia ostrzeżeń dla użytkownika.
  /// </summary>
  public class ScriptAnalyzer
  {
    private RegexManager regexManager;
    private List<Metadata> changeList;
    public List<RegexFindItem> ListMatchedItems = new List<RegexFindItem>();
    public List<int> newLinePosition = new List<int>();

    private string scriptFromMetaData;

    public List<Metadata> ChangeList
    {
      set
      {
        changeList = value;
      }
      get
      {
        return changeList;
      }
    }

    private string script;
    public string Script
    {
      get
      {
        return script;
      }
      set
      {
        script = value;
        AnalyzeScript();
      }
    }

    /// <summary>
    /// Konstruktor w którym odczytywane zostają ustawienia Regexów. 
    /// </summary>
    public ScriptAnalyzer()
    {
      regexManager = Settings.Default.RegexManager;
    }

    /// <summary>
    /// Metoda wykonuje analizę skryptu końcowego. 
    /// Uruchamia wszystkie zdefiniowane regexy a gdy wyszuka przyporządkowania
    /// to nalicza kolekcję ListMatchedItems.
    /// </summary>
    private void AnalyzeScript()
    {
      // Na potrzeby łatwiejszego wyszukiwania regexami
      script += Environment.NewLine + @"SET TERM ^;";

      CalculateNewLinePosition();
      
      foreach (var regexItem in regexManager)
      {
        try
        {
          Match match = Regex.Match(script.ToString(), regexItem.Value.Value, RegexOptions.IgnoreCase, TimeSpan.FromSeconds(1));

          while (match.Success)
          {
            RegexFindItem rfm = new RegexFindItem()
            {
              RegexItemID = regexItem.Key
            };

            rfm.Name = script.Substring(match.Index, match.Length);
            rfm.NumberLine = SearchNumberLineFromIndexChar(match.Index) + 1;
            rfm.SelectFromIndex = match.Index;
            rfm.SelectToIndex = match.Index + match.Length;

            ListMatchedItems.Add(rfm);
            match = match.NextMatch();
          }
        }
        catch (RegexMatchTimeoutException)
        {
          // ignoruj ztimeoutowane matchowania
        }
      }
    }

    /// <summary>
    /// Metoda wylicza w którym miejscu znajdują się znaki nowej linii.
    /// </summary>
    private void CalculateNewLinePosition()
    {
      newLinePosition.Add(0);
      int nLPos = 0;

      while ((nLPos = script.IndexOf('\n', nLPos)) != -1)
      {
        newLinePosition.Add(nLPos);
        nLPos++;
      }
    }

    /// <summary>
    /// Metoda zwraca numer linii przez wyszukanie w liście
    /// </summary>
    /// <param name="index">Pozycja znaku dla którego zwracam nr linii w którym się znajduje</param>
    /// <returns>Numer lini w której znajduje się znak o pozycji przekazanej w parametrze index</returns>
    private int SearchNumberLineFromIndexChar(int index)
    {

      int first = 0;
      int last = newLinePosition.Count;

      if (last <= 0)
        return 0;

      int result = last / 2;
      int lastResult = int.MaxValue; 

      while (result != lastResult)
      {
        if (newLinePosition[result] >= index)
        {
          last = result;
        }
        else
        {
          first = result;
        }

        lastResult = result;
        result = (last + first) / 2;
      }

      return result;
    }

  }

  public class RegexFindItem
  {
    public String RegexItemID
    {
      get;
      set;
    }

    public String Name
    {
      get;
      set;
    }
    public int NumberLine
    {
      get;
      set;
    }

    public int SelectFromIndex;
    public int SelectToIndex;
  }
}
