﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SignatureHelper.Analyzer
{
  /// <summary>
  /// Klasa przechoduje konfigurację regexów.
  /// Została utworzona na potrzeby serializacji konfiguracji do XML'a.
  /// </summary>
  public class RegexManager : Dictionary<String, RegexSettingItem>, IXmlSerializable
  {
    public XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(XmlReader reader)
    {
      while (reader.Read() &&
          !(reader.NodeType == XmlNodeType.EndElement && reader.LocalName == this.GetType().Name))
      {
        var ID = reader["ID"];
        if (ID == null)
          throw new FormatException();

        var name = reader["Name"];
        var value = reader["Value"];

        RegexSettingItem settingItem = new RegexSettingItem()
        {
          ID = ID,
          Name = name,
          Value = value
        };

        this[ID] = settingItem;
      }
    }

    public void WriteXml(XmlWriter writer)
    {
      foreach (var entry in this)
      {
        writer.WriteStartElement("Pair");
        writer.WriteAttributeString("ID", (string) entry.Key);
        writer.WriteAttributeString("Name", (string) entry.Value.Name);
        writer.WriteAttributeString("Value", (string) entry.Value.Value);
        writer.WriteEndElement();
      }
    }

    /// <summary>
    /// Dodaje do kolekcji nowy element ustawień
    /// </summary>
    /// <param name="item"></param>
    public void Add(RegexSettingItem item)
    {
      this.Add(item.ID, item);
    }

  }

  /// <summary>
  /// Klaas opisująca ustawienia Regexów. 
  /// </summary>
  public class RegexSettingItem
  {
    public String ID
    {
      get;
      set;
    }
    
    public string Name
    {
      get;
      set;
    }

    public string Value
    {
      get;
      set;
    }

    public RegexSettingItem()
    {
      if (String.IsNullOrEmpty(ID))
      {
        ID = Guid.NewGuid().ToString();
      }
    }
  }
}
