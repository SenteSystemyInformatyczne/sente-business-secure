﻿namespace SignatureHelper
{
  partial class SettingsForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
      this.fileBrowserDialogSrcPath = new System.Windows.Forms.OpenFileDialog();
      this.folderBrowserDialogSrcPath = new System.Windows.Forms.FolderBrowserDialog();
      this.panel1 = new System.Windows.Forms.Panel();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.button_DBAdd = new System.Windows.Forms.Button();
      this.button_DBDel = new System.Windows.Forms.Button();
      this.listBox_databases = new System.Windows.Forms.ListBox();
      this.panel_DBDetail = new System.Windows.Forms.Panel();
      this.textBox_ConnectionPool = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.encodingDropDown = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.btnChooseDBPath = new System.Windows.Forms.Button();
      this.button_DBCancel = new System.Windows.Forms.Button();
      this.button_DBSave = new System.Windows.Forms.Button();
      this.textBox_DBAlias = new System.Windows.Forms.TextBox();
      this.textBox_DBRole = new System.Windows.Forms.TextBox();
      this.lblRoleSrc = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label18 = new System.Windows.Forms.Label();
      this.textBox_DBAddress = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.textBox_DBPass = new System.Windows.Forms.MaskedTextBox();
      this.textBox_DBPort = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.textBox_DBUser = new System.Windows.Forms.TextBox();
      this.textBox_DBPath = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.listBox_profiles = new System.Windows.Forms.ListBox();
      this.button_addProfil = new System.Windows.Forms.Button();
      this.button_delProfile = new System.Windows.Forms.Button();
      this.panel_profileDetail = new System.Windows.Forms.Panel();
      this.btnProfileCancel = new System.Windows.Forms.Button();
      this.btnProflieSave = new System.Windows.Forms.Button();
      this.label21 = new System.Windows.Forms.Label();
      this.label20 = new System.Windows.Forms.Label();
      this.comboBox_DBDest = new System.Windows.Forms.ComboBox();
      this.comboBox_DBSource = new System.Windows.Forms.ComboBox();
      this.textBox_profileName = new System.Windows.Forms.TextBox();
      this.buttonChooseSrcPath = new System.Windows.Forms.Button();
      this.label19 = new System.Windows.Forms.Label();
      this.textBoxSrcPath = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.lblWersjaNieaktualna = new System.Windows.Forms.Label();
      this.btnLogPath = new System.Windows.Forms.Button();
      this.tbLogPath = new System.Windows.Forms.TextBox();
      this.labelLogPath = new System.Windows.Forms.Label();
      this.label28 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.tbTempFilePath = new System.Windows.Forms.TextBox();
      this.label29 = new System.Windows.Forms.Label();
      this.txtIbExpert = new System.Windows.Forms.TextBox();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.cb_RecompileAll = new System.Windows.Forms.CheckBox();
      this.cb_GrantFilter = new System.Windows.Forms.CheckBox();
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.rbBackupLock = new System.Windows.Forms.RadioButton();
      this.rbBackupGbak = new System.Windows.Forms.RadioButton();
      this.label6 = new System.Windows.Forms.Label();
      this.cb_ShowScripts = new System.Windows.Forms.CheckBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.cb_ShowGenScript = new System.Windows.Forms.CheckBox();
      this.linesLabel = new System.Windows.Forms.Label();
      this.linesTextBox = new System.Windows.Forms.TextBox();
      this.button_OtherSave = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.localEncodingDropDown = new System.Windows.Forms.ComboBox();
      this.label9 = new System.Windows.Forms.Label();
      this.btnSelectFirebirdBinPath = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.txtFirebirdBinFolder = new System.Windows.Forms.TextBox();
      this.label24 = new System.Windows.Forms.Label();
      this.label25 = new System.Windows.Forms.Label();
      this.label26 = new System.Windows.Forms.Label();
      this.textBox_LocalFBRole = new System.Windows.Forms.TextBox();
      this.maskedTextBox_LocalFBPass = new System.Windows.Forms.MaskedTextBox();
      this.textBox_LocalFBUser = new System.Windows.Forms.TextBox();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.panel2 = new System.Windows.Forms.Panel();
      this.btn5Delete = new System.Windows.Forms.Button();
      this.btn4Add = new System.Windows.Forms.Button();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.btn4Clear = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.textBoxRegexValue = new System.Windows.Forms.TextBox();
      this.textBoxRegexName = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.listBox_regex = new System.Windows.Forms.ListBox();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
      this.btnCheckSettings = new System.Windows.Forms.ToolStripButton();
      this.errPrv = new System.Windows.Forms.ErrorProvider(this.components);
      this.panel1.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.panel_DBDetail.SuspendLayout();
      this.panel_profileDetail.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.panel2.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errPrv)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.tabControl1);
      this.panel1.Controls.Add(this.toolStrip1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(867, 517);
      this.panel1.TabIndex = 2;
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage3);
      this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl1.Location = new System.Drawing.Point(0, 25);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(867, 492);
      this.tabControl1.TabIndex = 38;
      this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.groupBox2);
      this.tabPage1.Controls.Add(this.listBox_profiles);
      this.tabPage1.Controls.Add(this.button_addProfil);
      this.tabPage1.Controls.Add(this.button_delProfile);
      this.tabPage1.Controls.Add(this.panel_profileDetail);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(859, 466);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Profile";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.button_DBAdd);
      this.groupBox2.Controls.Add(this.button_DBDel);
      this.groupBox2.Controls.Add(this.listBox_databases);
      this.groupBox2.Controls.Add(this.panel_DBDetail);
      this.groupBox2.Location = new System.Drawing.Point(3, 166);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(848, 254);
      this.groupBox2.TabIndex = 38;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Bazy danych";
      // 
      // button_DBAdd
      // 
      this.button_DBAdd.Location = new System.Drawing.Point(3, 205);
      this.button_DBAdd.Name = "button_DBAdd";
      this.button_DBAdd.Size = new System.Drawing.Size(75, 34);
      this.button_DBAdd.TabIndex = 40;
      this.button_DBAdd.Text = "Dodaj";
      this.button_DBAdd.UseVisualStyleBackColor = true;
      this.button_DBAdd.Click += new System.EventHandler(this.button_DBAdd_Click);
      // 
      // button_DBDel
      // 
      this.button_DBDel.Location = new System.Drawing.Point(167, 205);
      this.button_DBDel.Name = "button_DBDel";
      this.button_DBDel.Size = new System.Drawing.Size(80, 34);
      this.button_DBDel.TabIndex = 41;
      this.button_DBDel.Text = "Usuń";
      this.button_DBDel.UseVisualStyleBackColor = true;
      this.button_DBDel.Click += new System.EventHandler(this.button_DBDel_Click);
      // 
      // listBox_databases
      // 
      this.listBox_databases.FormattingEnabled = true;
      this.listBox_databases.Location = new System.Drawing.Point(6, 16);
      this.listBox_databases.Name = "listBox_databases";
      this.listBox_databases.Size = new System.Drawing.Size(241, 186);
      this.listBox_databases.TabIndex = 39;
      this.listBox_databases.SelectedValueChanged += new System.EventHandler(this.listBox_databases_SelectedValueChanged);
      // 
      // panel_DBDetail
      // 
      this.panel_DBDetail.Controls.Add(this.textBox_ConnectionPool);
      this.panel_DBDetail.Controls.Add(this.label12);
      this.panel_DBDetail.Controls.Add(this.encodingDropDown);
      this.panel_DBDetail.Controls.Add(this.label8);
      this.panel_DBDetail.Controls.Add(this.btnChooseDBPath);
      this.panel_DBDetail.Controls.Add(this.button_DBCancel);
      this.panel_DBDetail.Controls.Add(this.button_DBSave);
      this.panel_DBDetail.Controls.Add(this.textBox_DBAlias);
      this.panel_DBDetail.Controls.Add(this.textBox_DBRole);
      this.panel_DBDetail.Controls.Add(this.lblRoleSrc);
      this.panel_DBDetail.Controls.Add(this.label1);
      this.panel_DBDetail.Controls.Add(this.label18);
      this.panel_DBDetail.Controls.Add(this.textBox_DBAddress);
      this.panel_DBDetail.Controls.Add(this.label5);
      this.panel_DBDetail.Controls.Add(this.textBox_DBPass);
      this.panel_DBDetail.Controls.Add(this.textBox_DBPort);
      this.panel_DBDetail.Controls.Add(this.label2);
      this.panel_DBDetail.Controls.Add(this.label4);
      this.panel_DBDetail.Controls.Add(this.textBox_DBUser);
      this.panel_DBDetail.Controls.Add(this.textBox_DBPath);
      this.panel_DBDetail.Controls.Add(this.label3);
      this.panel_DBDetail.Location = new System.Drawing.Point(262, 16);
      this.panel_DBDetail.Name = "panel_DBDetail";
      this.panel_DBDetail.Size = new System.Drawing.Size(577, 234);
      this.panel_DBDetail.TabIndex = 38;
      // 
      // textBox_ConnectionPool
      // 
      this.textBox_ConnectionPool.Location = new System.Drawing.Point(314, 59);
      this.textBox_ConnectionPool.Name = "textBox_ConnectionPool";
      this.textBox_ConnectionPool.Size = new System.Drawing.Size(81, 20);
      this.textBox_ConnectionPool.TabIndex = 17;
      this.textBox_ConnectionPool.TextChanged += new System.EventHandler(this.textBox_ConnectionPool_TextChanged);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(169, 61);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(138, 13);
      this.label12.TabIndex = 16;
      this.label12.Text = "Max liczba połączeń do DB";
      // 
      // encodingDropDown
      // 
      this.encodingDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.encodingDropDown.FormattingEnabled = true;
      this.encodingDropDown.Items.AddRange(new object[] {
            "WIN1250",
            "UTF8"});
      this.encodingDropDown.Location = new System.Drawing.Point(467, 58);
      this.encodingDropDown.Name = "encodingDropDown";
      this.encodingDropDown.Size = new System.Drawing.Size(75, 21);
      this.encodingDropDown.TabIndex = 15;
      this.encodingDropDown.SelectedIndexChanged += new System.EventHandler(this.encodingDropDown_SelectedIndexChanged);
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(401, 61);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(60, 13);
      this.label8.TabIndex = 14;
      this.label8.Text = "Kodowanie";
      this.label8.Click += new System.EventHandler(this.label8_Click);
      // 
      // btnChooseDBPath
      // 
      this.btnChooseDBPath.Location = new System.Drawing.Point(545, 85);
      this.btnChooseDBPath.Name = "btnChooseDBPath";
      this.btnChooseDBPath.Size = new System.Drawing.Size(29, 21);
      this.btnChooseDBPath.TabIndex = 3;
      this.btnChooseDBPath.Text = "...";
      this.btnChooseDBPath.UseVisualStyleBackColor = true;
      this.btnChooseDBPath.Click += new System.EventHandler(this.btnChooseDBPath_Click);
      // 
      // button_DBCancel
      // 
      this.button_DBCancel.Location = new System.Drawing.Point(468, 189);
      this.button_DBCancel.Name = "button_DBCancel";
      this.button_DBCancel.Size = new System.Drawing.Size(75, 32);
      this.button_DBCancel.TabIndex = 9;
      this.button_DBCancel.Text = "Anuluj";
      this.button_DBCancel.UseVisualStyleBackColor = true;
      this.button_DBCancel.Click += new System.EventHandler(this.button_DBCancel_Click);
      // 
      // button_DBSave
      // 
      this.button_DBSave.Location = new System.Drawing.Point(383, 189);
      this.button_DBSave.Name = "button_DBSave";
      this.button_DBSave.Size = new System.Drawing.Size(75, 32);
      this.button_DBSave.TabIndex = 8;
      this.button_DBSave.Text = "Zapisz";
      this.button_DBSave.UseVisualStyleBackColor = true;
      this.button_DBSave.Click += new System.EventHandler(this.button_DBSave_Click);
      // 
      // textBox_DBAlias
      // 
      this.textBox_DBAlias.Location = new System.Drawing.Point(103, 7);
      this.textBox_DBAlias.Name = "textBox_DBAlias";
      this.textBox_DBAlias.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBAlias.TabIndex = 0;
      this.textBox_DBAlias.TextChanged += new System.EventHandler(this.textBox_DBAlias_TextChanged);
      // 
      // textBox_DBRole
      // 
      this.textBox_DBRole.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBRole.Location = new System.Drawing.Point(103, 163);
      this.textBox_DBRole.Name = "textBox_DBRole";
      this.textBox_DBRole.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBRole.TabIndex = 7;
      this.textBox_DBRole.TextChanged += new System.EventHandler(this.textBox_DBRole_TextChanged);
      // 
      // lblRoleSrc
      // 
      this.lblRoleSrc.AutoSize = true;
      this.lblRoleSrc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.lblRoleSrc.Location = new System.Drawing.Point(3, 165);
      this.lblRoleSrc.Name = "lblRoleSrc";
      this.lblRoleSrc.Size = new System.Drawing.Size(29, 13);
      this.lblRoleSrc.TabIndex = 11;
      this.lblRoleSrc.Text = "Rola";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label1.Location = new System.Drawing.Point(3, 35);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(74, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Adres serwera";
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(3, 10);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(54, 13);
      this.label18.TabIndex = 13;
      this.label18.Text = "Alias bazy";
      // 
      // textBox_DBAddress
      // 
      this.textBox_DBAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBAddress.Location = new System.Drawing.Point(103, 33);
      this.textBox_DBAddress.Name = "textBox_DBAddress";
      this.textBox_DBAddress.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBAddress.TabIndex = 1;
      this.textBox_DBAddress.TextChanged += new System.EventHandler(this.textBox_DBAddress_TextChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label5.Location = new System.Drawing.Point(3, 139);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(36, 13);
      this.label5.TabIndex = 9;
      this.label5.Text = "Hasło";
      // 
      // textBox_DBPass
      // 
      this.textBox_DBPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBPass.Location = new System.Drawing.Point(103, 137);
      this.textBox_DBPass.Name = "textBox_DBPass";
      this.textBox_DBPass.PasswordChar = '*';
      this.textBox_DBPass.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBPass.TabIndex = 6;
      this.textBox_DBPass.TextChanged += new System.EventHandler(this.textBox_DBPass_TextChanged);
      // 
      // textBox_DBPort
      // 
      this.textBox_DBPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBPort.Location = new System.Drawing.Point(103, 59);
      this.textBox_DBPort.Name = "textBox_DBPort";
      this.textBox_DBPort.Size = new System.Drawing.Size(60, 20);
      this.textBox_DBPort.TabIndex = 2;
      this.textBox_DBPort.TextChanged += new System.EventHandler(this.textBox_DBPort_TextChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label2.Location = new System.Drawing.Point(3, 61);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(26, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Port";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label4.Location = new System.Drawing.Point(3, 113);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(29, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "User";
      // 
      // textBox_DBUser
      // 
      this.textBox_DBUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBUser.Location = new System.Drawing.Point(103, 111);
      this.textBox_DBUser.Name = "textBox_DBUser";
      this.textBox_DBUser.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBUser.TabIndex = 5;
      this.textBox_DBUser.TextChanged += new System.EventHandler(this.textBox_DBUser_TextChanged);
      // 
      // textBox_DBPath
      // 
      this.textBox_DBPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_DBPath.Location = new System.Drawing.Point(103, 85);
      this.textBox_DBPath.Name = "textBox_DBPath";
      this.textBox_DBPath.Size = new System.Drawing.Size(439, 20);
      this.textBox_DBPath.TabIndex = 4;
      this.textBox_DBPath.TextChanged += new System.EventHandler(this.textBox_DBPath_TextChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label3.Location = new System.Drawing.Point(3, 87);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(45, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "Ścieżka";
      // 
      // listBox_profiles
      // 
      this.listBox_profiles.FormattingEnabled = true;
      this.listBox_profiles.Location = new System.Drawing.Point(8, 6);
      this.listBox_profiles.Name = "listBox_profiles";
      this.listBox_profiles.Size = new System.Drawing.Size(242, 108);
      this.listBox_profiles.TabIndex = 33;
      this.listBox_profiles.SelectedValueChanged += new System.EventHandler(this.listBox_Profiles_SelectedValueChanged);
      // 
      // button_addProfil
      // 
      this.button_addProfil.Location = new System.Drawing.Point(8, 117);
      this.button_addProfil.Name = "button_addProfil";
      this.button_addProfil.Size = new System.Drawing.Size(75, 34);
      this.button_addProfil.TabIndex = 34;
      this.button_addProfil.Text = "Dodaj";
      this.button_addProfil.UseVisualStyleBackColor = true;
      this.button_addProfil.Click += new System.EventHandler(this.button_addProfil_Click);
      // 
      // button_delProfile
      // 
      this.button_delProfile.Location = new System.Drawing.Point(170, 117);
      this.button_delProfile.Name = "button_delProfile";
      this.button_delProfile.Size = new System.Drawing.Size(80, 34);
      this.button_delProfile.TabIndex = 35;
      this.button_delProfile.Text = "Usuń";
      this.button_delProfile.UseVisualStyleBackColor = true;
      this.button_delProfile.Click += new System.EventHandler(this.button_delProfile_Click);
      // 
      // panel_profileDetail
      // 
      this.panel_profileDetail.Controls.Add(this.btnProfileCancel);
      this.panel_profileDetail.Controls.Add(this.btnProflieSave);
      this.panel_profileDetail.Controls.Add(this.label21);
      this.panel_profileDetail.Controls.Add(this.label20);
      this.panel_profileDetail.Controls.Add(this.comboBox_DBDest);
      this.panel_profileDetail.Controls.Add(this.comboBox_DBSource);
      this.panel_profileDetail.Controls.Add(this.textBox_profileName);
      this.panel_profileDetail.Controls.Add(this.buttonChooseSrcPath);
      this.panel_profileDetail.Controls.Add(this.label19);
      this.panel_profileDetail.Controls.Add(this.textBoxSrcPath);
      this.panel_profileDetail.Controls.Add(this.label16);
      this.panel_profileDetail.Location = new System.Drawing.Point(265, 6);
      this.panel_profileDetail.Name = "panel_profileDetail";
      this.panel_profileDetail.Size = new System.Drawing.Size(586, 154);
      this.panel_profileDetail.TabIndex = 36;
      // 
      // btnProfileCancel
      // 
      this.btnProfileCancel.Location = new System.Drawing.Point(467, 112);
      this.btnProfileCancel.Name = "btnProfileCancel";
      this.btnProfileCancel.Size = new System.Drawing.Size(75, 32);
      this.btnProfileCancel.TabIndex = 7;
      this.btnProfileCancel.Text = "Anuluj";
      this.btnProfileCancel.UseVisualStyleBackColor = true;
      this.btnProfileCancel.Click += new System.EventHandler(this.btnProfileCancel_Click);
      // 
      // btnProflieSave
      // 
      this.btnProflieSave.Location = new System.Drawing.Point(383, 112);
      this.btnProflieSave.Name = "btnProflieSave";
      this.btnProflieSave.Size = new System.Drawing.Size(75, 32);
      this.btnProflieSave.TabIndex = 6;
      this.btnProflieSave.Text = "Zapisz";
      this.btnProflieSave.UseVisualStyleBackColor = true;
      this.btnProflieSave.Click += new System.EventHandler(this.btnProflieSave_Click);
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(6, 59);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(80, 13);
      this.label21.TabIndex = 5;
      this.label21.Text = "Baza docelowa";
      // 
      // label20
      // 
      this.label20.AutoSize = true;
      this.label20.Location = new System.Drawing.Point(6, 32);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(78, 13);
      this.label20.TabIndex = 4;
      this.label20.Text = "Baza źródłowa";
      // 
      // comboBox_DBDest
      // 
      this.comboBox_DBDest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox_DBDest.FormattingEnabled = true;
      this.comboBox_DBDest.Location = new System.Drawing.Point(112, 56);
      this.comboBox_DBDest.Name = "comboBox_DBDest";
      this.comboBox_DBDest.Size = new System.Drawing.Size(430, 21);
      this.comboBox_DBDest.TabIndex = 3;
      this.comboBox_DBDest.SelectedIndexChanged += new System.EventHandler(this.comboBox_DBDest_SelectedIndexChanged);
      // 
      // comboBox_DBSource
      // 
      this.comboBox_DBSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox_DBSource.FormattingEnabled = true;
      this.comboBox_DBSource.Location = new System.Drawing.Point(112, 29);
      this.comboBox_DBSource.Name = "comboBox_DBSource";
      this.comboBox_DBSource.Size = new System.Drawing.Size(430, 21);
      this.comboBox_DBSource.TabIndex = 2;
      this.comboBox_DBSource.SelectedIndexChanged += new System.EventHandler(this.comboBox_DBSource_SelectedIndexChanged);
      // 
      // textBox_profileName
      // 
      this.textBox_profileName.Location = new System.Drawing.Point(112, 3);
      this.textBox_profileName.Name = "textBox_profileName";
      this.textBox_profileName.Size = new System.Drawing.Size(430, 20);
      this.textBox_profileName.TabIndex = 1;
      this.textBox_profileName.TextChanged += new System.EventHandler(this.textBox_profileName_TextChanged);
      // 
      // buttonChooseSrcPath
      // 
      this.buttonChooseSrcPath.Location = new System.Drawing.Point(548, 82);
      this.buttonChooseSrcPath.Name = "buttonChooseSrcPath";
      this.buttonChooseSrcPath.Size = new System.Drawing.Size(29, 21);
      this.buttonChooseSrcPath.TabIndex = 4;
      this.buttonChooseSrcPath.Text = "...";
      this.buttonChooseSrcPath.UseVisualStyleBackColor = true;
      this.buttonChooseSrcPath.Click += new System.EventHandler(this.buttonChooseSrcPath_Click);
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(6, 6);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(71, 13);
      this.label19.TabIndex = 0;
      this.label19.Text = "Nazwa profilu";
      // 
      // textBoxSrcPath
      // 
      this.textBoxSrcPath.Location = new System.Drawing.Point(112, 83);
      this.textBoxSrcPath.Name = "textBoxSrcPath";
      this.textBoxSrcPath.Size = new System.Drawing.Size(430, 20);
      this.textBoxSrcPath.TabIndex = 5;
      this.textBoxSrcPath.TextChanged += new System.EventHandler(this.textBoxSrcPath_TextChanged);
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label16.Location = new System.Drawing.Point(3, 86);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(97, 13);
      this.label16.TabIndex = 20;
      this.label16.Text = "Folder ze skryptami";
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.groupBox5);
      this.tabPage2.Controls.Add(this.groupBox4);
      this.tabPage2.Controls.Add(this.groupBox3);
      this.tabPage2.Controls.Add(this.button_OtherSave);
      this.tabPage2.Controls.Add(this.groupBox1);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(859, 466);
      this.tabPage2.TabIndex = 2;
      this.tabPage2.Text = "Pozostałe ustawienia";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.lblWersjaNieaktualna);
      this.groupBox5.Controls.Add(this.btnLogPath);
      this.groupBox5.Controls.Add(this.tbLogPath);
      this.groupBox5.Controls.Add(this.labelLogPath);
      this.groupBox5.Controls.Add(this.label28);
      this.groupBox5.Controls.Add(this.button1);
      this.groupBox5.Controls.Add(this.button2);
      this.groupBox5.Controls.Add(this.tbTempFilePath);
      this.groupBox5.Controls.Add(this.label29);
      this.groupBox5.Controls.Add(this.txtIbExpert);
      this.groupBox5.Location = new System.Drawing.Point(6, 6);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(507, 89);
      this.groupBox5.TabIndex = 34;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Ustawienia katalogów";
      // 
      // lblWersjaNieaktualna
      // 
      this.lblWersjaNieaktualna.AutoSize = true;
      this.lblWersjaNieaktualna.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.lblWersjaNieaktualna.ForeColor = System.Drawing.Color.Red;
      this.lblWersjaNieaktualna.Location = new System.Drawing.Point(1, 51);
      this.lblWersjaNieaktualna.Name = "lblWersjaNieaktualna";
      this.lblWersjaNieaktualna.Size = new System.Drawing.Size(0, 13);
      this.lblWersjaNieaktualna.TabIndex = 41;
      // 
      // btnLogPath
      // 
      this.btnLogPath.Location = new System.Drawing.Point(468, 61);
      this.btnLogPath.Name = "btnLogPath";
      this.btnLogPath.Size = new System.Drawing.Size(29, 21);
      this.btnLogPath.TabIndex = 36;
      this.btnLogPath.Text = "...";
      this.btnLogPath.UseVisualStyleBackColor = true;
      this.btnLogPath.Click += new System.EventHandler(this.btnChangeTbLogPath_Click);
      // 
      // tbLogPath
      // 
      this.tbLogPath.Location = new System.Drawing.Point(148, 62);
      this.tbLogPath.Name = "tbLogPath";
      this.tbLogPath.Size = new System.Drawing.Size(314, 20);
      this.tbLogPath.TabIndex = 37;
      // 
      // labelLogPath
      // 
      this.labelLogPath.AutoSize = true;
      this.labelLogPath.Location = new System.Drawing.Point(1, 65);
      this.labelLogPath.Name = "labelLogPath";
      this.labelLogPath.Size = new System.Drawing.Size(100, 13);
      this.labelLogPath.TabIndex = 40;
      this.labelLogPath.Text = "Folder zapisu logów";
      // 
      // label28
      // 
      this.label28.AutoSize = true;
      this.label28.Location = new System.Drawing.Point(1, 15);
      this.label28.Name = "label28";
      this.label28.Size = new System.Drawing.Size(141, 13);
      this.label28.TabIndex = 33;
      this.label28.Text = "Folder plików tymczasowych";
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(468, 11);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(29, 21);
      this.button1.TabIndex = 31;
      this.button1.Text = ",,,";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.btnTempFilePath_Click);
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(468, 35);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(29, 21);
      this.button2.TabIndex = 34;
      this.button2.Text = "...";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.btnChangeIBExpertPath_Click);
      // 
      // tbTempFilePath
      // 
      this.tbTempFilePath.Location = new System.Drawing.Point(148, 12);
      this.tbTempFilePath.Name = "tbTempFilePath";
      this.tbTempFilePath.Size = new System.Drawing.Size(314, 20);
      this.tbTempFilePath.TabIndex = 32;
      // 
      // label29
      // 
      this.label29.AutoSize = true;
      this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label29.Location = new System.Drawing.Point(1, 38);
      this.label29.Name = "label29";
      this.label29.Size = new System.Drawing.Size(85, 13);
      this.label29.TabIndex = 39;
      this.label29.Text = "Folder IBExperta";
      // 
      // txtIbExpert
      // 
      this.txtIbExpert.Location = new System.Drawing.Point(148, 35);
      this.txtIbExpert.Name = "txtIbExpert";
      this.txtIbExpert.Size = new System.Drawing.Size(314, 20);
      this.txtIbExpert.TabIndex = 35;
      this.txtIbExpert.TextChanged += new System.EventHandler(this.txtIbExpert_TextChanged_1);
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.cb_RecompileAll);
      this.groupBox4.Controls.Add(this.cb_GrantFilter);
      this.groupBox4.Controls.Add(this.textBox2);
      this.groupBox4.Controls.Add(this.textBox1);
      this.groupBox4.Controls.Add(this.rbBackupLock);
      this.groupBox4.Controls.Add(this.rbBackupGbak);
      this.groupBox4.Controls.Add(this.label6);
      this.groupBox4.Controls.Add(this.cb_ShowScripts);
      this.groupBox4.Location = new System.Drawing.Point(6, 101);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(507, 357);
      this.groupBox4.TabIndex = 34;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Ustawienia pełnego procesu";
      // 
      // cb_RecompileAll
      // 
      this.cb_RecompileAll.AutoSize = true;
      this.cb_RecompileAll.Location = new System.Drawing.Point(6, 58);
      this.cb_RecompileAll.Name = "cb_RecompileAll";
      this.cb_RecompileAll.Size = new System.Drawing.Size(261, 17);
      this.cb_RecompileAll.TabIndex = 239;
      this.cb_RecompileAll.Text = "Po aktualizacji bazy rekompiluj procedury i triggery";
      this.cb_RecompileAll.UseVisualStyleBackColor = true;
      // 
      // cb_GrantFilter
      // 
      this.cb_GrantFilter.AutoSize = true;
      this.cb_GrantFilter.Location = new System.Drawing.Point(6, 38);
      this.cb_GrantFilter.Name = "cb_GrantFilter";
      this.cb_GrantFilter.Size = new System.Drawing.Size(230, 17);
      this.cb_GrantFilter.TabIndex = 233;
      this.cb_GrantFilter.Text = "Odfiltrowywanie niestandardowych grantów";
      this.cb_GrantFilter.UseVisualStyleBackColor = true;
      // 
      // textBox2
      // 
      this.textBox2.BackColor = System.Drawing.SystemColors.Window;
      this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBox2.Location = new System.Drawing.Point(252, 118);
      this.textBox2.Multiline = true;
      this.textBox2.Name = "textBox2";
      this.textBox2.ReadOnly = true;
      this.textBox2.Size = new System.Drawing.Size(246, 207);
      this.textBox2.TabIndex = 238;
      this.textBox2.Text = resources.GetString("textBox2.Text");
      // 
      // textBox1
      // 
      this.textBox1.BackColor = System.Drawing.SystemColors.Window;
      this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBox1.Location = new System.Drawing.Point(6, 119);
      this.textBox1.Multiline = true;
      this.textBox1.Name = "textBox1";
      this.textBox1.ReadOnly = true;
      this.textBox1.Size = new System.Drawing.Size(246, 144);
      this.textBox1.TabIndex = 237;
      this.textBox1.Text = resources.GetString("textBox1.Text");
      // 
      // rbBackupLock
      // 
      this.rbBackupLock.AutoSize = true;
      this.rbBackupLock.Location = new System.Drawing.Point(254, 95);
      this.rbBackupLock.Name = "rbBackupLock";
      this.rbBackupLock.Size = new System.Drawing.Size(221, 17);
      this.rbBackupLock.TabIndex = 236;
      this.rbBackupLock.TabStop = true;
      this.rbBackupLock.Text = "lockowanie i plik .delta (eksperymentalne)";
      this.rbBackupLock.UseVisualStyleBackColor = true;
      // 
      // rbBackupGbak
      // 
      this.rbBackupGbak.AutoSize = true;
      this.rbBackupGbak.Location = new System.Drawing.Point(6, 95);
      this.rbBackupGbak.Name = "rbBackupGbak";
      this.rbBackupGbak.Size = new System.Drawing.Size(96, 17);
      this.rbBackupGbak.TabIndex = 235;
      this.rbBackupGbak.TabStop = true;
      this.rbBackupGbak.Text = "gbak / odgbak";
      this.rbBackupGbak.UseVisualStyleBackColor = true;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label6.Location = new System.Drawing.Point(3, 78);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(249, 13);
      this.label6.TabIndex = 234;
      this.label6.Text = "Tryb zapewniania bezpieczeństwa procesu";
      // 
      // cb_ShowScripts
      // 
      this.cb_ShowScripts.AutoSize = true;
      this.cb_ShowScripts.Location = new System.Drawing.Point(6, 19);
      this.cb_ShowScripts.Name = "cb_ShowScripts";
      this.cb_ShowScripts.Size = new System.Drawing.Size(334, 17);
      this.cb_ShowScripts.TabIndex = 232;
      this.cb_ShowScripts.Text = "Pokaż wygenerowane skrypty przed wgraniem na bazę docelową";
      this.cb_ShowScripts.UseVisualStyleBackColor = true;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.cb_ShowGenScript);
      this.groupBox3.Controls.Add(this.linesLabel);
      this.groupBox3.Controls.Add(this.linesTextBox);
      this.groupBox3.Location = new System.Drawing.Point(519, 12);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(337, 83);
      this.groupBox3.TabIndex = 33;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Ustawienia okna głównego";
      // 
      // cb_ShowGenScript
      // 
      this.cb_ShowGenScript.AutoSize = true;
      this.cb_ShowGenScript.Location = new System.Drawing.Point(9, 53);
      this.cb_ShowGenScript.Name = "cb_ShowGenScript";
      this.cb_ShowGenScript.Size = new System.Drawing.Size(238, 17);
      this.cb_ShowGenScript.TabIndex = 32;
      this.cb_ShowGenScript.Text = "Pokaż opcję generowania skryptu dla tematu";
      this.cb_ShowGenScript.UseVisualStyleBackColor = true;
      this.cb_ShowGenScript.CheckedChanged += new System.EventHandler(this.cb_ShowGenScript_CheckedChanged);
      // 
      // linesLabel
      // 
      this.linesLabel.AutoSize = true;
      this.linesLabel.Location = new System.Drawing.Point(6, 30);
      this.linesLabel.Name = "linesLabel";
      this.linesLabel.Size = new System.Drawing.Size(179, 13);
      this.linesLabel.TabIndex = 31;
      this.linesLabel.Text = "Maks. liczba wierszy z komunikatami";
      // 
      // linesTextBox
      // 
      this.linesTextBox.Location = new System.Drawing.Point(198, 27);
      this.linesTextBox.Name = "linesTextBox";
      this.linesTextBox.Size = new System.Drawing.Size(133, 20);
      this.linesTextBox.TabIndex = 10;
      // 
      // button_OtherSave
      // 
      this.button_OtherSave.Location = new System.Drawing.Point(778, 426);
      this.button_OtherSave.Name = "button_OtherSave";
      this.button_OtherSave.Size = new System.Drawing.Size(75, 32);
      this.button_OtherSave.TabIndex = 231;
      this.button_OtherSave.Text = "Zapisz";
      this.button_OtherSave.UseVisualStyleBackColor = true;
      this.button_OtherSave.Click += new System.EventHandler(this.button_OtherSave_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.localEncodingDropDown);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.btnSelectFirebirdBinPath);
      this.groupBox1.Controls.Add(this.label7);
      this.groupBox1.Controls.Add(this.txtFirebirdBinFolder);
      this.groupBox1.Controls.Add(this.label24);
      this.groupBox1.Controls.Add(this.label25);
      this.groupBox1.Controls.Add(this.label26);
      this.groupBox1.Controls.Add(this.textBox_LocalFBRole);
      this.groupBox1.Controls.Add(this.maskedTextBox_LocalFBPass);
      this.groupBox1.Controls.Add(this.textBox_LocalFBUser);
      this.groupBox1.Location = new System.Drawing.Point(519, 101);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(337, 158);
      this.groupBox1.TabIndex = 28;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Lokalne ustawienia Firebirda";
      // 
      // localEncodingDropDown
      // 
      this.localEncodingDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.localEncodingDropDown.FormattingEnabled = true;
      this.localEncodingDropDown.Items.AddRange(new object[] {
            "WIN1250",
            "UTF8"});
      this.localEncodingDropDown.Location = new System.Drawing.Point(65, 124);
      this.localEncodingDropDown.Name = "localEncodingDropDown";
      this.localEncodingDropDown.Size = new System.Drawing.Size(266, 21);
      this.localEncodingDropDown.TabIndex = 39;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(7, 127);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(60, 13);
      this.label9.TabIndex = 38;
      this.label9.Text = "Kodowanie";
      // 
      // btnSelectFirebirdBinPath
      // 
      this.btnSelectFirebirdBinPath.Location = new System.Drawing.Point(302, 97);
      this.btnSelectFirebirdBinPath.Name = "btnSelectFirebirdBinPath";
      this.btnSelectFirebirdBinPath.Size = new System.Drawing.Size(29, 21);
      this.btnSelectFirebirdBinPath.TabIndex = 37;
      this.btnSelectFirebirdBinPath.Text = "...";
      this.btnSelectFirebirdBinPath.UseVisualStyleBackColor = true;
      this.btnSelectFirebirdBinPath.Click += new System.EventHandler(this.btnSelectFirebirdBinPath_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label7.Location = new System.Drawing.Point(6, 99);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(53, 13);
      this.label7.TabIndex = 15;
      this.label7.Text = "Folder bin";
      // 
      // txtFirebirdBinFolder
      // 
      this.txtFirebirdBinFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtFirebirdBinFolder.Location = new System.Drawing.Point(65, 97);
      this.txtFirebirdBinFolder.Name = "txtFirebirdBinFolder";
      this.txtFirebirdBinFolder.Size = new System.Drawing.Size(231, 20);
      this.txtFirebirdBinFolder.TabIndex = 14;
      // 
      // label24
      // 
      this.label24.AutoSize = true;
      this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label24.Location = new System.Drawing.Point(6, 21);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(29, 13);
      this.label24.TabIndex = 7;
      this.label24.Text = "User";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label25.Location = new System.Drawing.Point(6, 47);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(36, 13);
      this.label25.TabIndex = 9;
      this.label25.Text = "Hasło";
      // 
      // label26
      // 
      this.label26.AutoSize = true;
      this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label26.Location = new System.Drawing.Point(6, 73);
      this.label26.Name = "label26";
      this.label26.Size = new System.Drawing.Size(29, 13);
      this.label26.TabIndex = 13;
      this.label26.Text = "Rola";
      // 
      // textBox_LocalFBRole
      // 
      this.textBox_LocalFBRole.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_LocalFBRole.Location = new System.Drawing.Point(65, 71);
      this.textBox_LocalFBRole.Name = "textBox_LocalFBRole";
      this.textBox_LocalFBRole.Size = new System.Drawing.Size(266, 20);
      this.textBox_LocalFBRole.TabIndex = 9;
      // 
      // maskedTextBox_LocalFBPass
      // 
      this.maskedTextBox_LocalFBPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.maskedTextBox_LocalFBPass.Location = new System.Drawing.Point(65, 45);
      this.maskedTextBox_LocalFBPass.Name = "maskedTextBox_LocalFBPass";
      this.maskedTextBox_LocalFBPass.PasswordChar = '*';
      this.maskedTextBox_LocalFBPass.Size = new System.Drawing.Size(266, 20);
      this.maskedTextBox_LocalFBPass.TabIndex = 8;
      this.maskedTextBox_LocalFBPass.TextChanged += new System.EventHandler(this.maskedTextBox_LocalFBPass_TextChanged);
      // 
      // textBox_LocalFBUser
      // 
      this.textBox_LocalFBUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.textBox_LocalFBUser.Location = new System.Drawing.Point(65, 19);
      this.textBox_LocalFBUser.Name = "textBox_LocalFBUser";
      this.textBox_LocalFBUser.Size = new System.Drawing.Size(266, 20);
      this.textBox_LocalFBUser.TabIndex = 7;
      this.textBox_LocalFBUser.TextChanged += new System.EventHandler(this.textBox_LocalFBUser_TextChanged);
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.panel2);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage3.Size = new System.Drawing.Size(859, 466);
      this.tabPage3.TabIndex = 3;
      this.tabPage3.Text = "Analiza skryptu";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.btn5Delete);
      this.panel2.Controls.Add(this.btn4Add);
      this.panel2.Controls.Add(this.groupBox6);
      this.panel2.Controls.Add(this.listBox_regex);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(3, 3);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(853, 460);
      this.panel2.TabIndex = 0;
      // 
      // btn5Delete
      // 
      this.btn5Delete.Location = new System.Drawing.Point(130, 414);
      this.btn5Delete.Name = "btn5Delete";
      this.btn5Delete.Size = new System.Drawing.Size(87, 40);
      this.btn5Delete.TabIndex = 3;
      this.btn5Delete.Text = "Usuń";
      this.btn5Delete.UseVisualStyleBackColor = true;
      this.btn5Delete.Click += new System.EventHandler(this.btn5Delete_Click);
      // 
      // btn4Add
      // 
      this.btn4Add.Location = new System.Drawing.Point(5, 413);
      this.btn4Add.Name = "btn4Add";
      this.btn4Add.Size = new System.Drawing.Size(78, 41);
      this.btn4Add.TabIndex = 2;
      this.btn4Add.Text = "Dodaj";
      this.btn4Add.UseVisualStyleBackColor = true;
      this.btn4Add.Click += new System.EventHandler(this.btn4Add_Click);
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.btn4Clear);
      this.groupBox6.Controls.Add(this.button3);
      this.groupBox6.Controls.Add(this.textBoxRegexValue);
      this.groupBox6.Controls.Add(this.textBoxRegexName);
      this.groupBox6.Controls.Add(this.label11);
      this.groupBox6.Controls.Add(this.label10);
      this.groupBox6.Location = new System.Drawing.Point(245, 9);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(603, 445);
      this.groupBox6.TabIndex = 1;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = "Ustawienia regexów";
      // 
      // btn4Clear
      // 
      this.btn4Clear.Location = new System.Drawing.Point(483, 402);
      this.btn4Clear.Name = "btn4Clear";
      this.btn4Clear.Size = new System.Drawing.Size(98, 37);
      this.btn4Clear.TabIndex = 5;
      this.btn4Clear.Text = "Anuluj";
      this.btn4Clear.UseVisualStyleBackColor = true;
      this.btn4Clear.Click += new System.EventHandler(this.btn4Clear_Click);
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(376, 402);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(98, 37);
      this.button3.TabIndex = 4;
      this.button3.Text = "Zapisz";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // textBoxRegexValue
      // 
      this.textBoxRegexValue.Location = new System.Drawing.Point(68, 80);
      this.textBoxRegexValue.Multiline = true;
      this.textBoxRegexValue.Name = "textBoxRegexValue";
      this.textBoxRegexValue.Size = new System.Drawing.Size(513, 296);
      this.textBoxRegexValue.TabIndex = 3;
      // 
      // textBoxRegexName
      // 
      this.textBoxRegexName.Location = new System.Drawing.Point(68, 40);
      this.textBoxRegexName.Name = "textBoxRegexName";
      this.textBoxRegexName.Size = new System.Drawing.Size(513, 20);
      this.textBoxRegexName.TabIndex = 2;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(19, 83);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(41, 13);
      this.label11.TabIndex = 1;
      this.label11.Text = "Regex:";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(19, 43);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(43, 13);
      this.label10.TabIndex = 0;
      this.label10.Text = "Nazwa:";
      // 
      // listBox_regex
      // 
      this.listBox_regex.FormattingEnabled = true;
      this.listBox_regex.Location = new System.Drawing.Point(5, 9);
      this.listBox_regex.Name = "listBox_regex";
      this.listBox_regex.Size = new System.Drawing.Size(212, 394);
      this.listBox_regex.TabIndex = 0;
      this.listBox_regex.SelectedIndexChanged += new System.EventHandler(this.listBox_regex_SelectedIndexChanged);
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.btnCheckSettings});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(867, 25);
      this.toolStrip1.TabIndex = 15;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // toolStripButton2
      // 
      this.toolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
      this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
      this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton2.Name = "toolStripButton2";
      this.toolStripButton2.Size = new System.Drawing.Size(54, 22);
      this.toolStripButton2.Text = "Zam&knij";
      this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
      // 
      // btnCheckSettings
      // 
      this.btnCheckSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.btnCheckSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnCheckSettings.Name = "btnCheckSettings";
      this.btnCheckSettings.Size = new System.Drawing.Size(114, 22);
      this.btnCheckSettings.Text = "Sprawdź ustawienia";
      this.btnCheckSettings.Click += new System.EventHandler(this.btnCheckSettings_Click);
      // 
      // errPrv
      // 
      this.errPrv.ContainerControl = this;
      // 
      // SettingsForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(867, 517);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "SettingsForm";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Ustawienia";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
      this.Load += new System.EventHandler(this.SettingsForm_Load);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.panel_DBDetail.ResumeLayout(false);
      this.panel_DBDetail.PerformLayout();
      this.panel_profileDetail.ResumeLayout(false);
      this.panel_profileDetail.PerformLayout();
      this.tabPage2.ResumeLayout(false);
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.tabPage3.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errPrv)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.OpenFileDialog fileBrowserDialogSrcPath;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogSrcPath;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton toolStripButton2;
    private System.Windows.Forms.ToolStripButton btnCheckSettings;
    private System.Windows.Forms.ErrorProvider errPrv;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button button_DBAdd;
    private System.Windows.Forms.Button button_DBDel;
    private System.Windows.Forms.ListBox listBox_databases;
    private System.Windows.Forms.Panel panel_DBDetail;
    private System.Windows.Forms.TextBox textBox_DBAlias;
    private System.Windows.Forms.TextBox textBox_DBRole;
    private System.Windows.Forms.Label lblRoleSrc;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox textBox_DBAddress;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.MaskedTextBox textBox_DBPass;
    private System.Windows.Forms.TextBox textBox_DBPort;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBox_DBUser;
    private System.Windows.Forms.TextBox textBox_DBPath;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ListBox listBox_profiles;
    private System.Windows.Forms.Button button_addProfil;
    private System.Windows.Forms.Button button_delProfile;
    private System.Windows.Forms.Panel panel_profileDetail;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.ComboBox comboBox_DBDest;
    private System.Windows.Forms.ComboBox comboBox_DBSource;
    private System.Windows.Forms.TextBox textBox_profileName;
    private System.Windows.Forms.Button buttonChooseSrcPath;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.TextBox textBoxSrcPath;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Button button_DBCancel;
    private System.Windows.Forms.Button button_DBSave;
    private System.Windows.Forms.Button btnProfileCancel;
    private System.Windows.Forms.Button btnProflieSave;
    private System.Windows.Forms.Button btnChooseDBPath;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Button btnLogPath;
    private System.Windows.Forms.TextBox tbLogPath;
    private System.Windows.Forms.Label labelLogPath;
    private System.Windows.Forms.Label label28;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.TextBox tbTempFilePath;
    private System.Windows.Forms.Label label29;
    private System.Windows.Forms.TextBox txtIbExpert;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.RadioButton rbBackupLock;
    private System.Windows.Forms.RadioButton rbBackupGbak;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.CheckBox cb_ShowScripts;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label linesLabel;
    private System.Windows.Forms.TextBox linesTextBox;
    private System.Windows.Forms.Button button_OtherSave;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.TextBox textBox_LocalFBRole;
    private System.Windows.Forms.MaskedTextBox maskedTextBox_LocalFBPass;
    private System.Windows.Forms.TextBox textBox_LocalFBUser;
    private System.Windows.Forms.Button btnSelectFirebirdBinPath;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox txtFirebirdBinFolder;
    private System.Windows.Forms.CheckBox cb_GrantFilter;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.ComboBox encodingDropDown;
    private System.Windows.Forms.ComboBox localEncodingDropDown;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label lblWersjaNieaktualna;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ListBox listBox_regex;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.Button btn4Clear;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.TextBox textBoxRegexValue;
    private System.Windows.Forms.TextBox textBoxRegexName;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Button btn5Delete;
    private System.Windows.Forms.Button btn4Add;
    private System.Windows.Forms.CheckBox cb_ShowGenScript;
    private System.Windows.Forms.TextBox textBox_ConnectionPool;
    private System.Windows.Forms.CheckBox cb_RecompileAll;
    private System.Windows.Forms.Label label12;
  }
}