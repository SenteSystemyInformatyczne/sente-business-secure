﻿using FirebirdSql.Data.FirebirdClient;
using SignatureHelper.DatabaseExtraction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignatureHelper
{
  public class CliTool
  {
    public FbConnectionStringBuilder fbconnstr = new FbConnectionStringBuilder();
    public string srcDir = "";
    public string srcVer = "";
    public string outScriptName = "";
    public string targetDb = "";
    public string outDirectory = "";

    public void GenerateScript()
    {
      MetadataExtractor me = new MetadataExtractor();
      if (string.IsNullOrWhiteSpace(srcVer))
        srcVer = GetCurrentVersionFromFile(Path.Combine(srcDir, "VERSION"));
      me.ParseFiles("", Path.Combine(srcDir, srcVer));

      StringBuilder errors = new StringBuilder();
      if (me.eList.Count > 0)
      {
        errors.AppendLine("Poniższe pliki zawierają błędy. WYGENEROWANE SKRYPTY MOGĄ NIE WGRYWAC SIĘ PRAWIDŁOWO!!:");
        foreach (string s in me.eList)
          errors.AppendLine(s);
      }
      try
      {
        StringBuilder script = new StringBuilder();

        string x = me.ScriptCreate();
        script.AppendLine(x);

        x = me.X_ScriptCreate();
        script.AppendLine(x);

        x = me.AppiniSQLCreate();
        script.AppendLine(x);

        string s = script.ToString().Trim();
        using (StreamWriter sw = new StreamWriter(Path.Combine(outDirectory, outScriptName)))
        {
          sw.Write(s);
          sw.Close();
        }
      }
      catch (Exception ex)
      {
        errors.AppendLine(ex.Message);
      }
      using (StreamWriter sw = new StreamWriter(Path.Combine(outDirectory, "error.log"), true))
      {
        sw.WriteLine(DateTime.Now.ToString());
        sw.Write(errors);
        sw.Close();
      }
    }

    string GetCurrentVersionFromFile(string path)
    {
      try
      {
        using (StreamReader sr = new StreamReader(path))
        {
          string ver = sr.ReadLine();
          sr.Close();
          return ver;
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Błąd odczytu wersji z pliku");
        return null;
      }
    }
  }
}
