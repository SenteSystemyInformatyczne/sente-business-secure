﻿using SHDataContracts.Enums;
using SHlib.DatabaseUpdate;
using SHlib.DataExtraction;
using SHlib.Utilities;
using SHlib.Utilities.FormsWrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shtool
{
  /// <summary>
  /// Klasa implementująca poszczególne operacje dostępne z cli
  /// </summary>
  public class CliTool
  {
    public DbSettings DbSettings
    {
      get;
      private set;
    }
    public string srcDir = "";
    public string outScriptName = "";
    public string targetDb = "";
    public string outDirectory = "";
    public string diffFile = "";
    public bool appiniAsInsert = true;
    public bool grantFilter = false;
    public string Signature
    {
      get;
      set;
    }

/// <summary>
/// Właściwość mówiąca o tym, czy filtrować granty tylko dla standardowych użytkowników.
/// </summary>
    public bool GrantFilter
    {
      get
      {
        return grantFilter;
      }
      set
      {
        grantFilter = value;
      }
    }

    public enum CliMode
    {
      Undefined,
      ImportToDb,
      ExportSingleTopic,
      ExportAll,
      GetInputUpdateScript,
      GetOutputUpdateScript,
      Postprocess,
      FastUpdate,
      UpdateCRC
    };

    public bool AllwaysPath = false;

    public bool IgnoreIdErrors = false;

    public CliMode Mode
    {
      get;
      set;
    }


    public CliTool()
    {
      DbSettings = new DbSettings();
      SQLRepository.Instance.AppendMessage = WriteMessage;
    }

    private int maxmsglen;

    private void WriteMessage(string obj)
    {
      Write(obj);
    }

    private void Write(string obj, bool inplace = false)
    {
      if (!inplace && !obj.EndsWith("\n"))
        obj += "\n";
      if (inplace && !obj.EndsWith("\r"))
        obj += "\r";
      if (inplace)
      {
        obj = obj.Substring(0, Math.Min(obj.Length, 79));

        var clearstr = string.Empty;
        while (clearstr.Length < maxmsglen)
          clearstr += ' ';
        Console.Write(clearstr + "\r");
      }

      Console.Write(obj);
      maxmsglen = Math.Max(maxmsglen, obj.Length);
    }

    private void WriteCount(int cnt)
    {
      Write("");
      WriteMessage("Przetwarzam " + cnt + " obiektów");
    }

    private int lastprogress = 0;

    private void WriteProgress(int i, int cnt, string phase)
    {
      int proc = (int) (1.0 * i / cnt * 100);
      if (proc != lastprogress)
      {
        Write(phase + " " + proc + " %...", true);
        lastprogress = proc;
      }
    }

    ProgressInfo ProgressInfoFactory()
    {
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Source, WriteMessage, ErrorMessage, GrantFilter);
      ProgressInfo pi = new ProgressInfo();
      pi.Write = WriteMessage;
      pi.WriteCount = WriteCount;
      pi.WriteProgress = WriteProgress;
      pi.MessageBoxForUUIDMismatch = MessageBoxForUUIDMismatch;

      return pi;
    }

    internal void ExportSignatureScript()
    {
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Source, WriteMessage, ErrorMessage, GrantFilter);
      var pi = ProgressInfoFactory();
      de.ExportSignature(outDirectory, Signature, pi);
    }

    private void ErrorMessage(Exception obj)
    {
      while (obj != null)
      {
        WriteMessage(obj.Message);
        WriteMessage(obj.StackTrace);
        obj = obj.InnerException;
      }
      Environment.Exit(1);
    }

    internal void ExportallScript()
    {
      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Source, WriteMessage, ErrorMessage, GrantFilter);
      var pi = ProgressInfoFactory();
      de.ExportAll(outDirectory, pi, AllwaysPath, true, true);
    }

    private int MessageBoxForUUIDMismatch(string caption, string message, int buttons, int icon, int defaultButtons)
    {
      WriteMessage(caption);

      //hack aby w cli nie pokazywać pytań
      var messagelines = message.Split(new char[] { '\n' });
      var noquestions = messagelines.Where(ms => !ms.EndsWith("?"));
      message = noquestions.Aggregate((acc, e) => acc + e + "\n");

      WriteMessage(message);

      if (!IgnoreIdErrors)
      {
        WriteMessage("Przerwano pracę programu. Dodaj flagę -nouid aby zignorować ostrzeżenie");
        return (int) DialogResult.Cancel;
      }

      return (int) DialogResult.No;
    }

    internal void GenerateScript()
    {
      Console.WriteLine("Generowanie skryptu");
      var pi = ProgressInfoFactory();
      FilesystemExtractor fe = new FilesystemExtractor(WriteMessage, ErrorMessage);
      fe.GenerateScript(outDirectory, pi, ConnectionType.Remote, appiniAsInsert, true, TransferProcMode, alwaysGenerateHashes: false);
    }

    internal void Postprocess()
    {
      Console.WriteLine("Postprocessing skryptu różnicowego");
      var pi = ProgressInfoFactory();
      DifferentialFile.Postprocess(diffFile, pi, outDirectory);
    }

    internal void ExportScript()
    {
      if (string.IsNullOrWhiteSpace(Signature))
      {
        Console.WriteLine("Eksportowanie całej bazy");
        ExportallScript();
      }
      else
      {
        Console.WriteLine("Eksportowanie obiektów o sygnaturze " + Signature);
        ExportSignatureScript();
      }
    }

    public void Process()
    {
      if (Mode == CliTool.CliMode.GetInputUpdateScript)
      {
        GenerateInputPatchShScript();
      }
      else
        if (Mode == CliTool.CliMode.GetOutputUpdateScript)
      {
        GenerateOutputPatchShScript();
      }
      else if (Mode == CliTool.CliMode.ImportToDb)
      {
        DbSettings.Type = ConnectionType.Remote;
        Connection.Instance.SetConnection(ConnectionType.Remote, DbSettings);
        GenerateScript();
      }
      else if (Mode == CliMode.Postprocess)
      {
        Postprocess();
      }
      else if (Mode == CliMode.FastUpdate)
      {
        FastUpdate();
      }
      else if (Mode == CliMode.UpdateCRC)
      {
        UpdateCrc();
      }
      else
      {
        DbSettings.Type = ConnectionType.Source;
        Connection.Instance.SetConnection(ConnectionType.Source, DbSettings);
        ExportScript();
      }
    }

    private void UpdateCrc()
    {
      Console.WriteLine("Aktualizacja hashy wszystkich obiektów w bazie danych");

      DbSettings.Type = ConnectionType.Remote;
      Connection.Instance.SetConnection(ConnectionType.Remote, DbSettings);

      var pi = ProgressInfoFactory();
      try
      {
        MetadataChecksum.UpdateAllHashesInDatabase(ConnectionType.Remote, pi, WriteMessage, ErrorMessage, this.GrantFilter);
      }
      catch (Exception ex)
      {
        WriteMessage(ex.Message);
      }
    }

    private void FastUpdate()
    {
      Console.WriteLine("Generowanie skryptu szybkiej aktualizacji bazy docelowej");
      string fpath = Path.Combine(outDirectory, "shfastupdate.sql");

      DbSettings.Type = ConnectionType.Remote;
      Connection.Instance.SetConnection(ConnectionType.Remote, DbSettings);

      DatabaseExtractor de = new DatabaseExtractor(ConnectionType.Remote, WriteMessage, ErrorMessage, this.GrantFilter);
      string scriptContent = "";
      try
      {
        var pi = ProgressInfoFactory();
        scriptContent = de.FastUpdate(srcDir, pi, this.GrantFilter);
      }
      catch (Exception exp)
      {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(exp.Message);
        if (exp.InnerException != null)
          sb.AppendLine(exp.InnerException.Message);

        WriteMessage(sb.ToString());
        throw;
      }
      
      if (string.IsNullOrWhiteSpace(scriptContent))
      {
        Console.WriteLine("Nie utworzono pliku wynikowego " + fpath+ ". Skrypt był pusty");
      }
      else
      {
        File.WriteAllText(fpath, scriptContent);
        Console.WriteLine("Wygenerowano plik " + fpath);
      }   
    }

    public MetadataExtractor.TrMode TransferProcMode
    {
      get;
      set;
    }

    internal void GenerateInputPatchShScript()
    {
      Console.WriteLine("Generowanie skryptu patchującego bazę źródłową dla Signature Helpera");
      //MetadataExtractor me = new MetadataExtractor(DbSettings, null, WriteMessage, ErrorMessage);
      string fpath = Path.Combine(outDirectory, "shinputpatch.sql");
      File.WriteAllText(fpath, DatabasePatch.GetPatchForInputDatabase());
      Console.WriteLine("Wygenerowano plik " + fpath);
    }

    internal void GenerateOutputPatchShScript()
    {
      Console.WriteLine("Generowanie skryptu patchującego bazę docelową dla Signature Helpera");
      //MetadataExtractor me = new MetadataExtractor(null, DbSettings, WriteMessage, ErrorMessage);
      string fpath = Path.Combine(outDirectory, "shoutputpatch.sql");
      File.WriteAllText(fpath, DatabasePatch.GetPatchForOutputDatabase());
      Console.WriteLine("Wygenerowano plik " + fpath);
    }
  }
}
