﻿using NDesk.Options;
using SHlib.DatabaseUpdate;
using SHlib.DataExtraction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace shtool
{
  class Program
  {
    static int Main(string[] args)
    {
      Console.WriteLine("shtool.exe " + Assembly.GetExecutingAssembly().GetName().Version);
      CliTool cli = new CliTool();

      cli.outScriptName = "esystem.sql";
      cli.Mode = CliTool.CliMode.Undefined;

      bool showhelp = false;

      var opts = new OptionSet()
          {
            { "s|src=", "Ścieżka do skryptów", v => {
                cli.srcDir = v;
                cli.outDirectory = v;
                return;
              }
            },
            { "h|?|help", "Pomoc", v => showhelp = true },
            { "d|ddir=", "Docelowy folder skryptu wyjściowego", v => cli.outDirectory = v },
            //{ "o|out=", "Nazwa pliku wynikowego skryptu SQL", v => cli.outScriptName = v },
            { "dbhost=", "Adres serwera bazy danych", v => cli.DbSettings.ServerAddress = v },
            { "dbpath=", "Ścieżka bazy danych na serwerze", v => cli.DbSettings.DatabaseFilePath = v },
            { "dblogin=", "Login użytkownika bazy danych", v => cli.DbSettings.User = v },
            { "dbpass=", "Hasło użytkownika bazy danych", v => cli.DbSettings.Password = v },
            { "dbcp=", "Maksymalna liczba połączeń do bazy", v => cli.DbSettings.ConnectionPoolSize = v },
            { "dbrole=", "Rola użytkownika bazy danych", v => cli.DbSettings.Role = v },
            { "dbchar=", "Kodowanie bazy danych", v => cli.DbSettings.Encoding = v },
            { "uis|update-input-script", "Skrypt aktualizacyjny procedur SH do bazy źródłowej. Przydatne, jeżeli nie mamy SYSDBA do bazy z której wyciągamy obiekty. Wtedy trzeba ten skrypt wgrać jako SYSDBA osobno.", v => cli.Mode = CliTool.CliMode.GetInputUpdateScript },
            { "uos|update-output-script", "Skrypt aktualizacyjny SH do bazy docelowej. Przydatne, jeżeli nie mamy SYSDBA do bazy na którą wgrywamy obiekty. Wtedy trzeba ten skrypt wgrać jako SYSDBA osobno.", v => cli.Mode = CliTool.CliMode.GetOutputUpdateScript },
            { "au|allways-update", "Zawsze wgrywaj nowe wersje procedur SH", v => { cli.AllwaysPath = true; } },
            { "nai|noappiniinserts", "Nie emituj instertów do appini", v => cli.appiniAsInsert = false },
            { "gen|genscript", "Generuj skrypt z wszystkich obiektów na bazę", v => cli.Mode = CliTool.CliMode.ImportToDb },
            { "notr", "Nie generuj skryptów transferowych (np przy wgrywaniu na pustą bazę)", v => cli.TransferProcMode = MetadataExtractor.TrMode.NoTr},
            { "trb|trbefore", "Generuj tylko skrypty transferowe przed wgraniem skryptu różnicowego", v => cli.TransferProcMode = MetadataExtractor.TrMode.TrBefore},
            { "tra|trafter", "Generuj tylko skrypty transferowe po wgraniu skryptu różnicowego", v => cli.TransferProcMode = MetadataExtractor.TrMode.TrAfter},
            { "exp|export=", "Eksportuj pojedynczy temat", v => {
                cli.Mode = CliTool.CliMode.ExportSingleTopic;
                cli.Signature = v;
              }
            },
            { "p|postprocess=", "Postprocessing skrytpu różnicowego", v => { cli.diffFile = v; cli.Mode = CliTool.CliMode.Postprocess; } },
            { "expall|export-all", "Eksportuj całą bazę", v => cli.Mode = CliTool.CliMode.ExportAll  },
            { "nouid|ignore-db-uuid", "Ignoruj niezgodności między uuid repozytorium a bazą danych", v=> { cli.IgnoreIdErrors = true; DatabasePatch.IgnoreUUIDError=true; } },
            { "gf|grants-filter", "Filtruj granty - tylko dla standardowych użytkowników", v => cli.GrantFilter = true  },
            { "fu|fast-update", "Przeprowadź szybki proces aktualizacji bazy z użyciem kodów CRC", v => cli.Mode = CliTool.CliMode.FastUpdate },
            { "uch|update-crc-hashes", "Przelicz kody CRC obiektów w bazie danych", v => cli.Mode = CliTool.CliMode.UpdateCRC }
          };

      var res = opts.Parse(args);
      foreach (var r in res)
        Console.WriteLine(r);

      if (cli.TransferProcMode != MetadataExtractor.TrMode.Default)
        cli.Mode = CliTool.CliMode.ImportToDb;

      if (showhelp || cli.Mode == CliTool.CliMode.Undefined)
      {
        opts.WriteOptionDescriptions(Console.Out);
      }
      else
      {
        try
        {
          cli.Process();
        }
        catch (InvalidOperationException e)
        {
          Console.WriteLine(e.Message);
          return 1;
        }
      }
      Console.WriteLine("\nDone");
      return 0;
    }


  }
}
