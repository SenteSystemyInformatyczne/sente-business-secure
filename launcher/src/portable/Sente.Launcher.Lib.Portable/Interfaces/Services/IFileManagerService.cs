﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IFileManagerService
  {
    void CreatePathIfNotExists(Uri path);
    Task WriteFileAsync(Uri localPath, Stream stream);
    Task WriteFileAsync(Uri localPath, string content);
    Task CopyFile(Uri locationPath, Uri relativeFilePath, Uri localDirectoryBase, IProgress<int> progress);
    Task<uint> CalculateFileCRC32(Uri localDirectoryBase, Uri relativeFilePath);
    Task<ulong> CalculateFileSize(Uri localDirectoryBase, Uri relativeFilePath);
    Task<string> ReadTextStreamAsync(Stream textStream);
    Stream OpenFile(Uri localDirectoryBase, Uri relativeFilePath);
    bool FileExists(Uri localDirectoryBase, Uri cRCDataRelativePath);
    void DeleteFile(Uri locationPath, Uri relativeFilePath);
    IEnumerable<string> EnumerateDirectory(Uri path, IList<string> include = null, IList<string> exclude = null);
  }
}
