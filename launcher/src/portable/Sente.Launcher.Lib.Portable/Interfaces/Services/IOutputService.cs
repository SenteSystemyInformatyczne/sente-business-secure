﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IOutputService : IProgress<int>
  {
    void DisplayMessage(string message);
    void HandleException(Exception e, bool critical = true);
    void SetProgress(int value);
    void Done(bool success);
    void Debug(string message);
    void DisplayError(string message);
  }
}
