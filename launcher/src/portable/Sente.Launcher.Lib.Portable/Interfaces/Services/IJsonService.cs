﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IJsonService
  {
    Task<T> Parse<T>(Stream content);
  }
}
