﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IOperatingSystemInteractions
  {
    Process ExecuteProcess(Uri baseUri, Uri relativePath, string arguments);
  }
}
