﻿using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface ICRC32DataService<T> where T : FileDescriptor
  {
    Task<IEnumerable<T>> Parse(string content, Func<string, string, T> descriptorFactory, IProgress<int> progress);
    Task<string> Generate(IEnumerable<T> files, IProgress<int> progress);
  }
}
