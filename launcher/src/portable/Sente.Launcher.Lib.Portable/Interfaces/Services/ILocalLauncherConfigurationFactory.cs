﻿using Sente.Launcher.Lib.Portable.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface ILocalLauncherConfigurationFactory
  {
    LocalLauncherConfiguration GetConfiguration(string environmentArguments);
  }
}
