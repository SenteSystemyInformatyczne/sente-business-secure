﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IHttpDownloadService
  {
    Uri BaseUrl
    {
      get;
    }

    Task<JObject> DownloadJsonAsync(string relativeUrl, IProgress<int> progress, CancellationToken token);
    Task<MemoryStream> DownloadFileAsync(Uri url, IProgress<int> progress, CancellationToken token);
  }
}
