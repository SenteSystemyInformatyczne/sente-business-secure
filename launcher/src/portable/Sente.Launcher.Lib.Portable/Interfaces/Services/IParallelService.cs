﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
  public interface IParallelService<TIn, TOut>
  {
    Task<IEnumerable<TOut>> ParallelMap(IEnumerable<TIn> input, Func<TIn, TOut> mapFunction, IProgress<int> progress);
  }
}
