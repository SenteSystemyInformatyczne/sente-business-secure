﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Interfaces.Services
{
    public interface IRegexPatternMatcherService
    {
      IReadOnlyList<string> Patterns
      {
        get;
      }
      void Prepare(IEnumerable<string> patterns);
      void Prepare(params string[] patterns);
      bool IsMatch(string content);
    }
}
