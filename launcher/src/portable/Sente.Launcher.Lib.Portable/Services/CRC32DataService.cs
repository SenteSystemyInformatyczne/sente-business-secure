﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using System.Text;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class CRC32DataService<T> : ICRC32DataService<T> where T : FileDescriptor
  {
    /// <summary>
    /// Wstrzykiwana usługa do zrównoleglenia obliczeń
    /// </summary>
    public IParallelService<string, T> ParallelService
    {
      get;
      set;
    }

    private static readonly char[] NewLine = new char[] { '\n' };
    private static readonly char[] Space = new char[] { ' ' };

    public async Task<IEnumerable<T>> Parse(string content, Func<string, string, T> descriptorFactory, IProgress<int> progress)
    {
      var crc32List = content
        .Split(NewLine, StringSplitOptions.RemoveEmptyEntries);

      var remoteFileDescriptors = await ParallelService.ParallelMap(crc32List, (line) =>
      {
        string[] tokens;
        var quotePos = line.LastIndexOf('"');
        if (quotePos > -1)
        {
          var left = line.Substring(1, quotePos - 1).Trim();
          var right = line.Substring(quotePos + 1).Trim();
          tokens = new string[] { left, right };
        }
        else
          tokens = line.Split(Space, StringSplitOptions.RemoveEmptyEntries);

        if (tokens.Length != 2)
          throw new Exception($"Two tokens expected instead of {tokens.Length}");
        try
        {
          return descriptorFactory(tokens[0], tokens[1]);
        }
        catch (Exception e)
        {
          throw new InvalidOperationException("Error parsing CRC32 file on line \n" + line, e);
        }
      }, progress);

      return remoteFileDescriptors;
    }

    public async Task<string> Generate(IEnumerable<T> files, IProgress<int> progress)
    {
      return await Task.Run(() =>
      {
        var count = files.Count();
        var current = 0;
        int lastReported = 0;
        var content = files.OrderBy(f => Uri.UnescapeDataString(f.RelativeFileUrl.OriginalString)).Aggregate(new StringBuilder(), (acc, f) =>
        {
          if (progress != null)
          {
            var progressValue = (int) (++current * 100.0 / count) - lastReported;
            lastReported += progressValue;
            progress.Report(progressValue);
          }
          return acc.AppendLine($"\"{Uri.UnescapeDataString(f.RelativeFileUrl.OriginalString)}\" {f.CRC32:x2}");//{f.size} zerwie komp. wsteczna
          
        });

        return content.ToString();
      });
    }
  }
}
