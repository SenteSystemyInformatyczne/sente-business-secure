﻿using LightInject;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class FileManagerService : IFileManagerService
  {
    public IServiceContainer Container
    {

      get; set;
    }

    public async Task CopyFile(Uri srcLocationPath, Uri relativeFilePath, Uri localDirectoryBase, IProgress<int> progress)
    {
      var from = new Uri(srcLocationPath, relativeFilePath);
      var to = new Uri(localDirectoryBase, relativeFilePath);
      var tounescaped = Uri.UnescapeDataString(to.LocalPath);

      if (File.Exists(tounescaped))
        File.Delete(tounescaped);

      await CopyStream(from, to, progress);
    }

    private async Task CopyStream(Uri from, Uri to, IProgress<int> progress)
    {
      using (var fromStream = OpenFile(from, null))
      {
        var fileLength = fromStream.Length;
        var dirPath = new Uri(to, ".");
        var dirPathUnescaped = Uri.UnescapeDataString(dirPath.OriginalString);
        if (!Directory.Exists(dirPathUnescaped))
        {
          Directory.CreateDirectory(dirPathUnescaped);
        }
        var toUnescaped = Uri.UnescapeDataString(to.LocalPath);
        using (var toStream = File.Create(toUnescaped))
        {
          byte[] buffer = new byte[4096];
          long totalCopied = 0;
          int currentBlockSize = 0;
          int lastReported = 0;
          while ((currentBlockSize = fromStream.Read(buffer, 0, buffer.Length)) > 0)
          {
            totalCopied += currentBlockSize;
            int currentProgress = (int) ((totalCopied * 100.0) / fileLength) - lastReported;
            lastReported += currentProgress;
            toStream.Write(buffer, 0, currentBlockSize);
            progress.Report(currentProgress);
          }
        }
      }
    }

    public void CreatePathIfNotExists(Uri path)
    {
      Directory.CreateDirectory(Path.GetDirectoryName(Uri.UnescapeDataString(path.LocalPath)));
    }

    public async Task WriteFileAsync(Uri localPath, Stream stream)
    {
      using (var filestream = new FileStream((Uri.UnescapeDataString(localPath.LocalPath)), FileMode.Create, FileAccess.Write))
      {
        await stream.CopyToAsync(filestream);
      }
    }

    public IEnumerable<string> EnumerateDirectory(Uri path, IList<string> include = null, IList<string> exclude = null)
    {
      IRegexPatternMatcherService includeRegexSvc = null, excludeRegexSvc = null;
      IOutputService output = Container.GetInstance<IOutputService>();
      output.Debug(string.Format(Language.SEARCHING_FILES, path));

      var rootedPath = path;
      if (!path.IsAbsoluteUri)
      {
        rootedPath = new Uri(new Uri(Directory.GetCurrentDirectory(), UriKind.Absolute), path);
      }

      var files = Directory.EnumerateFiles(Uri.UnescapeDataString(path.LocalPath), "*.*", SearchOption.AllDirectories);

      PrintFileSummary(output, Language.FILE_SUMMARY_HEADER_ALL, files);

      var list = files
        .Select(pth =>
        {
          var pathuri = new Uri(pth, UriKind.Absolute);

          var tmp = rootedPath.MakeRelativeUri(pathuri);
          return tmp.OriginalString;
        });

      if (include != null && include.Count > 0)
      {
        includeRegexSvc = Container.GetInstance<IRegexPatternMatcherService>();
        includeRegexSvc.Prepare(include);
      }

      if (exclude != null && exclude.Count > 0)
      {
        excludeRegexSvc = Container.GetInstance<IRegexPatternMatcherService>();
        excludeRegexSvc.Prepare(exclude);
      }

      var res = list.AsParallel().Select(f =>
      new
      {
        filename = f,
        excluded = excludeRegexSvc?.IsMatch(f) ?? false,
        included = includeRegexSvc?.IsMatch(f) ?? true
      });

      var included = res.Where(r => r.included && !r.excluded).Select(f => f.filename).OrderBy(r => r);
      var excluded = res.Where(r => !r.included || r.excluded).Select(f => f.filename).OrderBy(r => r);

      PrintFileSummary(output, Language.FILE_SUMMARY_INCLUDED, included);
      PrintFileSummary(output, Language.FILE_SUMMARY_EXCLUDED, excluded);

      OutputFiles(output, included, excluded);

      return included;
    }

    private void PrintFileSummary(IOutputService output, string header, IEnumerable<string> files)
    {
      var includedSummaryByExt = files
        .Select(f => Path.GetExtension(f)).GroupBy(f => f)
        .Aggregate(new StringBuilder(), (acc, e) => 
        acc.AppendFormat(Language.FILE_SUMMARY_ROW+Environment.NewLine, e.Key, e.Count()), 
        acc => acc.ToString());
      output.DisplayMessage(string.Empty);
      output.DisplayMessage(header);
      output.DisplayMessage(includedSummaryByExt);
    }

    private void OutputFiles(IOutputService output, IEnumerable<string> included, IEnumerable<string> excluded)
    {
      var sb = new StringBuilder();
      sb.AppendLine(Language.INCLUDED_FILES);
      foreach (var f in included)
        sb.AppendLine(f);
      output.Debug(sb.ToString());
      sb.Clear();

      var excludedSummaryByExt = excluded.Select(f => Path.GetExtension(f)).GroupBy(f => f);
      sb.AppendLine(Language.EXCLUDED_FILES);
      foreach (var f in excluded)
        sb.AppendLine(f);
      output.Debug(sb.ToString());
    }

    public async Task<uint> CalculateFileCRC32(Uri localDirectoryBase, Uri relativeFilePath)
    {
      using (var fileStream = OpenFile(localDirectoryBase, relativeFilePath))
      using (var crcStream = new Crc32Stream(fileStream))
      {
        return await crcStream.CalculateAsync();
      }
    }

    public async Task<ulong> CalculateFileSize(Uri localDirectoryBase, Uri relativeFilePath)
    {
      var fileFullPath = Uri.UnescapeDataString(new Uri(localDirectoryBase, relativeFilePath).LocalPath);
      return (ulong) new FileInfo(fileFullPath).Length;
    }

    public async Task<string> ReadTextStreamAsync(Stream textStream)
    {
      using (var textReader = new StreamReader(textStream))
      {
        return await textReader.ReadToEndAsync();
      }
    }

    public Stream OpenFile(Uri localDirectoryBase, Uri relativeFilePath)
    {
      Uri fileFullPath = localDirectoryBase;
      if (relativeFilePath != null)
        fileFullPath = new Uri(fileFullPath, relativeFilePath);
      return File.OpenRead(Uri.UnescapeDataString(fileFullPath.LocalPath));
    }

    public bool FileExists(Uri localDirectoryBase, Uri relativeFilePath)
    {
      var fileFullPath = Uri.UnescapeDataString(new Uri(localDirectoryBase, relativeFilePath).LocalPath);
      return File.Exists(fileFullPath);
    }

    public async Task WriteFileAsync(Uri localPath, string content)
    {
      using (var contentStream = new MemoryStream(Encoding.UTF8.GetBytes(content)))
      {
        await WriteFileAsync(localPath, contentStream);
      }
    }

    public void DeleteFile(Uri locationPath, Uri relativeFilePath)
    {
      if (FileExists(locationPath, relativeFilePath))
      {
        var path = new Uri(locationPath, relativeFilePath);
        File.Delete(Uri.UnescapeDataString(path.LocalPath));
      }
    }
  }
}
