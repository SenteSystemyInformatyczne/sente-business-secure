﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class JsonService : IJsonService
  {
    private JsonSerializer serializer;

    public JsonService()
    {
      serializer = new JsonSerializer();
    }

    public async Task<T> Parse<T>(Stream content)
    {
      return await Task.Run(() =>
      {
        if (content != null)//bez tego niektóre testy nie przechodzą :]
        {
          using (var textReader = new StreamReader(content, Encoding.UTF8))
          using (var jsonReader = new JsonTextReader(textReader))
            return serializer.Deserialize<T>(jsonReader);
        }
        else
          return default(T);
      });
    }
  }
}
