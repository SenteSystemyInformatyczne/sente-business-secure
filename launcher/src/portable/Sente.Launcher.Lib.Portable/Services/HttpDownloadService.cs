﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class HttpDownloadService : IHttpDownloadService
  {
    HttpClient client;

    public Uri BaseUrl
    {
      get;
      private set;
    }

    public HttpDownloadService(Uri baseUrl, int timeoutSeconds = 600)
    {
      this.BaseUrl = baseUrl;
      client = new HttpClient();
      client.Timeout = TimeSpan.FromSeconds(timeoutSeconds);
    }

    public async Task<JObject> DownloadJsonAsync(string relativeUrl, IProgress<int> progress, CancellationToken token)
    {
      JObject result = null;
      using (var stream = await DownloadFileAsync(new Uri(BaseUrl, relativeUrl), progress, token))
      {
        using (var reader = new StreamReader(stream, Encoding.UTF8))
        {
          var jsonstring = await reader.ReadToEndAsync();
          result = (JObject) JsonConvert.DeserializeObject(jsonstring);
        }
      }
      return result;
    }

    public async Task<MemoryStream> DownloadFileAsync(Uri url, IProgress<int> progress, CancellationToken token)
    {
      url = new Uri(BaseUrl, url);

      var response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

      if (!response.IsSuccessStatusCode)
      {
        throw new Exception(string.Format("The request {1} returned with HTTP status code {0}", response.StatusCode, url));
      }

      var total = response.Content.Headers.ContentLength.HasValue ? response.Content.Headers.ContentLength.Value : -1L;
      var canReportProgress = total != -1 && progress != null;
      var memoryStream = new MemoryStream();
      using (var stream = await response.Content.ReadAsStreamAsync())
      {
        var totalRead = 0;
        var buffer = new byte[4096];
        var isMoreToRead = true;
        int lastReported = 0;
        do
        {
          token.ThrowIfCancellationRequested();

          var read = await stream.ReadAsync(buffer, 0, buffer.Length, token);

          if (read == 0)
          {
            isMoreToRead = false;
          }
          else
          {
            await memoryStream.WriteAsync(buffer, 0, read);

            totalRead += read;

            if (canReportProgress)
            {
              var progressValue = (totalRead * 100.0 / total) - lastReported;
              lastReported += (int) progressValue;
              progress.Report((int) progressValue);
            }
          }
        } while (isMoreToRead);
      }

      if (memoryStream.Position > 0)
        memoryStream.Position = 0;

      return memoryStream;
    }
  }
}
