﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class RegexPatternMatcherService : IRegexPatternMatcherService
  {
    protected Regex PatternRegex;

    public IReadOnlyList<string> Patterns
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public bool IsMatch(string content)
    {
      if (PatternRegex == null)
        throw new ArgumentNullException();
      var res = PatternRegex.IsMatch(content);
      return res;
    }

    public void Prepare(params string[] patterns)
    {
      Prepare(patterns.AsEnumerable());
    }

    public void Prepare(IEnumerable<string> patterns)
    {
      var pattern = new StringBuilder();
      
      bool first = true;
      foreach (var pat in patterns)
      {      
        if (!first)
          pattern.Append('|');
        pattern.Append('^');
        if (pat.StartsWith("`"))
          pattern.Append(pat);
        else
          pattern.Append(WildcardToRegexp(pat));
        first = false;
        pattern.Append('$');
      }
      
      PatternRegex = new Regex(pattern.ToString(), RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled);
    }

    const string DotTempSub = "[[.]]";
    const string StarTempSub = "[[*]]";
    const string QuestTempSub = "[[?]]";
    const string AnyDirectoryPathSeparator = @"[\\\/]";
    const string NotDirectoryPathSeparator = @"[^\\\/]";
    const string NotDirectoryAndFileExtSeparator = @"[^\\\/.]";
    const string FileExtSeparator = @"\.";
    const string IdentFollowedByDirectorySeparator = "(" + NotDirectoryPathSeparator + "+" + AnyDirectoryPathSeparator + ")";

    private string WildcardToRegexp(string pat)
    {
      return pat
        .Replace(".", DotTempSub)
        .Replace("*", StarTempSub)
        .Replace("?", QuestTempSub)
        .Replace(@"\", AnyDirectoryPathSeparator)
        .Replace("/", AnyDirectoryPathSeparator)
        .Replace($"{StarTempSub}{StarTempSub}{AnyDirectoryPathSeparator}", $"{IdentFollowedByDirectorySeparator}+")
        .Replace($"{StarTempSub}{AnyDirectoryPathSeparator}", IdentFollowedByDirectorySeparator)
        .Replace($"{StarTempSub}{DotTempSub}", $"({NotDirectoryPathSeparator}+{AnyDirectoryPathSeparator})*({NotDirectoryAndFileExtSeparator}*{FileExtSeparator})+")
        .Replace(StarTempSub, $"({NotDirectoryPathSeparator}*)")
        .Replace(QuestTempSub, $"({NotDirectoryPathSeparator})")
        .Replace(DotTempSub, FileExtSeparator);
    }
  }
}
