﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using System;
using System.Text;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class ConsoleOutputService : OutputServiceBase, IOutputService
  {
    const int ConsoleMaxWidth = 80;
    int lastReportWidth;
    int progress = 0;

    public bool Verbose
    {
      get;
      set;
    }

    public override void Debug(string message)
    {
      if (Verbose)
        DisplayMessage(string.Format(Language.VERBOSE, message));
    }

    public override void DisplayMessage(string message)
    {
      if (message.Length < lastReportWidth)
        message = message.PadRight(lastReportWidth);

      Console.WriteLine(message);

      lastReportWidth = 0;
    }

    public override void Done(bool success)
    {
      Environment.Exit(success ? 0 : 1);
    }

    public override void HandleException(Exception e, bool critical = true)
    {
      //w konsoli chcemy wypisać pełne szczegóły błędu
      StringBuilder sb = new StringBuilder();

      FormatException(e, sb);

      DisplayMessage(sb.ToString());
    }

    public override void Report(int value)
    {
      progress += value;
      var message = $"{progress} %";

      if (message.Length > ConsoleMaxWidth - 1)
        message = message.Substring(0, ConsoleMaxWidth - 4) + "...";

      var prevReportWidth = lastReportWidth;
      lastReportWidth = message.Length+1;

      if (message.Length < lastReportWidth)
        message = message.PadRight(lastReportWidth);

      Console.Write($"{message}\r");
    }

    public override void SetProgress(int value)
    {
      progress = value;
    }

    /// <summary>
    /// Metoda wyświetla informację o błedzie. 
    /// </summary>
    /// <param name="message">treść komunikatu.</param>
    public override void DisplayError(string message)
    {
      DisplayMessage(message);
    }
  }
}
