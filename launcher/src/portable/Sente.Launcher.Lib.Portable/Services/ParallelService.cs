﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public enum ParallelServiceSplitMethod
  {
    Simple,
    Continous
  }

  public class ParallelService<TIn, TOut> : IParallelService<TIn, TOut>
  {
    public int Workers
    {
      get;
      set;
    }

    public ParallelServiceSplitMethod SplitMethod
    {
      get;
      set;
    }

    public ParallelService()
    {
      Workers = Environment.ProcessorCount;
      SplitMethod = ParallelServiceSplitMethod.Simple;
    }

    internal IEnumerable<IEnumerable<TIn>> Partition(IEnumerable<TIn> input, int numberOfParts)
    {
      if (SplitMethod == ParallelServiceSplitMethod.Simple)
      {
        return input.Select((elem, idx) => new
        {
          element = elem,
          index = idx
        }).GroupBy(e => e.index % numberOfParts, res => res.element);
      }
      else
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<TOut>> ParallelMap(IEnumerable<TIn> input, Func<TIn, TOut> mapFunction, IProgress<int> progress)
    {
      List<Task<IEnumerable<TOut>>> workerList = new List<Task<IEnumerable<TOut>>>();

      var inputPartition = Partition(input, Workers);
      var count = input.Count();
      var current = 0;
      int lastReported = 0;

      foreach (var part in inputPartition)
        workerList.Add(Task.Run(() =>
        {
          List<TOut> partResult = new List<TOut>();
          foreach (var item in part)
          {
            partResult.Add(mapFunction(item));
            if (progress != null)
            {
              var currentVal = System.Threading.Interlocked.Increment(ref current);
              var currentProgress = (int) (currentVal * 100.0 / count) - lastReported;
              lastReported += currentProgress;
              progress?.Report(currentProgress);
            }
          }
          return partResult.AsEnumerable();
        }));

      await Task.WhenAll(workerList);

      var result = Enumerable.Empty<TOut>();
      foreach (var task in workerList)
        result = result.Concat(task.Result);

      return result;
    }
  }
}
