﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public class OperatingSystemInteractions : IOperatingSystemInteractions
  {
    public Process ExecuteProcess(Uri baseUri, Uri relativePath, string arguments)
    {
      var uri = new Uri(baseUri, relativePath);
      ProcessStartInfo psi = new ProcessStartInfo(Uri.UnescapeDataString(uri.LocalPath), arguments);
      psi.UseShellExecute = false;
      psi.WorkingDirectory = Uri.UnescapeDataString(baseUri.LocalPath);
      return Process.Start(psi);
    }
  }
}
