﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  public abstract class OutputServiceBase : IOutputService
  {
    public abstract void Debug(string message);
    public abstract void DisplayMessage(string message);

    public abstract void Done(bool success);

    public abstract void HandleException(Exception e, bool critical = true);

    public abstract void Report(int value);

    public abstract void SetProgress(int value);

    public abstract void DisplayError(string message);

    protected void FormatException(Exception e, StringBuilder sb)
    {
      sb.AppendFormat($"{e.GetType().Name}: {e.Message}\n");
      if (e.Data != null && e.Data.Count > 0)
      {
        sb.AppendLine("Additional data:");
        foreach (var key in e.Data.Keys)
          sb.AppendLine($"{key}: {e.Data[key]}");
      }

      sb.AppendLine("Full stacktrace:");
      sb.AppendLine(e.StackTrace);

      if (e.InnerException != null)
      {
        sb.AppendLine("Inner exception:");
        FormatException(e.InnerException, sb);
      }
    }
  }
}
