﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Services
{
  /// <summary>
  /// Strumień który zapisywane dane ignoruje, odpowiednik /dev/null
  /// </summary>
  public class DevNullStream : Stream
  {
    public override bool CanRead
    {
      get
      {
        return false;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return true;
      }
    }

    public override long Length
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    long position = 0;
    public override long Position
    {
      get
      {
        return position;
      }

      set
      {
        throw new NotImplementedException();
      }
    }

    public override void Flush()
    {
      throw new NotImplementedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }

    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      //do nothing.
      position += count;
    }

    public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
    {
      //also do nothing
      position += count;
      return Task.FromResult<object>(null);
    }
  }
}
