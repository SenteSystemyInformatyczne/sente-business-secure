﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Sente.Launcher.Lib.Portable.Services
{
  /// <summary>
  /// Strumień, który umie policzyć CRC32 z danych z innego strumienia
  /// </summary>
  public class Crc32Stream : Stream, IDisposable
  {
    static uint[] vector;

    public Stream UnderlyingStream
    {
      get;
      private set;
    }

    private uint? crc32;
    public uint? CRC32
    {
      get
      {
        return ~crc32;
      }
    }

    public async Task<uint> CalculateAsync()
    {
      var devNull = new DevNullStream();
      await this.CopyToAsync(devNull);
      return CRC32.Value;
    }

    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    public uint Calculate()
    {
      var devNull = new DevNullStream();
      this.CopyTo(devNull);
      return CRC32.Value;
    }

    public override bool CanTimeout
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return false;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override long Length
    {
      get
      {
        return UnderlyingStream.Length;
      }
    }

    public override long Position
    {
      get
      {
        return UnderlyingStream.Position;
      }

      set
      {
        if (value != UnderlyingStream.Position)
          UnderlyingStream.Position = value;
      }
    }

    public void ComputeChecksumChunk(byte[] bytes, long count)
    {
      crc32 = crc32 ?? 0xffffffffU;
      for (int i = 0; i < count; ++i)
      {
        byte index = (byte) (((crc32) & 0xff) ^ bytes[i]);
        crc32 = (uint) ((crc32 >> 8) ^ vector[index]);
      } 
    }

    public override void Flush()
    {
      throw new NotImplementedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      var bytes = UnderlyingStream.Read(buffer, offset, count);
      ComputeChecksumChunk(buffer, bytes);
      return bytes;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }

    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }

    public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
    {
      var task = UnderlyingStream.ReadAsync(buffer, offset, count, cancellationToken);

      var newtask = task.ContinueWith(ti =>
      {
        if(!ti.IsFaulted)
          ComputeChecksumChunk(buffer, ti.Result);
        return ti.Result;
      });

      return newtask;
    }

    public Crc32Stream(Stream underlyingStream)
    {
      this.UnderlyingStream = underlyingStream;
    }

    static Crc32Stream()
    {
      uint poly = 0xedb88320;
      vector = new uint[256];
      uint temp = 0;
      for (uint i = 0; i < vector.Length; ++i)
      {
        temp = i;
        for (int j = 8; j > 0; --j)
        {
          if ((temp & 1) == 1)
          {
            temp = (uint) ((temp >> 1) ^ poly);
          }
          else
          {
            temp >>= 1;
          }
        }
        vector[i] = temp;
      }
    }
  }
}
