﻿using Sente.Launcher.Lib.Portable.Commands;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Sente.Launcher.Lib.Portable.Models
{
  public class ExecutionContext
  {
    public ExecutionContext()
    {
      CommandQueue = new CommandQueue();
    }

    public void AppendCommand(LauncherCommandBase command)
    {
      CommandQueue.AppendCommand(command);
    }

    public IEnumerable<Tuple<LocalFileDescriptor, RemoteFileDescriptor>> FilesToUpdate
    {
      get;
      private set;
    }

    public void FindFilesToUpdate()
    {
      var tmp = RemoteFiles
       .GroupJoin(LocalFiles,
         r => Uri.UnescapeDataString(r.RelativeFileUrl.OriginalString),
         l => Uri.UnescapeDataString(l.RelativeFileUrl.OriginalString),
         (r, l) => new
         {
           local = l,
           remote = r
         }
         );

      FilesToUpdate = tmp.SelectMany(x => x.local.DefaultIfEmpty(),
         (r, l) => new Tuple<LocalFileDescriptor, RemoteFileDescriptor>(
           l == null ? new LocalFileDescriptor(r.remote.RelativeFileUrl) : l,
           r.remote));
    }

    public bool TryGetCommand(out LauncherCommandBase command)
    {
      return CommandQueue.TryGetCommand(out command);
    }

    public CommandQueue CommandQueue
    {
      get;
      private set;
    }

    public List<LocalFileDescriptor> LocalFiles
    {
      get;
      internal set;
    } = new List<LocalFileDescriptor>();

    public List<RemoteFileDescriptor> RemoteFiles
    {
      get;
      internal set;
    } = new List<RemoteFileDescriptor>();

    public Uri TempLocationPath
    {
      get;
      internal set;
    }

    public Uri LocalDirectoryBase
    {
      get;
      internal set;
    }

    public Uri BaseRepositoryUrl
    {
      get;
      internal set;
    }

    public CancellationToken AbortAllCancellationToken
    {
      get;
      internal set;
    }

    public IProgress<int> CurrentProgress
    {
      get;
      internal set;
    }

    public RemoteLauncherConfiguration RemoteConfiguration
    {
      get;
      internal set;
    }
    public Uri RemoteConfigurationUrl
    {
      get;
      internal set;
    }
    public Uri LocalCRCDataFilePath
    {
      get;
      internal set;
    }

    public Uri ApplicationToExecute
    {
      get;
      internal set;
    }
    public string ApplicationArguments
    {
      get;
      internal set;
    }
    public List<string> IncludeFilePatterns
    {
      get;
      internal set;
    }
  }
}
