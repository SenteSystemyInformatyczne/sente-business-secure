﻿using System;

namespace Sente.Launcher.Lib.Portable.Model.FileDescriptors
{
  public class FileDescriptor
  {
    public uint? CRC32
    {
      get;
      internal set;
    }

    public ulong? Size
    {
      get;
      internal set;
    }

    public Uri RelativeFileUrl
    {
      get;
      private set;
    }

    protected FileDescriptor(Uri relativeFileUrl, uint? crc32)
    {
      RelativeFileUrl = relativeFileUrl;
      CRC32 = crc32;
    }

    public override int GetHashCode()
    {
      return RelativeFileUrl.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;
      if (ReferenceEquals(this, obj))
        return true;
      if (obj is FileDescriptor)
      {
        var fd = (FileDescriptor) obj;
        return fd.RelativeFileUrl.Equals(this.RelativeFileUrl);
      }
      return false;
    }

    public override string ToString()
    {
      return Uri.UnescapeDataString(RelativeFileUrl.OriginalString) + " " + CRC32;
    }
  }
}
