﻿using System;

namespace Sente.Launcher.Lib.Portable.Model.FileDescriptors
{
  public class RemoteFileDescriptor : FileDescriptor
  {
    public RemoteFileDescriptor(Uri relativeFileUrl, uint crc32) : base(relativeFileUrl, crc32)
    {
    }
  }
}
