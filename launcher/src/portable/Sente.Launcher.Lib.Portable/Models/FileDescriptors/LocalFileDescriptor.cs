﻿using System;

namespace Sente.Launcher.Lib.Portable.Model.FileDescriptors
{
  public class LocalFileDescriptor : FileDescriptor
  {
    public LocalFileDescriptor(Uri relativePath, uint crc32) : base(relativePath, crc32)
    {
    }

    public LocalFileDescriptor(Uri relativePath) : base(relativePath, null) { }
  }
}
