﻿using Sente.Launcher.Lib.Portable.Commands;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Models
{
  public class CommandQueue
  {
    readonly ConcurrentQueue<LauncherCommandBase> queue = new ConcurrentQueue<LauncherCommandBase>();

    public void AppendCommand(LauncherCommandBase command)
    {
      queue.Enqueue(command);
    }

    public bool TryGetCommand(out LauncherCommandBase command)
    {
      command = null;
      while (queue.Count > 0
       && !queue.TryDequeue(out command))
        ;
      return command != null;
    }
  }
}
