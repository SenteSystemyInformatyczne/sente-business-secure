﻿using System;
using Sente.Launcher.Lib.Portable.Commands;

namespace Sente.Launcher.Lib.Portable.Models.Configurations
{
  public class ReplaceFileConfiguration
  {
    public string Mode
    {
      get;
      set;
    }

    public ReplaceFileMode ReplaceMode
    {
      get
      {
        return Parse(Mode);
      }
    }

    private ReplaceFileMode Parse(string mode)
    {
      if(string.IsNullOrWhiteSpace(mode))
        return ReplaceFileMode.WhenLocalAndRemoteCRC32DoesNotMatch;

      switch (mode.ToLower())
      {
        case "always":
          return ReplaceFileMode.Always;
        case "diff":
          return ReplaceFileMode.WhenLocalAndRemoteCRC32DoesNotMatch;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    public string RelativeFilePath
    {
      get;
      set;
    }
  }
}
