﻿namespace Sente.Launcher.Lib.Portable.Models.Configurations
{
  public enum LauncherConfigurationMode
  {
    None,
    LauncherApp,
    ManagerApp
  }
  public class LocalLauncherConfiguration
  {
    public string RemoteLauncherConfigurationUrl
    {
      get;
      set;
    }

    public int DownloadTimeout
    {
      get;
      set;
    }

    private string _targetDirUrl;

    public string OutputFile
    {
      get;
      set;
    }

    public string Include
    {
      get;
      set;
    }

    public string TargetDirUrl
    {
      get
      {
        return _targetDirUrl;
      }
      set
      {
        if (string.IsNullOrWhiteSpace(value))
          value = "./";
        else if (!value.EndsWith("/"))
          value += "/";
        _targetDirUrl = value;
      }
    }

    public string ApplicationToExecute
    {
      get;
      set;
    }

    public string ApplicationArguments
    {
      get;
      set;
    }

    public LauncherConfigurationMode Mode
    {
      get;
      set;
    }

    public bool Verbose
    {
      get;
      set;
    }
    public string IncludeAlso
    {
      get;
      set;
    }
  }
}
