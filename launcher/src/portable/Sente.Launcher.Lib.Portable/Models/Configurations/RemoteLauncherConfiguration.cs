﻿using System.Collections.Generic;
namespace Sente.Launcher.Lib.Portable.Models.Configurations
{
  public class RemoteLauncherConfiguration
  {
    public string BaseRepositoryUrl
    {
      get;
      set;
    }

    public string RelativeCRC32DataFileUrl
    {
      get;
      set;
    }

    public IList<ReplaceFileConfiguration> Commands
    {
      get;
      set;
    }
  }
}
