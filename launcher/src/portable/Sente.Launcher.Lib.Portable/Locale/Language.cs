﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Locale
{
  public class Language
  {
    private static ResourceManager rm;
    private static CultureInfo ci = CultureInfo.CurrentUICulture;

    internal static ResourceManager ResourceManager
    {
      get
      {
        if (rm == null)
        {
          var assembly = typeof(Language).GetTypeInfo().Assembly;
          rm = new ResourceManager($"{assembly.GetName().Name}.Locale.Language", assembly);
        }
        return rm;
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Bulk executing {0} commands of.
    /// </summary>
    internal static string BULK_EXECUTING_OF_COMMANDS
    {
      get
      {
        return ResourceManager.GetString("BULK_EXECUTING_OF_COMMANDS", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Calculating CRC32 of.
    /// </summary>
    internal static string CALCULATING_CRC_OF
    {
      get
      {
        return ResourceManager.GetString("CALCULATING_CRC_OF", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Checking local CRC32 data.
    /// </summary>
    internal static string CHECKING_LOCAL_CRC
    {
      get
      {
        return ResourceManager.GetString("CHECKING_LOCAL_CRC", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Comparing CRC32 file values.
    /// </summary>
    internal static string COMPARING_CRC
    {
      get
      {
        return ResourceManager.GetString("COMPARING_CRC", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Deleting file {0}.
    /// </summary>
    internal static string DELETING_FILE
    {
      get
      {
        return ResourceManager.GetString("DELETING_FILE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Deleting temporary file {0}.
    /// </summary>
    internal static string DELETING_TEMP_FILE
    {
      get
      {
        return ResourceManager.GetString("DELETING_TEMP_FILE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Downloading remote configuration.
    /// </summary>
    internal static string DOWNLOADING_CONFIG
    {
      get
      {
        return ResourceManager.GetString("DOWNLOADING_CONFIG", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Downloading CRC32 remote data.
    /// </summary>
    internal static string DOWNLOADING_CRC_DATA_REMOTE
    {
      get
      {
        return ResourceManager.GetString("DOWNLOADING_CRC_DATA_REMOTE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Downloading file.
    /// </summary>
    internal static string DOWNLOADING_FILE
    {
      get
      {
        return ResourceManager.GetString("DOWNLOADING_FILE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Downloading resource.
    /// </summary>
    internal static string DOWNLOADING_RESOURCE
    {
      get
      {
        return ResourceManager.GetString("DOWNLOADING_RESOURCE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Generating CRC32 data from files in.
    /// </summary>
    internal static string GENERATING_FILE_CRC
    {
      get
      {
        return ResourceManager.GetString("GENERATING_FILE_CRC", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Loading local CRC32 data file.
    /// </summary>
    internal static string LOADING_CRC_FILE
    {
      get
      {
        return ResourceManager.GetString("LOADING_CRC_FILE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Saving local CRC32 data file.
    /// </summary>
    internal static string SAVE_LOCAL_CRC
    {
      get
      {
        return ResourceManager.GetString("SAVE_LOCAL_CRC", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to sequentially.
    /// </summary>
    internal static string SEQUENTIALLY
    {
      get
      {
        return ResourceManager.GetString("SEQUENTIALLY", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to simultaneously.
    /// </summary>
    internal static string SIMULTANEOUSLY
    {
      get
      {
        return ResourceManager.GetString("SIMULTANEOUSLY", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Unknown mode.
    /// </summary>
    internal static string UNKNOWN_MODE
    {
      get
      {
        return ResourceManager.GetString("UNKNOWN_MODE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Updating file.
    /// </summary>
    internal static string UPDATING_FILE
    {
      get
      {
        return ResourceManager.GetString("UPDATING_FILE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Done with errors.
    /// </summary>
    internal static string DONE_WITH_ERRORS
    {
      get
      {
        return ResourceManager.GetString("DONE_WITH_ERRORS", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Done.
    /// </summary>
    internal static string DONE
    {
      get
      {
        return ResourceManager.GetString("DONE", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Downloading {0} files.
    /// </summary>
    internal static string DOWNLOADING_FILES
    {
      get
      {
        return ResourceManager.GetString("DOWNLOADING_FILES", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Updating {0} files.
    /// </summary>
    internal static string UPDATING_FILES
    {
      get
      {
        return ResourceManager.GetString("UPDATING_FILES", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Removing temp files.
    /// </summary>
    internal static string REMOVING_TEMP_FILES
    {
      get
      {
        return ResourceManager.GetString("REMOVING_TEMP_FILES", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Calculating crc of {0} files.
    /// </summary>
    internal static string CALCULATING_CRC_OF_ALL
    {
      get
      {
        return ResourceManager.GetString("CALCULATING_CRC_OF_ALL", ci);
      }
    }

    /// <summary>
    ///   Looks up a localized string similar to Launching main app.
    /// </summary>
    internal static string LAUNCHING_APP
    {
      get
      {
        return ResourceManager.GetString("LAUNCHING_APP", ci);
      }
    }

    /// <summary>
    ///   Update error!!! Please contact to admin.
    /// </summary>
    internal static string UPDATE_BUG_MESSAGE
    {
      get
      {
        return ResourceManager.GetString("UPDATE_BUG_MESSAGE", ci);
      }
    }

    /// <summary>
    ///   Exe not found: {0}
    /// </summary>
    internal static string FILEEXE_NOTFOUND
    {
      get
      {
        return ResourceManager.GetString("FILEEXE_NOTFOUND", ci);
      }
    }

    /// <summary>
    ///  Including {0} distinct patterns from file {1}
    /// </summary>
    internal static string INCLUDE_DISTINCT_PATTERNS
    {
      get
      {
        return ResourceManager.GetString("INCLUDE_DISTINCT_PATTERNS", ci);
      }
    }

    /// <summary>
    ///   file {0} not found.
    /// </summary>
    internal static string FILE_NOT_FOUND
    {
      get
      {
        return ResourceManager.GetString("FILE_NOT_FOUND", ci);
      }
    }

    /// <summary>
    ///   Including additional {0} distinct patterns from file {1}
    /// </summary>
    internal static string INCLUDE_ALSO_DISTINCT_PATTERNS
    {
      get
      {
        return ResourceManager.GetString("INCLUDE_ALSO_DISTINCT_PATTERNS", ci);
      }
    }

    /// <summary>
    ///   Using {0} distinct patterns in total
    /// </summary>
    internal static string INCLUDE_SUMMARY
    {
      get
      {
        return ResourceManager.GetString("INCLUDE_SUMMARY", ci);
      }
    }

    /// <summary>
    ///   IncludeAlso: cannot be specified without -Include switch
    /// </summary>
    internal static string INCLUDE_ALSO_ERROR
    {
      get
      {
        return ResourceManager.GetString("INCLUDE_ALSO_ERROR", ci);
      }
    }

    /// <summary>
    ///   Verbose: 
    /// </summary>
    internal static string VERBOSE
    {
      get
      {
        return ResourceManager.GetString("VERBOSE", ci);
      }
    }

    /// <summary>
    ///   {1}: Processing command {0}
    /// </summary>
    internal static string PROCESSING_COMMAND_SIMULT
    {
      get
      {
        return ResourceManager.GetString("PROCESSING_COMMAND_SIMULT", ci);
      }
    }

    /// <summary>
    ///   {1}: ERROR during execution of command {0}
    /// </summary>
    internal static string PROCESSING_COMMAND_SIMULT_ERR
    {
      get
      {
        return ResourceManager.GetString("PROCESSING_COMMAND_SIMULT_ERR", ci);
      }
    }

    /// <summary>
    ///   Total files summary:
    /// </summary>
    internal static string FILE_SUMMARY_HEADER_ALL
    {
      get
      {
        return ResourceManager.GetString("FILE_SUMMARY_HEADER_ALL", ci);
      }
    }

    /// <summary>
    ///  Included files summary:
    /// </summary>
    internal static string FILE_SUMMARY_INCLUDED
    {
      get
      {
        return ResourceManager.GetString("FILE_SUMMARY_INCLUDED", ci);
      }
    }

    /// <summary>
    ///   Excluded files summary:
    /// </summary>
    internal static string FILE_SUMMARY_EXCLUDED
    {
      get
      {
        return ResourceManager.GetString("FILE_SUMMARY_EXCLUDED", ci);
      }
    }

    /// <summary>
    ///    * {0} - {1}
    /// </summary>
    internal static string FILE_SUMMARY_ROW
    {
      get
      {
        return ResourceManager.GetString("FILE_SUMMARY_ROW", ci);
      }
    }

    /// <summary>
    ///  Searching files in {0}...
    /// </summary>
    internal static string SEARCHING_FILES
    {
      get
      {
        return ResourceManager.GetString("SEARCHING_FILES", ci);
      }
    }

    /// <summary>
    ///  Files included:
    /// </summary>
    internal static string INCLUDED_FILES
    {
      get
      {
        return ResourceManager.GetString("INCLUDED_FILES", ci);
      }
    }

    /// <summary>
    ///  Files excluded:
    /// </summary>
    internal static string EXCLUDED_FILES
    {
      get
      {
        return ResourceManager.GetString("EXCLUDED_FILES", ci);
      }
    }
  }
}
