﻿using LightInject;
using Sente.Launcher.Lib.Portable.Commands;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable
{
  public class Engine
  {
    public const string CRC32DataFileName = "files.crc";
    public readonly string CRC32DataFileNameExcludePattern = "*.crc";

    LocalLauncherConfiguration options;
    IOutputService outputService;
    IServiceContainer container;

    public Engine(
      IServiceContainer container,
      LocalLauncherConfiguration localOptions,
      IOutputService outputService)
    {
      this.container = container;
      options = localOptions;
      this.outputService = outputService;
    }

    public async Task Run()
    {
      var executionContext = new ExecutionContext();
      try
      {
        executionContext.CurrentProgress = outputService;
        FillLocalConfiguration(executionContext);
        QueueInitialCommands(executionContext);

        LauncherCommandBase currentCommand;
        while (executionContext.TryGetCommand(out currentCommand))
        {
          outputService.DisplayMessage(currentCommand.Name + "...");
          outputService.SetProgress(0);
          await currentCommand.ExecuteAsync(executionContext);
          outputService.SetProgress(100);
        }

        outputService.DisplayMessage(Language.DONE);
        outputService.Done(true);
      }
      catch(ArgumentException ae)
      {
        outputService.DisplayError(ae.Message);
        HandleError(executionContext);
      }
      catch (Exception e)
      {
        outputService.HandleException(e);
        HandleError(executionContext);
      }
    }

    private void HandleError(ExecutionContext executionContext)
    {
      RunApplicationAfterError(executionContext);
      outputService.DisplayMessage(Language.DONE_WITH_ERRORS);
      outputService.Done(false);
    }

    private void FillLocalConfiguration(ExecutionContext executionContext)
    {
      var dirbase = new Uri(options.TargetDirUrl ?? ".", UriKind.RelativeOrAbsolute);
      if (!dirbase.IsAbsoluteUri)
        dirbase = new Uri(new Uri(Directory.GetCurrentDirectory() + "/", UriKind.Absolute), dirbase);

      executionContext.TempLocationPath = new Uri(dirbase, new Uri("temp/", UriKind.Relative));
      outputService.Debug(executionContext.TempLocationPath.ToString());
      outputService.Debug(dirbase.ToString());
      executionContext.LocalDirectoryBase = dirbase;
      executionContext.LocalCRCDataFilePath = new Uri(options.OutputFile, UriKind.Relative);
      if (!string.IsNullOrWhiteSpace(options.ApplicationToExecute))
        executionContext.ApplicationToExecute = new Uri(options.ApplicationToExecute, UriKind.Relative);
      executionContext.ApplicationArguments = options.ApplicationArguments;

      if (!string.IsNullOrWhiteSpace(options.RemoteLauncherConfigurationUrl))
      {
        executionContext.RemoteConfigurationUrl =
          new Uri(options.RemoteLauncherConfigurationUrl, UriKind.Absolute);
      }

      if (!string.IsNullOrWhiteSpace(options.Include))
      {
        HandleIncludePatterns(executionContext);
      }
    }

    private void HandleIncludePatterns(ExecutionContext executionContext)
    {
      bool includeAlsoPresent = !string.IsNullOrWhiteSpace(options.Include);
      if (!string.IsNullOrWhiteSpace(options.Include))
      {
        if (File.Exists(options.Include))
        {
          executionContext.IncludeFilePatterns = File.ReadAllLines(options.Include).Distinct().ToList();
          outputService.Debug(string.Format(Language.INCLUDE_DISTINCT_PATTERNS,
            executionContext.IncludeFilePatterns.Count, options.Include));
        }
        else
        {
          FileInfo fi = new FileInfo(options.Include);
          throw new ArgumentException(string.Format("Include: "+Language.FILE_NOT_FOUND, fi.FullName));
        }

        if (!string.IsNullOrWhiteSpace(options.IncludeAlso))
        {
          if (File.Exists(options.IncludeAlso))
          {
            var additionalFilePatterns = File.ReadAllLines(options.IncludeAlso).Distinct().ToList();
            outputService.Debug(string.Format(Language.INCLUDE_DISTINCT_PATTERNS, additionalFilePatterns, options.Include));
            if (executionContext.IncludeFilePatterns == null)
              executionContext.IncludeFilePatterns = additionalFilePatterns;
            else
              executionContext.IncludeFilePatterns = executionContext.IncludeFilePatterns
                .Concat(additionalFilePatterns).Distinct().ToList();
            outputService.Debug(string.Format(Language.INCLUDE_ALSO_DISTINCT_PATTERNS, 
              executionContext.IncludeFilePatterns.Count, options.IncludeAlso));
          }
          else
          {
            FileInfo fi = new FileInfo(options.IncludeAlso);
            throw new ArgumentException(string.Format("IncludeAlso: "+Language.FILE_NOT_FOUND, fi.FullName));
          }
        }
      }
      else
      {
        if (includeAlsoPresent)
          outputService.DisplayError(string.Format(Language.INCLUDE_ALSO_ERROR));
      }
    }

    private void QueueInitialCommands(ExecutionContext executionContext)
    {
      var initialCommands = new List<LauncherCommandBase>();

      if (options.Mode == LauncherConfigurationMode.LauncherApp)
      {
        initialCommands.Add(container.GetInstance<Uri, DetermineIfUpdateIsNeededDueToNoCrcFileCommand>(executionContext.LocalCRCDataFilePath));
      }
      else if (options.Mode == LauncherConfigurationMode.ManagerApp)
      {
        initialCommands.Add(container.GetInstance<Uri, Uri, List<string>, List<string>, GenerateCRC32DataFileCommand>(
          executionContext.LocalDirectoryBase,
          executionContext.LocalCRCDataFilePath,
          new List<string>() { CRC32DataFileNameExcludePattern },
          executionContext.IncludeFilePatterns
          ));
      }
      else
        throw new ArgumentOutOfRangeException(Language.UNKNOWN_MODE);

      foreach (var command in initialCommands)
      {
        executionContext.AppendCommand(command);
      }
    }

    public static void ConfigureContainer(IServiceContainer container)
    {
      container.RegisterInstance<IServiceContainer>(container);
      container.Register<Engine>();

      container.Register<ICRC32DataService<FileDescriptor>, CRC32DataService<FileDescriptor>>();
      container.Register<ICRC32DataService<LocalFileDescriptor>, CRC32DataService<LocalFileDescriptor>>();
      container.Register<ICRC32DataService<RemoteFileDescriptor>, CRC32DataService<RemoteFileDescriptor>>();
      container.Register<Uri, int, IHttpDownloadService>((factory, baseRepoUrl, timeout) => new HttpDownloadService(baseRepoUrl, timeout));
      container.Register<IJsonService, JsonService>();
      container.Register<IFileManagerService, FileManagerService>();
      container.Register<IRegexPatternMatcherService, RegexPatternMatcherService>();
      container.Register<IParallelService<string, RemoteFileDescriptor>, ParallelService<string, RemoteFileDescriptor>>();
      container.Register<IParallelService<string, LocalFileDescriptor>, ParallelService<string, LocalFileDescriptor>>();
      container.Register<IOperatingSystemInteractions, OperatingSystemInteractions>();

      container.Register<Uri, DetermineIfUpdateIsNeededDueToNoCrcFileCommand>(
        (factory, localCrcUri) => new DetermineIfUpdateIsNeededDueToNoCrcFileCommand(localCrcUri)
        {
          Container = container,
          FileManagerService = factory.GetInstance<IFileManagerService>()
        });

      container.Register<Uri, Uri, List<string>, List<string>, GenerateCRC32DataFileCommand>(
        (factory, basePath, localCrc32FilePath, excludedPatterns, includePatterns) =>
        new GenerateCRC32DataFileCommand(basePath, localCrc32FilePath, excludedPatterns, includePatterns)
        {
          Container = container,
          FileManager = factory.GetInstance<IFileManagerService>()
        });

      container.Register<Uri, LoadCRC32DataFromFileCommand>(
        (factory, localFile) => new LoadCRC32DataFromFileCommand(localFile)
        {
          CRC32ParseService = factory.GetInstance<ICRC32DataService<LocalFileDescriptor>>(),
          FileManagerService = factory.GetInstance<IFileManagerService>()
        });

      container.Register<LocalFileDescriptor, CalculateLocalFileCRC32Command>(
        (factory, localFile) => new CalculateLocalFileCRC32Command(localFile)
        {
          FileManager = factory.GetInstance<IFileManagerService>()
        }
        );

      container.Register<ExecutionContext, LocalFileDescriptor,
        RemoteFileDescriptor, DownloadFileCommand>(
        (factory, context, localFile, remoteFile) => new DownloadFileCommand(context, localFile, remoteFile)
        {
          DownloadService = factory.GetInstance<Uri, int, IHttpDownloadService>(context.BaseRepositoryUrl, factory.GetInstance<LocalLauncherConfiguration>().DownloadTimeout),
          FileManagerService = factory.GetInstance<IFileManagerService>()
        });

      container.Register<ExecutionContext, LocalFileDescriptor, RemoteFileDescriptor, ReplaceFileCommand>(
        (factory, context, localFile, remoteFile) =>
        new ReplaceFileCommand(context, localFile, remoteFile, ReplaceFileMode.WhenLocalAndRemoteCRC32DoesNotMatch)
        {
          FileManagerService = factory.GetInstance<IFileManagerService>(),
          OutputService = factory.GetInstance<IOutputService>()
        });

      container.Register<List<LauncherCommandBase>, ExecuteAllCommandsSimultaneously>(
        (factory, commands) => new ExecuteAllCommandsSimultaneously(commands)
        {
          OutputService = factory.GetInstance<IOutputService>()
        });


      container.Register<Uri, Uri, SaveLocalFileCRC32Command>(
        (factory, basePath, filePath) => new SaveLocalFileCRC32Command(basePath, filePath)
        {
          CRC32DataService = factory.GetInstance<ICRC32DataService<FileDescriptor>>(),
          FileManager = factory.GetInstance<IFileManagerService>()
        });

      container.Register<Uri, Uri, SaveLocalFileCRC32AfterUpdateCommand>(
        (factory, basePath, filePath) => new SaveLocalFileCRC32AfterUpdateCommand(basePath, filePath)
        {
          CRC32DataService = factory.GetInstance<ICRC32DataService<FileDescriptor>>(),
          FileManager = factory.GetInstance<IFileManagerService>()
        });

      container.Register<Uri, Uri, DownloadJsonRemoteConfigurationCommand>(
        (factory, baseUrl, relativeUrl) => new DownloadJsonRemoteConfigurationCommand(baseUrl, relativeUrl)
        {
          Container = container,
          DownloadService = factory.GetInstance<Uri, int, IHttpDownloadService>(baseUrl, factory.GetInstance<LocalLauncherConfiguration>().DownloadTimeout),
          FileManagerService = factory.GetInstance<IFileManagerService>(),
          JsonService = factory.GetInstance<IJsonService>()
        });

      container.Register<Uri, Uri, DownloadCRC32DataCommand>((factory, baseuri, relativeuri) =>
        new DownloadCRC32DataCommand(baseuri, relativeuri)
        {
          CRC32ParseService = factory.GetInstance<ICRC32DataService<RemoteFileDescriptor>>(),
          FileManagerService = factory.GetInstance<IFileManagerService>(),
          DownloadService = factory.GetInstance<Uri, int, IHttpDownloadService>(baseuri, factory.GetInstance<LocalLauncherConfiguration>().DownloadTimeout)
        }
      );
      container.Register<DetermineIfUpdateIsNeededDueToDifferentCRCValuesCommand>();

      container.Register<Uri, LaunchMainApplicationCommand>((factory, appurl) =>
        new LaunchMainApplicationCommand(appurl)
        {
          OS = factory.GetInstance<IOperatingSystemInteractions>()
        }
      );

      container.Register<Uri, Uri, DeleteLocalTemporaryFileCommand>((factory, baseurl, fileurl) =>
         new DeleteLocalTemporaryFileCommand(baseurl, fileurl)
         {
           FileManager = factory.GetInstance<IFileManagerService>()
         }
      );
    }

    /// <summary>
    /// Metoda wykonywana gdy otrzymamy jakis expetion. 
    /// Wywoluje command z uruchomieniem aplikacji.
    /// </summary>
    /// <param name="executioncontext"></param>
    private async void RunApplicationAfterError(ExecutionContext executionContext)
    {
      LauncherCommandBase currentCommand;
      // Odczytanie ostatniej komendy
      if (executionContext.ApplicationToExecute != null)
      {
        // Wyswietlamy balloonhint 
        var outputService = container.GetInstance<IOutputService>();
        outputService.DisplayError(Language.UPDATE_BUG_MESSAGE);

        // Weryfikujemy czy istnieje plik exe ktory chcemy uruchomic
        // musze weryfikowac na tym poziomie poniewaz to jest obsluga juz bledu i gdy 
        // bedzie exception to wywala aplikacje.
        string pathToExeFile = executionContext.LocalDirectoryBase.AbsolutePath + executionContext.ApplicationToExecute;
        if (File.Exists(pathToExeFile))
        {
          currentCommand = container.GetInstance<Uri, LaunchMainApplicationCommand>(executionContext.ApplicationToExecute);

          if (currentCommand != null)
            await currentCommand.ExecuteAsync(executionContext);
        }
        else
        {
          string msg = string.Format(Language.FILEEXE_NOTFOUND, pathToExeFile);
          outputService.DisplayError(msg);
        }
      }
    }
  }
}
