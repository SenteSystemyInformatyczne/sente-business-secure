﻿using System;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using LightInject;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DetermineIfUpdateIsNeededDueToNoCrcFileCommand : LauncherCommandBase
  {
    public Uri CRCDataRelativePath
    {
      get;
      private set;
    }

    public IFileManagerService FileManagerService
    {
      get;
      set;
    }

    public IServiceContainer Container
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return Language.CHECKING_LOCAL_CRC;
      }
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      await Task.Run(() =>
      {
        var localCRCExists = FileManagerService.FileExists(context.LocalDirectoryBase, CRCDataRelativePath);
        if (localCRCExists)
        {
          context.AppendCommand(Container.GetInstance<Uri, LoadCRC32DataFromFileCommand>(CRCDataRelativePath));
        }

        var relativeUri = new Uri(context.RemoteConfigurationUrl.AbsolutePath, UriKind.Relative);
        var authority = context.RemoteConfigurationUrl.Authority;
        var scheme = context.RemoteConfigurationUrl.Scheme;
        var baseUri = new Uri($"{scheme}://{authority}", UriKind.Absolute);
        
        context.AppendCommand(Container.GetInstance<Uri, Uri, DownloadJsonRemoteConfigurationCommand>(baseUri, relativeUri));
      });
    }

    public DetermineIfUpdateIsNeededDueToNoCrcFileCommand(Uri crcDataRelativePath)
    {
      CRCDataRelativePath = crcDataRelativePath;
    }
  }
}
