﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class LoadCRC32DataFromFileCommand : LauncherCommandBase
  {
    public Uri RelativeFilePath
    {
      get;
      private set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      string content = null;
      using (var stream = FileManagerService.OpenFile(context.LocalDirectoryBase, RelativeFilePath))
        content = await FileManagerService.ReadTextStreamAsync(stream);

      var result = await CRC32ParseService.Parse(content,
        (path, crc32) => new LocalFileDescriptor(new Uri(Uri.EscapeDataString(path), UriKind.Relative),
        uint.Parse(crc32, NumberStyles.HexNumber)), context.CurrentProgress);

      context.LocalFiles = result.ToList();
    }

    public IFileManagerService FileManagerService
    {
      get;
      set;
    }

    public ICRC32DataService<LocalFileDescriptor> CRC32ParseService
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return Language.LOADING_CRC_FILE;
      }
    }

    public LoadCRC32DataFromFileCommand(Uri relativeFilePath)
    {
      RelativeFilePath = relativeFilePath;
    }
  }
}
