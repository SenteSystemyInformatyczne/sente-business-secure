﻿using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using System.IO;
using System.Threading.Tasks;
using System;
using Sente.Launcher.Lib.Portable.Services;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DownloadFileCommand : DownloadResourceCommand
  {
    public Uri LocalFilePath
    {
      get;
      private set;
    }

    public uint? ExpectedCRC
    {
      get;
      private set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.DOWNLOADING_FILE} {RelativeFileUrl}";
      }
    }

    protected override async Task Process(ExecutionContext context, MemoryStream memorystream)
    {
      FileManagerService.CreatePathIfNotExists(LocalFilePath);

      if (ExpectedCRC.HasValue)
      {
        using (Crc32Stream crcStream = new Crc32Stream(memorystream))
        {
          await FileManagerService.WriteFileAsync(LocalFilePath, crcStream);
          if (!crcStream.CRC32.HasValue)
            throw new InvalidOperationException("CRC from downloaded stream should be calculated");
          if (crcStream.CRC32 != ExpectedCRC)
            throw new InvalidDataException($"Dowloaded file {LocalFilePath} has different " +
              $"CRC32 checksum {crcStream.CRC32:x2} than expected {ExpectedCRC:x2}");
        }
      }
      else
      {
        await FileManagerService.WriteFileAsync(LocalFilePath, memorystream);
      }
    }

    public DownloadFileCommand(ExecutionContext context, LocalFileDescriptor localFile, RemoteFileDescriptor remoteFile)
      : base(context.BaseRepositoryUrl, remoteFile.RelativeFileUrl)
    {
      LocalFilePath = new Uri(context.TempLocationPath, localFile.RelativeFileUrl);
      ExpectedCRC = remoteFile.CRC32;
    }
  }
}
