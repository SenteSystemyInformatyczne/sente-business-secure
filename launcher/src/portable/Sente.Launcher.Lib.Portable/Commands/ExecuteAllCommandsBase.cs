﻿using Sente.Launcher.Lib.Portable.Locale;
using System.Collections.Generic;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public abstract class ExecuteAllCommandsBase : LauncherCommandBase
  {
    public IReadOnlyList<LauncherCommandBase> CommandsToExecute
    {
      get;
      private set;
    }

    public string BulkName
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        if (string.IsNullOrWhiteSpace(BulkName))
          return string.Format(Language.BULK_EXECUTING_OF_COMMANDS, CommandsToExecute.Count);
        else
          return BulkName;
      }
    }

    public ExecuteAllCommandsBase(List<LauncherCommandBase> commands)
    {
      CommandsToExecute = commands;
    }
  }
}
