﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Commands.Helpers;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class ExecuteAllCommandsSimultaneously : ExecuteAllCommandsBase
  {
    private int executedTasksNumber = 0;

    public IOutputService OutputService
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return $"{base.Name} {Language.SIMULTANEOUSLY}";
      }
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      Task[] tasks = new Task[CommandsToExecute.Count];

      var progressInterceptor = new ProgressInterceptor(context.CurrentProgress, CommandsToExecute.Count);
      context.CurrentProgress = progressInterceptor;

      for (int i = 0; i < CommandsToExecute.Count; i++)
        tasks[i] = ExecuteAsyncGracefully(context, CommandsToExecute[i]);

      await Task.WhenAll(tasks);
      context.CurrentProgress = progressInterceptor.OriginalProgress;
    }

    /// <summary>
    /// Metoda umożliwiająca lepsze logowanie tasków i dodatkowo wywołująca GC w celu minimalizacji wykorzystywanej pamięci prze Launcher.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="lcb"></param>
    /// <returns></returns>
    private async Task ExecuteAsyncGracefully(ExecutionContext context, LauncherCommandBase lcb)
    {
      OutputService.Debug(string.Format(Language.PROCESSING_COMMAND_SIMULT, lcb.Name,lcb.GetType().Name));
      executedTasksNumber++;
      if (executedTasksNumber % 100 == 0)
        GC.Collect();
      try
      {
        await lcb.ExecuteAsync(context);
      }
      catch (Exception e)
      {
        OutputService.Debug(string.Format(Language.PROCESSING_COMMAND_SIMULT_ERR, lcb.Name, lcb.GetType().Name));
        OutputService.HandleException(e, false);
      }
    }

    public ExecuteAllCommandsSimultaneously(List<LauncherCommandBase> commands) : base(commands)
    {
    }
  }
}
