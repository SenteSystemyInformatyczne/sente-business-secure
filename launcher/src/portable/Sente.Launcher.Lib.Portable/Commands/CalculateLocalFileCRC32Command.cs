﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class CalculateLocalFileCRC32Command : LauncherCommandBase
  {
    public LocalFileDescriptor LocalFile
    {
      get;
      private set;
    }

    public IFileManagerService FileManager
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.CALCULATING_CRC_OF} {LocalFile.RelativeFileUrl}";
      }
    }

    public async override Task ExecuteAsync(ExecutionContext context)
    {
      if (!context.LocalFiles.Contains(LocalFile))
        throw new InvalidOperationException();

      LocalFile.CRC32 = await FileManager.CalculateFileCRC32(
        context.LocalDirectoryBase,
        LocalFile.RelativeFileUrl);

      LocalFile.Size = await FileManager.CalculateFileSize(
        context.LocalDirectoryBase,
        LocalFile.RelativeFileUrl);
    }

    public CalculateLocalFileCRC32Command(LocalFileDescriptor localFile)
    {
      LocalFile = localFile;
    }
  }
}
