﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class SaveLocalFileCRC32AfterUpdateCommand : SaveLocalFileCRC32Command
  {
    public override async Task ExecuteAsync(ExecutionContext context)
    {
      var localset = new HashSet<LocalFileDescriptor>(context.LocalFiles);
      foreach (var pair in context.FilesToUpdate)
      {
        pair.Item1.CRC32 = pair.Item2.CRC32;
        if (!localset.Contains(pair.Item1))
          context.LocalFiles.Add(pair.Item1);
      }

      await base.ExecuteAsync(context);
    }

    public SaveLocalFileCRC32AfterUpdateCommand(Uri basePath, Uri crc32FilePath) : base(basePath, crc32FilePath)
    {
    }
  }
}
