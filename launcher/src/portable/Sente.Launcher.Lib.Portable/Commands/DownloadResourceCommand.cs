﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public abstract class DownloadResourceCommand : LauncherCommandBase
  {
    public Uri BaseRepositoryUrl
    {
      get;
      protected set;
    }

    public Uri RelativeFileUrl
    {
      get;
      protected set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.DOWNLOADING_RESOURCE} {RelativeFileUrl}";
      }
    }

    /// <summary>
    /// Wstrzykiwana usługa do pobierania treści
    /// </summary>
    public IHttpDownloadService DownloadService
    {
      get;
      set;
    }

    /// <summary>
    /// Wstrzykiwana usługa do zarządzania plikami i katalogami
    /// </summary>
    public IFileManagerService FileManagerService
    {
      get;
      set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      if (DownloadService.BaseUrl != BaseRepositoryUrl)
        throw new InvalidOperationException();

      using (var memorystream = await DownloadService.DownloadFileAsync(RelativeFileUrl, context.CurrentProgress, context.AbortAllCancellationToken))
      {
        await Process(context, memorystream);
      }
    }

    protected abstract Task Process(ExecutionContext context, MemoryStream memorystream);

    public DownloadResourceCommand(Uri baseRepositoryUrl, Uri relativeFileUrl) : base()
    {

      if (baseRepositoryUrl != null && !baseRepositoryUrl.IsAbsoluteUri)
        throw new ArgumentException();

      BaseRepositoryUrl = baseRepositoryUrl;
      RelativeFileUrl = relativeFileUrl;
    }
  }
}
