﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using LightInject;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Interfaces.Services;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DetermineIfUpdateIsNeededDueToDifferentCRCValuesCommand : LauncherCommandBase
  {
    public override string Name
    {
      get
      {
        return Language.COMPARING_CRC;
      }
    }

    public IServiceContainer Container
    {
      get;
      set;
    }
    public IOutputService OutputService
    {
      get;
      set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      await Task.Run(() =>
      {
        context.FindFilesToUpdate();
        if (context.FilesToUpdate.Any())
        {        
          var toDownload = context.FilesToUpdate.Where(p => p.Item1 == null
            || (p.Item1 != null && p.Item1.CRC32 != p.Item2.CRC32));

          OutputService.Debug("Updating files:");
          foreach (var e in toDownload)
            OutputService.Debug($"\n local:{e.Item1?.ToString()} remote:{e.Item2?.ToString()}");

          if (toDownload.Any())
          {
            IEnumerable<LauncherCommandBase> filesDownloadCommands =
              toDownload.Select(item => Container.GetInstance<ExecutionContext,
                LocalFileDescriptor,
                RemoteFileDescriptor,
                DownloadFileCommand>(context, item.Item1, item.Item2)
              );

            var command = Container.GetInstance<List<LauncherCommandBase>,
              ExecuteAllCommandsSimultaneously>(filesDownloadCommands.ToList());

            command.BulkName = string.Format(Language.DOWNLOADING_FILES, 
              filesDownloadCommands.Count());
            context.AppendCommand(command);

            IEnumerable<LauncherCommandBase> filesReplaceCommands =
              toDownload.Select(item =>
                Container.GetInstance<ExecutionContext, LocalFileDescriptor,
                RemoteFileDescriptor, ReplaceFileCommand>(context, item.Item1, item.Item2)
              );

            command = Container.GetInstance<List<LauncherCommandBase>,
              ExecuteAllCommandsSimultaneously>(filesReplaceCommands.ToList());

            command.BulkName = string.Format(Language.UPDATING_FILES,
              filesReplaceCommands.Count());
            context.AppendCommand(command);

            context.AppendCommand(Container.GetInstance<Uri, Uri,
              SaveLocalFileCRC32AfterUpdateCommand>(context.LocalDirectoryBase,
                context.LocalCRCDataFilePath)
            );

            IEnumerable<LauncherCommandBase> tempFilesToDeleteCommands =
              toDownload.Select(item =>
                Container.GetInstance<Uri, Uri, DeleteLocalTemporaryFileCommand>(
                  context.TempLocationPath,
                  item.Item1.RelativeFileUrl)
              );

            command = Container.GetInstance<List<LauncherCommandBase>,
              ExecuteAllCommandsSimultaneously>(tempFilesToDeleteCommands.ToList());

            command.BulkName = string.Format(Language.REMOVING_TEMP_FILES,
              tempFilesToDeleteCommands.Count());

            context.AppendCommand(command);
          }
        }

        if (context.ApplicationToExecute != null)
          context.AppendCommand(Container
            .GetInstance<Uri, LaunchMainApplicationCommand>(context.ApplicationToExecute));
      });
    }
  }
}
