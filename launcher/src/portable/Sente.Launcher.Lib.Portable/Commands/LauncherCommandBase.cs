﻿using Newtonsoft.Json.Linq;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Lib.Portable.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public abstract class LauncherCommandBase
  {
    public abstract string Name
    {
      get;
    }

    public abstract Task ExecuteAsync(ExecutionContext context);

    public LauncherCommandBase(ExecutionContext context)
    {
    }

    public LauncherCommandBase()
    {
    }

    public override string ToString()
    {
      return Name;
    }
  }
}
