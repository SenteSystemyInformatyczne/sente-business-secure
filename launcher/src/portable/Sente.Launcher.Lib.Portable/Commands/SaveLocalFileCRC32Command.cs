﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using System;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class SaveLocalFileCRC32Command : LauncherCommandBase
  {
    public override string Name
    {
      get
      {
        return $"{Language.SAVE_LOCAL_CRC} {Crc32FilePath}";
      }
    }

    public ICRC32DataService<FileDescriptor> CRC32DataService
    {
      get; set;
    }

    public IFileManagerService FileManager
    {
      get; set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      var content = await CRC32DataService.Generate(context.LocalFiles, context.CurrentProgress);
      await FileManager.WriteFileAsync(new Uri(BasePath, Crc32FilePath), content);
    }

    public Uri BasePath
    {
      get;
      private set;
    }

    public Uri Crc32FilePath
    {
      get;
      private set;
    }

    public SaveLocalFileCRC32Command(Uri basePath, Uri crc32FilePath)
    {
      BasePath = basePath;
      Crc32FilePath = crc32FilePath;
    }
  }
}
