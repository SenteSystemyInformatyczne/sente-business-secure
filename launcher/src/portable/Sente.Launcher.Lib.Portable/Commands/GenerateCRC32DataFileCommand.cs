﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using LightInject;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class GenerateCRC32DataFileCommand : LauncherCommandBase
  {
    public override string Name
    {
      get
      {
        return $"{Language.GENERATING_FILE_CRC} {BasePath}";
      }
    }

    public IServiceContainer Container
    {

      get; set;
    }

    public IFileManagerService FileManager
    {
      get; set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {

      await Task.Run(() =>
      {
        var commands = new List<LauncherCommandBase>();
        var files = new List<LocalFileDescriptor>();
        var outputService = Container.GetInstance<IOutputService>();

        foreach (var fileName in FileManager.EnumerateDirectory(BasePath, IncludePatterns, ExcludePatterns))
        {
          var uri = new Uri(fileName, UriKind.Relative);    
          var file = new LocalFileDescriptor(uri);
          files.Add(file);
          commands.Add(Container.GetInstance<FileDescriptor, CalculateLocalFileCRC32Command>(file));
        }

        context.LocalFiles = files;
        var command = Container.GetInstance<List<LauncherCommandBase>, ExecuteAllCommandsSimultaneously>(commands);
        command.BulkName = string.Format(Language.CALCULATING_CRC_OF_ALL, commands.Count);
        context.AppendCommand(command);
        context.AppendCommand(Container.GetInstance<Uri, Uri, SaveLocalFileCRC32Command>(BasePath, CRC32FilePath));
      });
    }

    public Uri BasePath
    {
      get;
      private set;
    }

    public Uri CRC32FilePath
    {
      get;
      private set;
    }

    public List<string> ExcludePatterns
    {
      get;
      private set;
    }

    public List<string> IncludePatterns
    {
      get;
      private set;
    }

    public GenerateCRC32DataFileCommand(Uri basePath, Uri crc32filePath, List<string> excludePatterns = null, List<string> includePatterns = null)
    {
      BasePath = basePath;
      CRC32FilePath = crc32filePath;
      ExcludePatterns = excludePatterns;
      IncludePatterns = includePatterns;
    }
  }
}
