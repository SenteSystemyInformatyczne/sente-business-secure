﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands.Helpers
{
  public class ProgressInterceptor : IProgress<int>
  {
    public IProgress<int> OriginalProgress
    {
      get;
      private set;
    }

    private int tasksCount;
    private double currentProgress;
    private int lastReportedProgress;

    public ProgressInterceptor(IProgress<int> originalProgress, int numberOfTasks)
    {
      OriginalProgress = originalProgress;
      tasksCount = numberOfTasks;
      lastReportedProgress = 0;
      currentProgress = 0;
    }

    public void Report(int value)
    {
      currentProgress += value;
      var calculatedProgress = currentProgress * 1d / tasksCount - lastReportedProgress;
      lastReportedProgress += (int) calculatedProgress;
      OriginalProgress.Report((int) calculatedProgress);
    }
  }
}
