﻿using System;
using System.IO;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using LightInject;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DownloadJsonRemoteConfigurationCommand : DownloadResourceCommand
  {
    /// <summary>
    /// Wstrzykiwana usługa do obróbki jsona
    /// </summary>
    public IJsonService JsonService
    {
      get; set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.DOWNLOADING_CONFIG} {this.RelativeFileUrl}";
      }
    }

    public IServiceContainer Container
    {
      get; set;
    }

    protected override async Task Process(ExecutionContext context, MemoryStream memorystream)
    {
      context.RemoteConfiguration = await JsonService.Parse<RemoteLauncherConfiguration>(memorystream);

      var baseUri = new Uri(context.RemoteConfiguration.BaseRepositoryUrl);
      var relativeCRC32Uri = new Uri(context.RemoteConfiguration.RelativeCRC32DataFileUrl, UriKind.Relative);
      context.BaseRepositoryUrl = baseUri;

      context.AppendCommand(Container.GetInstance<Uri, Uri, DownloadCRC32DataCommand>(baseUri, relativeCRC32Uri));
      context.AppendCommand(Container.GetInstance<DetermineIfUpdateIsNeededDueToDifferentCRCValuesCommand>());
    }

    public DownloadJsonRemoteConfigurationCommand(Uri baseRepositoryUrl, Uri relativeFileUrl)
      : base(baseRepositoryUrl, relativeFileUrl)
    {
    }
  }
}
