﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public enum ReplaceFileMode
  {
    WhenLocalAndRemoteCRC32DoesNotMatch,
    Always
  }

  public class ReplaceFileCommand : LauncherCommandBase
  {
    public ReplaceFileMode ReplaceMode
    {
      get;
      private set;
    }

    public LocalFileDescriptor LocalFileDescriptor
    {
      get;
      private set;
    }

    public RemoteFileDescriptor RemoteFileDescriptor
    {
      get;
      private set;
    }

    public IFileManagerService FileManagerService
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.UPDATING_FILE} {LocalFileDescriptor.RelativeFileUrl}";
      }
    }

    public IOutputService OutputService
    {
      get;
      internal set;
    }

    public ReplaceFileCommand(
      ExecutionContext context,
      LocalFileDescriptor localFileDescriptor,
      RemoteFileDescriptor remoteFileDescriptor,
      ReplaceFileMode replaceMode) : base(context)
    {
      LocalFileDescriptor = localFileDescriptor;
      RemoteFileDescriptor = remoteFileDescriptor;
      ReplaceMode = replaceMode;
    }

    public async override Task ExecuteAsync(ExecutionContext context)
    {
      OutputService.Debug($"FileManagerService.CopyFile({context.TempLocationPath},{LocalFileDescriptor.RelativeFileUrl},{context.LocalDirectoryBase})");
      bool replace = ReplaceMode == ReplaceFileMode.Always || LocalFileDescriptor.CRC32 != RemoteFileDescriptor.CRC32;
      if (replace)
        await FileManagerService.CopyFile(context.TempLocationPath,
          LocalFileDescriptor.RelativeFileUrl,
          context.LocalDirectoryBase, context.CurrentProgress);
    }
  }
}
