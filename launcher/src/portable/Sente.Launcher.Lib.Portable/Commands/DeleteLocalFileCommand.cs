﻿using System;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DeleteLocalFileCommand : LauncherCommandBase
  {
    public Uri BaseUri
    {
      get;
      private set;
    }

    public Uri RelativeFileUri
    {
      get;
      private set;
    }

    public override string Name
    {
      get
      {
        return string.Format(Language.DELETING_FILE, RelativeFileUri);
      }
    }

    public IFileManagerService FileManager {
      get;
      set;
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      await Task.Run(() =>
      {
        FileManager.DeleteFile(BaseUri, RelativeFileUri);
      });
    }

    public DeleteLocalFileCommand(Uri baseUri, Uri fileUri)
    {
      BaseUri = baseUri;
      RelativeFileUri = fileUri;
    }
  }
}
