﻿using Sente.Launcher.Lib.Portable.Commands.Helpers;
using Sente.Launcher.Lib.Portable.Locale;
using Sente.Launcher.Lib.Portable.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class ExecuteAllCommandsSequentially : ExecuteAllCommandsBase
  {

    public override string Name
    {
      get
      {
        return $"{base.Name} {Language.SEQUENTIALLY}";
      }
    }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      var progressInterceptor = new ProgressInterceptor(context.CurrentProgress, CommandsToExecute.Count);
      context.CurrentProgress = progressInterceptor;

      for (int i = 0; i < CommandsToExecute.Count; i++)
        await CommandsToExecute[i].ExecuteAsync(context);

      context.CurrentProgress = progressInterceptor.OriginalProgress;
    }

    public ExecuteAllCommandsSequentially(List<LauncherCommandBase> commands) : base(commands)
    {
    }
  }
}
