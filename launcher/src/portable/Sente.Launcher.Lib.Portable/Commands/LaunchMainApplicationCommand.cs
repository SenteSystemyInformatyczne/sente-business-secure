﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models;
using Sente.Launcher.Lib.Portable.Interfaces.Services;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class LaunchMainApplicationCommand : LauncherCommandBase
  {
    public override string Name
    {
      get
      {
        return "Launching main application " + ApplicationUri;
      }
    }

    public Uri ApplicationUri
    {
      get;
      private set;
    }

    public IOperatingSystemInteractions OS {
      get;
      set;
   }

    public override async Task ExecuteAsync(ExecutionContext context)
    {
      await Task.Run(() =>
      {
        OS.ExecuteProcess(context.LocalDirectoryBase, ApplicationUri, context.ApplicationArguments);
      });
    }

    public LaunchMainApplicationCommand(Uri applicationUri)
    {
      ApplicationUri = applicationUri;
    }
  }
}
