﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Models;
using System.Text;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System.Globalization;
using Sente.Launcher.Lib.Portable.Locale;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DownloadCRC32DataCommand : DownloadResourceCommand
  {
    public ICRC32DataService<RemoteFileDescriptor> CRC32ParseService
    {
      get;
      set;
    }

    public override string Name
    {
      get
      {
        return $"{Language.DOWNLOADING_CRC_DATA_REMOTE} {this.RelativeFileUrl.OriginalString}";
      }
    }

    protected override async Task Process(ExecutionContext context, MemoryStream memorystream)
    {
      string content;
      using (var textReader = new StreamReader(memorystream, Encoding.UTF8))
      {
        content = await textReader.ReadToEndAsync();
      }

      var result = await CRC32ParseService.Parse(content,
        (url, crc32) => new RemoteFileDescriptor(new Uri(url, UriKind.Relative),
        uint.Parse(crc32, NumberStyles.HexNumber)), context.CurrentProgress);

      context.RemoteFiles = result.ToList();
    }

    public DownloadCRC32DataCommand(Uri baseRepositoryUrl, Uri relativeFileUrl)
      : base(baseRepositoryUrl, relativeFileUrl)
    {

    }
  }
}
