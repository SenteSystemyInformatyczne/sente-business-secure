﻿using Sente.Launcher.Lib.Portable.Locale;
using System;

namespace Sente.Launcher.Lib.Portable.Commands
{
  public class DeleteLocalTemporaryFileCommand : DeleteLocalFileCommand
  {
    public override string Name
    {
      get
      {
        return string.Format(Language.DELETING_TEMP_FILE, RelativeFileUri);
      }
    }

    public DeleteLocalTemporaryFileCommand(Uri baseUri, Uri fileUri) : base(baseUri, fileUri) { }
  }
}
