﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Microsoft.Extensions.Configuration;

namespace Sente.Launcher.Portable.Services
{
  public class LocalLauncherConfigurationFactory : ILocalLauncherConfigurationFactory
  {
    const string SectionName = "LocalSettings";

    public LocalLauncherConfiguration GetConfiguration(string environmentArguments)
    {
      var options = new LocalLauncherConfiguration()
      {
        Mode = LauncherConfigurationMode.LauncherApp,
        Verbose = true
      };
      Program.Configuration.GetSection(SectionName).Bind(options);

      bool definedArguments = !string.IsNullOrWhiteSpace(options.ApplicationArguments);
      string orgArguments = options.ApplicationArguments;

      options.ApplicationArguments = environmentArguments;
      if (definedArguments)
        options.ApplicationArguments += " " + orgArguments;
      
      return options;
    }
  }
}
