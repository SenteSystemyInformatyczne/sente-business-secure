﻿using LightInject;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sente.Launcher.Lib.Portable;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Lib.Portable.Services;
using Sente.Launcher.Portable.Services;
using System;
using System.IO;
using System.Linq;

namespace Sente.Launcher.Portable
{
  public class Program
  {
    const string SettingsFileName = "appsettings.json";

    public static IConfigurationRoot Configuration
    {
      get;
      private set;
    }

    public static IServiceProvider ServiceProvider
    {
      get;
      private set;
    }

    public static IServiceContainer Container
    {
      get;
      private set;
    }

    public static void Main(string[] args)
    {
      Configure(args);

      using (Container.BeginScope())
      {
        var engine = Container.GetInstance<Engine>();
        engine.Run().Wait();
      }

#if DEBUG
      Console.WriteLine("Program in debug configuration. Press any key to exit.");
      Console.ReadKey();
#endif      
    }

    private static void Configure(string[] args)
    {
      var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile(SettingsFileName, optional: false)
              .AddEnvironmentVariables();

      Configuration = builder.Build();

      IServiceCollection services = new ServiceCollection()
        .AddLogging();

      ServiceProvider = services.BuildServiceProvider();

      Container = new ServiceContainer();
      Container.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

      //engine internal configuration
      Engine.ConfigureContainer(Container);

      //program specific configuration
      string argsstring = string.Empty;
      if (args.Count() > 0)
      {
        argsstring = args
        .Aggregate((acc, param) => acc + (acc.Length == 0 ? "" : " ") + param);
      }

      var configuration = new LocalLauncherConfigurationFactory().GetConfiguration(argsstring);

      Container.RegisterInstance<LocalLauncherConfiguration>(configuration);

      var outputService = new ConsoleOutputService()
      {
        Verbose = configuration.Verbose
      };

      Container.RegisterInstance<IOutputService>(outputService);     
    }
  }
}
