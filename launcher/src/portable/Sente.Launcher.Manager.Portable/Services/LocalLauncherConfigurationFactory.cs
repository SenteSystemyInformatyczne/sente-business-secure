﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System.Collections.Generic;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using System;
using System.IO;
using System.Linq;
using Sente.Launcher.Lib.Portable;

namespace Sente.Launcher.Manager.Portable.Services
{
  internal class LocalLauncherConfigurationFactory : ILocalLauncherConfigurationFactory
  {
    const string TargetDirOption = "TargetDir";
    const string VerboseOption = "Verbose";
    const string Include = "Include";
    const string IncludeAlso = "IncludeAlso";
    const string Output = "Output";

    internal Dictionary<string, string> Options
    {
      get; private set;
    }

    public LocalLauncherConfigurationFactory()
    {
      Options = GetDefaultOptions();
    }

    public LocalLauncherConfiguration GetConfiguration(string environmentArguments)
    {
      var configuration = new LocalLauncherConfiguration();

      configuration.TargetDirUrl = Program.Configuration[TargetDirOption.ToLowerInvariant()];
      configuration.Verbose = Program.Configuration[VerboseOption].ToLowerInvariant() == "true";
      configuration.Mode = LauncherConfigurationMode.ManagerApp;
      configuration.Include = Program.Configuration[Include.ToLowerInvariant()];
      configuration.IncludeAlso = Program.Configuration[IncludeAlso.ToLowerInvariant()];
      configuration.OutputFile = Program.Configuration[Output.ToLowerInvariant()];

      return configuration;
    }

    private Dictionary<string, string> GetDefaultOptions()
    {
      return new Dictionary<string, string>
            {
                {TargetDirOption, Directory.GetCurrentDirectory() },
                {VerboseOption, "false" },
                {Include, null },
                {IncludeAlso, null },
                {Output, Engine.CRC32DataFileName }
            };
    }

    public Dictionary<string, string> GetSwitchMappings()
    {
      return Options.Select(item =>
          new KeyValuePair<string, string>(
              "-" + item.Key.Substring(item.Key.LastIndexOf(':') + 1),
              item.Key))
              .ToDictionary(
                  item => item.Key, item => item.Value);
    }
  }
}