﻿using LightInject;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Sente.Launcher.Lib.Portable;
using Sente.Launcher.Manager.Portable.Services;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Services;

namespace Sente.Launcher.Manager.Portable
{
    public class Program
    {
    public static IConfigurationRoot Configuration
    {
      get;
      private set;
    }

    public static IServiceProvider ServiceProvider
    {
      get;
      private set;
    }

    public static IServiceContainer Container
    {
      get;
      private set;
    }

    public static void Main(string[] args)
    {
      Configure(args);

      using (Container.BeginScope())
      {
        var engine = Container.GetInstance<Engine>();
        engine.Run().Wait();
      }

#if DEBUG
      Console.WriteLine("Program in debug configuration. Press any key to exit.");
      Console.ReadKey();
#endif      
    } 

    private static void Configure(string[] args)
    {
      var configurationFactory = new LocalLauncherConfigurationFactory();

      var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddInMemoryCollection(configurationFactory.Options)
              .AddCommandLine(args, configurationFactory.GetSwitchMappings())
              .AddEnvironmentVariables();

      Configuration = builder.Build();

      IServiceCollection services = new ServiceCollection()
        .AddLogging();

      ServiceProvider = services.BuildServiceProvider();

      Container = new ServiceContainer();
      Container.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

      //engine internal configuration
      Engine.ConfigureContainer(Container);

      //program specific configuration
      var argsstring = args
        .Aggregate((acc, param) => acc + (acc.Length == 0 ? "" : " ") + param);

      var config = configurationFactory.GetConfiguration(argsstring);

      Container.RegisterInstance(config);

      var outputService = new ConsoleOutputService()
      {
        Verbose = config.Verbose
      };

      Container.RegisterInstance<IOutputService>(outputService);      
    }
  }
}
