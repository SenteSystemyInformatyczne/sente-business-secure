﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Sente.Launcher.Win.ViewModel
{
  class MainViewModel : BaseViewModel, IOutputService
  {
    ViewModels.ProgressInfo progressInfo;
    /// <summary>
    /// Wartość aktualnego postępu
    /// </summary>
    public int ProgressValue
    {
      get
      {
        return progressInfo.progressValue;
      }
      set
      {
        SetProperty(ref progressInfo.progressValue, value);
      }
    }

    /// <summary>
    /// Tytuł aktualnie wykonywanego zadania
    /// </summary>
    public string ProgressDescription
    {
      get
      {
        return progressInfo.progressDescription;
      }
      set
      {
        SetProperty(ref progressInfo.progressDescription, value);
      }
    }

    static readonly Brush okBackgroundBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xE6, 0xE6, 0xE6));
    static readonly Brush okForegroundBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x06, 0xB0, 0x25));
    static readonly Brush errBrush = new SolidColorBrush(Colors.Red);

    public MainViewModel()
    {
      progressInfo = new ViewModels.ProgressInfo();
      ProgressBackgroundBrush = okBackgroundBrush;
      ProgressForegroundBrush = okForegroundBrush;
    }

    /// <summary>
    /// Metoda implementacji interfejsu IOutputService.
    /// Ustawia tytuł aktualnie wykonywanego zadania.
    /// </summary>
    /// <param name="message">Tytuł zadania</param>
    public void DisplayMessage(string message)
    {
      ProgressDescription = message;
      App.Log.Info(message);
    }

    /// <summary>
    /// Metoda implementacji interfejsu IOutputService.
    /// Ustawia aktualny postęp zadania.
    /// </summary>
    /// <param name="value"></param>
    public void Report(int value)
    {
      ProgressValue += value;
    }

    DispatcherTimer dispatcherTimer;

    public void Done(bool success)
    {
      if (ErrorOccured)
      {
        dispatcherTimer = new DispatcherTimer();
        dispatcherTimer.Tick += delayedExit;
        dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
        dispatcherTimer.Start();
      }
      else
      {
        delayedExit(this, EventArgs.Empty);
      }
    }

    private void delayedExit(object sender, EventArgs e)
    {
      if (dispatcherTimer != null)
        dispatcherTimer.Stop();
      App.Current.Shutdown(!ErrorOccured ? 0 : 1);
    }

    public void SetProgress(int value)
    {
      ProgressValue = value;
    }

    private Brush progressBackgroundBrush;
    public Brush ProgressBackgroundBrush
    {
      get{
        return progressBackgroundBrush;
      }
      set
      {
        SetProperty(ref progressBackgroundBrush, value);
      }
    }

    private Brush progressForegroundBrush;
    public Brush ProgressForegroundBrush
    {
      get
      {
        return progressForegroundBrush;
      }
      set
      {
        SetProperty(ref progressForegroundBrush, value);
      }
    }

    public bool ErrorOccured
    {
      get;
      set;
    }

    public void HandleException(Exception e, bool critical = true)
    {
      ProgressBackgroundBrush = errBrush;
      ProgressForegroundBrush = errBrush;
      ErrorOccured = ErrorOccured || critical;
      App.Log.Fatal(e);
      if (critical)
        App.Log.Fatal(e);
      else
        App.Log.Info(e);

    }

    public void Debug(string message)
    {
      App.Log.Debug(message);
    }

    /// <summary>
    /// Metoda wyświetla MessageBox z przekazaną informacją.
    /// </summary>
    /// <param name="message"></param>
    public void DisplayError(string message)
    {
      MessageBoxResult result = System.Windows.MessageBox.Show(message, "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
    }
  }
}
