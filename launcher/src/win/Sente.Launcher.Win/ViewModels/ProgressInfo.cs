﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sente.Launcher.Win.ViewModels
{
  internal class ProgressInfo
  {
    /// <summary>
    /// Opis aktualnie wykonywanej akcji.
    /// Wyświetlany centralnie na pasku postępu.
    /// </summary>
    internal string progressDescription;

    /// <summary>
    /// Wartość paska postępu [0,100]
    /// </summary>
    internal int progressValue;
  }
}
