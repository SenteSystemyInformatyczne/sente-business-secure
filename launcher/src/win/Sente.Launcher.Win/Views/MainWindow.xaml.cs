﻿using LightInject;
using Sente.Launcher.Lib.Portable;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Win.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sente.Launcher.Win
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      using (App.Container.BeginScope())
      {
        var viewModel = App.Container.GetInstance<MainViewModel>();
        App.Container.RegisterInstance<IOutputService>(viewModel);

        DataContext = viewModel;

        App.Container.GetInstance<Engine>().Run();
      }
    }
  }
}
