﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using System.Configuration;
using System;

namespace Sente.Launcher.Win.Services
{
  class LocalLauncherConfigurationFactory : ILocalLauncherConfigurationFactory
  {
    const string RemoteUrlKey = "RemoteLauncherConfigurationUrl";
    const string TargetDirKey = "TargetDirUrl";
    const string ApplicationToExecute = "ApplicationToExecute";
    const string ApplicationArguments = "ApplicationArguments";
    const string DownloadTimeout = "DownloadTimeout";

    public LocalLauncherConfiguration GetConfiguration(string environmentArguments)
    {
      var configuration = new LocalLauncherConfiguration();

      configuration.Mode = LauncherConfigurationMode.LauncherApp;
      configuration.RemoteLauncherConfigurationUrl = ConfigurationManager.AppSettings[RemoteUrlKey];
      configuration.TargetDirUrl = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings[TargetDirKey]);
      configuration.ApplicationToExecute = ConfigurationManager.AppSettings[ApplicationToExecute];
      configuration.ApplicationArguments = ConfigurationManager.AppSettings[ApplicationArguments];

      int dlTimeout;
      if (int.TryParse(ConfigurationManager.AppSettings[DownloadTimeout] ?? string.Empty, out dlTimeout))
      {
        configuration.DownloadTimeout = dlTimeout;
      }
      else
      {
        configuration.DownloadTimeout = 600;
      }
      

      bool definedArguments = !string.IsNullOrWhiteSpace(configuration.ApplicationArguments);
      string orgArguments = configuration.ApplicationArguments;

      configuration.ApplicationArguments = environmentArguments;
      if (definedArguments)
        configuration.ApplicationArguments += " " + orgArguments;

      return configuration;
    }
  }
}
