﻿using LightInject;
using NLog;
using Sente.Launcher.Lib.Portable;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Win.Services;
using Sente.Launcher.Win.ViewModel;
using System;
using System.Linq;
using System.Windows;

namespace Sente.Launcher.Win
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    private static Logger logger = LogManager.GetLogger("Launcher");
    public static Logger Log
    {
      get
      {
        return logger;
      }
    }

    private static IServiceContainer container;
    public static IServiceContainer Container
    {
      get
      {
        if (container == null)
        {
          container = ConfigureContainer();
        }

        return container;
      }
    }

    private static IServiceContainer ConfigureContainer()
    {
      var container = new ServiceContainer();
      container.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

      //engine internal configuration
      Engine.ConfigureContainer(container);

      //program specific configuration
      container.Register<MainViewModel>(new PerContainerLifetime());

      var args = Environment.GetCommandLineArgs().AsEnumerable();

      var argsstring = "";
      if (args.Count() > 1)
      {
        args = args.Skip(1);//pierwszy to nazwa programu w tym wypadku launchera
        argsstring = args
        .Aggregate((acc, param) => acc + (acc.Length == 0 ? "" : " ") + param);
      }

      var configuration = new LocalLauncherConfigurationFactory().GetConfiguration(argsstring);

      container.RegisterInstance<LocalLauncherConfiguration>(configuration);

      return container;
    }
  }
}
