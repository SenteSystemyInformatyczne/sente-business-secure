﻿using NSubstitute;
using Sente.Launcher.Lib.Portable.Commands;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using System;
using System.IO;
using System.Text;
using System.Threading;
using Xunit;
using Models = Sente.Launcher.Lib.Portable.Models;

namespace Sente.Launcher.Tests.Commands
{
  public class DownloadFileCommandTests
  {
    const string PerfectlyFineString = "2M0a1r6t1u2l0a4";
    static readonly byte[] pattern = Encoding.ASCII.GetBytes(PerfectlyFineString);

    [Fact]
    public async void ShouldDownloadFile()
    {
      MemoryStream contentStream = null;
      try
      {
        //given
        string relativePath = "./params/file.prt";
        string baseUrl = "http://tempuri.org/";
        string tempPath = @"z:\windowz\szysztem";

        Uri baseUri = new Uri(baseUrl, UriKind.Absolute);
        Uri relativeUri = new Uri(relativePath, UriKind.Relative);
        Uri tempUri = new Uri(tempPath);
        Uri localUri = new Uri(tempUri, relativeUri);
        Uri repoUri = new Uri(baseUri, relativeUri);

        contentStream = new MemoryStream(pattern);

        var downloadServiceMock = Substitute.For<IHttpDownloadService>();
        downloadServiceMock.DownloadFileAsync(null, null, default(CancellationToken))
          .ReturnsForAnyArgs(contentStream);
        downloadServiceMock.BaseUrl.Returns(baseUri);

        var fileServiceMock = Substitute.For<IFileManagerService>();

        var executionContext = new Models.ExecutionContext()
        {
          BaseRepositoryUrl = baseUri,
          TempLocationPath = tempUri
        };

        var localFileDescriptor = new LocalFileDescriptor(relativeUri, 0);
        var remoteFileDescriptor = new RemoteFileDescriptor(relativeUri, 0);
        remoteFileDescriptor.CRC32 = null;

        var command = new DownloadFileCommand(executionContext, localFileDescriptor, remoteFileDescriptor)
        {
          DownloadService = downloadServiceMock,
          FileManagerService = fileServiceMock
        };

        //when
        await command.ExecuteAsync(executionContext);

        //then
        Received.InOrder(async () =>
        {
          await downloadServiceMock.DownloadFileAsync(Arg.Is(relativeUri), null, Arg.Is(executionContext.AbortAllCancellationToken));
          fileServiceMock.CreatePathIfNotExists(Arg.Is(localUri));
          await fileServiceMock.WriteFileAsync(Arg.Is(localUri), Arg.Is<Stream>(
            ms => ms == contentStream
          ));
        });

      }
      finally
      {
        if (contentStream != null)
          contentStream.Dispose();
      }
    }
  }
}
