﻿using LightInject;
using Newtonsoft.Json;
using NSubstitute;
using Sente.Launcher.Lib.Portable.Commands;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Xunit;
using Models = Sente.Launcher.Lib.Portable.Models;

namespace Sente.Launcher.Tests.Commands
{
  public class DownloadJsonRemoteConfigurationCommandTests
  {
    internal const string Url = @"http://localhost:29311/DBR/Precious/Super/Stable";
    internal const string Pattern =
@"{
    'BaseRepositoryUrl': '" + Url + @"',
    'RelativeCRC32DataFileUrl' : 'config.config'
  }";

    static readonly byte[] PatternRaw = Encoding.ASCII.GetBytes(Pattern);

    IServiceContainer Container
    {
      get;
      set;
    }

    public DownloadJsonRemoteConfigurationCommandTests()
    {
      Container = new ServiceContainer();
      Container.ScopeManagerProvider = new PerLogicalCallContextScopeManagerProvider();

      var mockCRC = Substitute.For<DownloadCRC32DataCommand>(null,null);
      var mockUpdateDecision = Substitute.For<DetermineIfUpdateIsNeededDueToDifferentCRCValuesCommand>();

      Container.Register<Uri, Uri, DownloadCRC32DataCommand>((factory, url, url2) => mockCRC);
      Container.RegisterInstance(mockUpdateDecision);
    }

    [Fact]
    public async void ShouldDownloadAndParseJson()
    {
      MemoryStream contentStream = null;
      try
      {
        //given
        string relativePath = "config.json";
        string baseUrl = "http://tempuri.org/";

        Uri baseUri = new Uri(baseUrl, UriKind.Absolute);
        Uri relativeUri = new Uri(relativePath, UriKind.Relative);

        contentStream = new MemoryStream(PatternRaw);

        var downloadServiceMock = Substitute.For<IHttpDownloadService>();
        downloadServiceMock.DownloadFileAsync(null, null, default(CancellationToken))
          .ReturnsForAnyArgs(contentStream);
        downloadServiceMock.BaseUrl.Returns(baseUri);

        var executionContext = new Models.ExecutionContext()
        {
          BaseRepositoryUrl = baseUri
        };

        var jsonServiceMock = Substitute.For<IJsonService>();
        jsonServiceMock
          .Parse<RemoteLauncherConfiguration>(Arg.Any<Stream>())
          .Returns(async ci => await new JsonService().Parse<RemoteLauncherConfiguration>(ci.Arg<Stream>()));

        var command = new DownloadJsonRemoteConfigurationCommand(baseUri, relativeUri)
        {
          DownloadService = downloadServiceMock,
          JsonService = jsonServiceMock,
          Container = this.Container
        };

        //when
        await command.ExecuteAsync(executionContext);

        //then
        Received.InOrder(async () =>
        {
          await downloadServiceMock.DownloadFileAsync(Arg.Is(relativeUri), null, Arg.Is(executionContext.AbortAllCancellationToken));
          await jsonServiceMock.Parse<RemoteLauncherConfiguration>(Arg.Is<Stream>(stream => stream == contentStream));
        });

        Assert.NotNull(executionContext.RemoteConfiguration);
        Assert.Equal(Url, executionContext.RemoteConfiguration.BaseRepositoryUrl);
      }
      finally
      {
        if (contentStream != null)
          contentStream.Dispose();
      }
    }

    [Fact]
    public async void ShouldDownloadAndParseJsonWithCommands()
    {
      MemoryStream contentStream = null;
      try
      {
        //given
        string relativePath = "config.json";
        string baseUrl = "http://tempuri.org/";

        RemoteLauncherConfiguration remoteConf = new RemoteLauncherConfiguration()
        {
          BaseRepositoryUrl = baseUrl,
          RelativeCRC32DataFileUrl = relativePath,
          Commands = new List<ReplaceFileConfiguration>()
          {
            new ReplaceFileConfiguration()
            {
              RelativeFilePath = "plik1.txt"
            },
            new ReplaceFileConfiguration()
            {
              Mode = "always",
              RelativeFilePath = "plik2.txt"
            }
          }
        };

        var pattern = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(remoteConf));

        Uri baseUri = new Uri(baseUrl, UriKind.Absolute);
        Uri relativeUri = new Uri(relativePath, UriKind.Relative);

        contentStream = new MemoryStream(pattern);

        var downloadServiceMock = Substitute.For<IHttpDownloadService>();
        downloadServiceMock.DownloadFileAsync(null, null, default(CancellationToken))
          .ReturnsForAnyArgs(contentStream);
        downloadServiceMock.BaseUrl.Returns(baseUri);

        var executionContext = new Models.ExecutionContext()
        {
          BaseRepositoryUrl = baseUri
        };

        var jsonServiceMock = Substitute.For<IJsonService>();
        jsonServiceMock
          .Parse<RemoteLauncherConfiguration>(Arg.Any<Stream>())
          .Returns(async ci => await new JsonService().Parse<RemoteLauncherConfiguration>(ci.Arg<Stream>()));

        var command = new DownloadJsonRemoteConfigurationCommand(baseUri, relativeUri)
        {
          DownloadService = downloadServiceMock,
          JsonService = jsonServiceMock,
          Container = this.Container
        };

        //when
        await command.ExecuteAsync(executionContext);

        //then
        Received.InOrder(async () =>
        {
          await downloadServiceMock.DownloadFileAsync(Arg.Is(relativeUri), null, Arg.Is(executionContext.AbortAllCancellationToken));
          await jsonServiceMock.Parse<RemoteLauncherConfiguration>(Arg.Is<Stream>(stream => stream == contentStream));
        });

        Assert.NotNull(executionContext.RemoteConfiguration);
        Assert.Equal(remoteConf.Commands.Count, executionContext.RemoteConfiguration.Commands.Count);
        Assert.All(remoteConf.Commands, cmd => executionContext.RemoteConfiguration.Commands.Contains(cmd));
      }
      finally
      {
        if (contentStream != null)
          contentStream.Dispose();
      }
    }
  }
}
