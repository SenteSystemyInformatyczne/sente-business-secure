﻿using NSubstitute;
using Sente.Launcher.Lib.Portable.Commands;
using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using Sente.Launcher.Lib.Portable.Services;
using Sente.Launcher.Tests.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;
using Models = Sente.Launcher.Lib.Portable.Models;

namespace Sente.Launcher.Tests.Commands
{
  public class DownloadCRC32DataCommandTests
  {
    const string file1 = "file1.bpl";
    const string file2 = "file2.exe";
    const uint crc1 = uint.MaxValue;
    const uint crc2 = uint.MinValue;

    static readonly string CRC32DataLine1 =
      $"{file1} {crc1}";
    static readonly string CRC32DataLine2 =
      $"{file2} {crc2}";

    static readonly string CRC32Data =
        CRC32DataLine1 + "\n"
      + CRC32DataLine2;

    static readonly byte[] pattern = Encoding.ASCII.GetBytes(CRC32Data);

    [Fact]
    public async void ShouldParseCRC32DataFile()
    {
      MemoryStream contentStream = null;
      try
      {
        //given
        string relativePath = "config.json";
        string baseUrl = "http://tempuri.org/";

        Uri baseUri = new Uri(baseUrl, UriKind.Absolute);
        Uri relativeUri = new Uri(relativePath, UriKind.Relative);

        contentStream = new MemoryStream(pattern);

        var downloadServiceMock = Substitute.For<IHttpDownloadService>();
        downloadServiceMock.DownloadFileAsync(null, null, default(CancellationToken))
          .ReturnsForAnyArgs(contentStream);
        downloadServiceMock.BaseUrl.Returns(baseUri);

        var parseCrc32ServiceMock = Substitute.For<ICRC32DataService<RemoteFileDescriptor>>();
        parseCrc32ServiceMock.Parse(Arg.Any<string>(), Arg.Any<Func<string, string, RemoteFileDescriptor>>(), null)
          .Returns(
            new List<RemoteFileDescriptor>()
            {
              new RemoteFileDescriptor(new Uri(file1, UriKind.Relative), crc1),
              new RemoteFileDescriptor(new Uri(file2, UriKind.Relative), crc2)
            }.AsEnumerable()
          );

        var executionContext = new Models.ExecutionContext()
        {
          BaseRepositoryUrl = baseUri
        };

        var command = new DownloadCRC32DataCommand(baseUri, relativeUri)
        {
          DownloadService = downloadServiceMock,
          CRC32ParseService = parseCrc32ServiceMock
        };

        //when
        await command.ExecuteAsync(executionContext);

        //then
        Received.InOrder(async () =>
        {
          await downloadServiceMock.DownloadFileAsync(Arg.Is(relativeUri), null, Arg.Is(executionContext.AbortAllCancellationToken));
          await parseCrc32ServiceMock.Parse(Arg.Is<string>(s =>
             s == CRC32Data
            ), Arg.Any<Func<string, string, RemoteFileDescriptor>>(), null);
        });

        Assert.NotNull(executionContext.RemoteFiles);
        Assert.Equal(2, executionContext.RemoteFiles.Count);

        Assert.All(executionContext.RemoteFiles,
          file => Assert.True((file.RelativeFileUrl.ToString() == file1 && file.CRC32 == crc1)
            || (file.RelativeFileUrl.ToString() == file2 && file.CRC32 == crc2)));
      }
      finally
      {
        if (contentStream != null)
          contentStream.Dispose();
      }
    }

    private bool IsCollectionValid(IEnumerable<string> l)
    {
      return l.ElementAt(0) == CRC32DataLine1 && l.ElementAt(1) == CRC32DataLine2;
    }
  }
}
