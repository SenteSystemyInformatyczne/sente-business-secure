﻿using System.IO;
using System.Text;

namespace Sente.Launcher.Tests.Commands
{
  internal class StringStreamHelper
  {
    internal static string ReadToEnd(Stream stream)
    {
      if (stream.Position != 0)
        stream.Position = 0;
      using (var reader = new StreamReader(stream, Encoding.UTF8))
        return reader.ReadToEnd();
    }
  }
}