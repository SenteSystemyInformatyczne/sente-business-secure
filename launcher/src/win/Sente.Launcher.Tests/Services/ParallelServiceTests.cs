﻿using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sente.Launcher.Tests.Services
{
  public class ParallelServiceTests
  {
    [Fact]
    public async void ShouldCalculateProperResults()
    {
      //given
      var size = 10000;
      var input = Enumerable.Range(0, size);
      var service = new ParallelService<int,int>();

      //when
      var result = await service.ParallelMap(input, i => 0, null);

      //then
      Assert.Equal(size, result.Count());
      Assert.True(result.All(e => e == 0));
    }

    [Fact]
    public void ShouldSplitEquivalentPartitions()
    {
      //given
      var size = 10000;
      var input = Enumerable.Range(0, size);
      var service = new ParallelService<int, int>();
      var processorCount = Environment.ProcessorCount;
      var partitionSize = size / processorCount;

      //when
      IEnumerable<IEnumerable<int>> 
        partitions = service.Partition(input,processorCount);

      //then
      Assert.Equal(processorCount, partitions.Count());
      Assert.All(partitions, (x) => { Assert.Equal(partitionSize, x.Count());});
    }
     
  }
}
