﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sente.Launcher.Lib.Portable.Models.Configurations;
using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sente.Launcher.Tests.Services
{
  public class JsonServiceTests
  {
    internal const string Url = @"http://localhost:29311/config.json";
    internal const string TargetPath = @"C:/sente/s4/";
    internal const LauncherConfigurationMode Mode1 = LauncherConfigurationMode.ManagerApp;
    internal const string Pattern =
    @"
      {
          'RemoteLauncherConfigurationUrl': '" + Url + @"'
      }";
    internal const string Pattern2 =
    @"
      {
          'RemoteLauncherConfigurationUrl': '" + Url + @"',
          'TargetDirUrl': '" + TargetPath + @"',
          'Mode': 'ManagerApp'
      }";

    internal const string EmptyPattern =
    @"
      {
      }";
    internal const string BadPattern =
    @"{
          {{{...
      }";

    [Fact]
    public async void ShouldParseJson()
    {
      //given

      var service = new JsonService();
      LocalLauncherConfiguration result = null;

      //when

      using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(Pattern)))
      {
        result = await service.Parse<LocalLauncherConfiguration>(stream);
      }

      //then
      Assert.NotNull(result);
      Assert.Equal(Url, result.RemoteLauncherConfigurationUrl);
      Assert.Equal(null, result.TargetDirUrl);
      Assert.Equal(LauncherConfigurationMode.None, result.Mode);
    }

    [Fact]
    public async void ShouldResultBeNull()
    {
      //given

      var service = new JsonService();
      LocalLauncherConfiguration result = null;

      //when
      result = await service.Parse<LocalLauncherConfiguration>(null);

      //then
      Assert.Null(result);
    }

    [Fact]
    public async void ShouldResultBeEmpty()
    {
      //given

      var service = new JsonService();
      LocalLauncherConfiguration result = null;

      //when

      using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(EmptyPattern)))
      {
        result = await service.Parse<LocalLauncherConfiguration>(stream);
      }

      //then
      Assert.NotNull(result);
      Assert.Equal(null, result.RemoteLauncherConfigurationUrl);
      Assert.Equal(null, result.TargetDirUrl);
      Assert.Equal(LauncherConfigurationMode.None, result.Mode);
    }
    [Fact]
    public async void ShouldRaiseJsonReaderException()
    {
      //given

      var service = new JsonService();

      //when and thenLauncherApp

      Exception ex = null;
      using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(BadPattern)))
      {
        ex = await Assert.ThrowsAsync<JsonReaderException>(async () => await service.Parse<LocalLauncherConfiguration>(stream));
      }

    }

    [Fact]
    public async void ReadAllSettings()
    {
      //given

      var service = new JsonService();
      LocalLauncherConfiguration result = null;

      //when

      using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(Pattern2)))
      {
        result = await service.Parse<LocalLauncherConfiguration>(stream);
      }

      //then
      Assert.NotNull(result);
      Assert.Equal(Url, result.RemoteLauncherConfigurationUrl);
      Assert.Equal(TargetPath, result.TargetDirUrl);
      Assert.Equal(LauncherConfigurationMode.ManagerApp, result.Mode);
    }

  }
}
