﻿using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Sente.Launcher.Lib.Portable.Model.FileDescriptors;
using NSubstitute;
using Sente.Launcher.Lib.Portable.Models;
using System.Globalization;

namespace Sente.Launcher.Tests.Services
{
  public class CRC32DataServiceTests
  {
    const uint PatternCRC32 = 0x19856404U;
    const string PatternRaport = @"""C://file1"" 19856404
""C://file2"" 19856404
";
    Uri PatternUri1 = new Uri("C://file1");
    Uri PatternUri2 = new Uri("C://file2");

    [Fact]
    public async void ShouldGenerateCrcRaport()
    {
      //given
      List<LocalFileDescriptor> list = new List<LocalFileDescriptor>();
      list.Add(Substitute.For<LocalFileDescriptor>(PatternUri1, PatternCRC32));
      list.Add(Substitute.For<LocalFileDescriptor>(PatternUri2, PatternCRC32));
      var service = new CRC32DataService<LocalFileDescriptor>();
      //when
      var content = await service.Generate(list, Substitute.For<System.IProgress<int>>());

      //then
      Assert.NotNull(content);
      Assert.Equal(PatternRaport, content);
    }

    [Fact]
    public async void ShouldParseCrcRaport()
    {
      //given
      var service = new CRC32DataService<LocalFileDescriptor>();
      var parallelService = new ParallelService<string, LocalFileDescriptor>();
      service.ParallelService = parallelService;

      //when
      var result = await service.Parse(PatternRaport,
        (path, crc32) => new LocalFileDescriptor(new Uri(path, UriKind.RelativeOrAbsolute),
        uint.Parse(crc32, NumberStyles.HexNumber)), Substitute.For<System.IProgress<int>>());

      //then
      Assert.NotNull(result);
      Assert.Equal(2, result.Count());
      Assert.Equal(PatternCRC32, result.First().CRC32);
      Assert.Equal(PatternUri1, result.First().RelativeFileUrl);
      Assert.Equal(PatternCRC32, result.Last().CRC32);
      Assert.Equal(PatternUri2, result.Last().RelativeFileUrl);
    }
  }
}
