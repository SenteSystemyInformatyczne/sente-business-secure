﻿using Sente.Launcher.Lib.Portable.Interfaces.Services;
using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sente.Launcher.Tests.Services
{
  public class RegexPatternMatcherServiceTests
  {
    IRegexPatternMatcherService regexSvc = new RegexPatternMatcherService();

    static RegexPatternMatcherServiceTests()
    {
    }

    [Fact]
    public void ShouldBeAbleToUseStarWildcardForFileName()
    {
      //given
      var positiveFilenames = new string[] { "file.ext", "file", "file.", "file.with.triple.ext", "file with spaces" };
      var negativeFilenames = new string[] { "directory/", "directory\\", "\\directory", "/directory" };
      //when
      regexSvc.Prepare("*");
      //then
      foreach (var file in positiveFilenames)
        Assert.True(regexSvc.IsMatch(file));
      foreach (var file in negativeFilenames)
        Assert.False(regexSvc.IsMatch(file));
    }

    [Fact]
    public void ShouldBeAbleToUseStarDotWildcardForFileName()
    {
      //given
      var positiveFilenames = new string[] { "file.ext", "file.", "file.with.triple.ext", "file with spaces.", ".git" };
      var negativeFilenames = new string[] { "directory/", "directory\\", "\\directory", "/directory", "file" };
      //when
      regexSvc.Prepare("*.*");
      //then
      foreach (var file in positiveFilenames)
        Assert.True(regexSvc.IsMatch(file));
      foreach (var file in negativeFilenames)
        Assert.False(regexSvc.IsMatch(file));
    }

    [Fact]
    public void ShouldBeAbleToUseStarWildcardForDirectory()
    {
      //given
      var positiveFilenames = new string[] { "directory/", "directory\\" };
      var negativeFilenames = new string[] { "file.ext", "file", "file.", "file.with.triple.ext" };
      //when
      regexSvc.Prepare("*/", "*\\");
      //then
      foreach (var file in positiveFilenames)
        Assert.True(regexSvc.IsMatch(file));
      foreach (var file in negativeFilenames)
        Assert.False(regexSvc.IsMatch(file));
    }

    [Fact]
    public void ShouldBeAbleToUseDoubleStarWildcardForDirectory()
    {
      //given
      var positiveFilenames = new string[] { "directory/", "directory\\", "first\\second\\third\\", "first/second/third/" };
      var negativeFilenames = new string[] { "file.ext", "file", "file.", "file.with.triple.ext", "directory/file.txt", "directory\\file.txt" };
      //when
      regexSvc.Prepare("**/", "**\\");
      //then
      foreach (var file in positiveFilenames)
        Assert.True(regexSvc.IsMatch(file));
      foreach (var file in negativeFilenames)
        Assert.False(regexSvc.IsMatch(file));
    }

    [Fact]
    public void ShouldBeAbleToUseQuestionMarkWildcard()
    {
      //given
      var positiveFilenames = new string[] { "f", "." };
      var negativeFilenames = new string[] { "/", "\\" };
      //when
      regexSvc.Prepare("?");
      //then
      foreach (var file in positiveFilenames)
        Assert.True(regexSvc.IsMatch(file));
      foreach (var file in negativeFilenames)
        Assert.False(regexSvc.IsMatch(file));
    }

    [Fact]
    public void ShouldBeAbleToSelectAllPrt()
    {
      //given
      var positiveFilenames = new string[] {
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.prt",
        "params/esystem/report/txt/Dokummag_pkwiu.prt",
        "report/esystem/anal_vat_spr.zxv",
        "report/esystem/Txt/fk_st_es_w2.prt",
        "scon3.bpl",
        "sserver3.exe",
        "prnt.prt",
        "report.prt/esystem/cos"
      };
      
      //when
      regexSvc.Prepare("*.prt");
      var res = positiveFilenames.Where(f => regexSvc.IsMatch(f));
      var compare = positiveFilenames.Where(f => f.EndsWith("prt"));

      //then
      Assert.Equal(res.Count(), res.Intersect(compare).Count());
    }

    [Fact]
    public void ShouldBeAbleToSelectAllFilesWithESystemInPath()
    {
      //given
      var positiveFilenames = new string[] {
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.prt",
        "params/esystem/report/txt/Dokummag_pkwiu.prt",
        "report/esystem/anal_vat_spr.zxv",
        "report/esystem/Txt/fk_st_es_w2.prt",
        "scon3.bpl",
        "sserver3.exe",
        "prnt.prt",
        "report.prt/esystem/cos"
      };

      //when
      regexSvc.Prepare("**\\esystem\\**\\*");
      var res = positiveFilenames.Where(f => regexSvc.IsMatch(f));
      var compare = positiveFilenames.Where(f => f.Contains("esystem"));

      //then
      Assert.Equal(res.Count(), res.Intersect(compare).Count());
    }

    [Fact]
    public void ShouldBeAbleToSelectAllFilesWithoutExtension()
    {
      //given
      var positiveFilenames = new string[] {
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.prt",
        "params/esystem/report/txt/Dokummag_pkwiu.",
        "report/esystem/anal_vat_spr.zxv",
        "report/esystem/Txt/fk_st_es_w2.prt",
        "scon3.bpl",
        "sserver3.",
        "prnt.prt",
        "report.prt/esystem/cos"
      };

      //when
      regexSvc.Prepare("*.");
      var res = positiveFilenames.Where(f => regexSvc.IsMatch(f));
      var compare = positiveFilenames.Where(f => f.EndsWith("."));

      //then
      Assert.Equal(res.Count(), res.Intersect(compare).Count());
    }

    [Fact]
    public void ShouldBeAbleToSelectAllFilesWithManyExtensions()
    {
      //given
      var positiveFilenames = new string[] {
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.bak",
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.prt",
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.prt.bak",
        "params/esystem/report/rpt_swiadectwo_pracy_DO201705.bak.old.prt",
        "prnt.bak",
        "prnt.prt",
        "prnt.prt.bak",
        "prnt.bak.old.prt",
      };

      //when
      regexSvc.Prepare("*.*.*");
      var res = positiveFilenames.Where(f => regexSvc.IsMatch(f));
      var compare = positiveFilenames.Where(f => f.IndexOf('.')!=f.LastIndexOf('.'));

      //then
      Assert.Equal(res.Count(), res.Intersect(compare).Count());
    }
  }
}
