﻿using Sente.Launcher.Lib.Portable.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sente.Launcher.Tests.Services
{
  public class CRC32StreamTests
  {
    const string PerfectlyFineString = "2M0a1r6t1u2l0a4";
    static readonly byte[] pattern = Encoding.ASCII.GetBytes(PerfectlyFineString);
    const uint PatternCRC32 = 0x19856404U;

    [Fact]
    public void CrcShouldNotHasValueBeforeCalculations()
    {
      //given
      var sourceStream = new DevNullStream();
      var crcStream = new Crc32Stream(sourceStream);

      //then
      Assert.Null(crcStream.CRC32);
    }

    [Fact]
    public void ShouldCalculateCRC32()
    {
      //given
      var sourceStream = new MemoryStream(pattern);
      var crcStream = new Crc32Stream(sourceStream);

      //when
      var val = crcStream.Calculate();

      //then
      Assert.NotNull(crcStream.CRC32);
      Assert.Equal(val, crcStream.CRC32);
      Assert.Equal(crcStream.CRC32, PatternCRC32);
    }

    [Fact]
    public void ShouldReadStreamAndCalculateCRC32()
    {
      //given
      var sourceStream = new MemoryStream(pattern);
      var crcStream = new Crc32Stream(sourceStream);
      byte[] buffer = new byte[pattern.Length];

      //when
      var val = crcStream.Read(buffer,0,pattern.Length);

      //then
      Assert.NotNull(crcStream.CRC32);
      Assert.Equal(val, pattern.Length);
      Assert.Equal(buffer, pattern);
      Assert.Equal(crcStream.CRC32, PatternCRC32);
    }

    [Fact]
    public async Task ShouldCalculateCRC32Asynchronously()
    {
      //given
      var sourceStream = new MemoryStream(pattern);
      var crcStream = new Crc32Stream(sourceStream);

      //when
      await crcStream.CalculateAsync();

      //then
      Assert.NotNull(crcStream.CRC32);
      Assert.Equal(crcStream.CRC32, PatternCRC32);
    }

    [Fact]
    public void ShouldPositionIncrease()
    {
      //given
      var sourceStream = new MemoryStream(pattern);
      var crcStream = new Crc32Stream(sourceStream);
      byte[] buffer = new byte[pattern.Length];

      //when
      var val = crcStream.Read(buffer, 0, pattern.Length / 2);

      //then
      Assert.NotNull(crcStream.CRC32);
      Assert.Equal(val, pattern.Length / 2);
      Assert.Equal(pattern.Length / 2, crcStream.Position);
    }

    [Fact]
    public void ShouldSetPositionAndReadHalfOfBuffer()
    {
      //given
      var sourceStream = new MemoryStream(pattern);
      var crcStream = new Crc32Stream(sourceStream);
      int partSize = pattern.Length / 2;
      byte[] buffer = new byte[pattern.Length];
      byte[] patternRsult = new byte[pattern.Length];
      System.Buffer.BlockCopy(pattern, partSize, patternRsult, 0, partSize);

      //when
      crcStream.Position = partSize;
      
      //then 
      Assert.Equal(partSize, crcStream.Position);

      //when
      var val = crcStream.Read(buffer, 0, partSize);
      Assert.Equal(patternRsult, buffer);

      //then
      Assert.NotNull(crcStream.CRC32);
      Assert.Equal(val, partSize);
      Assert.Equal(buffer.Length-1, crcStream.Position);
    }

  }
}
