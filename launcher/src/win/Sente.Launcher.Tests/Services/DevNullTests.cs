﻿using Sente.Launcher.Lib.Portable.Services;
using System.Threading.Tasks;
using Xunit;

namespace Sente.Launcher.Tests.Services
{
  public class DevNullTests
  {
    [Fact]
    public void ShouldBeAbleToWriteBytes()
    {
      //given
      var buffer = new byte[1024];
      var devnull = new DevNullStream();

      //when
      devnull.Write(buffer, 0, buffer.Length);

      //then
      Assert.Equal(buffer.Length, devnull.Position);
    }

    [Fact]
    public async Task ShouldBeAbleToWriteBytesAsynchronously()
    {
      //given
      var buffer = new byte[1024];
      var devnull = new DevNullStream();

      //when
      await devnull.WriteAsync(buffer, 0, buffer.Length);

      //then
      Assert.Equal(buffer.Length, devnull.Position);
    }
  }
}
