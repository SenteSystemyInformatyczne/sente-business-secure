pipeline
{
	agent none
	stages
	{
		stage('Zadania globalne')
		{
			steps
			{
				println 'JEDI code library support set to '+jclDbgLib+' by user.'
				script
				{
					if (jclDbgLib == 'true')
					{
						jclDbgLib = 'JCLDEBUG'
					}
					else
					{
						jclDbgLib = ''
					}
				}
			}
			
		}
		stage('Przygotowanie źródeł')
		{
			agent
			{
				node
				{
					label 'WINSLAVE'
				}
			}
			steps
			{
				checkout([$class: 'GitSCM',
				branches: [[name: '*/'+srcBranch]],
				doGenerateSubmoduleConfigurations: false,
				extensions: 
				[
					[$class: 'LocalBranch'],
					[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true]
				],
				gitTool: 'Default',
				submoduleCfg: [],
				userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:sentefirm/s4.git']]])
				
				bat 'addings\\autoscripts\\s4\\prebuild.bat '+appVersion
				bat 'addings\\autoscripts\\s4\\disablejcl.bat '+jclDbgLib
			}
		}
		stage('Kompilacja')
		{
			agent
			{
				node
				{
					label 'WINSLAVE'
				}
			}
			steps
			{
				script
				{
					def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
					withEnv(['BDS=C:\\Program Files (x86)\\CodeGear\\RAD Studio\\6.0', 'Defines='+jclDbgLib+'']) 
					{
						bat "\"${msb}\" /v:q eSystem.groupproj"
					}
					writeFile file: 'version', text: appVersion
				}
			}
		}
		stage('Archiwizacja artefaktów')
		{
			agent
			{
				node
				{
					label 'WINSLAVE'
				}
			}
			steps
			{
				archiveArtifacts artifacts: 'version,Out/**', caseSensitive: false, defaultExcludes: false, excludes: 'Out/migracja/,Out/obj3/,Out/*.map,Out/*.tds,Out/*.bpi', onlyIfSuccessful: true
			}
		}
		stage('Sprzątanie')
		{
			agent
			{
				node
				{
					label 'WINSLAVE'
				}
			}
			steps
			{
				cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
				dir(env.WORKSPACE + '@tmp')
				{
					deleteDir()
				}
			}
		}
	}
}