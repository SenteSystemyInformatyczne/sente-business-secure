node('master') {
    stage('Inicjalizacja repozytorium SenteS4') {
        checkout([$class: 'GitSCM', branches: [[name: 'jenkins_autocommit']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'LocalBranch'], [$class: 'SparseCheckoutPaths', sparseCheckoutPaths: [[path: 'Out/*']]]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:sentefirm/s4.git']]])
    }

    stage('Zgrywanie zmian z aplikacji firmowej') {
        sh '''
            { set +x; } 2> /dev/null

            # Database config file
            mkdir -p Out/dbi && rsync -avm --delete --include="esystem.dbi" --include="esystem_min.dbi" --exclude="*" /mnt/esystem/dbi/ Out/dbi

            # Database admin config file
            mkdir -p Out/params/dbi && rsync -avm --delete /mnt/esystem/params/dbi/esystem.dbi Out/params/dbi

            # Report settings file
            mkdir -p Out/report && rsync -avm --delete /mnt/esystem/report/esystem.rpt Out/report

            # Report and report txt templates
            mkdir -p Out/report/esystem && rsync -avm --delete --include="*/" --include="*.prt" --include="*.rpt" --exclude="*" /mnt/esystem/report/esystem/ Out/report/esystem

            # Report admin settings file
            mkdir -p Out/params/esystem && rsync -avm --delete /mnt/esystem/params/esystem/esystem.rpt Out/params/esystem

            # Report and report txt admin templates
            mkdir -p Out/params/esystem/report && rsync -avm --delete --include="*/" --include="*.prt" --include="*.rpt" --exclude="*" /mnt/esystem/params/esystem/report/ Out/params/esystem/report
        '''
    }

    stage('Wykrywanie niescalonych plików') {
        sh '''
            { set +x; } 2> /dev/null
            if [ -n "$(git ls-files -u)" ]; then
              echo "There were unmerged files detected.";
              exit 1;
            else
              echo "No unmerged files detected.";
            fi
            '''
    }

    stage('Zatwierdzanie i przesyłanie zmian do repozytorium zdalnego') {
        withCredentials([usernamePassword(credentialsId: 'gitlab-Jenkins-pass', passwordVariable: 'gitpass', usernameVariable: 'gituser')]) {
            sh '''
                { set +x; } 2> /dev/null
                if [ -n "$(git status --porcelain | tail)" ]; then
                  echo "There were some changes detected.";
                  git status --porcelain;
                  if [ "''' + test_mode + '''" = "0" ]; then
                    git add .;
                    git commit -m "$JOB_NAME $(date +"%Y-%m-%d_%H-%M-%S")";
                    git push "http://${gituser}:${gitpass}@git.office.sente.pl/sentefirm/s4.git" "jenkins_autocommit";
                    echo "All changes have been successfully pushed to remote repository.";
                  else
                    echo "Test mode completed. No modifications have been made to the remote repository.";
                  fi
                else
                  echo "No changes detected. No modifications have been made to the remote repository.";
                fi
                '''
        }
    }

    stage('Sprzątanie') {
        cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
        dir(env.WORKSPACE + '@tmp') {
            deleteDir()
        }
    }
}