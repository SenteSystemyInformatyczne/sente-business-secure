pipeline {
	agent none
	stages {
		stage('Define exclude lists') {
			steps {
				script {
					excS4Files = "*.lnk *.log *.txt"
					excS4Dirs = "!Dev_20161007 !kosz cseditor do_zus doc expimp Files JPK kopia launcher logs migracja platnik przelewy *SenteUpdater* temp test"
					excNeosFiles = "*.log *.pass"
					excNeosDirs = "archive bin customprojects dbi Download EDE libraries logs media Neos.Plugins Neos.Projects projects stats"
				}
			}
		}
		stage('Prepare environment') {
			agent {
				label 'WINSLAVE'
			}
			steps {
				script {
					if(fileExists("u:\\")) {
						bat 'net use u: /delete /y'
					}
					if(fileExists("y:\\")) {
						bat 'net use y: /delete /y'
					}
					if(fileExists("p:\\")) {
						bat 'net use p: /delete /y'
					}
					if(fileExists("q:\\")) {
						bat 'net use q: /delete /y'
					}
					if(fileExists("r:\\")) {
						bat 'net use r: /delete /y'
					}
					if(fileExists("t:\\")) {
						bat 'net use t: /delete /y'
					}
				}
				withCredentials([usernamePassword(credentialsId: '7515c7d4-2baf-4daa-a9ab-8958959e296f', passwordVariable: 'hypPass', usernameVariable: 'hypUser')]) {
					withCredentials([usernamePassword(credentialsId: 'bb65b8f1-0a98-4fb5-8d8d-d00feafb76cd', passwordVariable: 'jPass', usernameVariable: 'jUser')]) {
						bat 'net use u: \\\\10.168.0.168\\firmupdate /user:' + hypUser + ' ' + hypPass + ' /persistent:yes'
						bat 'net use y: ' + src_s4 + ' /user:' + jUser + ' ' + jPass + ' /persistent:yes'
						bat 'net use p: ' + src_neos_sente + ' /user:' + jUser + ' ' + jPass + ' /persistent:yes'
						bat 'net use q: ' + src_neos_prodsched + ' /user:' + jUser + ' ' + jPass + ' /persistent:yes'
						bat 'net use r: ' + src_neos_prodworkflow + ' /user:' + jUser + ' ' + jPass + ' /persistent:yes'
						bat 'net use t: ' + src_neos_prodklient + ' /user:' + jUser + ' ' + jPass + ' /persistent:yes'
					}
				}
			}
		}
		stage('Backup') {
            agent {
                node {
                    label 'WINSLAVE'
                }
            }
            steps {
                parallel(
                    'S4': {
						bat "mkdir u:\\s4_backup_${bak_suffix}"
                        bat '''robocopy y:\\ u:\\s4_backup_''' + bak_suffix + ''' /E /R:1 /XF ''' + excS4Files + ''' /XD ''' + excS4Dirs + '''
	                        if %ERRORLEVEL% lss 8 goto :eof'''
                    },
                    'Neos (sente)': {
                    	bat "mkdir u:\\neos_sente_backup_${bak_suffix}"
						bat '''robocopy p:\\ u:\\neos_sente_backup_''' + bak_suffix + ''' /E /R:1 /XF ''' + excNeosFiles + ''' /XD ''' + excNeosDirs + '''
	                        if %ERRORLEVEL% lss 8 goto :eof'''
                    },
					'Neos (prodsched)': {
                    	bat "mkdir u:\\neos_prodsched_backup_${bak_suffix}"
						bat '''robocopy q:\\ u:\\neos_prodsched_backup_''' + bak_suffix + ''' /E /R:1 /XF ''' + excNeosFiles + ''' /XD ''' + excNeosDirs + '''
	                        if %ERRORLEVEL% lss 8 goto :eof'''
                    },
					'Neos (prodworkflow)': {
						bat "mkdir u:\\neos_prodworkflow_backup_${bak_suffix}"
						bat '''robocopy r:\\ u:\\neos_prodworkflow_backup_''' + bak_suffix + ''' /E /R:1 /XF ''' + excNeosFiles + ''' /XD ''' + excNeosDirs + '''
	                        if %ERRORLEVEL% lss 8 goto :eof'''
                    },
					'Neos (prodklient)': {
						bat "mkdir u:\\neos_prodklient_backup_${bak_suffix}"
						bat '''robocopy t:\\ u:\\neos_prodklient_backup_''' + bak_suffix + ''' /E /R:1 /XF ''' + excNeosFiles + ''' /XD ''' + excNeosDirs + '''
	                        if %ERRORLEVEL% lss 8 goto :eof'''
                    }
                )
            }
        }
	}
	post {
        failure {
			node('WINSLAVE') {
				bat 'net use u: /delete /y'
				bat 'net use y: /delete /y'
				bat 'net use p: /delete /y'
				bat 'net use q: /delete /y'
				bat 'net use r: /delete /y'
				bat 'net use t: /delete /y'
			}
        }
		success {
			node('WINSLAVE') {
				bat 'net use u: /delete /y'
				bat 'net use y: /delete /y'
				bat 'net use p: /delete /y'
				bat 'net use q: /delete /y'
				bat 'net use r: /delete /y'
				bat 'net use t: /delete /y'
				cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, cleanWhenUnstable: false, deleteDirs: true, notFailBuild: true
				dir(env.WORKSPACE + '@tmp')	{
					deleteDir()
				}
			}
		}
    }
}