node ("WINSLAVE") {
    dir(bak_path) {
        stage ("Konfiguracja środowiska") {
            if(fileExists("u:\\")){
                bat 'net use u: /delete /Y'
            }
            if(fileExists("y:\\")){
                bat 'net use y: /delete /Y'
            }
            if(fileExists("z:\\")){
                bat 'net use z: /delete /Y'
            }
        }
        withCredentials([usernamePassword(credentialsId: 'bb65b8f1-0a98-4fb5-8d8d-d00feafb76cd', passwordVariable: 'jenkins_pass', usernameVariable: 'jenkins_user')]) {
            bat 'net use u: \\\\10.168.0.168\\firmupdate /USER:dbr Sente.667 /persistent:yes'
            bat 'net use y: '+s4_path+' /USER:'+jenkins_user+' '+jenkins_pass+' /persistent:yes'
			
            parallel (
                s4: {
                    stage ("Restore klienta S4") {
                        bat 'xcopy "u:\\s4_backup_'+bak_suffix+'\\*" "y:\\" /s /i /y > s4_restore_'+bak_suffix+'.log'
                    }
                },
                neos: {
                    stage ("Restore serwera Neos (sente)") {
						bat 'net use z: '+neos_sente_path+' /USER:'+jenkins_user+' '+jenkins_pass+' /persistent:yes'
						bat 'xcopy "u:\\neos_sente_backup_'+bak_suffix+'\\*" "z:\\" /s /i /y > neos_sente_restore_'+bak_suffix+'.log'
						bat 'net use z: /delete /Y'
                    }
                    stage ("Restore serwera Neos (prodsched)") {
						bat 'net use z: '+neos_prodsched_path+' /USER:'+jenkins_user+' '+jenkins_pass+' /persistent:yes'
						bat 'xcopy "u:\\neos_prodsched_backup_'+bak_suffix+'\\*" "z:\\" /s /i /y > neos_prodsched_restore_'+bak_suffix+'.log'
						bat 'net use z: /delete /Y'
                    }
                    stage ("Restore serwera Neos (prodworkflow)") {
						bat 'net use z: '+neos_prodworkflow_path+' /USER:'+jenkins_user+' '+jenkins_pass+' /persistent:yes'
						bat 'xcopy "u:\\neos_prodworkflow_backup_'+bak_suffix+'\\*" "z:\\" /s /i /y > neos_prodworkflow_restore_'+bak_suffix+'.log'
						bat 'net use z: /delete /Y'
                    }
					stage ("Restore serwera Neos (prodklient)") {
						bat 'net use z: '+neos_prodklient_path+' /USER:'+jenkins_user+' '+jenkins_pass+' /persistent:yes'
						bat 'xcopy "u:\\neos_prodklient_backup_'+bak_suffix+'\\*" "z:\\" /s /i /y > neos_prodklient_restore_'+bak_suffix+'.log'
						bat 'net use z: /delete /Y'
                    }
				}
            )
            
            bat 'net use u: /delete /Y'
            bat 'net use y: /delete /Y'
        }
    }
}