node('WINSLAVE') {
	stage('Dystrybucja S4') {
		withCredentials([usernamePassword(credentialsId: '7515c7d4-2baf-4daa-a9ab-8958959e296f', passwordVariable: 'hyppass', usernameVariable: 'hypuser')]) {
            //Unmount q drive if exists
			if(fileExists("q:\\")) {
                bat 'net use q: /delete /Y'
            }
            bat 'net use q: \\\\10.168.0.168\\download '+hyppass+' /USER:'+hypuser+' /persistent:yes'                    
            //Determine S4 version
			copyArtifacts filter: 'version', projectName: 'SenteFirm.S4Firm.Build', selector: lastSuccessful()
			def appVer = readFile encoding: 'UTF-8', file: 'version'
            println 'S4 version := '+appVer+' (from artifacts archive)'
            //Remove previous S4 repository if exists
			bat 'if exist q:\\DBR\\S4Firm\\'+appVer+' (rmdir /s /q q:\\DBR\\S4Firm\\'+appVer+')'
            //Copy S4 from last successful build
			copyArtifacts filter: 'Out/**', projectName: 'SenteFirm.S4Firm.Build', selector: lastSuccessful(), target: 'q:\\DBR\\S4Firm\\'+appVer+'\\'
			bat '''@echo off
			    robocopy q:\\DBR\\S4Firm\\'''+appVer+'''\\Out\\ q:\\DBR\\S4Firm\\'''+appVer+'''\\ /move /e
                if %ERRORLEVEL% lss 8 echo Robocopy exited with code %ERRORLEVEL%. Visit https://ss64.com/nt/robocopy-exit.html for more details.'''
            //Generate files.crc
			bat 'q:\\DBR\\LauncherCLI\\Sente.Launcher.Manager.Portable.exe -TargetDir=file://q:\\DBR\\S4Firm\\'+appVer+'\\'
            //Update repository url in config.json
            def cfgSrc = readFile encoding: 'UTF-8', file: 'q:\\Clients\\SenteFirm\\config.json'
            cfgSrc = cfgSrc.replaceAll(/(\d+\.)+(\d+)/, appVer)
            writeFile encoding: 'UTF-8', file: 'q:\\Clients\\SenteFirm\\config.json', text: cfgSrc
            //Unmount q drive
            bat 'net use q: /delete /Y'
		}
	}

	stage('Sprzątanie') {
        cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
        dir(env.WORKSPACE + '@tmp') {
            deleteDir()
        }
    }
}