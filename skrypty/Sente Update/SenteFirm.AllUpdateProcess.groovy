String sufix=new Date().format('yyyy-MM-dd_hh-mm-ss')
println 'Sufiks plików backupu: $sufix'

node('master') {
	try {
		stage('Weryfikacja spójności bazy danych ze skryptami') {
			build job: 'SenteFirm.DBVsRepositoryIntegrityCheck', parameters: [string(name: 'dbscripts_branch', value: 'jenkins_autocommit'), string(name: 'db_host', value: db_host), string(name: 'db_path', value: db_path)]
		}
		
		stage('Wyłączanie usług') {
			try{
				svcDown()
			} catch(err) {
				echo "Błąd wyłączenia usług."
				throw(err)
			}
		}
		
		stage('Odłączanie użytkowników bazy danych i aplikacji') {
			try {
				withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
					writeFile encoding: 'UTF-8', file: 'systerminate.sql', text: systerminateSql()
					def isqlcmd = "isql-fb -b -ch WIN1250 -s 3 -u ${dbuser} -p ${dbpass} ${db_host}:${db_path} -i systerminate.sql"
					runCmd(isqlcmd)
					sleep 10 // czekamy 10s, żeby dać czas na odłączenie się aplikacji
					
					def cmd = "gfix -user ${dbuser} -password ${dbpass} -shut single -force 0 ${db_host}:${db_path}"
					runCmd(cmd)
				}
			} catch(err) {
				echo "Błąd odłączania użytkowników bazy danych."
				throw(err)
			}
		}
		
		stage('Backup bazy danych') {
			try {
				withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
					def cmd = "gbak -v -user ${dbuser} -password ${dbpass} ${db_host}:${db_path} /mnt/firmupdate/sente_backup_${sufix}.fbk > sente_backup_${sufix}.log"
					runCmd(cmd)
				}
			} catch(err) {
				echo "Błąd wykonywania backupu bazy danych."
				throw(err)
			}
		}
		
		stage('Wyłączanie bazy danych') {
			try {
				withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
					dir('database') {
						writeFile encoding: 'UTF-8', file: 'DelAttachments.sql', text: delAttachments()
						def isqlcmd = "isql-fb -b -ch WIN1250 -s 3 -u ${dbuser} -p ${dbpass} ${db_host}:${db_path} -i DelAttachments.sql"
						runCmd(isqlcmd)
					}
					def gfixcmd = "gfix -user ${dbuser} -password ${dbpass} -shut full -force 0 ${db_host}:${db_path}"
					runCmd(gfixcmd)
				}
			} catch(err) {
				echo "Błąd wyłączania bazy danych."
				throw(err)
			}
		}

		parallel db_restore: {
			stage('Odgbak bazy i sprawdzenie integralności danych') {
				node('FIREBIRD') {
					withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
						def cmd = "gbak -v -r o -user ${dbuser} -password ${dbpass} /mnt/firmupdate/sente_backup_${sufix}.fbk 10.168.6.200:/mnt/db/restore_sente.fdb>dbintegrity.log"
						runCmd(cmd)
					}
				}
				
				node('WINSLAVE') {
					checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'autoscripts']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:dro/autoscripts.git']]])
					withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
						def ibecmd = "IBEScript -D\"10.168.6.200:/mnt/db/restore_sente.fdb\" -U\"${dbuser}\" -P\"${dbpass}\" -L3 -C\"WIN1250\""
					}
					bat "${ibecmd} AutoScripts\\database\\IBERecompileAllProcedure.sql"
					bat "${ibecmd} AutoScripts\\database\\IBERecompileAllTrigger.sql"
				}
			}
		}, s4_firm: {
			build job: 'SenteFirm.APPAutoCommit', parameters: [string(name: 'test_mode', value: '0')]
			build job: 'SenteFirm.DBIRPTAutoCommit', parameters: [string(name: 'test_mode', value: '0')]
			build job: 'SenteFirm.S4Firm.Build', parameters: [string(name: 'srcBranch', value: dbscripts_branch), string(name: 'appVersion', value: s4_ver), booleanParam(name: 'jclDbgLib', value: true)]
		},
		failFast: true
		
		node('WINSLAVE') {
			stage('Aktualizacja skryptów bazy') {
				if(fileExists('dbscripts')) {
					bat 'rmdir /s /q dbscripts'
				}
				checkout([$class: 'GitSCM', branches: [[name: dbscripts_branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dbscripts'], [$class: 'CleanBeforeCheckout']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:sentefirm/s4.git']]])
			}
			
			stage('Aktualizacja Signature Helpera') {
				def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
				checkout([$class: 'GitSCM', branches: [[name: 'rc']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'sh']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:dro/signaturehelper.git']]])
				dir('sh') {
					bat '.nuget\\NuGet restore'
				}
				dir('sh/shtool') {
					bat "\"${msb}\" shtool.csproj /m /nr:false"
				}
				
				withEnv(['PATH+=C:\\Program Files (x86)\\Firebird\\Firebird_2_5\\bin;C:\\Program Files (x86)\\HK-Software\\IBExpert']) {
					dir('database') {
						stage('Konfiguracja skryptów') {
							withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
								writeFile encoding: 'UTF-8', file: 'CreateNewDB.sql', text: createNewDBcreateNewDB(dbuser, dbpass)
							}
							writeFile encoding: 'UTF-8', file: 'ADDSYSTables.sql', text: addSysTables()
						}
						stage('Tworzenie pustej bazy') {
							node('FIREBIRD') {
								if(fileExists('/mnt/db/FIRMMETA.FDB')) {
									sh 'rm /mnt/db/FIRMMETA.FDB'
								}
							}
							bat 'IBEScript CreateNewDB.sql'
							withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
								bat "IBEScript ADDSYSTables.sql -D\"10.168.6.200:/mnt/db/FIRMMETA.FDB\" -U\"${dbuser}\" -P\"${dbpass}\" -L3 -C\"WIN1250\""
							}
						}
					}
					
					stage('Tworzenie metadanych') {
						withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
							bat "sh\\Out\\Net35\\shtool.exe --src=dbscripts\\dbscripts --gen -dbhost 10.168.6.200 -dbpath /mnt/db/restore_sente.fdb -dblogin ${dbuser} -dbpass ${dbpass}"
						}
						dir('dbscripts\\dbscripts') {
							bat 'cp *.sql ..\\..\\'
						}
					}
					
					stage('Wgrywanie metadanych') {
						withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
							bat "IBEScript esystem.sql -D\"10.168.6.200:/mnt/db/FIRMMETA.FDB\" -U\"${dbuser}\" -P\"${dbpass}\" -L3 -C\"WIN1250\""
						}
					}
					
					stage('Wgrywanie transferówek TRB') {
					    withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
							bat "IBEScript before.esystem.sql -D\"10.168.6.200:/mnt/db/FIRMMETA.FDB\" -U\"${dbuser}\" -P\"${dbpass}\" -L3 -C\"WIN1250\""
						}
					}
				}
			}
			
			stage('Tworzenie skryptu różnicowego') {
				withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
					writeFile encoding: 'UTF-8', file: 'IBECompareWORK.sql', text: ibeCompare(dbuser, dbpass)
				}
				withEnv(['PATH+=C:\\Program Files (x86)\\Firebird\\Firebird_2_5\\bin;C:\\Program Files (x86)\\HK-Software\\IBExpert']) {
					bat 'IBEScript IBECompareWORK.sql -V"IBECompareWORK.log"'
				}
			}
			
			stage('Postprocessing skryptu różnicowego') {
				bat 'sh\\Out\\Net35\\shtool.exe --postprocess=FirmDiffDB.sql'
			}
			
			stage('Aktualizacja kopii bazy') {
				withEnv(['PATH+=C:\\Program Files (x86)\\HK-Software\\IBExpert']) {
					withCredentials([usernamePassword(credentialsId: 'Firebird-node-db-pass', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
						def isqlcmd = "isql -b -ch WIN1250 -s 3 -u ${dbuser} -p \"${dbpass}\" 10.168.6.200:/mnt/db/restore_sente.fdb"
						bat "${isqlcmd} -i before.esystem.sql"
						bat "IBEScript postdiffscript.sql -D\"10.168.6.200:/mnt/db/restore_sente.fdb\" -P\"${dbpass}\" -U\"${dbuser}\" -L3 -C\"WIN1250\""
						bat "${isqlcmd} -i after.esystem.sql"
						bat "${isqlcmd} -i app.sql"
					}
				}
			}
		}
	} catch(err) {
		echo "Caught: ${err}"
		withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
			def cmd = "gfix -user ${dbuser} -password ${dbpass} -online ${db_host}:${db_path}"
			runCmd(cmd)
		}
		try {
			svcUp()
		} catch(er) {
			echo "Błąd włączenia usług."
			echo er
		}
		throw(err)
	}
	
	try {
		stage('Aktualizacja bazy') {
			withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
				def cmd = "gfix -user ${dbuser} -password ${dbpass} -online single ${db_host}:${db_path}"
				runCmd(cmd)
				node('WINSLAVE') {
					withEnv(['PATH+=C:\\Program Files (x86)\\HK-Software\\IBExpert']) {
						def isqlcmd = "isql -b -ch WIN1250 -s 3 -u \"${dbuser}\" -p \"${dbpass}\" ${db_host}:${db_path}"
						bat "${isqlcmd} -i before.esystem.sql"
						bat "IBEScript postdiffscript.sql -D\"${db_host}:${db_path}\" -P\"${dbpass}\" -U\"${dbuser}\" -L3 -C\"WIN1250\""
						bat "${isqlcmd} -i after.esystem.sql"
						bat "${isqlcmd} -i app.sql"
					}
				}
			}
		}
	} catch(err) {
		echo "Caught: ${err}"
		stage('Przywracanie bazy danych') {
			node('FIREBIRD') {
				withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
					def cmd = "gbak -v -r o -user ${dbuser} -password ${dbpass} /mnt/firmupdate/sente_backup_${sufix}.fbk ${db_host}:${db_path}>dbintegrity.log"
					runCmd(cmd)
				}
			}
		}
		try {
			svcUp()
		} catch(er) {
			echo "Błąd włączenia usług."
			echo er
		}
		throw(err)
	}
	
	try {
		parallel neosAndS4: {
			stage('Backup programów S4 i Neos') {
				build job: 'SenteFirm.ProgramBackup', parameters: [string(name: 'src_s4', value: src_s4), string(name: 'src_neos_sente', value: src_neos_sente), string(name: 'src_neos_prodsched', value: src_neos_prodsched), string(name: 'src_neos_prodworkflow', value: src_neos_prodworkflow), string(name: 'src_neos_prodklient', value: src_neos_prodklient), string(name: 'bak_suffix', value: sufix)]
			}
			stage('Aktualizacja programów S4 i Neos') {
				build job: 'SenteFirm.UpdateS4Neos', parameters: [string(name: 'neos_ver', value: neos_ver), string(name: 'neos_rel', value: neos_rel)]
			}
		}, launcher: {
			stage('Aktualizacja repozytorium S4 dla aplikacji Sente Launcher') {
				build job: 'SenteFirm.LauncherRepoUpdate'
			}
		}
	} catch(err) {
		echo "Caught: ${err}"
		stage('Próba przywrócenia starej wersji'){
			build job: 'SenteFirm.ProgramRestore', parameters: [string(name: 'bak_suffix', value: sufix), string(name: 's4_path', value: src_s4), string(name: 'neos_sente_path', value: src_neos_sente), string(name: 'neos_prodsched_path', value: src_neos_prodsched), string(name: 'neos_prodworkflow_path', value: src_neos_prodworkflow), string(name: 'neos_prodklient_path', value: src_neos_prodklient)]
		}
	}
	
	stage('Włączenie bazy danych') {
		try {
			withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
				def cmd = "gfix -user ${dbuser} -password ${dbpass} -online ${db_host}:${db_path}"
				runCmd(cmd)
			}
		} catch(err) {
			echo "Wystąpił problem z włączaniem bazy."
			echo err
		}
	}
	
    stage('Włączanie usług') {
        try {
            svcUp()
        } catch(err) {
            echo "Błąd włączenia usług."
            throw(err)
        }
    }
}

def sprint(branchName) { //Parametr do przekazywania początku nazwy brancha
    def res = bat returnStdout: true, script: 'git branch -r' //Komenda do uruchomienia
    def max = 0 //Zmienna przechowująca najwyższy numer sprintu
    def branches = res.split("\n") //Odczytanie wyniku wykonania komendy do tablicy
    for (int i = 0; i < branches.size(); i++) { //Iterujemy po tablicy branchy
        def m = branches[i] =~ /\/${branchName}[0-9]+/ //Matchujemy według wzorca
        if (m) {
            def num = m.group().substring(branchName.length()+1).toInteger() //Wyciągnięcie numeru sprintu i konwersja do inta
            if(num > max){
                max = num //Przypisanie najwyższej wartości do max
            }
        }
    }
    return branchName + max.toString() //Utworzenie wartości wynikowej
}

def runCmd(cmd) {
  if(isUnix()) {
    sh cmd
  } else {
    bat cmd
  }
}

def executeService(svc,name,action,waitingFor,timeOut) {
    withCredentials([string(credentialsId: 'TokenKey', variable: 'secret')]) {
        def token = getToken(svc,secret)
        new URL("${svc}/execute/${name}/${token}?action=${action}").getText()
        timeout(timeOut){ //timeout w minutach
            waitUntil {
                sleep 1
                def status = new URL("${svc}/${name}/report").getText()
                return (status == waitingFor)
            }
        }
    }
}

def getToken(svc,secret) {
    def plainToken = new URL("${svc}/token").getText()
    def toHash = plainToken+secret
  	def hashedToken = java.security.MessageDigest.getInstance("SHA-256").digest(toHash.bytes)
    def result = ""
  	for(def i = 0; i < hashedToken.length; i++) {
  		result += String.format("%02x", hashedToken[i])
    }
  	echo "getToken result: "+result
  	return result
}

def svcUp() {
    parallel (
        taskman: {
            //Neos - Nowy Taskman
	        executeService("http://10.168.1.248:31234","StartNeos","start","Running",5)
        },
        scheduler: {
            //Neos - Scheduler, S4
	        executeService("http://10.168.1.248:31235","StartNeos","start","Running",5)
        },
        workflow: {
            //Neos - Workflow
        	executeService("http://10.168.1.248:31236","StartNeos","start","Running",5)
        },
        taskmanclient:{
            //Neos - Nowy Taskman dla klientów	
            executeService("http://10.168.1.248:31237","StartNeos","start","Running",5)
        }
        )
    build job: 'SenteFirm.Rundeck.Rekrutacja', parameters: [string(name: 'job_param_action', value: 'wlacz')]
    build job: 'SenteFirm.Rundeck.Taskman', parameters: [string(name: 'job_param_action', value: 'odblokuj')]
    build job: 'SenteFirm.Rundeck.Cron', parameters: [string(name: 'job_param_action', value: 'wlacz')]
}

def svcDown() {
    parallel (
        taskman: {
            //Neos - Nowy Taskman
            executeService("http://10.168.1.248:31234","StartNeos","stop","Stopped",1)
        },
        scheduler: {
            //Neos - Scheduler, S4
	        executeService("http://10.168.1.248:31235","StartNeos","stop","Stopped",1)
        },
        workflow: {
            //Neos - Workflow
	        executeService("http://10.168.1.248:31236","StartNeos","stop","Stopped",1)
        },
        taskmanclient: {
            //Neos - Nowy Taskman dla klientów
	        executeService("http://10.168.1.248:31237","StartNeos","stop","Stopped",1)
        }
        )
    build job: 'SenteFirm.Rundeck.Rekrutacja', parameters: [string(name: 'job_param_action', value: 'wylacz')]
    build job: 'SenteFirm.Rundeck.Taskman', parameters: [string(name: 'job_param_action', value: 'zablokuj')]
    build job: 'SenteFirm.Rundeck.Cron', parameters: [string(name: 'job_param_action', value: 'wylacz')]
}

def ibeCompare(dbuser, dbpass) {
    return '''execute ibeblock
    as
    begin
      cbb = \'execute ibeblock (LogMessage variant)
             as
             begin
               ibec_progress(LogMessage);
             end\';

      SourceDB = ibec_CreateConnection(__ctInterBase, \'DBName="10.168.6.200:/mnt/db/FIRMMETA.FDB";
                                       ClientLib=gds32.dll;
                                       User=''' + dbuser + '''; Password=''' + dbpass + '''; Names=WIN1250; SqlDialect=3\');
      TargetDB = ibec_CreateConnection(__ctInterBase, \'DBName="10.168.6.200:/mnt/db/restore_sente.fdb";
                                       ClientLib=gds32.dll;
                                       User=''' + dbuser + '''; Password=''' + dbpass + '''; Names=WIN1250; SqlDialect=3\');
      try
        ibec_CompareMetadata(SourceDB, TargetDB, \'FirmDiffDB.sql\',
                             \'ServerVersion=FB25;
    						  OmitUDFs;
                              OmitChecks;
                              IgnoreColumnPositions;
                              IgnoreIBEObjects\',
                             cbb);
      finally
        ibec_CloseConnection(TargetDB);
        ibec_CloseConnection(SourceDB);
      end;
    end;'''
}

def createNewDB(dbuser, dbpass) {
    return '''/******************************************************************************/
    /****         Generated by IBExpert 2011.06.29 11-04-2016 15:04:23         ****/
    /******************************************************************************/

    SET SQL DIALECT 3;

    SET NAMES WIN1250;

    CREATE DATABASE \'10.168.6.200:/mnt/db/FIRMMETA.FDB\'
    USER \'''' + dbuser + '''\' PASSWORD \'''' + dbpass + '''\'
    PAGE_SIZE 16384
    DEFAULT CHARACTER SET WIN1250 COLLATION WIN1250;



    /******************************************************************************/
    /****                        User Defined Functions                        ****/
    /******************************************************************************/

    DECLARE EXTERNAL FUNCTION "ABS"
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'IB_UDF_abs\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "ASCII_VAL"
    CHAR(1)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'IB_UDF_ascii_val\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "BIN_AND"
    INTEGER,
    INTEGER
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'IB_UDF_bin_and\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "BIN_OR"
    INTEGER,
    INTEGER
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'IB_UDF_bin_or\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION B_LONGSUBSTR
    BLOB,
    INTEGER,
    INTEGER
    RETURNS CSTRING(16383) FREE_IT
    ENTRY_POINT \'fn_b_longsubstr\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION DATETOSTR
    TIMESTAMP,
    CSTRING(255)
    RETURNS CSTRING(255)
    ENTRY_POINT \'fn_datetostr\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION DIV
    INTEGER,
    INTEGER
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'IB_UDF_div\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "FLOOR"
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'IB_UDF_floor\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION I64ROUND
    NUMERIC(18,4) BY DESCRIPTOR,
    NUMERIC(18,4) BY DESCRIPTOR
    RETURNS PARAMETER 2
    ENTRY_POINT \'fbround\' MODULE_NAME \'fbudf\';


    DECLARE EXTERNAL FUNCTION INCDATETIME
    TIMESTAMP,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER,
    INTEGER
    RETURNS TIMESTAMP
    ENTRY_POINT \'fn_incdatetime\' MODULE_NAME \'rfunc\';

    DECLARE EXTERNAL FUNCTION LONGSTRREPLACE
    CSTRING(16384),
    CSTRING(16384),
    CSTRING(16384)
    RETURNS CSTRING(16384) FREE_IT
    ENTRY_POINT \'fn_longstrreplace\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION LONGSUBSTR
    CSTRING(16383),
    INTEGER,
    INTEGER
    RETURNS CSTRING(16383)
    ENTRY_POINT \'fn_substr\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION MD5SUM
    CSTRING(16383)
    RETURNS CSTRING(32) FREE_IT
    ENTRY_POINT \'fn_md5sum\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION "MOD"
    INTEGER,
    INTEGER
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'IB_UDF_mod\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "POWER"
    DOUBLE PRECISION,
    DOUBLE PRECISION
    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'fn_power\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION "RAND"

    RETURNS DOUBLE PRECISION BY VALUE
    ENTRY_POINT \'IB_UDF_rand\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION "ROUND"
    INTEGER BY DESCRIPTOR,
    INTEGER BY DESCRIPTOR
    RETURNS PARAMETER 2
    ENTRY_POINT \'fbround\' MODULE_NAME \'fbudf\';


    DECLARE EXTERNAL FUNCTION RTRIM
    CSTRING(255)
    RETURNS CSTRING(255) FREE_IT
    ENTRY_POINT \'IB_UDF_rtrim\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION SHASH_HMAC
    CSTRING(6),
    CSTRING(25),
    CSTRING(1024),
    CSTRING(512)
    RETURNS CSTRING(512) FREE_IT
    ENTRY_POINT \'fb_shash_hmac\' MODULE_NAME \'fb_shash\';


    DECLARE EXTERNAL FUNCTION SHASH_HMAC_BLOB
    CSTRING(6),
    CSTRING(25),
    BLOB,
    CSTRING(512)
    RETURNS CSTRING(512) FREE_IT
    ENTRY_POINT \'fb_shash_hmac_blob\' MODULE_NAME \'fb_shash\';


    DECLARE EXTERNAL FUNCTION STRFUZZYCMP
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'SENTE_UDF_strfuzzycmp\' MODULE_NAME \'sente_udf\';


    DECLARE EXTERNAL FUNCTION STRFUZZYCMP2
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(100),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'SENTE_UDF_strfuzzycmp2\' MODULE_NAME \'sente_udf\';


    DECLARE EXTERNAL FUNCTION STRFUZZYCMP5
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(10240),
    CSTRING(100),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'SENTE_UDF_strfuzzycmp5\' MODULE_NAME \'sente_udf\';


    DECLARE EXTERNAL FUNCTION STRLEN
    CSTRING(32767)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'IB_UDF_strlen\' MODULE_NAME \'ib_udf\';


    DECLARE EXTERNAL FUNCTION STRMULTICMP
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'SENTE_UDF_strmulticmp\' MODULE_NAME \'sente_udf\';


    DECLARE EXTERNAL FUNCTION STRMULTICMPAND
    CSTRING(10240),
    CSTRING(10240)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'SENTE_UDF_strmulticmpand\' MODULE_NAME \'sente_udf\';


    DECLARE EXTERNAL FUNCTION STRPOS
    CSTRING(16384),
    CSTRING(16384)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT \'fn_strpos\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION STRREPLACE
    CSTRING(256),
    CSTRING(256),
    CSTRING(256)
    RETURNS CSTRING(256) FREE_IT
    ENTRY_POINT \'fn_strreplace\' MODULE_NAME \'rfunc\';


    DECLARE EXTERNAL FUNCTION SUBSTR
    CSTRING(1024),
    SMALLINT,
    SMALLINT
    RETURNS CSTRING(1024) FREE_IT
    ENTRY_POINT \'IB_UDF_substr\' MODULE_NAME \'ib_udf\';




    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    DESCRIBE FUNCTION DATETOSTR
    \'PR45221\';

    DESCRIBE FUNCTION LONGSTRREPLACE
    \'PR20279\';

    DESCRIBE FUNCTION LONGSUBSTR
    \'PR22974\';

    DESCRIBE FUNCTION SHASH_HMAC
    \'PR37632\';

    DESCRIBE FUNCTION SHASH_HMAC_BLOB
    \'PR37632\';

    DESCRIBE FUNCTION STRFUZZYCMP
    \'PR67927\';

    DESCRIBE FUNCTION STRFUZZYCMP2
    \'PR67927\';

    DESCRIBE FUNCTION STRFUZZYCMP5
    \'PR67927\';

    /******************************************************************************/
    /****                                Tables                                ****/
    /******************************************************************************/


    CREATE GENERATOR IBE$VERSION_HISTORY_ID_GEN;

    CREATE TABLE IBE$VERSION_HISTORY (
    IBE$VH_ID           INTEGER NOT NULL,
    IBE$VH_MODIFY_DATE  TIMESTAMP NOT NULL,
    IBE$VH_USER_NAME    VARCHAR(67),
    IBE$VH_OBJECT_TYPE  SMALLINT NOT NULL,
    IBE$VH_OBJECT_NAME  VARCHAR(67) NOT NULL,
    IBE$VH_HEADER       VARCHAR(32000),
    IBE$VH_BODY         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    IBE$VH_DESCRIPTION  BLOB SUB_TYPE 1 SEGMENT SIZE 80
    );

    CREATE TABLE IBE$SCRIPTS (
    IBE$SCRIPT_NAME           VARCHAR(64) CHARACTER SET UNICODE_FSS NOT NULL,
    IBE$SCRIPT_TYPE           VARCHAR(15) CHARACTER SET UNICODE_FSS NOT NULL,
    IBE$SCRIPT_SOURCE         BLOB SUB_TYPE 1 SEGMENT SIZE 1024,
    IBE$SCRIPT_BLR            BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_DESCRIPTION    BLOB SUB_TYPE 1 SEGMENT SIZE 1024,
    IBE$SCRIPT_STATE          VARCHAR(15) CHARACTER SET UNICODE_FSS DEFAULT \'INVALID\' NOT NULL,
    IBE$SCRIPT_ACTION_ID      INTEGER,
    IBE$SCRIPT_FORM           BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_PARAM_HISTORY  BLOB SUB_TYPE 0 SEGMENT SIZE 1024,
    IBE$SCRIPT_ADD_DATA       BLOB SUB_TYPE 1 SEGMENT SIZE 1024
    );


    /******************************************************************************/
    /****                             Primary Keys                             ****/
    /******************************************************************************/

    ALTER TABLE IBE$VERSION_HISTORY ADD PRIMARY KEY (IBE$VH_ID);


    /******************************************************************************/
    /****                               Indices                                ****/
    /******************************************************************************/

    CREATE INDEX IBE$SCRIPTS_BY_ACTION_ID ON IBE$SCRIPTS (IBE$SCRIPT_ACTION_ID);
    CREATE UNIQUE INDEX IBE$SCRIPTS_BY_NAME ON IBE$SCRIPTS (IBE$SCRIPT_NAME);


    /******************************************************************************/
    /****                               Triggers                               ****/
    /******************************************************************************/


    SET TERM ^ ;



    /******************************************************************************/
    /****                         Triggers for tables                          ****/
    /******************************************************************************/



    /* Trigger: IBE$VERSION_HISTORY_BI */
    CREATE OR ALTER TRIGGER IBE$VERSION_HISTORY_BI FOR IBE$VERSION_HISTORY
    ACTIVE BEFORE INSERT POSITION 0
    AS
    BEGIN
    IF (NEW.IBE$VH_ID IS NULL) THEN
    NEW.IBE$VH_ID = GEN_ID(IBE$VERSION_HISTORY_ID_GEN,1);
    NEW.IBE$VH_USER_NAME = USER;
    NEW.IBE$VH_MODIFY_DATE = \'NOW\';
    END
    ^


    SET TERM ; ^



    /******************************************************************************/
    /****                              Privileges                              ****/
    /******************************************************************************/


    /* Privileges of users */
    GRANT ALL ON IBE$VERSION_HISTORY TO "ADMIN";
    GRANT ALL ON IBE$VERSION_HISTORY TO SENTE;
    GRANT ALL ON IBE$VERSION_HISTORY TO SENTELOGIN;
    GRANT ALL ON IBE$SCRIPTS TO "ADMIN";
    GRANT ALL ON IBE$SCRIPTS TO SENTE;
    GRANT ALL ON IBE$SCRIPTS TO SENTELOGIN;



    /******************************************************************************/
    /****                              Exceptions                              ****/
    /******************************************************************************/

    CREATE EXCEPTION UNIVERSAL 'Wyjątek uniwersalny o definiowanej treści.';
    '''
}

def addSysTables() {
    return '''/******************************************************************************/
    /****                                Tables                                ****/
    /******************************************************************************/


    CREATE GENERATOR GEN_SYS_HASHCODES;

    CREATE TABLE SYS_HASHCODES (
    ID        INTEGER NOT NULL,
    NAME      VARCHAR(64) NOT NULL,
    "TYPE"    SMALLINT NOT NULL,
    HASHCODE  INTEGER NOT NULL
    );


    /******************************************************************************/
    /****                             Primary Keys                             ****/
    /******************************************************************************/

    ALTER TABLE SYS_HASHCODES ADD CONSTRAINT PK_SYS_HASHCODES PRIMARY KEY (ID);


    /******************************************************************************/
    /****                               Indices                                ****/
    /******************************************************************************/

    CREATE INDEX SYS_HASHCODES_NAME_TYPE ON SYS_HASHCODES (NAME, "TYPE");


    /******************************************************************************/
    /****                               Triggers                               ****/
    /******************************************************************************/


    SET TERM ^ ;



    /******************************************************************************/
    /****                         Triggers for tables                          ****/
    /******************************************************************************/



    /* Trigger: SYS_HASHCODES_BI */
    CREATE OR ALTER TRIGGER SYS_HASHCODES_BI FOR SYS_HASHCODES
    ACTIVE BEFORE INSERT POSITION 0
    as
    begin
    if (new.id is null) then
    new.id = gen_id(gen_sys_hashcodes,1);
    end
    ^


    SET TERM ; ^



    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    COMMENT ON TABLE SYS_HASHCODES IS
    \'BS87678\';



    /******************************************************************************/
    /****                         Fields descriptions                          ****/
    /******************************************************************************/

    COMMENT ON COLUMN SYS_HASHCODES."TYPE" IS
    \'Zgodnie z enumem SHlib.DatabaseExtraction.DbType
    None = 0,
    Domain,
    Sequence,
    Exception,
    Table,
    Field,
    Procedure,
    View,
    Trigger,
    TransferProcedure,
    PrimaryKey,
    UniqueKey,
    ForeignKey,
    Index,
    AppSection,
    XkProcedure\';



    /******************************************************************************/
    /****                              Privileges                              ****/
    /******************************************************************************/


    /* Privileges of users */
    GRANT ALL ON SYS_HASHCODES TO SENTE;

    /* Privileges of triggers */
    GRANT UPDATE, REFERENCES ON SYS_HASHCODES TO TRIGGER SYS_HASHCODES_BI;

    /******************************************************************************/
    /****                               Domains                                ****/
    /******************************************************************************/

    CREATE DOMAIN SYS_TRHIST_ID AS
    INTEGER;

    CREATE DOMAIN SYS_TRHIST_NAZWA AS
    VARCHAR(32)
    COLLATE PXW_PLK;

    CREATE DOMAIN SYS_TRHIST_STAN AS
    SMALLINT
    DEFAULT 0
    NOT NULL;

    CREATE DOMAIN SYS_TRHIST_TEMAT AS
    VARCHAR(32)
    COLLATE PXW_PLK;

    CREATE DOMAIN SYS_TRHIST_TIMESTAMP AS
    TIMESTAMP;



    /******************************************************************************/
    /****                              Generators                              ****/
    /******************************************************************************/

    CREATE GENERATOR GEN_SYS_TRHIST;
    SET GENERATOR GEN_SYS_TRHIST TO 0;



    /******************************************************************************/
    /****                                Tables                                ****/
    /******************************************************************************/



    CREATE TABLE SYS_TRHIST (
    ID      SYS_TRHIST_ID NOT NULL,
    TEMAT   SYS_TRHIST_TEMAT,
    NAZWA   SYS_TRHIST_NAZWA,
    STAN    SYS_TRHIST_STAN,
    TIMEST  SYS_TRHIST_TIMESTAMP
    );




    /******************************************************************************/
    /****                          Unique Constraints                          ****/
    /******************************************************************************/

    ALTER TABLE SYS_TRHIST ADD CONSTRAINT UNQ_SYS_TRHIST_NAZWA UNIQUE (NAZWA);


    /******************************************************************************/
    /****                             Primary Keys                             ****/
    /******************************************************************************/

    ALTER TABLE SYS_TRHIST ADD CONSTRAINT PK_SYS_TRHIST PRIMARY KEY (ID);


    /******************************************************************************/
    /****                               Indices                                ****/
    /******************************************************************************/

    CREATE INDEX SYS_TRHIST_STAN ON SYS_TRHIST (STAN);
    CREATE INDEX SYS_TRHIST_TEMAT ON SYS_TRHIST (TEMAT);
    CREATE INDEX SYS_TRHIST_TIMEST ON SYS_TRHIST (TIMEST);


    /******************************************************************************/
    /****                               Triggers                               ****/
    /******************************************************************************/


    SET TERM ^ ;



    /******************************************************************************/
    /****                         Triggers for tables                          ****/
    /******************************************************************************/



    /* Trigger: SYS_TRHIST_BI */
    CREATE OR ALTER TRIGGER SYS_TRHIST_BI FOR SYS_TRHIST
    ACTIVE BEFORE INSERT POSITION 0
    as
    begin
    if (new.id is null) then
    new.id = gen_id(gen_sys_trhist,1);
    end
    ^


    SET TERM ; ^



    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    COMMENT ON DOMAIN SYS_TRHIST_ID IS
    \'BS89147\';

    COMMENT ON DOMAIN SYS_TRHIST_NAZWA IS
    \'BS89147\';

    COMMENT ON DOMAIN SYS_TRHIST_STAN IS
    \'BS89147
    0 - założona
    1 - wgrana\';

    COMMENT ON DOMAIN SYS_TRHIST_TEMAT IS
    \'BS89147\';

    COMMENT ON DOMAIN SYS_TRHIST_TIMESTAMP IS
    \'BS89147\';



    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    COMMENT ON TABLE SYS_TRHIST IS
    \'BS89147\';



    /******************************************************************************/
    /****                         Fields descriptions                          ****/
    /******************************************************************************/

    COMMENT ON COLUMN SYS_TRHIST.ID IS
    \'BS89147\';

    COMMENT ON COLUMN SYS_TRHIST.TEMAT IS
    \'BS89147\';

    COMMENT ON COLUMN SYS_TRHIST.NAZWA IS
    \'BS89147\';

    COMMENT ON COLUMN SYS_TRHIST.STAN IS
    \'BS89147
    0 - założona
    1 - wgrana\';

    COMMENT ON COLUMN SYS_TRHIST.TIMEST IS
    \'BS89147\';



    /******************************************************************************/
    /****                              Privileges                              ****/
    /******************************************************************************/


    /* Privileges of triggers */
    GRANT UPDATE, REFERENCES ON SYS_TRHIST TO TRIGGER SYS_TRHIST_BI;

    /******************************************************************************/
    /****                                Tables                                ****/
    /******************************************************************************/


    CREATE GENERATOR GEN_SYS_TR_TEMP;

    CREATE TABLE SYS_TR_TEMP (
    REF          BIGINT NOT NULL,
    TRANSFER_ID  VARCHAR(32),
    IDX_INT      BIGINT,
    IDX_STRING   VARCHAR(255),
    DATA         VARCHAR(4000)
    );




    /******************************************************************************/
    /****                             Primary Keys                             ****/
    /******************************************************************************/

    ALTER TABLE SYS_TR_TEMP ADD CONSTRAINT PK_SYS_TR_TEMP PRIMARY KEY (REF);


    /******************************************************************************/
    /****                               Indices                                ****/
    /******************************************************************************/

    CREATE INDEX SYS_TR_TEMP_IDX_ID ON SYS_TR_TEMP (TRANSFER_ID);
    CREATE INDEX SYS_TR_TEMP_IDX_INT ON SYS_TR_TEMP (IDX_INT);
    CREATE INDEX SYS_TR_TEMP_IDX_STRING ON SYS_TR_TEMP (IDX_STRING);


    /******************************************************************************/
    /****                               Triggers                               ****/
    /******************************************************************************/


    SET TERM ^ ;



    /******************************************************************************/
    /****                         Triggers for tables                          ****/
    /******************************************************************************/



    /* Trigger: SYS_TR_TEMP_BI */
    CREATE OR ALTER TRIGGER SYS_TR_TEMP_BI FOR SYS_TR_TEMP
    ACTIVE BEFORE INSERT POSITION 0
    as
    begin
    if (new.ref is null) then
    new.ref = gen_id(gen_sys_tr_temp,1);
    end
    ^


    SET TERM ; ^



    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    COMMENT ON TABLE SYS_TR_TEMP IS
    \'PR88509\';



    /******************************************************************************/
    /****                             Descriptions                             ****/
    /******************************************************************************/

    COMMENT ON TRIGGER SYS_TR_TEMP_BI IS
    \'PR88509\';'''
}

def delAttachments() {
    return '''DELETE FROM MON$ATTACHMENTS
      WHERE MON$ATTACHMENT_ID <> CURRENT_CONNECTION;'''
}

def systerminateSql() {
  return 'execute procedure sys_terminate;'
}