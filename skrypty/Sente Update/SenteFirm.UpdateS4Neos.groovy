def relFolder

switch (neos_rel) {
    case "rc":
        relFolder = "release.candidate"
        break
    case "rtm":
        relFolder = "release.to.manufacture"
        break
    default:
        relFolder = ""
        break
}

node('WINSLAVE') {
    if(fileExists("s:\\")){
        bat 'net use s: /delete /Y'
    }
    if(fileExists("t:\\")){
        bat 'net use t: /delete /Y'
    }

    withCredentials([usernamePassword(credentialsId: 'bb65b8f1-0a98-4fb5-8d8d-d00feafb76cd', passwordVariable: 'jenkinspass', usernameVariable: 'jenkinsuser')]) {
        bat 'net use s: \\\\sente.local\\firma\\dbr\\release '+jenkinspass+' /USER:'+jenkinsuser+' /persistent:yes'
        bat 'net use t: \\\\sente.local\\firma\\jenkinsbuild '+jenkinspass+' /USER:'+jenkinsuser+' /persistent:yes'
    }

    def neosPath
    node('master') {
        neosPath = createPath('neos', neos_ver, relFolder)
    }

    parallel (
        s4: {
			stage('Przygotowanie aktualnej wersji S4') {
				bat 'if exist t:\\s4 (rmdir /s /q t:\\s4)'
				copyArtifacts projectName: 'SenteFirm.S4Firm.Build', selector: lastSuccessful(), target: '\\\\sente.local\\firma\\jenkinsbuild\\s4\\'
			}
        },
        neos: {
            stage('Przygotowanie aktualnej wersji Neosa') {
                bat 'if exist t:\\neos (rmdir /s /q t:\\neos)'
				bat '''rmdir /s /q t:\\neos
                    robocopy s:\\'''+neosPath+''' t:\\neos /e /xf *.smd
                    if %ERRORLEVEL% lss 8 exit /b 0'''
            }
        }
    )

	bat 'net use s: /delete /Y'
    bat 'net use t: /delete /Y'
	
    stage('Aktualizacja Neosa (sente)') {
        executeService("http://10.168.1.248:31234","UpdateNeos")
    }
	stage('Aktualizacja Neosa (prodsched)') {
        executeService("http://10.168.1.248:31235","UpdateNeos")
    }
	stage('Aktualizacja Neosa (prodworkflow)') {
        executeService("http://10.168.1.248:31236","UpdateNeos")
    }
	stage('Aktualizacja Neosa (prodklient)') {
        executeService("http://10.168.1.248:31237","UpdateNeos")
    }
    stage('Aktualizacja S4') {
        executeService("http://10.168.1.248:31234","UpdateS4")
    }
}

def executeService(svc,name){
    withCredentials([string(credentialsId: 'TokenKey', variable: 'secret')]) {
        def token = getToken(svc,secret)
        return new URL("${svc}/execute/${name}/${token}").getText()
    }
}

def getToken(svc,secret){
    def plainToken = new URL("${svc}/token").getText()
    def toHash = plainToken+secret
  	def hashedToken = java.security.MessageDigest.getInstance("SHA-256").digest(toHash.bytes)
    def result = ""
  	for(def i = 0; i < hashedToken.length; i++){
  		result += String.format("%02x", hashedToken[i])
    }
  	echo "getToken result: "+result
  	return result
}

def createPath(app, ver, rel) {
    def path = '/mnt/release/'+app+'/'+rel+'/'+ver
   	for (i = 0; i <2; i++) {	
  		new File(path).eachDir() { dir ->
       		path = dir.getPath()
       		println 'DEBUG [path]: '+path
    	}
	}
	return path.substring(13).replaceAll('/','\\\\')
}