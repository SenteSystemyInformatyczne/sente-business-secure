node ("master") {
    parallel (
        dbscripts: {
            stage ("Aktualizacja skryptów bazy") {
                checkout([$class: 'GitSCM', branches: [[name: dbscripts_branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'dbscripts'], [$class: 'CleanBeforeCheckout'], [$class: 'CleanCheckout']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:sentefirm/s4.git']]])
            }
        },
        sh: {
            stage('Aktualizacja SignatureHelpera') {
                checkout([$class: 'GitSCM', branches: [[name: 'rc']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'sh']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:dro/signaturehelper.git']]])
                dir('sh/shtool'){
                    sh 'xbuild shtool.csproj'
                }
            }
        }
    )
    stage ("Eksport metadanych bazy") {
        withCredentials([usernamePassword(credentialsId: 'a78709c6-5d60-474f-a7d7-01fe10b6c892', passwordVariable: 'dbpass', usernameVariable: 'dbuser')]) {
            sh "mono sh/Out/Net35/shtool.exe -d dbscripts/dbscripts --dbcp=20 --dbchar=WIN1250 --dbhost=$db_host -dbpath=$db_path --dblogin=$dbuser --dbpass=$dbpass --expall --au"
        }
    }
    stage ("Porównanie zmian") {
        try {
            dir('dbscripts'){
                sh 'git status -s | grep -i -v \"\\(sys.*ddl\\|sys.*parameters\\|sys_getcomment\\|sys_quote_reserved\\|database.uuid\\)\" || exit 0'
                sh '''git status -s | grep -i -v \"\\(sys.*ddl\\|sys.*parameters\\|sys_getcomment\\|sys_quote_reserved\\|database.uuid\\)\" | while read -r line
                      do
                        echo \"Wykryto zmiany!\" | exit 1 
                      done'''
            }
        } catch(err) {
            echo 'Po wgraniu skryptu różnicowego wciąż występują rozbieżności.'
            throw(err)
        }
    }
}