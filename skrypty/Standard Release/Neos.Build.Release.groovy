def relType
def relFolder
def deployPath = '\\\\10.168.0.168\\public\\release'

switch (release) {
	case "rc":
        relType = "release candidate"
        relFolder = "release.candidate"
        break
    case "rtm":
        relType = "release to manufacture"
        relFolder = "release.to.manufacture"
        break
    default:
        relType = "work"
        relFolder = ""
        break
}

node('WINSLAVE') {
    stage('Inicjalizacja repozytorium') {
        checkout([$class: 'GitSCM', branches: [[name: srcBranch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true], [$class: 'LocalBranch', localBranch: srcBranch], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'dbr-Jenkins-kompilator', url: 'git@git.office.sente.pl:dro/neos.git']]])
    }
    
	parallel (
		nuget: {
			stage('Przygotowanie paczek NuGet') {
				bat ".nuget\\nuget restore Neos.sln"
			}
		},
		relVersion: {
			stage('Przygotowanie numeru wersji') {
				bat "addings\\autoscripts\\neos\\prebuild.bat ${relVersion} \""+relType+"\""
			}
		}
	)
    
    stage('Kompilacja') {
        def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
        bat "\"${msb}\" Neos.sln /m /nr:false"
    }
    
	stage('Testowanie') {
		gitlabCommitStatus('test') {
			def testDir = 'Out\\Net35'
			if(fileExists('Out\\Tests')) {
				testDir = 'Out\\Tests'
			} else if(fileExists(testDir + '\\Tests')) {
				testDir += '\\Tests'
			}
			
			try {
				bat "C:\\OpenCover\\OpenCover.Console.exe -register -target:\"packages\\xunit.runner.console.2.3.1\\tools\\net452\\xunit.console.exe\" -targetargs:\""+testDir+"\\Neos.Tests.dll "+testDir+"\\Neos.Tests.Integrations.dll -xml Neos.Tests.Results.xml -noshadow\" -filter:\"+[Neos*]* -[Neos.Tests*]*\" -output:xunit_opencovertests.xml"
				step([$class: 'XUnitPublisher', testTimeMargin: '3000', thresholdMode: 1, thresholds: [[$class: 'FailedThreshold', failureNewThreshold: '0', failureThreshold: '0', unstableNewThreshold: '0', unstableThreshold: '0'], [$class: 'SkippedThreshold', failureNewThreshold: '50', failureThreshold: '50', unstableNewThreshold: '100', unstableThreshold: '25']], tools: [[$class: 'XUnitDotNetTestType', deleteOutputFiles: true, failIfNotNew: true, pattern: 'Neos.Tests.*.xml', skipNoTestFiles: false, stopProcessingIfError: true]]])
			} catch(e) {
				echo e
				currentBuild.result = 'FAILURE'
			}
		}
	}

    stage('Kopiowanie binarek') {
        parallel(
            emisja: {
                bat 'xcopy ' + deployPath + '\\baretail\\baretail.exe Out\\Net35 /q /y'
                bat "addings\\autoscripts\\neos\\deploy.bat " + deployPath + "\\neos\\" + relFolder + " ${relVersion} ${srcBranch} --auto-location"
            },
            dokumentacja: {
                bat "addings\\autoscripts\\neos\\gendoc.bat NEOS"
                bat "mkdir " + deployPath + "\\neos\\dokumentacja\\${relVersion}"
                bat "xcopy gendoc\\serwer\\* " + deployPath + "\\neos\\dokumentacja\\${relVersion} /s /i"
            }
        )
    }
	
    stage('Kompresja binarek') {
	    if (archive == 'true') {
			bat deployPath + '\\7z\\7za.exe a -m0=lzma -mx=9 ' + deployPath + '\\neos\\'+relFolder+'\\NEOS_'+relVersion.replaceAll(/\./, '_')+'_'+release.toUpperCase()+'.zip ' + deployPath + '\\neos\\'+relFolder+'\\'+relVersion
		}
	}
	
	stage('Tag wersji') {
		bat 'git tag '+relVersion
		bat 'git push --tags origin '+srcBranch
	}
	
    stage('Nowa numeracja work') {
 		bat 'git reset --hard'
        bat 'git clean -fd'
		bat 'addings\\autoscripts\\neos\\prebuild.bat ' + nextVersion + ' \"work\"'
        bat 'git add -u'
        bat 'git commit -m \"Nowa numeracja work\"'
        bat 'git push origin ' + srcBranch
    }
	
    stage('Sprzątanie') {
        cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
        dir(env.WORKSPACE + '@tmp') {
            deleteDir()
        }
    }
	
    build job: 'Neos.NuGet.Build', parameters: [string(name: 'branch', value: srcBranch), string(name: 'version', value: relVersion), string(name: 'release', value: release)], wait: false
}