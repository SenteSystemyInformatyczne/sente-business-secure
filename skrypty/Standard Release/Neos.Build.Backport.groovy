def relType
def relFolder

switch (release) {
    case "rc":
        relType = "release candidate"
        relFolder = "release.candidate"
        break
    case "rtm":
        relType = "release to manufacture"
        relFolder = "release.to.manufacture"
        break
    default:
        relType = "work"
        relFolder = ""
        break
}

node('WINSLAVE') {
    try {
        stage('Przygotowanie źródeł') {
			checkout([$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false], [$class: 'LocalBranch', localBranch: branch]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'dbr-Jenkins-kompilator', url: 'git@git.office.sente.pl:dro/neos.git']]])
        }
        stage('Kompilacja') {
			def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
            parallel(
                nuget: {
                    bat '.nuget\\nuget restore Neos.sln'
                },
                version: {
                    bat 'addings\\autoscripts\\neos\\prebuild.bat '+version+' \"'+relType+'\"'
                }
            )
			bat "\"${msb}\" Neos.sln /m /nr:false"
        }
        stage('Testowanie') {
			gitlabCommitStatus('test') {
				def testDir = 'Out\\Net35'
				if(fileExists('Out\\Tests')) {
					testDir = 'Out\\Tests'
				} else if(fileExists(testDir + '\\Tests')) {
					testDir += '\\Tests'
				}
				try {
					bat "C:\\OpenCover\\OpenCover.Console.exe -register -target:\"packages\\xunit.runner.console.2.3.1\\tools\\net452\\xunit.console.exe\" -targetargs:\""+testDir+"\\Neos.Tests.dll "+testDir+"\\Neos.Tests.Integrations.dll -xml Neos.Tests.Results.xml -noshadow\" -filter:\"+[Neos*]* -[Neos.Tests*]*\" -output:xunit_opencovertests.xml"
					step([$class: 'XUnitPublisher', testTimeMargin: '3000', thresholdMode: 1, thresholds: [[$class: 'FailedThreshold', failureNewThreshold: '0', failureThreshold: '0', unstableNewThreshold: '0', unstableThreshold: '0'], [$class: 'SkippedThreshold', failureNewThreshold: '50', failureThreshold: '50', unstableNewThreshold: '100', unstableThreshold: '25']], tools: [[$class: 'XUnitDotNetTestType', deleteOutputFiles: true, failIfNotNew: true, pattern: 'Neos.Tests.*.xml', skipNoTestFiles: false, stopProcessingIfError: true]]])
				} catch(e) {
					echo e
					currentBuild.result = 'FAILURE'
				}
			}
		}
        stage('Kopiowanie binarek') {
            if(fileExists("s:\\")){
                bat 'net use s: /delete /Y'
            }
            withCredentials([usernamePassword(credentialsId: 'bb65b8f1-0a98-4fb5-8d8d-d00feafb76cd', passwordVariable: 'jdpass', usernameVariable: 'jduser')]) {                
				bat 'net use s: \\\\sente.local\\firma\\dbr\\release '+jdpass+' /user:'+jduser+' /persistent:yes'
            }				
            bat 'xcopy s:\\baretail\\baretail.exe Out\\Net35 /q /y'
            bat 'addings\\autoscripts\\neos\\deploy.bat s:\\neos\\'+relFolder+' '+version+' '+branch+' --auto-location'
        }
		stage('Kompresja binarek') {
		    if (archive == 'true') {
				bat 's:\\7z\\7za.exe a -m0=lzma -mx=9 s:\\neos\\'+relFolder+'\\NEOS_'+version.replaceAll(/\./, '_')+'_'+release.toUpperCase()+'.zip s:\\neos\\'+relFolder+'\\'+version+''
			}
		}
        bat 'net use s: /delete /y'
        stage('Tag wersji') {
		    bat 'git tag '+version
		    bat 'git push --tags origin '+branch
	    }
        stage('Sprzątanie') {
            cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
            dir(env.WORKSPACE + '@tmp') {
                deleteDir()
            }
        }
    } catch(e) {
        if(fileExists('s:\\')) {
            bat 'net use s: /delete /y'
        }
    }
}