def relFolder
def deployPath = '\\\\10.168.0.168\\public\\release'

switch (release) {
    case "rc":
         relFolder = "release.candidate"
        break
    case "rtm":
         relFolder = "release.to.manufacture"
        break
    default:
         relFolder = ""
        break
}

node('WINSLAVE') {
	stage('Przygotowanie źródeł') {
		checkout([$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true], [$class: 'RelativeTargetDirectory', relativeTargetDir: 's4'], [$class: 'LocalBranch', localBranch: branch]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'dbr-Jenkins-kompilator', url: 'git@git.office.sente.pl:dro/s4.git']]])
	}
	dir('s4') {
		stage('Aktualizacja wersji aplikacji') {
			bat 'addings\\autoscripts\\s4\\prebuild.bat '+version
		}
		stage('Konfiguracja JCL') {
			bat 'addings\\autoscripts\\s4\\disablejcl.bat '+IncludeDbgLib
		}
		stage('Kompilacja S4') {
			def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
			withEnv(['BDS=C:\\Program Files (x86)\\CodeGear\\RAD Studio\\6.0']) {
				bat "\"${msb}\" /v:q eSystem.groupproj"
			}
		}
		stage('Kopiowanie binarek') {
			bat 'addings\\autoscripts\\s4\\deploy.bat '+deployPath+'\\s4\\'+relFolder+' '+version+' '+branch+' --auto-location'
		}
		stage('Kompresja binarek') {
			if (archive == 'true') {
				bat deployPath+'\\7z\\7za.exe a -m0=lzma -mx=9 '+deployPath+'\\s4\\'+relFolder+'\\S4_'+version.replaceAll(/\./, '_')+'_'+release.toUpperCase()+'.zip '+deployPath+'\\s4\\'+relFolder+'\\'+version
			}
		}
		stage('Tag wersji') {
			bat 'git tag ' + version
			bat 'git push --tags origin ' + branch
		}
		stage('Sprzątanie') {
			cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
			dir(env.WORKSPACE + '@tmp') {
				deleteDir()
			}
		}
	}
}