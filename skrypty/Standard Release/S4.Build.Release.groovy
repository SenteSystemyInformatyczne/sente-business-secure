def relFolder
def deployPath = '\\\\10.168.0.168\\public\\release'

switch (release) {
    case "rc":
        relFolder = "release.candidate"
        break
    case "rtm":
        relFolder = "release.to.manufacture"
        break
    default:
        relFolder = ""
        break
}

node('WINSLAVE') {
    stage('Przygotowanie źródeł') {
		checkout([$class: 'GitSCM', branches: [[name: parent_branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true], [$class: 'RelativeTargetDirectory', relativeTargetDir: 's4'], [$class: 'LocalBranch', localBranch: parent_branch]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'dbr-Jenkins-kompilator', url: 'git@git.office.sente.pl:dro/s4.git']]])
    }
    dir('s4') {
        stage('Merge zmian modułów') {
            mergeBranch('FK')
            mergeBranch('PERSONEL')
			mergeBranch('SID')
        }
        stage('Konfiguracja JCL') {
            bat 'addings\\autoscripts\\s4\\disablejcl.bat ' + IncludeDbgLib
        }
        stage('Kompilacja S4') {
            def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
            withEnv(['BDS=C:\\Program Files (x86)\\CodeGear\\RAD Studio\\6.0']) {
                bat "\"${msb}\" /v:q eSystem.groupproj"
            }
        }
        stage('Kopiowanie binarek') {
            bat 'addings\\autoscripts\\s4\\deploy.bat ' + deployPath + '\\s4\\' + relFolder + ' ' + version + ' ' + parent_branch + ' --auto-location'
        }
		stage('Kompresja binarek') {
		    if (archive == 'true') {
				bat deployPath + '\\7z\\7za.exe a -m0=lzma -mx=9 ' + deployPath + '\\s4\\'+relFolder+'\\S4_'+version.replaceAll(/\./, '_')+'_'+release.toUpperCase()+'.zip ' + deployPath + '\\s4\\'+relFolder+'\\'+version+''
			}
		}
        stage('Push zmian do repozytorium zdalnego') {
            bat 'git push origin ' + parent_branch
        }
		stage('Tag wersji') {
			bat 'git tag ' + version
			bat 'git push --tags origin ' + parent_branch
		}
        stage('Aktualizacja wersji aplikacji') {
            bat 'addings\\autoscripts\\s4\\prebuild.bat ' + new_version
            bat 'git add -u'
            bat 'git commit -m "Aktualizacja wersji aplikacji"'
            bat 'git push origin ' + parent_branch
        }
        stage('Izolowanie zmian modułów') {
            isolateModuleChanges('FK')
            isolateModuleChanges('PERSONEL')
			isolateModuleChanges('SID')
        }
        stage('Tworzenie branchy modułów') {
            createBranch('FK')
            createBranch('PERSONEL')
			createBranch('SID')
        }
        stage('Sprzątanie') {
            cleanWs cleanWhenAborted: false, cleanWhenFailure: false, cleanWhenNotBuilt: false, notFailBuild: true
            dir(env.WORKSPACE + '@tmp') {
                deleteDir()
            }
        }
	}
}

def mergeBranch(name) {
    bat 'git merge origin/' + name + '_' + version
}

def createBranch(name) {
	bat 'git checkout ' + version
    bat 'git checkout -b ' + name + '_' + new_version
    bat 'git push origin ' + name + '_' + new_version
}

def isolateModuleChanges(name) {
	bat 'git checkout ' + parent_branch
	bat 'git checkout -b ' + name + '_' + version + '-ZMIANY.IZOLOWANE'
	bat 'git merge --squash origin/' + name + '_' + version
	bat 'git commit --allow-empty -m "Izolowane zmiany z brancha ' + name + '_' + version + '"'
	bat 'git push origin ' + name + '_' + version + '-ZMIANY.IZOLOWANE'
}