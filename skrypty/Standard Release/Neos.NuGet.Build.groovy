def relType
    
switch (release) {
    case "rc":
        relType = "release candidate"
        break
    case "rtm":
        relType = "release to manufacture"
        break
    default:
        relType = "work"
        break
}   

node ("WINSLAVE") {
    stage("Przygotowanie źródeł"){
        checkout([$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'neos'], [$class: 'CleanBeforeCheckout']], gitTool: 'Default', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '424eb11b-9eb8-4f86-a2a2-e62d0a9e03a5', url: 'git@git.office.sente.pl:dro/neos.git']]])
    }

    dir('neos'){
        stage("Inicjalizacja AutoScripts"){
            bat 'git submodule update --init --remote'
        }
        
		stage("Przygotowanie kompilacji"){
            parallel(
                nuget: {
                    bat '.nuget\\nuget restore Neos.sln'
                },
                version: {
                    bat 'addings\\autoscripts\\neos\\prebuild.bat '+version+' \"'+relType+'\"'
                }
            )
        }
        
		stage("Kompilacja"){
			def msb = tool name: 'MSBuild 4.0', type: 'hudson.plugins.msbuild.MsBuildInstallation'
			bat "\"${msb}\" Neos.sln /m /nr:false"
        }
        
		stage("Testowanie"){
			gitlabCommitStatus("test") {
				try{
					def code = bat returnStatus: true, script: 'packages\\xunit.runner.console.2.3.1\\tools\\net452\\xunit.console.exe Out\\Tests\\Neos.Tests.dll -xml Neos.Tests.Results.xml'
					def codei = bat returnStatus: true, script: 'packages\\xunit.runner.console.2.3.1\\tools\\net452\\xunit.console.exe Out\\Tests\\Neos.Tests.Integrations.dll -xml Neos.Tests.Integrations.Results.xml'
					step([$class: 'XUnitPublisher', testTimeMargin: '3000', thresholdMode: 1, thresholds: [[$class: 'FailedThreshold', failureNewThreshold: '0', failureThreshold: '0', unstableNewThreshold: '0', unstableThreshold: '0'], [$class: 'SkippedThreshold', failureNewThreshold: '50', failureThreshold: '50', unstableNewThreshold: '100', unstableThreshold: '25']], tools: [[$class: 'XUnitDotNetTestType', deleteOutputFiles: true, failIfNotNew: true, pattern: 'Neos.Tests.*.xml', skipNoTestFiles: false, stopProcessingIfError: true]]])
				} catch(e) {
					echo "ERROR"
					echo e
				}
			}
		}
		
		stage("Tworzenie paczek NuGet"){
		    createNupkg("Neos.Core")
		    createNupkg("Neos.Common")
		    createNupkg("Neos.Telemetry")
		    if(fileExists("neos\\Neos.Core.Interop")){
		        createNupkg("Neos.Core.Interop")
		    }
		}
    }
}

def createNupkg(project){
    if(!fileExists("Neos.Core\\${project}.nuspec")){
	    bat "copy addings\\autoscripts\\neos\\${project}.nuspec ${project}"
	}
	dir(project){
	    withCredentials([string(credentialsId: 'NuGet-API-Key', variable: 'apikey')]) {
	        bat "..\\.nuget\\nuget.exe pack ${project}.csproj -properties ver=${version}"
	        bat "..\\.nuget\\nuget.exe push -src http://10.168.0.168:5000 -ApiKey ${apikey} ${project}.${version}.nupkg"
	    }
	}    
}